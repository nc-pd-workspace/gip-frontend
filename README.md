# GS INSIGHT PLATFORM
// todo

프로젝트 목표 description 작성


## 라이브러리 스펙
[create-react-app](https://create-react-app.dev)

[antd](https://ant.design/components/overview/)

[redux-toolkit](https://redux-toolkit.js.org/)

[axios](https://github.com/axios/axios)

[react-query](https://react-query.tanstack.com/)

[date-fns](https://date-fns.org/)

[apexcharts](https://apexcharts.com/)


## 주요 폴더 구조
```

├── public
├── src
│   ├── pages
│   ├── components // 공통 presentation component
│   ├── hooks // 공통 hooks
│   ├── redux // 각각의 slice를 취합하여 store 업데이트
│   │   └── store.js
│   ├── services // 서비스 로직
│   ├── styles // GlobalStyle, util 등 공통 스타일 
│   ├── utils // util 함수 
│   └── views
│       ├── somePage  // page 별로 관리
│       │   ├── components
│       │   ├── containers
│       │   ├── hooks
│       │   ├── services
│       │   └── redux
│       │       └── slice.js
│       ├── shared // header, sidebar, footer 등 state 에 영향을 공통 요소 
│       │   └── someSharedModule
│       │       ├── components
│       │       ├── containers
│       │       ├── hooks
│       │       └── redux
│       │           └── slice.js
│       └── viewFrame // 탭구조를 관리 하기 위한 뷰 컨테이너

```

## 실행

```bash
yarn run start
```
