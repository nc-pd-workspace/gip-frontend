import values from 'lodash-es/values';

export const PageTypes = {
  MAIN: 'main',
  BRAND_BRAND_LANDSCAPE: 'brandLandscape',
  BRAND_TOP_BRAND: 'topBrand',
  CUSTOMER_ALL_ACTIVITY_CUSTOMER: 'allActivityCustomer',
  CUSTOMER_NEW_CUSTOMER: 'newCustomer',
  CUSTOMER_STEP_CUSTOMER: 'stepCustomer',
  CUSTOMER_TOP5_BRAND_CUSTOMER: 'top5BrandCustomer',
  PERIOD_CUSTOMER_PERFORMANCE: 'brandPerformance',
  PERIOD_STEP_CONVERSION: 'stepConversion',
  PERIOD_PRODUCT_CUSTOMER: 'productCustomer',
  PERIOD_INFLOW_KEYWORD: 'inflowKeyword',
  ACCOUNT_MY_PAGE: 'myPage',
  ACCOUNT_MY_PARTNER_PAGE: 'myPartnerPage',
  SETTINGS_PARTNERS: 'partners',
  SETTINGS_USERS: 'users',
  SETTINGS_USER_DETAIL: 'userDetail',
  DEV_COMPONENT: 'component',
};

export const pageTitle = {
  [PageTypes.MAIN]: '대시보드 메인',
  [PageTypes.BRAND_BRAND_LANDSCAPE]: '브랜드 현황',
  [PageTypes.BRAND_TOP_BRAND]: 'TOP10 브랜드 현황',
  [PageTypes.CUSTOMER_ALL_ACTIVITY_CUSTOMER]: '전체 활동고객 분석',
  [PageTypes.CUSTOMER_NEW_CUSTOMER]: '신규 활동고객 분석',
  [PageTypes.CUSTOMER_STEP_CUSTOMER]: '단계별 활동고객 분석',
  [PageTypes.CUSTOMER_TOP5_BRAND_CUSTOMER]: 'TOP5 브랜드 활동고객 분석',
  [PageTypes.PERIOD_CUSTOMER_PERFORMANCE]: '브랜드 성과 분석',
  [PageTypes.PERIOD_STEP_CONVERSION]: '단계별 고객 전환 분석',
  [PageTypes.PERIOD_PRODUCT_CUSTOMER]: '상품별 고객 분석',
  [PageTypes.PERIOD_INFLOW_KEYWORD]: '유입 검색어 분석',
  [PageTypes.ACCOUNT_MY_PAGE]: '내 정보',
  [PageTypes.ACCOUNT_MY_PARTNER_PAGE]: '파트너 정보',
  [PageTypes.SETTINGS_PARTNERS]: '파트너 관리',
  [PageTypes.SETTINGS_USERS]: '사용자 관리',
  [PageTypes.DEV_COMPONENT]: '개발용 컴포넌트',
  [PageTypes.SETTINGS_USER_DETAIL]: '사용자 상세',
};

export const isPageId = (id) => values(PageTypes).includes(id);
