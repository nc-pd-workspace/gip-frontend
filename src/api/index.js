import axios from 'axios';

import Common from './common';
import Main from './main';
import Users from './users';
import AllActivityCustomer from './allActivityCustomer';
import ProductCustomer from './productCustomer';
import Partners from './partners';
import NewCustomer from './newCustomer';
import CustomerPerformance from './customerPerformance';
import StepConversion from './stepConversion';
import StepCustomer from './stepCustomer';
import MyPartners from './myPartners';
import BrandLandscape from './brandLandscape';
import TopBrand from './topBrand';
import Top5BrandCustomer from './top5BrandCustomer';
import Login from './login';
import { apiLogout } from '../redux/lib';
import { store } from '../redux/store';

/* http 객체 생성 :: axios 모듈 사용 */
export const http = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

let isTokenRefreshing = false;
const refreshSubscribers = [];

const onTokenRefreshed = (accessToken) => {
  refreshSubscribers.map((callback) => callback(accessToken));
};

const addRefreshSubscriber = (callback) => {
  refreshSubscribers.push(callback);
};

// 토큰 갱신 서버통신
export const onReissue = async () => {
  const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
  const token = JSON.parse(sessionUserInfo).refreshToken;
  const instance = axios.create({
    headers: {
      'Authorization-gip-refresh-token': token,
    },
  });
  await instance.get(`${process.env.REACT_APP_SERVER_URL}/api/auth/token/reissue`, {})
    .then((response) => {
      const reIssueData = response.data;
      if (response.status === 200 && reIssueData.error.errorCode === '0000') {
        const userInfo = window.sessionStorage.getItem('GIPADMIN_USER');
        const userInfoObject = JSON.parse(userInfo);
        userInfoObject.accessToken = reIssueData.data.accessToken;
        userInfoObject.refreshToken = reIssueData.data.refreshToken;
        userInfoObject.expiredDate = reIssueData.data.expiredDate;
        userInfoObject.refreshTokenExpiredDate = reIssueData.data.refreshTokenExpiredDate;
        store.dispatch({ type: 'common/updateToken', payload: reIssueData.data });
        window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(userInfoObject));
      }
    }).catch((error) => {
      refreshErrorHandle(error);
    });
};

export const refreshErrorHandle = async (err) => {
  const {
    config,
    response: { status, data },
  } = err;
  const originalRequest = config;
  if (status === 500 && data.error && ['0004'].indexOf(data.error.errorCode) > -1) {
    apiLogout('동일한 아이디의 사용자가 접속하여 자동으로\n로그아웃 되었습니다.');
  }
  if (status === 500 && data.error && ['0205'].indexOf(data.error.errorCode) > -1) {
    apiLogout('사용이 중지된 사용자 아이디입니다.');
  }
  if (status === 500 && data.error && ['0002'].indexOf(data.error.errorCode) > -1) {
    let userInfo = null;

    const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
    if (sessionUserInfo) {
      userInfo = JSON.parse(sessionUserInfo);
    } else {
      apiLogout('로그인 시간이 만료되어 자동으로 로그아웃 되었습니다.\n로그인 후 사용해 주세요.');
    }

    if (!isTokenRefreshing) {
      // isTokenRefreshing이 false인 경우에만 token refresh 요청
      isTokenRefreshing = true;

      try {
        onReissue();
      } catch (e) {
        // 토큰 발급에 에러가 난 경우 로그아웃 처리.
        apiLogout('로그인 시간이 만료되어 자동으로 로그아웃 되었습니다.\n로그인 후 사용해 주세요.');
      }

      isTokenRefreshing = false;
      // 토큰 발급받기 진행중인 api도 request promise que에 add
      const retryOriginalRequest = new Promise((resolve) => {
        addRefreshSubscriber((accessToken) => {
          originalRequest.headers.Authorization = accessToken;
          resolve(http(originalRequest));
        });
      });

      // token이 refreshed 된 상태 일 때 promise que에 쌓은 요청 모두 처리
      onTokenRefreshed(userInfo.refreshToken);
      return retryOriginalRequest;
    }
    // token이 재발급 되는 동안의 요청은 promise que에 저장
    const retryOriginalRequest = new Promise((resolve) => {
      addRefreshSubscriber((accessToken) => {
        originalRequest.headers.Authorization = accessToken;
        resolve(http(originalRequest));
      });
    });
    return retryOriginalRequest;
  }
  return Promise.reject(err);
};

const interceptRequest = async (config) => {
  let userInfo = null;
  const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
  if (sessionUserInfo) {
    userInfo = JSON.parse(sessionUserInfo);
  }
  if (userInfo) {
    config.headers['Authorization-gip-access-token'] = userInfo.accessToken;
  }
  return config;
};

http.interceptors.request.use(interceptRequest);
http.interceptors.response.use((response) => response, refreshErrorHandle);

export default {
  Common: Common({ http }),
  Main: Main({ http }),
  AllActivityCustomer: AllActivityCustomer({ http }),
  ProductCustomer: ProductCustomer({ http }),
  StepCustomer: StepCustomer({ http }),
  Top5BrandCustomer: Top5BrandCustomer({ http }),
  Partners: Partners({ http }),
  Login: Login({ http }),
  Users: Users({ http }),
  NewCustomer: NewCustomer({ http }),
  CustomerPerformance: CustomerPerformance({ http }),
  StepConversion: StepConversion({ http }),
  BrandLandscape: BrandLandscape({ http }),
  TopBrand: TopBrand({ http }),
  MyPartners: MyPartners({ http }),
};
