const stepConversion = ({ http }) => ({
  getStepConversion: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/search', { params }, config),
  getPreferencePurchaseLargeCate: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/bar/categoryL', { params }, config),
  getPreferencePurchaseMiddleCate: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/bar/categoryM', { params }, config),
  getPreferencePurchaseBrand: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/brand', { params }, config),
  getCustomerCharacteMembership: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/radarMultiple', { params }, config),
  getCustomerCharacteColumnDistributed: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/columnDistributed', { params }, config),
  getCustomerCharacteTreemapDistributed: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/treemapDistributed', { params }, config),
  getCustomerCharacteStackedBar: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/stackedBar', { params }, config),
  getAnnualPurchaseCount: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/stackedColumn/count', { params }, config),
  getAnnualPurchasePay: ({ params, config }) => http.get('/api/statistics/period/stageCustomer/chart/stackedColumn/amount', { params }, config),

});

export default stepConversion;
