const customerPerformance = ({ http }) => ({
  getCustomerStatistics: ({ params, config }) => http.get('/api/statistics/period/brandResult/search', { params }, config),
  getCustomerInflow: ({ params, config }) => http.get('/api/statistics/period/inflowKeyword/search', { params }, config),
  getAllAciveCustomer: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/columnRotated/totalActivity', { params }, config),
  getLastYearOrder: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/columnRotated/lastYear', { params }, config),
  getTotalActivityStage: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/columnRotated/totalActivityStage', { params }, config),
  getActivityStageTrans: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/columnRotated/totalActivityStageTrans', { params }, config),
  getCustomerNumber: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/lineDashed', { params }, config),
  getTotalOrder: ({ params, config }) => http.get('/api/statistics/period/brandResult/chart/multipleYAxis', { params }, config),
});

export default customerPerformance;
