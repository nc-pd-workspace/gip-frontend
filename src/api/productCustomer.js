const ProductCustomer = ({ http }) => ({
  productCustomerList: ({ params, config }) => http.get('/api/statistics/period/productCustomer/search', { params }, config),
  productCustomerStatistics: ({ params, config }) => http.get('/api/statistics/period/productCustomer/summary', { params }, config),
  productCustomerLineChart: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/line', { params }, config),
  productCustomerMultipleYAxis: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/multipleYAxis', { params }, config),
  // 성별/연령별 그래프 API
  productCustomerBarNegative: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/stackedBar', { params }, config),
  // 멤버십 등급 그래프 API
  productCustomerRadarMultiple: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/radarMultiple', { params }, config),
  // 지역(전국) 그래프 API
  productCustomerColumnDistributed: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/columnDistributed', { params }, config),
  // 지역(수도권) 그래프 API
  productCustomerTreemapDistributed: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/treemapDistributed', { params }, config),
  // 연간 구매 형태 > 연간 구매 횟수 그래프 API
  productCustomerStackedColumnCount: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/stackedColumn/count', { params }, config),
  // 연간 구매 형태 > 연간 구매 금액 그래프 API
  productCustomerStackedColumnAmount: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/stackedColumn/amount', { params }, config),
  // 카테고리/브랜드 구매 선호도 > 카테고리(대) 그래프 API
  productCustomerBarCategoryL: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/bar/categoryL', { params }, config),
  //  카테고리/브랜드 구매 선호도 > 카테고리(중) 그래프 API
  productCustomerBarCategoryM: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/bar/categoryM', { params }, config),
  // 브랜드 그래프 API
  productCustomerBrandGraph: ({ params, config }) => http.get('/api/statistics/period/productCustomer/chart/brand', { params }, config),
});

export default ProductCustomer;
