const Top5BrandCustomer = ({ http }) => ({

  // TOP5브랜드 활동고객 분석 검색 API
  // competitorsActivityList: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(list), 1000);
  // }),
  top5BrandCustomerList: ({ params, config }) => http.get('/api/statistics/customer/rankBrand/search', { params }, config),

  //  TOP5브랜드 활동고객 분석 메인그래프 API
  top5BrandCustomerLineData: ({ params, config }) => http.get('/api/statistics/customer/rankBrand/chart/lineData', { params }, config),

});

export default Top5BrandCustomer;
