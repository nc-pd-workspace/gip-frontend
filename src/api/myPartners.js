const MyPartners = ({ http }) => ({
  // getMyPatnerInfo: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(partnerInfo), 1000);
  // }),
  // 내 파트너 기본 정보
  getMyPatnerInfo: ({ params, config }) => http.get(`/api/my/partner?ptnIdx=${params.partnerIdx}`, config),

  // getMyPatnerAccountList: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(accountList), 1000);
  // }),
  // 거래처 조회
  getMyPatnerAccountList: ({ params, config }) => http.get('/api/common/popup/sup', { params }, config),

  // 내 파트너 소속 사용자
  // getMyPatnerList: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(partnerList), 1000);
  // }),
  getMyPatnerList: ({ params, config }) => http.get(`/api/my/partner/user?ptnIdx=${params.idx}&page=${params.page}&size=${params.size}`, config),
  // getMyPatnerList: ({ params, config }) => http.get('/my/partner/user', config),

  // getMyPatnerTree: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(myPartnerTreeData), 1000);
  // }),
  // 내 파트너 목록
  getMyPatnerTree: ({ params, config }) => http.get('/api/my/partner/list', config),
  // getMyPatnerTree: ({ params, config }) => http.get(`/my/partner/list?ptnIdx=${params.idx}`, config),

  // 내 파트너 정보 수정 API
  updatePatnerInfo: ({ params, config }) => http.put('/api/my/partner', params.dto, config),
  // updatePatnerDetailInfo: ({ params, config }) => http.put(`/system/partner/${params.ptnIdx}`, params.detailData, config),

});

export default MyPartners;
