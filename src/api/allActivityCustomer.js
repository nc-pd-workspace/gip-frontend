const AllActivityCustomer = ({ http }) => ({
  // allActivityCustomerStatistics: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(statistics), 1000);
  // }),

  allActivityCustomerStatistics: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/search', { params }, config),
  // 전체 활동고객 분석 메인 그래프 API
  allActivityCustomerLineAndColumn: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/lineAndColumn', { params }, config),
  // 성별/연령별 그래프 API
  allActivityCustomerBarNegative: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/barNegative', { params }, config),
  // 멤버십 등급 그래프 API
  allActivityCustomerRadarMultiple: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/radarMultiple', { params }, config),
  // 지역(전국) 그래프 API
  allActivityCustomerColumnDistributed: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/columnDistributed', { params }, config),
  // 지역(수도권) 그래프 API
  allActivityCustomerTreemapDistributed: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/treemapDistributed', { params }, config),
  // 연간 구매 형태 > 연간 구매 횟수 그래프 API
  allActivityCustomerStackedColumnCount: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/stackedColumn/count', { params }, config),
  // 연간 구매 형태 > 연간 구매 금액 그래프 API
  allActivityCustomerStackedColumnAmount: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/stackedColumn/amount', { params }, config),
  // 카테고리/브랜드 구매 선호도 > 카테고리(대) 그래프 API
  allActivityCustomerBarCategoryL: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/bar/categoryL', { params }, config),
  //  카테고리/브랜드 구매 선호도 > 카테고리(중) 그래프 API
  allActivityCustomerBarCategoryM: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/chart/bar/categoryM', { params }, config),
  // 브랜드 그래프 API
  allActivityCustomerBrandGraph: ({ params, config }) => http.get('/api/statistics/customer/totalActivity/char/array/brand', { params }, config),

});

export default AllActivityCustomer;
