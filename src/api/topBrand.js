const TopBrand = ({ http }) => ({
  topBrandList: ({ params, config }) => http.get('/api/statistics/brand/rank/search', { params }, config),
});

export default TopBrand;
