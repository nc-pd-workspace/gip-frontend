const Login = ({ http }) => ({
  // postLogin: ({ params, config } = {}) => http.get('/my/info', { params }, config),
  postLogin: ({ params, config } = {}) => http.post('/api/auth/login', { ...params }, config),
  postLoginPass: ({ params, config }) => http.post('/api/auth/login/certification/pass', { ...params }, config),
  postLoginCert: ({ params, config }) => http.post('/api/auth/login/certification', { ...params }, config),
  getEmailResend: ({ params, config }) => http.get('/api/auth/reSendEmail', { params, ...config }),
  putNextCharge: ({ params, config }) => http.put('/api/auth/newPassword/expired/nextChange', { ...params }, config),
  putSetPassword: ({ params, config }) => http.put('/api/auth/newPassword', { ...params }, config),
  putChangePassword: ({ params, config }) => http.put('/api/auth/newPassword/expired', { ...params }, config),
  putTermsAgree: ({ params, config }) => http.put('/api/auth/terms', { ...params }, config),
  getIdSearch: ({ params, config }) => http.get('/api/auth/user/find', { params }, config),
  getIdSearchCert: ({ params, config }) => http.get('/api/auth/user/find/certification', { params }, config),
  getFindPassword: ({ params, config }) => http.get('/api/auth/newPassword/sendEmail', { params }, config),
  getFindPasswordCert: ({ params, config }) => http.get('/api/auth/newPassword/certification', { params }, config),
});

export default Login;
