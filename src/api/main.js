const Main = ({ http }) => ({
  // getMainStatistics: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(statistics), 1000);
  // }),
  getMainStatistics: ({ params, config }) => http.get(`/api/dashboard?ptnIdx=${params.ptnIdx}`, { }, config),

  // getMainCharts: ({ params, config }) => http.get('/api/v1/main/charts', { params }, config),
  // 실적지수
  // getMainLineCharts: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(lineChartMockData), 1000);
  // }),
  getMainLineCharts: ({ params, config }) => http.get(`/api/dashboard/chart/lineData?ptnIdx=${params.ptnIdx}`, { }, config),

  // 단계별
  // getMainColumnChart: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(columnChartMockData), 1000);
  // }),
  getMainColumnChart: ({ params, config }) => http.get(`/api/dashboard/chart/column?ptnIdx=${params.ptnIdx}`, { }, config),

  // 유입경로별
  // getMainDonutChart: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(donutChartMockData), 1000);
  // }),
  getMainDonutChart: ({ params, config }) => http.get(`/api/dashboard/chart/donut?ptnIdx=${params.ptnIdx}`, { }, config),
  // 카테고리(대)
  getMainPolarChart: ({ params, config }) => http.get(`/api/dashboard/chart/polar?ptnIdx=${params.ptnIdx}`, { }, config),
  // 성별/연령별
  getMainBarNegativeChart: ({ params, config }) => http.get(`/api/dashboard/chart/barNegative?ptnIdx=${params.ptnIdx}`, { }, config),
  // 전체 활동고객수
  getMainlineAndColumnChart: ({ params, config }) => http.get(`/api/dashboard/chart/lineAndColumn?ptnIdx=${params.ptnIdx}`, { }, config),

});

export default Main;
