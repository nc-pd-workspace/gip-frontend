// import http from './index';

const Common = ({ http }) => ({
  getHeaderPartnerList: ({ params, config }) => http.get('/api/auth/partner', { params }, config),
  getHeaderUserInfo: ({ params, config }) => http.get('/api/auth/user', { params }, config),
  getMyInfo: ({ params, config }) => http.get('/api/my/info', { params }, config),
  putMyInfo: ({ params, config }) => http.put('/api/my/info', { ...params }, config),
  putUserStatus: ({ params, config }) => http.put(`/api/common/user/${params.usrIdx}/status`, { ...params }, config),
  putUserType: ({ params, config }) => http.put(`/api/common/user/${params.usrIdx}/type`, { ...params }, config),
  getSaleChannelList: ({ params, config }) => http.get('/api/common/code/list/saleChannel', {}, config),
  putPartnerStatusChange: ({ params, config }) => http.put(`/api/common/partner/${params.ptnIdx}/status`, { ...params }, config),
  getCategoryOptions: ({ params, config }) => http.get('/api/common/searchKeyword/category', { params }, config),
  getBrandOptions: ({ params, config }) => http.get('/api/common/searchKeyword/brand', { params }, config),
  getCustomerSelects: ({ params, config }) => http.get('/api/common/searchKeyword/customer', { params }, config),
  productCustomerModalList: ({ params, config }) => http.get('/api/common/popup/product', { params }, config),
  getUserRole: ({ params, config }) => http.get('/api/common/user/role', params, config),
  // 거래처 중복 API
  supDuplicate: ({ params, config }) => http.get('/api/common/sup/duplicate', { params }, config),
  getLastUpdateDate: ({ params, config }) => http.get('/api/common/statistics/updateDate', { params, ...config }),
  // 파트너 상태 변경 전 유효성 검사 API
  partnerStatusCheck: ({ params, config }) => http.get(`/api/common/partner/${params.ptnIdx}/status/check?stCd=${params.stCd}`, {}, config),
});

export default Common;
