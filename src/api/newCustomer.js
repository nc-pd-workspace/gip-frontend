const NewCustomer = ({ http }) => ({

  // 신규 활동고객 분석 검색 API
  // newCustomerList: ({ params, config }) => new Promise((resolve) => {
  //   setTimeout(() => resolve(list), 1000);
  // }),
  newCustomerList: ({ params, config }) => http.get('/api/statistics/customer/newActivity/search', { params }, config),

  // 신규 활동고객 분석 그래프1 API
  newCustomerChartSpline: ({ params, config }) => http.get('/api/statistics/customer/newActivity/chart/spline', { params }, config),

  //  신규 활동고객 분석 그래프2 API
  newCustomerChartStackedBar: ({ params, config }) => http.get('/api/statistics/customer/newActivity/chart/stackedBar', { params }, config),

});

export default NewCustomer;
