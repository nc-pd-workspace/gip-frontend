const StepCustomer = ({ http }) => ({
  //  단계별 활동고객 분석 검색 API
  stepCustomerStatistics: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/search', { params }, config),
  //   메인 그래프 API
  stepCustomerLineAndColumn: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/lineAndColumn', { params }, config),
  // 카테고리/브랜드 구매 선호도 > 카테고리(대) 그래프 API
  stepCustomerBarCategoryL: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/bar/categoryL', { params }, config),
  // 카테고리/브랜드 구매 선호도 > 카테고리(중) 그래프 API
  stepCustomerBarCategoryM: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/bar/categoryM', { params }, config),
  // 카테고리/브랜드 구매 선호도 > 브랜드 그래프 API
  stepCustomerBrandGraph: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/array/brand', { params }, config),
  //  고객 특성 > 성별/연령별 그래프 API
  stepCustomerBarNegative: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/barNegative', { params }, config),
  //  고객 특성 > 멤버십 등급 그래프 API
  stepCustomerRadarMultiple: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/radarMultiple', { params }, config),
  //  고객 특성 > 지역(전국) 그래프 API
  stepCustomerColumnDistributed: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/columnDistributed', { params }, config),
  //  고객 특성 > 지역(수도권) 그래프 API
  stepCustomerTreemapDistributed: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/treemapDistributed', { params }, config),
  //  연간 구매 형태 > 연간 구매 금액 그래프 API
  stepCustomerStackedColumnAmount: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/stackedColumn/amount', { params }, config),
  //  연간 구매 형태 > 연간 구매 횟수 그래프 API
  stepCustomerStackedColumnCount: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/stackedColumn/count', { params }, config),
  //  유입경로 그래프 API
  stepCustomerDonutGraph: ({ params, config }) => http.get('/api/statistics/customer/stageActivity/chart/donut', { params }, config),

});

export default StepCustomer;
