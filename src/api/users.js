const Users = ({ http }) => ({
  getUserList: ({ params, config }) => http.get('/api/system/user/search', { params }, config),
  getUserInfo: ({ params, config }) => http.get(`/api/system/user/${params.usrIdx}`, {}, config),
  putUserInfo: ({ params, config }) => http.put(`/api/system/user/${params.usrIdx}`, { ...params }, config),
});

export default Users;
