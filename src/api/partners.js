const Partners = ({ http }) => ({
  getPatnerInfo: ({ params, config }) => http.get(`/api/system/partner/${params.idx}`, {}, config),
  getPatnerTree: ({ params, config }) => http.get('/api/system/partner', {}, config),
  updatePatnerDetailInfo: ({ params, config }) => http.put(`/api/system/partner/${params.ptnIdx}`, params.detailData, config),
  insertPatnerDetailInfo: ({ params, config }) => http.post('/api/system/partner', params.detailData, config),
  getPartnerIdCheck: ({ params, config }) => http.get(`/api/system/partner/duplicate?ptnId=${params.ptnId}`, {}, config),
  updatePatnerDepth: ({ params, config }) => http.put(`/api/system/partner/${params.ptnIdx}/depth`, params, config),
  getUserList: ({ params, config }) => http.get(`/api/system/partner/${params.partnerIdx}/user?page=${params.page}&size=${params.size}`, {}, config),
  getUserIdCheck: ({ params, config }) => http.get('/api/common/user/id/duplicate', { params }, config),
  getMyPartnerUserIdCheck: ({ params, config }) => http.get(`/api/system/partner/{ptnIdx}/user?ptnIdx=${params.partnerIdx}&usrId=${params.usrId}`, {}, config),
  getCommonPartnerList: ({ params, config }) => http.get('/api/common/partner/list', { params }, config),
  insertUser: ({ params, config }) => http.post('/api/common/user', params, config),

});

export default Partners;
