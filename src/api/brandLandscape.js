const BrandLandscape = ({ http }) => ({
  brandLandscapeList: ({ params, config }) => http.get('/api/statistics/brand/search', { params }, config),
  brandMainChart: ({ params, config }) => http.get('/api/statistics/brand/chart/multipleYAxis', { params }, config),
});

export default BrandLandscape;
