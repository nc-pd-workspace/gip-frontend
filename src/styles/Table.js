import { css } from 'styled-components';

export const cssTableHiddenHeader = css`
  table {
    border-top: none !important;
    table-layout: auto !important;
  }
  .ant-table-thead {
    display: none !important;
  }
  .ant-table-placeholder {
    display: none;
  }
  .ant-spin-container {
    transition: none;
  }
`;

export const cssTable = css`
  position: relative;
  padding: 0;
  table {
    border-top:1px solid #E3E4E7;
    visibility: unset !important;

    .ant-table-measure-row {
      display: none;
    }
    tr {
      th {
        font-size:12px;
        color: var(--color-gray-700);
        background-color: #fff;
        padding: 7px 10px 6px;
        border-bottom:1px solid #E3E4E7;
        &:before {
          display: none;
        }
      }
      td {
        padding: 13px 10px;
        font-size:13px;
        border-bottom:1px solid #E3E4E7;
        &.ant-table-cell-row-hover {
          background-color: #F7F8FA;
          cursor: pointer;
        }
      }
      &:last-child td{
        border-bottom: none !important;
      }
    }
    tr.currentSelectedRow > td {
      background-color: #F2F9FF !important;
      font-weight: 700;
    }
    tr.noClick > td.ant-table-cell-row-hover {
      background-color: #FFF;
      cursor: auto;
    }
    .cellNumber {
      letter-spacing: -.75px;
    }
  }
  .ant-table-body {
    table {
      border-top: none;
    }
  }
`;
