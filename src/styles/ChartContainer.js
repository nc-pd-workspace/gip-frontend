import styled from 'styled-components';

export const ChartContainer = styled.div`
  position: relative;
  display: flex;
  margin-top: 10px;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 

  .chart-area{
    height: 355px;
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
    border-radius: 4px;
    padding: 20px 8px;
    .chart-title {
      padding: 0 0 0 12px;
      font-size: 14px;
      line-height: 21px;
      font-weight: 700;
      color: #111;
    }
    .count {
      font-size: 20px;
      font-weight: 700;
    }
    .percent {
      line-height: 31px
    }
    &.heightAuto {
      height: auto !important;
    }
    &.padding20 {
      .chart-title {
        padding-left: 0;
      }
      padding: 20px !important;
    }
  }

  .tag-area{
    height: 400px;
    width: 100%;
    position: relative;
  }
`;
