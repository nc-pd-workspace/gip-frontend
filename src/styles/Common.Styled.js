import entries from 'lodash-es/entries';

import { css } from 'styled-components';

import { COLORS } from './Colors';

const colorEntries = entries(COLORS);

/**
 * createTextColorStyle
 * Color set 을 이용하여 text color 를 위한 css class 를 제공한다
 * example
 * .primary {
 *   color: #3B7BF6
 * }
 * .light-gray-1 {
 *   color: #F7F8FA
 * }
 */
export const createTextColorStyle = () => colorEntries.map((item) => css`
  .${item[0].toLowerCase().replace(/_/g, '-')} {
    color: ${item[1]};
  }
`);
