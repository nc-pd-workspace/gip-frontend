import axios from 'axios';
import moment from 'moment';

function round(num) {
  const m = Number((Math.abs(num) * 100).toPrecision(15));
  return (Math.round(m) / 100) * Math.sign(num);
}
/** 핸드폰 입력용 replace number 치환 함수 */
export const getPhoneValidationTransform = (number) => number.replace(/[^0-9]/g, '').replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/, '$1-$2-$3').replace('--', '-');

export const getProductValidationTransform = (text) => text.replace(/[^0-9,]/g, '');

export const getOnlyNumberValidation = (number) => number.replace(/[^0-9]/g, '');

export const getOnlyNumberAndEngTransform = (text) => text.replace(/[^0-9A-Za-z]/g, '');

export const getChartValue = (number, type) => {
  if (number >= 1000000000000) return `${round(number / 1000000000000).toLocaleString()}조`;
  if (number >= 100000000000) return `${round(number / 100000000000).toLocaleString()}천억`;
  if (number >= 100000000) return `${round(number / 100000000).toLocaleString()}억`;
  if (number >= 10000000) return `${round(number / 10000000).toLocaleString()}천만`;
  if (number >= 10000) return `${round(number / 10000).toLocaleString()}만`;
  if (number >= 1000 && type !== 'money') return `${round(number / 1000).toLocaleString()}천`;
  return `${number.toLocaleString()}`;
};

export const getChartValueMoney = (number) => `${getChartValue(number, 'money')}${number !== 0 ? '원' : ''}`;
export const getChartValueUnit = (number) => `${getChartValue(number, 'unit')}${number !== 0 ? '건' : ''}`;
export const getChartValuePeople = (number) => `${getChartValue(number, 'people')}${number !== 0 ? '명' : ''}`;

export const getChartYAxisMax = (number) => {
  let returnValue;

  if (number >= 10) {
    for (let num = 1000000000000; num >= 1; num /= 10) {
      if (number > num) {
        returnValue = Math.ceil(number / num) * num;
        break;
      }
    }
  } else {
    returnValue = 10;
  }

  const returnValueDigit = 10 ** (returnValue.toString().length - 1);
  if (number % returnValueDigit > returnValueDigit * 0.8) {
    returnValue += returnValueDigit;
  }

  return returnValue;
};
export const getChartYAxisMin = (number) => {
  let returnValue;
  if (number >= 0) return 0;

  if (number >= 1) {
    returnValue = 0;
  } else if (number <= -1) {
    for (let num = 1000000000000; num >= 1; num /= 10) {
      if (Math.abs(number) > num) {
        returnValue = Math.ceil(Math.abs(number) / (num * 10)) * (num * 10);
        break;
      }
    }
  }
  // 소수점 계산
  if (returnValue === undefined) {
    for (let num = 1; num >= 0.0001; num /= 10) {
      if (Math.abs(number) > num) {
        returnValue = num * 10;
        break;
      }
    }
  }
  return returnValue * Math.sign(number);
};

export const getChartLineYAxisMinMax = (min, max) => {
  let returnMaxValue;
  let returnMinValue;
  const absMin = Math.abs(min); // 우선 부호 제거
  const absMax = Math.abs(max); // 우선 부호 제거

  // 1. 예외. min과 max가 모두 0일 때 0과 10을 리턴한다.
  if (min === 0 && max === 0) {
    return { min: 0, max: 10 };
  }

  if (max >= 10) {
    for (let num = 1000000000000; num >= 10; num /= 10) {
      if (max >= num) {
        returnMaxValue = Math.ceil(max / num) * num;
        break;
      }
    }
  } else if (max <= -10) {
    for (let num = 1000000000000; num >= 10; num /= 10) {
      if (absMax >= num) {
        returnMaxValue = Math.floor(absMax / num) * num * -1;
        break;
      }
    }
  } else if (max > 0) {
    // 최대값이 10 미만 양수일 경우엔 10 리턴.
    returnMaxValue = 10;
  } else {
    returnMaxValue = 0;
  }

  if (min <= -10) {
    for (let num = 1000000000000; num >= 10; num /= 10) {
      if (absMin >= num) {
        returnMinValue = Math.ceil(absMin / num) * num * -1;
        break;
      }
    }
  } else if (min >= 10) {
    for (let num = 1000000000000; num >= 10; num /= 10) {
      if (absMin >= num) {
        returnMinValue = Math.floor(absMin / num) * num;
        break;
      }
    }
  } else if (min < 0) {
    returnMinValue = -10;
  } else {
    returnMinValue = 0;
  }

  const returnMinDigit = returnMinValue === 0 ? 0 : Math.abs(returnMinValue).toString().length - 1;
  const returnMaxDigit = returnMaxValue === 0 ? 0 : Math.abs(returnMaxValue).toString().length - 1;

  // 자릿수가 다르다면 균일하게 맞춰주는 작업 진행
  if (returnMinDigit !== returnMaxDigit) {
    if (returnMinDigit > returnMaxDigit) {
      if (10 ** returnMinDigit > Math.abs(returnMaxValue)) {
        returnMaxValue = 10 ** returnMinDigit * (returnMaxValue >= 0 ? 1 : -1);
      }
    } else if (10 ** returnMaxDigit > Math.abs(returnMinValue)) {
      if (returnMinValue > 0) returnMinValue = 0;
      else returnMinValue = 10 ** returnMaxDigit * -1;
    }
  }

  // min과 max가 같을 경우 가장 큰 자릿수만큼 max값을 한뎁스 올린다.
  if (returnMinValue === returnMaxValue) {
    returnMaxValue += 10 ** (returnMaxDigit > returnMinDigit ? returnMaxDigit : returnMinDigit);
  }

  return {
    max: returnMaxValue,
    min: returnMinValue,
  };
};
export const idSearchPw = (id, password) => {
  let tmp = '';
  let cnt = 0;
  for (let i = 0; i < id.length - 3; i += 1) {
    tmp = id.substr(i, 4);
    if (password.indexOf(tmp) > -1) {
      cnt += 1;
    }
  }
  if (cnt > 0) {
    return true;
  }
  return false;
};

export const eventYaxisIndex = {
  mounted: () => {
    const apexchartsSvg = document.querySelector('.totalOrderChart').querySelector('.apexcharts-svg');
    const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
    apexchartsSvg.appendChild(apexchartsGraphical);
  },
};

export const excelDownload = async (apiUrl, accessToken, fileName, params, subFileName) => {
  const result = await axios.get(`${process.env.REACT_APP_SERVER_URL}${apiUrl}`, {
    headers: {
      'Authorization-gip-access-token': accessToken,
      'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8',
    },
    responseType: 'arraybuffer',
    params,
  })
    .then((response) => {
      const url = window.URL.createObjectURL(
        new Blob(
          [response.data],
          { type: response.headers['content-type'] },
        ),
      );
      const link = document.createElement('a');
      link.href = url;
      let downloadFileName;
      if (subFileName) {
        downloadFileName = `${fileName}_${subFileName === 'date' ? '일별' : '월별'}_${moment().format('YYYYMMDD')}.csv`;
      } else {
        downloadFileName = `${fileName}_${moment().format('YYYYMMDD')}.csv`;
      }
      link.setAttribute(
        'download',
        downloadFileName,
      );
      document.body.appendChild(link);
      link.click();
      return 'success';
    })
    .catch((error) => 'fail');

  return result;
};
