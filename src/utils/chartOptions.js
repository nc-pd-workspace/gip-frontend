import isNil from 'lodash-es/isNil';

import { CHART_COLORS, CHART_VARIATION } from '../styles/chartColors';

export const legendDefault = {
  show: true,
  position: 'bottom',
  showForSingleSeries: true,
  horizontalAlign: 'center',
  markers: {
    width: 10,
    height: 10,
    strokeWidth: 0,
    radius: 2,
    offsetX: -1,
    offsetY: 1,
  },
  itemMargin: {
    horizontal: 8,
    vertical: 0,
  },
  labels: {
    colors: 'var(--color-gray-700)',
    useSeriesColors: false,
  },
};

// 성별/연령별
export const optionsBarNegative = {
  chart: {
    zoom: {
      enabled: false,
    },
    toolbar: {
      show: false,
    },
    type: 'bar',
    height: 350,
    stacked: true,
    offsetY: -10,
  },
  plotOptions: {
    bar: {
      horizontal: true,
    },
  },
  grid: {
    show: false,
  },
  stroke: {
    width: 0,
    colors: ['#fff'],
  },
  dataLabels: {
    enabled: true,
    distributed: false,
    formatter(val) {
      return `${Math.abs(Math.round(val))}`;
    },
  },
  colors: [
    CHART_COLORS.GENDER_MALE,
    CHART_COLORS.GENDER_MALE,
    CHART_COLORS.GENDER_FEMALE,
    CHART_COLORS.GENDER_UNKNOWN,
  ],
  xaxis: {
    tickAmount: 5,
    min: 0,
  },
  tooltip: {
    enabled: true,
    shared: true,
    followCursor: false,
    intersect: false,
    inverseOrder: true,
    y: {
      formatter(val) {
        return `${val}%`;
      },
    },
  },
  fill: {
    opacity: 1,
  },
  legend: {
    ...legendDefault,
  },
};

// 고객 등급
export const optionsRadarMultiple = {
  chart: {
    toolbar: {
      show: false,
    },
    zoom: {
      enabled: false,
    },
    type: 'radar',
    dropShadow: {
      enabled: true,
      blur: 1,
      left: 1,
      top: 1,
    },
  },
  title: {},
  stroke: {
    width: 1,
  },
  fill: {
    opacity: 0.1,
  },
  colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
  tooltip: {
    y: {
      formatter(val) {
        return `${(val).toLocaleString()}명`;
      },
    },
  },
  legend: {
    ...legendDefault,
    position: 'right',
    horizontalAlign: 'center',
    offsetX: 0,
    offsetY: 5,
    itemMargin: {
      horizontal: 8,
      vertical: 4,
    },
  },
  responsive: [{
    breakpoint: 1280,
    options: {
      legend: {
        position: 'bottom',
        offsetX: -10,
        offsetY: 0,
      },
    },
  }],
};

export const optionsColumnDistributed = {
  chart: {
    toolbar: {
      show: false,
    },
    type: 'bar',
  },
  zoom: {
    enabled: false,
  },
  // colors,
  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true,
    },
  },
  dataLabels: {
    enabled: false,
  },
  legend: {
    show: false,
  },
  tooltip: {
    enabled: true,
    shared: true,
    followCursor: false,
    intersect: false,
    inverseOrder: true,
    x: {
      formatter(val) {
        return val;
      },
    },
    y: {
      formatter(val) {
        if (isNil(val)) return '-';
        return `${(val).toLocaleString()}명`;
      },
    },
  },
  colors: CHART_VARIATION,
};

export const optionsTreemapDistributed = {
  legend: {
    show: false,
  },
  chart: {
    toolbar: {
      show: false,
    },
    type: 'treemap',
  },
  tooltip: {
    y: {
      formatter(val) {
        return `${(val).toLocaleString()}명`;
      },
    },
  },
  dataLabels: {
    offsetY: -2,
    style: {
      fontSize: '12px',
    },
  },
  stroke: {
    show: true,
    curve: 'smooth',
    lineCap: 'butt',
    colors: ['#FFF'],
    width: 1,
    dashArray: 0,
  },
  colors: CHART_VARIATION,
  plotOptions: {
    treemap: {
      distributed: true,
      enableShades: false,
    },
  },
};

export const optionsStackedColumn = {
  chart: {
    toolbar: {
      show: false,
    },
    type: 'bar',
    stacked: true,
    stackType: '100%',
  },
  responsive: [
    {
      breakpoint: 480,
      options: {
        legend: {
          position: 'bottom',
          offsetX: -10,
          offsetY: 0,
        },
      },
    },
  ],
  dataLabels: {
    enabled: true,
    style: {
      colors: [
        CHART_COLORS.TEXT_BLACK,
        CHART_COLORS.TEXT_BLACK,
        CHART_COLORS.TEXT_BLACK,
        CHART_COLORS.TEXT_BLACK,
        CHART_COLORS.TEXT_BLACK,
        CHART_COLORS.TEXT_WHITE,
        CHART_COLORS.TEXT_WHITE,
        CHART_COLORS.TEXT_WHITE,
        CHART_COLORS.TEXT_WHITE,
        CHART_COLORS.TEXT_WHITE,
      ],
    },
    formatter(val) {
      return `${Math.abs(Math.round(val))}`;
    },
  },
  xaxis: {
    rotate: 0,
    labels: {
      hideOverlappingLabels: false,
      style: {
        fontSize: '11px',
        cssClass: 'apexcharts-xaxis-label-overlapping',
      },
      offsetY: -2,
    },
  },
  yaxis: {
    tickAmount: 5,
    min: 0,
    labels: {
      formatter(val) {
        return `${Math.abs(Math.round(val))}%`;
      },
    },
  },
  fill: {
    opacity: 1,
  },
  tooltip: {
    enabled: true,
    shared: true,
    followCursor: false,
    intersect: false,
    inverseOrder: true,
  },
};

export const optionsBarCategory = {
  chart: {
    toolbar: {
      show: false,
    },
    type: 'bar',
  },
  plotOptions: {
    bar: {
      borderRadius: 2,
      horizontal: true,
      barHeight: '50%',
    },
  },
  dataLabels: {
    enabled: false,
  },
  tooltip: {
    followCursor: true,
    y: {
      formatter(val) {
        return `${(val).toLocaleString()}명`;
      },
    },
  },
  legend: {
    ...legendDefault,
    show: false,
    showForSingleSeries: true,
    position: 'bottom',
    horizontalAlign: 'center',
    customLegendItems: ['전체 활동고객수'],
  },
};

export const optionsDonutGraph = {
  chart: {
    type: 'donut',
    height: '270px',
  },
  plotOptions: {
    pie: {
      donut: {
        size: '65%',
      },
    },
  },
  stroke: {
    show: false,
    width: 0,
  },
  dataLabels: {
    enabled: true,
    dropShadow: {
      enabled: false,
    },
    formatter(val) {
      return `${Math.abs(Math.round(val))}%`;
    },
  },
  legend: {
    ...legendDefault,
    position: 'right',
    itemMargin: {
      horizontal: 8,
      vertical: 4,
    },
  },
  colors: CHART_VARIATION,
};
