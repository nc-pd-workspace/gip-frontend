import { jsonParse } from './jsonParse';

export const loadOpenedPages = () => jsonParse(window.sessionStorage.getItem('openedPages'));
export const saveOpenedPages = (pages) => window.sessionStorage.setItem('openedPages', JSON.stringify(pages));
