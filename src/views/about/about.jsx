import TypeIt from 'typeit-react';

import { Link } from 'react-router-dom';

import styles from './about.module.css';
import logoGip from '../../Images/about/logoGip.svg';
import logoGray from '../../Images/about/logoGray.svg';
import benefitIcon01 from '../../Images/about/benefitIcon01.png';
import benefitIcon02 from '../../Images/about/benefitIcon02.png';
import benefitIcon03 from '../../Images/about/benefitIcon03.png';
import card01 from '../../Images/about/card01.png';
import card02 from '../../Images/about/card02.png';
import card03 from '../../Images/about/card03.png';
import card04 from '../../Images/about/card04.png';
import card05 from '../../Images/about/card05.png';
import card06 from '../../Images/about/card06.png';
import introduce01 from '../../Images/about/introduce01.png';
import introduce02 from '../../Images/about/introduce02.png';
import introduce03 from '../../Images/about/introduce03.png';

function About() {
  const mailTo = () => {
    window.location.href = 'mailto:doyun.koo@gsretail.com';
  };
  const moveToLogin = () => {
    window.location.href = '/login';
  };
  return (
    <div className={styles.about}>
      <header className={styles.header}>
        <section className={styles.wrapper}>
          <h1 className={styles.logo}>
            <Link to="/about">
              <img src={logoGip} alt="GIP" />
            </Link>
          </h1>
          <div className={styles.headerLogin}>
            <button className={styles.blueButton} onClick={moveToLogin}>로그인하러 가기</button>
          </div>
        </section>
      </header>
      <div className={styles.mainWrapper}>
        <section className={styles.mainVisual}>
          <article className={styles.wrapper}>
            <div className={styles.mainTextBox}>
              <div className={styles.text01}>GS리테일의 인사이트 플랫폼</div>
              <div className={styles.text02}>
                복잡한 데이터,
                <br />
                이제 GIP에서
                <br />
                편하게 확인하세요.
              </div>
              <div className={styles.button}>
                <button className={styles.blueButton} onClick={moveToLogin}>로그인하러 가기</button>
              </div>
            </div>
          </article>
        </section>
      </div>
      <section className={styles.section01}>
        <article className={styles.wrapper}>
          <div className={styles.typingWrap}>
            <div className={styles.typeing} id="typeing">
              <TypeIt
                options={{ loop: true }}
                getBeforeInit={(instance) => {
                  instance
                    .type('신규 활동 고객의 구매행동이 궁금할 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('어떤 키워드로 유입했는지 궁금할 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('전년 동기간 주문금액이 궁금할 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('동종업계 내 브랜드 실적이 궁금할 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('판매 전환 효율을 높이고 싶을 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('충성 고객을 확보하고 싶을 때')
                    .pause(750)
                    .delete()
                    .pause(500)
                    .type('이탈고객을 줄이고 싶을 때')
                    .pause(750)
                    .delete()
                    .pause(500);
                  return instance;
                }}
              />
            </div>
            <div className={styles.iconSearch} />
          </div>
          <div className={styles.text}>
            매일 변화하는 데이터를
            <br />
            GIP 대시보드에서 한 눈에 확인하세요.
          </div>
        </article>
        <div className={styles.slideImage}>
          <div><img src={card01} alt="" /></div>
          <div><img src={card02} alt="" /></div>
          <div><img src={card03} alt="" /></div>
          <div><img src={card04} alt="" /></div>
          <div><img src={card05} alt="" /></div>
          <div><img src={card06} alt="" /></div>
        </div>
      </section>
      <section className={`${styles.section02} ${styles.pageable}`}>
        <div className={styles.textBox}>
          <p className={styles.subTitle}>브랜드 현황</p>
          <p className={styles.title}>
            우리 브랜드의
            <br />
            <span className={styles.blue}>판매실적과 동종업계 순위</span>
            를
            <br />
            한 눈에 비교해보세요.
          </p>
          <ul>
            <li>
              우리 브랜드의 실적 현황은 물론, GS SHOP의 모든 브랜드와
              <br />
              카테고리별 주문금액, 주문수, 주문고객수를 비교해보세요.
            </li>
            <li>
              GS SHOP TOP10 브랜드의 실적과 순위를 함께 비교하고
              <br />
              분석할 수 있어요.
            </li>
          </ul>
        </div>
        <div className={styles.imgBox}><img src={introduce01} alt="브랜드 현황" /></div>
      </section>
      <section className={`${styles.section03} ${styles.pageable}`}>
        <div className={styles.textBox}>
          <p className={styles.subTitle}>고객 활동 분석</p>
          <p className={styles.title}>
            모든 활동고객의
            <br />
            <span className={styles.blue}>특성과 단계별 활동</span>
            을
            <br />
            한 눈에 살펴보세요.
          </p>
          <ul>
            <li>
              신규 활동고객을 포함하여 우리 브랜드의 전체 활동 고객들은
              <br />
              어떤 특성을 갖고 있는지 살펴보세요.
            </li>
            <li>
              활동고객수를 상세하게 조회하고 시각화된 데이터를 통해
              <br />
              세그먼트별로 최적의 판매 전략을 세울 수 있어요.
            </li>
          </ul>
        </div>
        <div className={styles.imgBox}><img src={introduce02} alt="고객 활동 분석" /></div>
      </section>
      <section className={`${styles.section04} ${styles.pageable}`}>
        <div className={styles.textBox}>
          <p className={styles.subTitle}>기간별 성과 분석</p>
          <p className={styles.title}>
            마케팅 기간 동안의
            <br />
            <span className={styles.blue}>판매실적과 고객수 변화</span>
            를
            <br />
            한 눈에 파악해보세요.
          </p>
          <ul>
            <li>
              마케팅/프로모션을 진행한 기간의 판매 성과, 고객수, 진입률,
              <br />
              매출 전환율 추이를 파악할 수 있어요.
            </li>
            <li>
              이전 비교 기간 대비 고객 활동 단계가 어떻게 변화했는지,
              <br />
              프로모션 상품의 판매 실적이 얼마나 늘었는지, 어떤 검색어를
              <br />
              통해 유입했는지 데이터를 통해 확인할 수 있어요.
            </li>
          </ul>
        </div>
        <div className={styles.imgBox}><img src={introduce03} alt="기간별 성과 분석" /></div>
      </section>
      <section className={styles.section05}>
        <article className={styles.wrapper}>
          <p className={styles.title}>GIP을 사용하면 좋은 점</p>
          <ul>
            <li>
              <p className={styles.image}><img src={benefitIcon01} alt="쉽게 확인하는 내 브랜드 현황" /></p>
              <p className={styles.title}>쉽게 확인하는 내 브랜드 현황</p>
              <p className={styles.contents}>
                GS리테일이 보유한
                <br />
                고객 데이터와 내 브랜드를 비교해서
                <br />
                브랜드현황을 파악할 수 있어요.
              </p>
            </li>
            <li>
              <p className={styles.image}><img src={benefitIcon02} alt="효율적인 마케팅 운영" /></p>
              <p className={styles.title}>효율적인 마케팅 운영</p>
              <p className={styles.contents}>
                효율적인 마케팅 운영을 위해 인사이트를
                <br />
                얻을 수 있는 지표를 제공합니다.
                <br />
                다양한 고객 반응을 손쉽게 확인하고 활용해보세요.
              </p>
            </li>
            <li>
              <p className={styles.image}><img src={benefitIcon03} alt="손쉬운 브랜드 관리" /></p>
              <p className={styles.title}>손쉬운 브랜드 관리</p>
              <p className={styles.contents}>
                브랜드 상황에 맞게 파트너사를 구성하고
                <br />
                브랜드 및 사용자를
                <br />
                통합 관리할 수 있어요.
              </p>
            </li>
          </ul>
        </article>
      </section>
      <section className={styles.section06}>
        <article className={styles.wrapper}>
          <div className={styles.siteWrap}>
            <p className={styles.contents01}><img src={logoGip} alt="GIP" /></p>
            <p className={styles.contents02}>GIP에서 쉽고, 편하게 인사이트를 발견하세요.</p>
          </div>
          <div className={styles.buttonWrap}>
            <button className={styles.blueButton} onClick={moveToLogin}>로그인하러 가기</button>
          </div>
        </article>
      </section>
      <footer className={styles.footer}>
        <article className={styles.wrapper}>
          <div className={styles.footerLogo}><img src={logoGray} alt="GIP" /></div>
          <ul className={styles.footerLink}>
            <li>
              <Link
                to="#"
                onClick={mailTo}
                className={styles.buttonMail}
              >
                이메일 문의
              </Link>
            </li>
            <li>
              <Link to="/privacyPolicy" target="_privacyPolicy">
                개인정보처리방침
              </Link>
            </li>
          </ul>
          <p className={styles.copyright}>
            ⓒ 2022. GS Retail Co. all rights reserved.
          </p>
        </article>
      </footer>
    </div>
  );
}

export default About;
