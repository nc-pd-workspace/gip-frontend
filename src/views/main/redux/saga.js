import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getLineChart, getStatistics, getColumnChart, getDonutChart, getPolarChart, getBarNegativeChart, getLineAndColumnChart } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const mainStatisticsSaga = createPromiseSaga(getStatistics, API.Main.getMainStatistics);
const mainLineChartSaga = createPromiseSaga(getLineChart, API.Main.getMainLineCharts);
const mainColumnChartSaga = createPromiseSaga(getColumnChart, API.Main.getMainColumnChart);
const mainDonutChartSaga = createPromiseSaga(getDonutChart, API.Main.getMainDonutChart);
const mainPolarChartSaga = createPromiseSaga(getPolarChart, API.Main.getMainPolarChart);
const mainBarNegativeChartSaga = createPromiseSaga(getBarNegativeChart, API.Main.getMainBarNegativeChart);
const mainLineAndColumnChartSaga = createPromiseSaga(getLineAndColumnChart, API.Main.getMainlineAndColumnChart);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getStatistics, mainStatisticsSaga);
  yield takeLatest(getLineChart, mainLineChartSaga);
  yield takeLatest(getColumnChart, mainColumnChartSaga);
  yield takeLatest(getDonutChart, mainDonutChartSaga);
  yield takeLatest(getPolarChart, mainPolarChartSaga);
  yield takeLatest(getBarNegativeChart, mainBarNegativeChartSaga);
  yield takeLatest(getLineAndColumnChart, mainLineAndColumnChartSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
