import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../redux/constants';

const statisticsFilter = (value) => {
  let returnValue = '';
  if (value) {
    returnValue = value;
  } else if (value === 0) {
    returnValue = '0';
  } else {
    returnValue = '-';
  }
  return returnValue;
};
const initialState = {
  statistics: asyncApiState.initial({
    summary: {},
    list: [],
  }),
  lineChart: asyncApiState.initial([]),
  columnChart: asyncApiState.initial([]),
  donutChart: asyncApiState.initial([]),
  polarChart: asyncApiState.initial([]),
  barNegativeChart: asyncApiState.initial([]),
  lineAndColumnChart: asyncApiState.initial([]),

};

export const { actions, reducer } = createSlice({
  name: 'main',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getStatistics: (state, { payload }) => {
      state.statistics = asyncApiState.request();
    },
    getStatisticsSuccess: (state, { payload }) => {
      const data = {
        summary: {
          totalCount: statisticsFilter(payload.data.totalActiveCust),
          changeRateByMonth: payload.data.totalActiveCustComparedMonth === 0 || payload.data.totalActiveCustComparedMonth ? payload.data.totalActiveCustComparedMonth : '',
          changeRateByYear: payload.data.totalActiveCustComparedYear === 0 || payload.data.totalActiveCustComparedYear ? payload.data.totalActiveCustComparedYear : '',
          conversionRate: statisticsFilter(Math.round(payload.data.purchaseConversionRate)),
        },
        list: [
          {
            title: '월 누적 주문금액',
            amount: {
              value: statisticsFilter(payload.data.totalOrderAmount),
              unit: '원',
            },
            changeRateByMonth: payload.data.totalOrderAmountComparedMonth === 0 || payload.data.totalOrderAmountComparedMonth ? (Math.round(payload.data.totalOrderAmountComparedMonth)) : '',
            changeRateByYear: payload.data.totalOrderAmountComparedYear === 0 || payload.data.totalOrderAmountComparedYear ? (Math.round(payload.data.totalOrderAmountComparedYear)) : '',
          },
          {
            title: '월 누적 주문수',
            amount: {
              value: statisticsFilter(payload.data.totalOrderCount),
              unit: '개',
            },
            changeRateByMonth: payload.data.totalOrderCountComparedMonth === 0 || payload.data.totalOrderCountComparedMonth ? (Math.round(payload.data.totalOrderCountComparedMonth)) : '',
            changeRateByYear: payload.data.totalOrderCountComparedYear === 0 || payload.data.totalOrderCountComparedYear ? (Math.round(payload.data.totalOrderCountComparedYear)) : '',
          },
          {
            title: '일 평균 주문고객수',
            amount: {
              value: statisticsFilter(payload.data.totalOrderCust),
              unit: '명',
            },
            changeRateByMonth: payload.data.totalOrderCustComparedMonth === 0 || payload.data.totalOrderCustComparedMonth ? (Math.round(payload.data.totalOrderCustComparedMonth)) : '',
            changeRateByYear: payload.data.totalOrderCustComparedYear === 0 || payload.data.totalOrderCustComparedYear ? (Math.round(payload.data.totalOrderCustComparedYear)) : '',
          },
          {
            title: '일 평균 전체 신규고객수',
            amount: {
              value: statisticsFilter(payload.data.totalNewCust),
              unit: '명',
            },
            changeRateByMonth: payload.data.totalNewCustComparedMonth === 0 || payload.data.totalNewCustComparedMonth ? (Math.round(payload.data.totalNewCustComparedMonth)) : '',
            changeRateByYear: payload.data.totalNewCustComparedYear === 0 || payload.data.totalNewCustComparedYear ? (Math.round(payload.data.totalNewCustComparedYear)) : '',
          },
        ],
        footerCategory: {
          popularityCategoryL: payload.data.popularityCategoryL || '',
          popularityCategoryM: payload.data.popularityCategoryM || '',
        },
      };
      const result = { ...payload, data: data || {} };

      state.statistics = asyncApiState.success(result);
    },
    getStatisticsFailure: (state, { payload }) => {
      state.statistics = asyncApiState.error(payload);
    },
    // 실적 지수
    getLineChart: (state, { payload }) => { state.lineChart = asyncApiState.request(); },
    getLineChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.lineChart = asyncApiState.success(result);
    },
    getLineChartFailure: (state, { payload }) => { state.lineChart = asyncApiState.error(payload); },
    // 단계별
    getColumnChart: (state, { payload }) => { state.columnChart = asyncApiState.request(); },
    getColumnChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // console.log('payload', payload);
      state.columnChart = asyncApiState.success(result);
    },
    getColumnChartFailure: (state, { payload }) => { state.columnChart = asyncApiState.error(payload); },
    // 유입경로별
    getDonutChart: (state, { payload }) => { state.donutChart = asyncApiState.request(); },
    getDonutChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // console.log('payload', payload);
      state.donutChart = asyncApiState.success(result);
    },
    getDonutChartFailure: (state, { payload }) => { state.donutChart = asyncApiState.error(payload); },
    // 카테고리(대)
    getPolarChart: (state, { payload }) => { state.polarChart = asyncApiState.request(); },
    getPolarChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // console.log('payload', payload);
      state.polarChart = asyncApiState.success(result);
    },
    getPolarChartFailure: (state, { payload }) => { state.polarChart = asyncApiState.error(payload); },
    // 성별/연령별
    getBarNegativeChart: (state, { payload }) => { state.barNegativeChart = asyncApiState.request(); },
    getBarNegativeChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // console.log('payload', payload);
      state.barNegativeChart = asyncApiState.success(result);
    },
    getBarNegativeChartFailure: (state, { payload }) => { state.barNegativeChart = asyncApiState.error(payload); },
    // 전체 활동고객수
    getLineAndColumnChart: (state, { payload }) => { state.lineAndColumnChart = asyncApiState.request(); },
    getLineAndColumnChartSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // console.log('payload', result);
      state.lineAndColumnChart = asyncApiState.success(result);
    },
    getLineAndColumnChartFailure: (state, { payload }) => { state.lineAndColumnChart = asyncApiState.error(payload); },

  },
});

export const {
  resetStore,
  updateState,
  getStatistics,
  getLineChart,
  getLineChartFailure,
  getColumnChart,
  getDonutChart,
  getPolarChart,
  getBarNegativeChart,
  getLineAndColumnChart,
} = actions;

export default reducer;
