import styled from 'styled-components';

import { COLORS } from '../../../../styles/Colors';

function SummaryPercentView({ data }) {
  if (data > 0) {
    return (
      <Percent color={COLORS.BLUE[500]}>
        {Math.abs(data)}
        % 증가
      </Percent>
    );
  }
  if (data < 0) {
    return (
      <Percent color={COLORS.RED[500]}>
        {Math.abs(data)}
        % 감소
      </Percent>
    );
  }
  return (
    <Percent color={COLORS.GRAY[500]}>
      {Math.abs(data)}
      %
    </Percent>
  );
}

const Percent = styled.div`
  display: inline-flex;
  height: 18px;
  line-height: 18px;
  font-weight: 700 !important;
  letter-spacing: -0.5px;
  align-items: center;
  color:${(props) => props.color}
`;

export default SummaryPercentView;
