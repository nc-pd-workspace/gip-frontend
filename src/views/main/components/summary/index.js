import styled from 'styled-components';

import Flex from '../../../../components/flex';
import Images from '../../../../Images';
import SummaryPercentView from './SummaryPercentView';

function Summary({ summary }) {
  return (
    <Container>
      <h1>
        이번 달 전체 활동고객수는
        <br />
        <span>
          {summary.totalCount && summary.totalCount.toLocaleString('ko-KR')}
          명
        </span>
        입니다.
      </h1>
      <Flex>
        <Info>
          <b>활동고객은</b>
          <p>&nbsp;브랜드에&nbsp;</p>
          <b>방문, 관심, 주문  등의 활동 이력</b>
          {summary.changeRateByMonth || summary.changeRateByYear
            ? <p>이&nbsp;있는 고객으로</p> : <p>이&nbsp;있는 고객 입니다.</p> }
        </Info>
      </Flex>
      <Flex>
        {summary.changeRateByMonth
        && (
          <Info>
            전월대비
            <span className="">
              <SummaryPercentView data={summary.changeRateByMonth} />
            </span>
            {(summary.changeRateByMonth && summary.changeRateByYear)
              ? (
                <>
                  ,&nbsp;
                </>
              ) : '하였습니다.'}
          </Info>
        )}
        {summary.changeRateByYear
        && (
          <Info>
            전년대비
            <span className="">
              <SummaryPercentView data={summary.changeRateByYear} />
            </span>
            하였습니다.
          </Info>
        ) }
      </Flex>
    </Container>
  );
}

const Container = styled.div`
  margin-bottom: 22px;
  background-image:url(${Images.mainDashboard});
  background-position: right 24px bottom 0;
  background-size: 108px 98px;
  background-repeat: no-repeat;
  h1 {
    font-size: 32px;
    font-weight: 700;
    line-height: 42px;
    margin-bottom: 10px;

    span {
      color: var(--color-blue-500);
    }
  }
`;

const Info = styled.div`
  display: flex;
  align-items: center;
  font-size: 14px;
  line-height: 21px;
  color: var(--color-gray-700);

  img {
    width: 16px;
    height: 16px;
  }

  span {
    font-weight: 700;
    margin-left: 2px;
    font-size: 14px;
    &.changeRateByMonth {
      color: var(--color-blue-500);
    }
    &.changeRateByYear {
      color: var(--color-red-500);
    }
  }
`;

export default Summary;
