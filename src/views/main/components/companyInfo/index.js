import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

import Images from '../../../../Images';

function CompanyInfo(props) {
  const mailTo = () => {
    // navigator.registerProtocolHandler('web+mailto', 'https://mail.google.com/mail/?extsrc=mailto&url=%s', 'Gmail');
    window.location.href = 'mailto:doyun.koo@gsretail.com';
  };

  return (
    <Container>
      <Coptyright>
        <EmailLink
          to="#"
          onClick={mailTo}
        >
          <img src={Images.iconMail} alt="이메일 문의" />
          이메일 문의
        </EmailLink>
        <PricacyLink to="/privacyPolicy" target="_privacyPolicy">
          개인정보처리방침
        </PricacyLink>
        <GsRetail>
          ⓒ 2022. GS Retail Co. all rights reserved.
        </GsRetail>
      </Coptyright>
    </Container>
  );
}

const LeftBullet = css`
  margin-left:10px;
  display:inline-flex;
  align-items: center;
  &:before {
    content:"";
    width:1px;
    height:9px;
    background-color:var(--color-gray-200);
    margin-right:10px;
  }
`;
const DefaultFontStyle = css`
  font-size:13px;
  line-height:20px;
`;

const Container = styled.div`
  margin-top: 30px;
`;
const Coptyright = styled.div`
  display: flex;
`;
const EmailLink = styled(Link)`
  ${DefaultFontStyle}
  color:var(--color-gray-600);
`;
const PricacyLink = styled(Link)`
  ${DefaultFontStyle}
  ${LeftBullet}
  color: var(--color-gray-900);
`;
const GsRetail = styled.div`
  ${DefaultFontStyle}
  flex: 1;
  color:var(--color-gray-400);
  text-align: right;
`;

export default CompanyInfo;
