import React from 'react';
import styled from 'styled-components';
import { List } from 'antd';

import StatisticsItem from './StatisticsItem';

function StatisticsList({ data }) {
  return (
    <Container>
      <List
        grid={{ gutter: 10, column: 4 }}
        dataSource={data}
        renderItem={({
          title, amount, increaseRate, decreaseRate,
        }) => (
          <List.Item>
            <StatisticsItem
              title={title}
              amount={amount}
              increaseRate={increaseRate}
              decreaseRate={decreaseRate}
            />
          </List.Item>
        )}
      />
    </Container>
  );
}

const Container = styled.div``;

export default StatisticsList;
