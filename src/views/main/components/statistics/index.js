import styled from 'styled-components';
import { List } from 'antd';

import { useSelector } from 'react-redux';

import { useEffect, useState } from 'react';

import moment from 'moment';

import StatisticsItem from './StatisticsItem';
import Paper from '../../../../components/paper';
import Footer from './Footer';
import LoadingComponent from '../../../../components/loading';

function Statistics({ data, footerCategory, isLoading }) {
  const { userInfo } = useSelector((state) => state.common);
  const [dashDate, setDashDate] = useState({
    startDate: moment().subtract(1, 'days').startOf('month').format('YYYY.MM.DD'),
    lastDate: moment().subtract(1, 'days').format('YYYY.MM.DD'),
  });

  function updatedays() {
    if (userInfo.lastUpdateDate) {
      const lastDate = userInfo.lastUpdateDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1. $2. $3.');
      const startDate = moment(lastDate).startOf('month').format('YYYY. MM. DD.');
      setDashDate({
        startDate,
        lastDate,
      });
    }
  }
  useEffect(() => {
    updatedays();
  }, [userInfo]);

  return (
    <Container>
      <ChartTitle>
        <h3>이번 달 데이터 요약</h3>
        <span>{`${dashDate.startDate} ~ ${dashDate.lastDate}`}</span>
      </ChartTitle>
      <Paper shadow={5}>
        <LoadingComponent isLoading={isLoading} />

        <Contents>
          <List
            grid={{ gutter: 10, column: 4 }}
            dataSource={data}
            renderItem={({
              title, amount, changeRateByMonth, changeRateByYear,
            }) => (
              <List.Item>
                <StatisticsItem
                  title={title}
                  amount={amount}
                  changeRateByMonth={changeRateByMonth}
                  changeRateByYear={changeRateByYear}
                />
              </List.Item>
            )}
          />
        </Contents>
        {footerCategory
        && <Footer footerCategory={footerCategory} />}
      </Paper>
    </Container>
  );
}

const Container = styled.div`
  .ant-list-item {
    margin: 0!important;
  }
  .ant-list {
    margin: 20px 0 2px;
  }
  .ant-row > div {
    flex: 1 1 0;
    min-width: fit-content !important;
    max-width: unset !important;
    width: 25%;
  }
  .ant-col {
    padding: 0 !important;
  }
`;

const Contents = styled.div`
  padding: 10px 0;
`;

const ChartTitle = styled.div`
  display: flex;
  align-items: flex-end;
  padding: 18px 0 16px;
  h3 {
    font-size: 20px;
    font-weight: 700;
    color: var(--color-gray-900);
  }
  span {
    display: block;
    padding-left: 6px;
    font-size: 12px;
    font-weight: 400;
    color: var(--color-gray-600);
  }
`;

export default Statistics;
