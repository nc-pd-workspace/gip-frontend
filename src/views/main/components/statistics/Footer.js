import styled from 'styled-components';

import { SvgNone } from '../../../../Images';
import { COLORS } from '../../../../styles/Colors';

function Footer({ footerCategory }) {
  return (
    <Container>
      <Title>전체 활동고객의 인기 카테고리</Title>
      <ul>
        <li>
          <div className="label">대카테고리</div>
          <div className="content">{footerCategory.popularityCategoryL ? footerCategory.popularityCategoryL : <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />}</div>
        </li>
        <li>
          <div className="label">중카테고리</div>
          <div className="content">{footerCategory.popularityCategoryM ? footerCategory.popularityCategoryM : <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />}</div>
        </li>
      </ul>

    </Container>
  );
}

const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 16px 0 21px;
  margin: 0 20px;
  border-top: 1px solid var(--color-gray-100);

  ul {
    display: flex;

    li {
      display: flex;
      align-items: center;

      & + li {
        margin-left: 40px;
      }

      .label {
        display: flex;
        align-items: center;
        height: 20px;
        padding: 0 6px;
        font-size: 12px;
        font-weight: 400;
        color: var(--color-gray-700);
        background: var(--background-default);
        margin-right: 6px;
        border-radius: 4px;
      }
      
      .content {
        font-size: 13px;
        line-height: 20px;
        color: var(--color-gray-900);
      }
    }
  }
`;

const Title = styled.div`
  font-size: 12px;
  line-height: 18px;
  letter-spacing: -0.5px;
  color: var(--color-gray-500);
  font-weight: 700;
  margin-right: 28px;
`;

export default Footer;
