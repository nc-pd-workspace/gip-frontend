import React from 'react';
import styled from 'styled-components';

import PercentView from '../../../../components/percentView';

function StatisticsItem({
  title, amount, changeRateByMonth, changeRateByYear,
}) {
  return (
    <Container>
      <Title>{title}</Title>
      <Total>
        <div className="count">{amount.value.toLocaleString('ko-KR')}</div>
        <div className="unit">{amount.unit}</div>
      </Total>
      <ChangeRate>
        <div className="item">
          <div className="text">
            전월
          </div>
          <PercentView data={changeRateByMonth} />
        </div>
        <div className="item">
          <div className="text">
            전년
          </div>
          <PercentView data={changeRateByYear} />
        </div>
      </ChangeRate>
    </Container>
  );
}

const Container = styled.div`
  padding-left: 20px;
  padding-right: 10px;
  position: relative;
  &::before {
    content: '';
    position:absolute;
    top: 0;
    bottom: 0;
    left: 0;
    height: 50px;
    width: 1px;
    background: var(--color-gray-200);
    margin: auto;
    .ant-row > div:first-child & {
      display:none;
    }
  }
  
`;

const Title = styled.div`
  color: var(--color-gray-500);
  font-size: 12px;
  line-height: 18px;
  font-weight: 700;
  margin-bottom: 6px;
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  color: var(--color-gray-900);
  .count {
    font-size: 24px;
    font-weight: 700;
    line-height: 36px;
    letter-spacing: -0.5px;
  }
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 18px;
    padding-bottom: 7px;
    margin-left: 2px;
  }
`;

const ChangeRate = styled.div`
  display: flex;
  align-items: center;
  margin-top: 2px;
  .item {
    display: flex;
    align-items: center;
    font-size: 12px;
    font-weight: 400;
    color: var(--color-gray-900);
    .unit {
      font-size: 12px;
    }
  }
  .item + .item {
    margin-left: 6px;
  }
`;

export default StatisticsItem;
