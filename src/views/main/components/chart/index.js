import styled from 'styled-components';
import { List } from 'antd';

import React, { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import moment from 'moment';

import ChartCard from '../../../../components/chartCard';
import TotalOrderAmountChartContainer from '../../containers/chart/TotalOrderAmountChartContainer';
import TotalOrderQuantityChartContainer from '../../containers/chart/TotalOrderQuantityChartContainer';
import TotalCustomerCountContainer from '../../containers/chart/TotalCustomerCountContainer';
import GenderAndAgeCustomerChartContainer from '../../containers/chart/GenderAndAgeCustomerChartContainer';
import NewAndActivityCustomerChartContainer from '../../containers/chart/NewAndActivityCustomerChartContainer';
import InflowPathChartContainer from '../../containers/chart/InflowPathChartContainer';
import CategoryActivityCustomerChartContainer from '../../containers/chart/CategoryActivityCustomerChartContainer';
import StepCustomerChartContainer from '../../containers/chart/StepCustomerChartContainer';

import {
  getLineChart,
  getColumnChart,
  getDonutChart,
  getPolarChart,
  getBarNegativeChart,
  getLineAndColumnChart,
} from '../../redux/slice';

function Chart() {
  const { lineChart, columnChart, donutChart, polarChart, barNegativeChart, lineAndColumnChart } = useSelector((state) => ({
    lineChart: state.main.lineChart,
    columnChart: state.main.columnChart,
    donutChart: state.main.donutChart,
    polarChart: state.main.polarChart,
    barNegativeChart: state.main.barNegativeChart,
    lineAndColumnChart: state.main.lineAndColumnChart,
  }));
  const { selectPtnIdx } = useSelector((state) => state.common);

  const dispatch = useDispatch();

  useEffect(() => {
    if (selectPtnIdx) {
      const params = {
        ptnIdx: selectPtnIdx,
      };
      dispatch(getLineChart({ params }));
      dispatch(getColumnChart({ params }));
      dispatch(getDonutChart({ params }));
      dispatch(getPolarChart({ params }));
      dispatch(getBarNegativeChart({ params }));
      dispatch(getLineAndColumnChart({ params }));
    }
  }, [selectPtnIdx]);

  const chartData = [
    {
      title: '실적 지수',
      description: '최근 1개월 기준',
      component: [
        {
          name: '*주문금액',
          component: <TotalOrderAmountChartContainer lineChart={lineChart?.data?.ordAmt} isLoading={lineChart?.status === 'pending'} status={lineChart?.status} />,
          caution: '* 표시는 GIP만의 방식으로 계산된 지수화 데이터입니다.',
          emptyData: !!(lineChart?.data?.ordAmt?.series && lineChart?.data?.ordQty?.series && lineChart?.data?.ordCustCnt?.series),
        },
        {
          name: '*주문수',
          component: <TotalOrderQuantityChartContainer lineChart={lineChart?.data?.ordQty} />,
          caution: '* 표시는 GIP만의 방식으로 계산된 지수화 데이터입니다.',
        },
        {
          name: '주문고객수',
          component: <TotalCustomerCountContainer lineChart={lineChart?.data?.ordCustCnt} />,
        },
      ],
    },
    {
      title: '성별/연령별 전체 활동고객수',
      description: '최근 1개월 기준',
      component: <GenderAndAgeCustomerChartContainer barNegativeChart={barNegativeChart?.data} isLoading={barNegativeChart?.status === 'pending'} />,
    },
    {
      title: '전체 활동고객수',
      description: '최근 1개월 기준',
      component: <NewAndActivityCustomerChartContainer lineAndColumnChart={lineAndColumnChart?.data} isLoading={lineAndColumnChart?.status === 'pending'} />,
    },
    {
      title: '유입경로별 전체 활동고객수',
      description: '최근 1개월 기준',
      component: <InflowPathChartContainer donutChart={donutChart?.data} isLoading={donutChart?.status === 'pending'} />,
    },
    {
      title: '카테고리(대) 전체 활동고객수',
      description: '최근 1개월 기준',
      component: <CategoryActivityCustomerChartContainer polarChart={polarChart?.data} isLoading={polarChart?.status === 'pending'} />,
    },
    {
      title: '단계별 전체 활동고객수',
      openPageTitle: 'stepCustomer',
      description: '최근 1개월 기준',
      component: <StepCustomerChartContainer columnChart={columnChart?.data} isLoading={columnChart?.status === 'pending'} />,
    },
  ];

  const { userInfo } = useSelector((state) => state.common);
  const [dashDate, setDashDate] = useState({
    lastDate: moment().subtract(1, 'days').format('YYYY.MM.DD'),
    recentStartDate: moment().subtract(1, 'days').subtract(1, 'month').format('YYYY.MM.DD'),
  });

  function updatedays() {
    if (userInfo.lastUpdateDate) {
      const lastDate = userInfo.lastUpdateDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1. $2. $3.');
      const recentDate = moment(lastDate).subtract(1, 'month').format('YYYY. MM. DD.');
      setDashDate({
        lastDate,
        recentStartDate: recentDate,
      });
    }
  }
  useEffect(() => {
    updatedays();
  }, [userInfo]);
  return (
    <Container>
      <ChartTitle>
        <h3>최근 데이터 요약</h3>
        <span>{`${dashDate.recentStartDate} ~ ${dashDate.lastDate}`}</span>
      </ChartTitle>
      <List
        dataSource={chartData}
        renderItem={({
          title, description, caution, component,
        }) => (
          <List.Item>
            <ChartCard
              title={title}
              description={description}
              caution={caution}
              height={404}
            >
              {component}
            </ChartCard>
          </List.Item>
        )}
      />
    </Container>
  );
}

const Container = styled.div`
  margin-top: 32px;
  .ant-list-split {
    .ant-list-items {
      display: flex;
      flex-wrap: wrap;
      margin: 0 -5px
    }
    .ant-list-item {
      padding: 5px;
      border-bottom: none;
      .Paper {
        width: 100%;
      }
    }
  }
  
  @media ( max-width: 1439px ) {
    .ant-list-item {
      flex: 0 0 50% !important;
      width: 50%;
    }
  }
  @media ( min-width: 1440px ) and ( max-width: 1919px ) {
    .ant-list-item {
      flex: 0 0 33.3% !important;
      width: 33%;
    }
  }
  @media ( min-width: 1920px ) {
    .ant-list-item {
      flex: 0 0 25% !important;
      width: 25%;
    }
  }
`;

const ChartTitle = styled.div`
  display: flex;
  align-items: flex-end;
  padding: 18px 0 16px;
  h3 {
    font-size: 20px;
    font-weight: 700;
    color: var(--color-gray-900);
  }
  span {
    display: block;
    padding-left: 6px;
    font-size: 12px;
    font-weight: 400;
    color: var(--color-gray-600);
  }
`;

export default Chart;
