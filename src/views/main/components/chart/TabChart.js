import styled from 'styled-components';

import EmptyGraph from '../../../../components/emptyGraph';

import TabList from '../../../../components/tabList';

function TabChart({ data }) {
  const Error = data.some((arr) => arr.component.props.status === 'error');
  const handleChange = (value) => {
  };
  return (
    <Container>
      {data[0].emptyData
        ? (
          <TabList
            data={data}
            fullHeight
            defaultActiveKey="1"
            onChange={handleChange}
          >
            {
              Error ? (
                <Error />
              ) : ({ name, component, caution }) => (
                <>
                  {component}
                  {
                    caution && (
                      <Footer>
                        <p>{caution}</p>
                      </Footer>
                    )
                  }
                </>
              )
            }
          </TabList>
        ) : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
  .ant-tabs-top>.ant-tabs-nav:before {
    border-bottom: none;
  }
  .ant-tabs-nav-list {
    border-radius: 4px;
    background-color:#F7F8FA;
  }
  .ant-tabs-nav-list .ant-tabs-tab .ant-tabs-tab-btn {
    border-radius: 0;
  }
  .ant-tabs-nav-list .ant-tabs-tab:first-child .ant-tabs-tab-btn {
    border-radius: 4px 0 0 4px;
  }
  .ant-tabs-nav-list .ant-tabs-tab:nth-child(3) .ant-tabs-tab-btn {
    border-radius: 0 4px 4px 0;
  }
  .ant-tabs-tab-active .ant-tabs-tab-btn {
    border-radius: 4px !important;
  }
`;
const Footer = styled.div`
  margin-top: -6px;
  p {
    color: #B1B5BA;
    font-size: 12px;
    font-weight: 400;
    text-align: right;
  }
`;
export default TabChart;
