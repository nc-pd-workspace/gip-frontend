import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

const chartMockData = {
  data: [
    {
      name: 'GS SHOP',
      data: [28, 29, 33, 36, 32, 32, 33],
      color: '#77B6EA',
    },
    {
      name: '아모레퍼시픽',
      data: [12, 11, 14, 18, 17, 13, 13],
      color: '#545454',
    },
  ],
  xaxis: {
    type: 'category', // category, datetime, numeric
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
  },
  yaxis: {
    min: 5,
    max: 40,
    tickAmount: 10,
  },
};

const chartData = {
  series: chartMockData.data.map(({ name, data }) => ({
    name, data,
  })),
  options: {
    chart: {
      height: 350,
      type: 'line',
      dropShadow: {
        enabled: true,
        color: '#000',
        top: 18,
        left: 7,
        blur: 10,
        opacity: 0.2,
      },
      toolbar: {
        show: false,
      },
    },
    colors: chartMockData.data.map(({ color }) => color),
    dataLabels: {
      enabled: true,
    },
    stroke: {
      curve: 'smooth',
    },
    grid: {
      borderColor: '#e7e7e7',
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5,
      },
    },
    markers: {
      size: 1,
    },
    xaxis: {
      categories: chartMockData.xaxis.categories,
      axisTicks: {
        show: false,
      },
      labels: {
        show: false,
      },
    },
    yaxis: {
      min: chartMockData.yaxis.min,
      max: chartMockData.yaxis.max,
      tickAmount: chartMockData.yaxis.tickAmount,
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      floating: true,
      offsetY: -25,
      offsetX: -5,
    },
  },
};

function MainLineChart() {
  return (
    <Container className="MainLineChart">
      <ReactApexChart options={chartData.options} series={chartData.series} type="line" height="100%" />
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default MainLineChart;
