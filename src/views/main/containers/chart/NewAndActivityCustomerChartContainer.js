import { useEffect, useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import ReactApexChart from 'react-apexcharts';

import { CHART_COLORS } from '../../../../styles/chartColors';
import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import { legendDefault } from '../../../../utils/chartOptions';

function NewAndActivityCustomerChartContainer({ lineAndColumnChart, isLoading }) {
  const [chartData, setChartData] = useState('');
  useEffect(() => {
    if (lineAndColumnChart?.series) {
      const lineAndColumnChartData = {
        series: lineAndColumnChart.series.map(({ name, type, data }) => ({ name, type, data })),
        options: {
          chart: {
            height: 350,
            type: 'line',
            toolbar: {
              show: false,
            },
            zoom: {
              enabled: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.newAndActivityChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
              },
            },
          },
          stroke: {
            curve: ['straight', 'straight'],
            width: [0, 3],
          },
          markers: {
            size: 3,
            colors: [CHART_COLORS.CUSTOMER_ACTIVITY, CHART_COLORS.CUSTOMER_NEW],
            strokeWidth: 1,
          },
          colors: [CHART_COLORS.CUSTOMER_ACTIVITY, CHART_COLORS.CUSTOMER_NEW],
          dataLabels: {
            enabled: false,
          },
          labels: lineAndColumnChart.labels,
          xaxis: {
            type: 'string',
            axisTicks: false,
            labels: {
              show: false,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            tooltip: {
              enabled: false,
            },
          },
          yaxis: {
            logarithmic: false,
            tickAmount: 5,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tooltip: {
              enabled: false,
              offsetX: 0,
            },
          },
          tooltip: {
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          legend: {
            ...legendDefault,
          },
        },
      };
      setChartData(lineAndColumnChartData);
    } else {
      setChartData('');
    }
  }, [lineAndColumnChart]);

  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {chartData
        ? <ReactApexChart options={chartData.options} series={chartData.series} type="line" height="100%" className="newAndActivityChart" /> : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default NewAndActivityCustomerChartContainer;
