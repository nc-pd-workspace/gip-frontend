import { useEffect, useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import ReactApexChart from 'react-apexcharts';
// import ApexCharts from 'apexcharts';

import { CHART_COLORS } from '../../../../styles/chartColors';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';

// const test = {
//   data: [
//     {
//       name: 'GS SHOP',
//       data: [28, 29, 33, 36, 32, 32, 33],
//       color: '#77B6EA',
//     },
//     {
//       name: '아모레퍼시픽',
//       data: [12, 11, 14, 18, 17, 13, 13],
//       color: '#545454',
//     },
//   ],
//   xaxis: {
//     type: 'category', // category, datetime, numeric
//     categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
//   },
//   yaxis: {
//     min: 5,
//     max: 40,
//     tickAmount: 10,
//   },
// };

function TotalCustomerCountContainer({ lineChart }) {
  const [chartData, setChartData] = useState('');
  // console.log(lineChart.series[0].name);
  useEffect(() => {
    if (lineChart?.series) {
      const lineChartData = {
        series: lineChart.series.map(({ name, data }) => ({
          name, data,
        })),
        options: {
          chart: {
            id: 'TotalCustomerChart',
            height: 350,
            type: 'line',
            toolbar: {
              show: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.totalCustomerCountChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
                // ApexCharts.exec('TotalCustomerChart', 'toggleSeries', lineChart.series[1].name);
              },
            },
          },
          colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: 'straight',
            width: 3,
          },
          grid: {
            borderColor: '#E3E4E7',
            row: {
              colors: ['transparent'],
            },
          },
          markers: {
            size: 3,
            colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
            strokeWidth: 1,
          },
          xaxis: {
            type: 'category',
            categories: lineChart.xcategories,
            tickPlacement: 'between',
            axisTicks: {
              show: false,
            },
            tooltip: {
              enabled: false,
            },
            labels: {
              show: false,
              hideOverlappingLabels: false,
              minHeight: 32,
              maxHeight: 32,
              style: {
                colors: 'var(--color-gray-400)',
                fontSize: '11px',
                fontWeight: 400,
                cssClass: 'apexcharts-xaxis-label',
              },
              offsetX: 2,
              offsetY: 0,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
          tooltip: {
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
        },
      };
      setChartData(lineChartData);
    } else {
      setChartData('');
    }
  }, [lineChart]);

  return (
    <Container>
      {chartData
      && <ReactApexChart options={chartData.options} series={chartData.series} type="line" height="100%" className="totalCustomerCountChart" /> }
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
  .apexcharts-legend {
    justify-content:flex-end!important;
    .apexcharts-legend-series {
      display: flex;
      align-items: center;
      margin: 0 0 0 15px!important;
      .apexcharts-legend-marker {
        width: 10px!important;
        height: 2px!important;
        border-radius: 0!important;
      }
    }
  }
`;

export default TotalCustomerCountContainer;
