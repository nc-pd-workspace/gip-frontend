import { useEffect, useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import ReactApexChart from 'react-apexcharts';

import { CHART_COLORS } from '../../../../styles/chartColors';
import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import Error from '../../../../components/error';
import { getChartValue, getChartYAxisMax } from '../../../../utils/utils';

function TotalOrderAmountChartContainer({ lineChart, isLoading, status }) {
  const [chartData, setChartData] = useState('');

  useEffect(() => {
    if (lineChart?.series) {
      const lineChartData = {
        series: lineChart.series.map(({ name, data }) => ({
          name, data,
        })),
        options: {
          chart: {
            type: 'line',
            toolbar: {
              show: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.totalOrderChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
              },
            },
          },
          colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: 'straight',
            width: 3,
          },
          grid: {
            borderColor: '#E3E4E7',
            row: {
              colors: ['transparent'],
            },
          },
          markers: {
            size: 3,
            colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
            strokeWidth: 1,
          },
          xaxis: {
            type: 'category',
            categories: lineChart.xcategories,
            tickPlacement: 'between',
            axisTicks: {
              show: false,
            },
            tooltip: {
              enabled: false,
            },
            labels: {
              show: false,
              hideOverlappingLabels: false,
              minHeight: 32,
              maxHeight: 32,
              style: {
                colors: 'var(--color-gray-400)',
                fontSize: '11px',
                fontWeight: 400,
                cssClass: 'apexcharts-xaxis-label',
              },
              offsetX: 2,
              offsetY: 0,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValue(val);
              },
            },
            tickAmount: 5,
          },
          tooltip: {
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return (val).toLocaleString();
              },
            },
          },
        },
      };
      setChartData(lineChartData);
    } else {
      setChartData('');
    }
  }, [lineChart]);

  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {
        status === 'error' ? (
          <Error />
        ) : (
          <>
            {chartData
              ? <ReactApexChart options={chartData.options} series={chartData.series} type="line" height="270" className="totalOrderChart" />
              : <EmptyGraph />}
          </>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  min-height: 270px;
  .apexcharts-legend {
    justify-content:flex-end!important;
    .apexcharts-legend-series {
      display: flex;
      align-items: center;
      margin: 0 0 0 15px!important;
      .apexcharts-legend-marker {
        width: 10px!important;
        height: 2px!important;
        border-radius: 0!important;
      }
    }
  }
`;

export default TotalOrderAmountChartContainer;
