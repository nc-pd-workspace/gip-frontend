import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

import { useEffect, useState } from 'react';

import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import { CHART_COLORS } from '../../../../styles/chartColors';
import { legendDefault } from '../../../../utils/chartOptions';
// import { getChartYAxisMax } from '../../../../utils/utils';

function GenderAndAgeCustomerChartContainer({ barNegativeChart, isLoading }) {
  // console.log('barNegativeChart', barNegativeChart);
  const [chartData, setChartData] = useState('');

  useEffect(() => {
    if (barNegativeChart?.series) {
      const maxMap = barNegativeChart.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxCeil = Math.ceil(maxValue / 10) * 10;
      const maxData = maxCeil + 10;

      const barNegativeChartData = {
        series: barNegativeChart.series.map(({ name, data }) => ({ name, data })),
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
            height: 350,
            stacked: true,
          },
          plotOptions: {
            bar: {
              horizontal: true,
            },
          },
          dataLabels: {
            enabled: true,
            formatter(val) {
              return Math.floor(val);
            },
          },
          stroke: {
            width: 0,
            colors: ['#fff'],
          },
          colors: [CHART_COLORS.GENDER_MALE, CHART_COLORS.GENDER_FEMALE, CHART_COLORS.GENDER_UNKNOWN],
          xaxis: {
            tickAmount: 5,
            min: 0,
            max: maxData,
            categories: barNegativeChart.categories,
            labels: {
              formatter(val, index) {
                if (index === 0) return '0';
                return `${val}%`;
              },
            },
          },
          grid: {
            show: false,
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            y: {
              formatter(val) {
                return `${val}%`;
              },
            },
          },
          yaxis: {
            title: {
              text: undefined,
            },
          },
          fill: {
            opacity: 1,
          },
          legend: {
            ...legendDefault,
          },
        },

      };
      setChartData(barNegativeChartData);
    } else {
      setChartData('');
    }
  }, [barNegativeChart]);
  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {chartData
        ? <ReactApexChart options={chartData.options} series={chartData.series} type="bar" height="100%" /> : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default GenderAndAgeCustomerChartContainer;
