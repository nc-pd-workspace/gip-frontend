import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

import { useEffect, useState } from 'react';

import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import { CHART_VARIATION } from '../../../../styles/chartColors';
import { getChartValuePeople } from '../../../../utils/utils';
import { legendDefault } from '../../../../utils/chartOptions';

function CategoryActivityCustomerChartContainer({ polarChart, isLoading }) {
  const [chartData, setChartData] = useState('');

  useEffect(() => {
    if (polarChart?.series) {
      const maxMap = polarChart.series;
      const maxValue = Math.max(...maxMap.flat());
      const maxTickAmount = maxValue * 1.2;

      const donutChartData = {
        series: polarChart.series,
        options: {
          chart: {
            type: 'polarArea',
            height: '320px',
          },
          colors: CHART_VARIATION,
          stroke: {
            width: 0,
            colors: ['#fff'],
          },
          fill: {
            opacity: 0.8,
          },
          legend: {
            ...legendDefault,
            itemMargin: {
              horizontal: 8,
              vertical: 4,
            },
          },
          labels: polarChart.labels,
          yaxis: {
            showAlways: true,
            show: true,
            position: 'front',
            floating: true,
            min: 0,
            max: maxTickAmount,
            type: 'numeric',
            tickAmount: 5,
            crosshairs: {
              show: false,
              position: 'front',
              stroke: {
                color: '#eee',
                width: 1,
                dashArray: 0,
              },
            },
            labels: {
              style: {
                colors: 'var(--color-gray-700)',
                fontSize: '10px',
                fontWeight: 400,
                cssClass: 'apexcharts-yaxis-label',
              },
              formatter(val) {
                return getChartValuePeople(val);
              },
            },
          },
          tooltip: {
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          responsive: [{
            breakpoint: 1200,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }, {
            breakpoint: 1440,
            options: {
              legend: {
                position: 'right',
              },
            },
          }, {
            breakpoint: 1700,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }, {
            breakpoint: 1900,
            options: {
              legend: {
                position: 'right',
              },
            },
          }, {
            breakpoint: 2160,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }],
        },
      };
      setChartData(donutChartData);
    } else {
      setChartData('');
    }
  }, [polarChart]);
  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {chartData
        ? <ReactApexChart options={chartData.options} series={chartData.series} type="polarArea" height="320" /> : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default CategoryActivityCustomerChartContainer;
