import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';

import { useEffect, useState } from 'react';

import { CHART_COLORS } from '../../../../styles/chartColors';
import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
// const test = {

//   series: [{
//     name: '방문고객',
//     data: [440, 550, 570, 560, 610, 580, 630, 600, 660],
//   }, {
//     name: '관심고객',
//     data: [660, 350, 510, 580, 670, 450, 510, 600, 740],
//   }, {
//     name: '구매고객',
//     data: [350, 410, 360, 260, 450, 480, 520, 530, 410],
//   },
//   {
//     name: '충성고객',
//     data: [350, 410, 360, 260, 450, 480, 520, 530, 410],
//   }],
//   options: {
//     chart: {
//       type: 'bar',
//       toolbar: {
//         show: false,
//       },
//     },
//     colors: [
//       CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.CUSTOMER_INTERESTED,
//       CHART_COLORS.CUSTOMER_PURCHASE, CHART_COLORS.CUSTOMER_LOYALTY,
//     ],
//     plotOptions: {
//       bar: {
//         horizontal: false,
//         columnWidth: '55%',
//         endingShape: 'rounded',
//       },
//     },
//     dataLabels: {
//       enabled: false,
//     },
//     stroke: {
//       show: true,
//       width: 1,
//       colors: ['transparent'],
//     },
//     xaxis: {
//       axisTicks: false,
//       labels: {
//         show: false,
//       },
//       categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
//     },
//     yaxis: {
//       min: 0,
//       max: 1200,
//     },
//     fill: {
//       opacity: 1,
//     },
//     tooltip: {
//       y: {
//         formatter(val) {
//           return `${val} 명`;
//         },
//       },
//     },
//   },
// };

function StepCustomerChartContainer({ columnChart, isLoading }) {
  // console.log('columnChart', columnChart);
  const [chartData, setChartData] = useState('');

  useEffect(() => {
    if (columnChart?.series) {
      const columnChartData = {
        series: columnChart.series.map(({ name, data }) => ({
          name, data,
        })),
        options: {
          chart: {
            type: 'bar',
            toolbar: {
              show: false,
            },
          },
          colors: [
            CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.CUSTOMER_INTERESTED,
            CHART_COLORS.CUSTOMER_PURCHASE, CHART_COLORS.CUSTOMER_LOYALTY,
          ],
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '100%',
              endingShape: 'rounded',
            },
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            show: true,
            width: 1,
            colors: ['transparent'],
          },
          xaxis: {
            categories: columnChart.xcategories,
            axisTicks: false,
            labels: {
              show: false,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
          },
          fill: {
            opacity: 1,
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
        },
      };
      setChartData(columnChartData);
    } else {
      setChartData('');
    }
  }, [columnChart]);

  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {chartData
        ? <ReactApexChart options={chartData.options} series={chartData.series} type="bar" height="100%" /> : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default StepCustomerChartContainer;
