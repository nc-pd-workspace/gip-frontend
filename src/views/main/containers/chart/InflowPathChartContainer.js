import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

import { useEffect, useState } from 'react';

import EmptyGraph from '../../../../components/emptyGraph';
import LoadingComponent from '../../../../components/loading';
import { CHART_VARIATION } from '../../../../styles/chartColors';
import { legendDefault } from '../../../../utils/chartOptions';

function InflowPathChartContainer({ donutChart, isLoading }) {
  const [chartData, setChartData] = useState('');

  useEffect(() => {
    if (donutChart?.series) {
      const donutChartData = {
        series: donutChart.series,
        options: {
          chart: {
            type: 'donut',
            height: '320px',
          },
          labels: donutChart.labels,
          plotOptions: {
            pie: {
              donut: {
                size: '65%',
              },
            },
          },
          stroke: {
            show: false,
            width: 0,
          },
          dataLabels: {
            enabled: true,
            dropShadow: {
              enabled: false,
            },
            formatter(val) {
              return `${Math.floor(val)}%`;
            },
          },
          legend: {
            ...legendDefault,
            itemMargin: {
              horizontal: 8,
              vertical: 4,
            },
          },
          colors: CHART_VARIATION,
          tooltip: {
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          responsive: [{
            breakpoint: 1200,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }, {
            breakpoint: 1440,
            options: {
              legend: {
                position: 'right',
              },
            },
          }, {
            breakpoint: 1700,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }, {
            breakpoint: 1900,
            options: {
              legend: {
                position: 'right',
              },
            },
          }, {
            breakpoint: 2160,
            options: {
              legend: {
                position: 'bottom',
              },
            },
          }],
        },
      };
      setChartData(donutChartData);
    } else {
      setChartData('');
    }
  }, [donutChart]);
  return (
    <Container>
      <LoadingComponent isLoading={isLoading} />
      {chartData
        ? <ReactApexChart options={chartData.options} series={chartData.series} type="donut" height="320" /> : <EmptyGraph />}
    </Container>
  );
}

const Container = styled.div`
  height: 100%;
`;

export default InflowPathChartContainer;
