import styled from 'styled-components';

import { useSelector, useDispatch } from 'react-redux';

import { useEffect } from 'react';

import Statistics from '../components/statistics';
import Summary from '../components/summary';
import Chart from '../components/chart';
import CompanyInfo from '../components/companyInfo';
import { getStatistics, resetStore } from '../redux/slice';

function MainContainer() {
  const { statistics } = useSelector((state) => ({
    statistics: state.main.statistics,
  }));
  const { selectPtnIdx } = useSelector((state) => state.common);

  const dispatch = useDispatch();

  useEffect(() => {
    if (selectPtnIdx) {
      const params = {
        ptnIdx: selectPtnIdx,
      };
      dispatch(getStatistics({ params }));
    }
    return () => {
      dispatch(resetStore());
    };
  }, [selectPtnIdx]);

  return (
    <>
      <Container className="mainContainer">
        {
          (statistics.status === 'success') && (
            <>
              <Summary summary={statistics.data.summary} />
              <Statistics data={statistics.data ? statistics.data.list : []} isLoading={statistics.status === 'pending'} footerCategory={statistics.data ? statistics.data.footerCategory : ''} />
            </>
          )
        }
        <Chart />
        <CompanyInfo />
      </Container>
    </>
  );
}

const Container = styled.div`
  z-index: 1;
  padding: 30px 30px 74px 30px;
  &.mainContainer {
    min-width: var(--main-minWidth) !important;
    max-width: auto !important;
    position: relative;
    &:before{
      content:"";
      position: absolute;
      top: -60px;
      left: 0;
      right: 0;
      height: 660px;
      background-image: linear-gradient(180deg, #EAF3FD 0%, #F7F8FA 100%);
      z-index: -1;
    }
  }
`;

export default MainContainer;
