export const brandOptions = [
  { label: '나이키', value: 'nike' },
  { label: '아디다스', value: 'adidas' },
  { label: '언더아머', value: 'underarmor' },
];

export const defaultCategoryOptions = [
  [
    { label: '뷰티', value: 'beauty' },
    { label: '생활/건강', value: 'life' },
    { label: '신발', value: 'shoes' },
  ],
  [
    { label: '아모레퍼시픽', value: 'beauty01', parent: 'beauty' },
    { label: '더페이스샵', value: 'beauty02', parent: 'beauty' },
    { label: '참존', value: 'beauty03', parent: 'beauty' },
    { label: '리빙', value: 'life01', parent: 'life' },
    { label: '가구', value: 'life02', parent: 'life' },
    { label: '수납', value: 'life03', parent: 'life' },
    { label: '나이키', value: 'shoes01', parent: 'shoes' },
    { label: '아디다스', value: 'shoes02', parent: 'shoes' },
    { label: '컨버스', value: 'shoes03', parent: 'shoes' },
  ],
  [
    { label: '화장품', value: 'beauty0101', parent: 'beauty01' },
    { label: '미백', value: 'beauty0201', parent: 'beauty02' },
    { label: '보습', value: 'beauty0301', parent: 'beauty03' },
    { label: '냄비', value: 'life0101', parent: 'life01' },
    { label: '장롱', value: 'life0201', parent: 'life02' },
    { label: '책상', value: 'life0202', parent: 'life02' },
    { label: '서랍', value: 'life0301', parent: 'life03' },
    { label: '조던', value: 'shoes0101', parent: 'shoes01' },
    { label: '운동화', value: 'shoes0102', parent: 'shoes01' },
    { label: 'Special Edition', value: 'shoes0103', parent: 'shoes01' },

  ],
];

export const columns = [
  {
    title: '',
    dataIndex: 'brandNm',
    key: 'brandNm',
    render: (text, row) => <span className={`brand ${row.key}`}>{text}</span>,
  },
  {
    title: '*주문금액',
    dataIndex: 'totalOrderAmount',
    key: 'totalOrderAmount',
    type: 'number',
  },
  {
    title: '전년대비',
    dataIndex: 'totalOrderAmountComparedYear',
    key: 'totalOrderAmountComparedYear',
    type: 'percent',
  },
  {
    title: '*주문수',
    dataIndex: 'totalOrderCount',
    key: 'totalOrderCount',
    type: 'number',
  },
  {
    title: '전년대비',
    dataIndex: 'totalOrderCountComparedYear',
    key: 'totalOrderCountComparedYear',
    type: 'percent',
  },
  {
    title: '주문고객수',
    dataIndex: 'totalOrderCust',
    key: 'totalOrderCust',
    type: 'number',
  },
  {
    title: '전년대비',
    dataIndex: 'totalOrderCustComparedYear',
    key: 'totalOrderCustComparedYear',
    type: 'percent',
  },
];
