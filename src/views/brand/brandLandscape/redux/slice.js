import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  brandLandScapeList: asyncApiState.initial([]),
  brandMainChart: asyncApiState.initial({}),
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'brand/brandLandscape',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getBrandLandscapeList: (state, { payload }) => {
      state.brandLandScapeList = asyncApiState.request([]);
    },
    getBrandLandscapeListSuccess: (state, { payload }) => {
      const result = payload.data;
      let arr = [];
      if (payload.data.totalAnalyze !== null && payload.data.brandAnalyze !== null) {
        const totalData = {
          key: 'total',
          brandNm: 'GS SHOP 브랜드 전체',
          brandCode: 'all',
          ...payload.data.totalAnalyze,
        };
        arr = [totalData, ...payload.data.brandAnalyze.map((v) => ({ key: v.brandCode, ...v }))];
      }

      state.brandLandScapeList = asyncApiState.success({ data: arr });
    },
    getBrandLandscapeListFailure: (state, { payload }) => {
      state.brandLandScapeList = asyncApiState.error([]);
    },
    getBrandMainChart: (state, { payload }) => {
      state.brandMainChart = asyncApiState.request({});
    },
    getBrandMainChartSuccess: (state, { payload }) => {
      const result = payload;

      state.brandMainChart = asyncApiState.success(result);
    },
    getBrandMainChartFailure: (state, { payload }) => {
      state.brandMainChart = asyncApiState.error({});
    },
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], [], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], [], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));

      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },
  },
});

export const { updateState, getBrandLandscapeList, getBrandMainChart, getCategoryOptions, getBrandOptions } = actions;

export default reducer;
