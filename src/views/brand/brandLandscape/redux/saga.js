import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

import { createPromiseSaga } from '../../../../redux/lib';

import { getBrandLandscapeList, getBrandMainChart, getCategoryOptions, getBrandOptions } from './slice';
import API from '../../../../api';

const brandLandscapeListSaga = createPromiseSaga(getBrandLandscapeList, API.BrandLandscape.brandLandscapeList);
const brandMainChartSaga = createPromiseSaga(getBrandMainChart, API.BrandLandscape.brandMainChart);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

/* dispatch type 구독 설정, 종류에 따라 watch함수 분기해도 좋음 */
function* watchCommon() {
  yield takeLatest(getBrandLandscapeList, brandLandscapeListSaga);
  yield takeLatest(getBrandMainChart, brandMainChartSaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
