import { useEffect, useRef, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';
import isNil from 'lodash-es/isNil';

import { CHART_COLORS } from '../../../../styles/chartColors';
import { getChartValuePeople, getChartValue, getChartLineYAxisMinMax, getChartYAxisMax } from '../../../../utils/utils';

import ResultChartBox from './ResultChartBox';
import { legendDefault } from '../../../../utils/chartOptions';

function FirstResultChart({ chartType, data }) {
  const getChartYAxisData = (d) => {
    const max = Math.max(...d);
    const min = Math.min(...d);

    return getChartLineYAxisMinMax(min, max);
  };
  const chartRef = useRef(null);
  const [options, setOptions] = useState({
    chart: {
      zoom: {
        enabled: false,
      },
      height: 420,
      type: 'line',
      stacked: false,
      toolbar: {
        show: false,
      },
    },
    colors: [CHART_COLORS.COMPARE_GS_SHOP_80, CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND_80, CHART_COLORS.BLUE600],
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [0, 2, 0, 2],
    },
    fill: {
      type: 'solid',
      pattern: {
        style: 'verticalLines',
        width: 1,
        height: 1,
        strokeWidth: 0,
      },
    },
    legend: {
      ...legendDefault,
    },
  });

  const formatterPercent = (val, isFixed = false) => {
    if (isNil(val)) return '-';
    if (val === 0) return 0;
    return `${Number.isInteger(val) ? val : val.toFixed(isFixed ? 2 : 0)}%`;
  };
  const formatterDate = (val) => {
    if (!val) return val;
    return `${val.length === 8 ? moment(val, 'YYYYMMDD').format('YY. M. D.') : moment(val, 'YYYYMM').format('YY. M.')}`;
  };

  const makeChartData = () => {
    if (!Object.keys(data).length) return;
    const localSeries = data[chartType] && data[chartType].series ? data[chartType].series : [];
    const localCategory = data[chartType] && data[chartType].labels ? data[chartType].labels : [];

    chartRef.current.chart.ctx.updateOptions({
      chart: {
        zoom: {
          enabled: false,
        },
        height: 420,
        type: 'line',
        stacked: false,
        toolbar: {
          show: false,
        },
      },
      colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.GRAY800, CHART_COLORS.COMPARE_BRAND, CHART_COLORS.BLUE800],
      markers: {
        size: 3,
        colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.GRAY800, CHART_COLORS.COMPARE_BRAND, CHART_COLORS.BLUE800],
        strokeWidth: 1,
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        width: [0, 2, 0, 2],
      },
      fill: {
        type: 'solid',
        pattern: {
          style: 'verticalLines',
          width: 1,
          height: 1,
          strokeWidth: 0,
        },
      },
      legend: {
        ...legendDefault,
      },
      xaxis: {
        type: 'category',
        categories: localCategory,
        axisTicks: {
          show: false,
          borderType: 'solid',
          color: '#78909C',
          height: 0,
          offsetX: 0,
          offsetY: 0,
        },
        tooltip: {
          enabled: false,
        },
        labels: {
          show: true,
          trim: false,
          hideOverlappingLabels: false,
          formatter(val) {
            return formatterDate(val);
          },
        },
      },
      yaxis: [
        {
          title: {
            text: data[chartType].series[0].name,
            offsetX: 6,
            style: {
              color: CHART_COLORS.COMPARE_GS_SHOP,
            },
          },
          axisBorder: {
            show: true,
            color: CHART_COLORS.COMPARE_GS_SHOP,
            offsetX: -6,
          },
          tickAmount: 5,
          min: 0,
          max: (number) => getChartYAxisMax(number),
          labels: {
            offsetX: 6,
            style: {
              cssClass: 'yaxis-label-COMPARE_GS_SHOP',
            },
            formatter(val) {
              switch (chartType) {
              case 'totalOrderCust': {
                return getChartValuePeople(val);
              }
              default: {
                return getChartValue(val);
              }
              }
            },
          },
        },
        {
          title: {
            text: data[chartType].series[1].name,
            offsetX: 6,
            style: {
              color: CHART_COLORS.GRAY800,
            },
          },
          axisBorder: {
            show: true,
            color: CHART_COLORS.GRAY800,
            offsetX: -6,
          },
          axisTicks: {
            show: false,
          },
          labels: {
            style: {
              cssClass: 'yaxis-label-GRAY800',
            },
            formatter(val) {
              return formatterPercent(val);
            },
          },
          tickAmount: 5,
          min: getChartYAxisData(localSeries[1].data).min,
          max: getChartYAxisData(localSeries[1].data).max,
        },
        {
          show: true,
          opposite: true,
          title: {
            text: data[chartType].series[2].name,
            offsetX: -6,
            style: {
              color: CHART_COLORS.COMPARE_BRAND,
            },
          },
          axisBorder: {
            show: true,
            color: CHART_COLORS.COMPARE_BRAND,
            offsetX: -6,
          },
          labels: {
            style: {
              cssClass: 'yaxis-label-COMPARE_BRAND',
            },
            formatter(val) {
              switch (chartType) {
              case 'totalOrderCust': {
                return getChartValuePeople(val);
              }
              default: {
                return getChartValue(val);
              }
              }
            },
          },
          tickAmount: 5,
          min: 0,
          max: (number) => getChartYAxisMax(number),
        },
        {
          show: true,
          title: {
            text: data[chartType].series[3].name,
            offsetX: -6,
            style: {
              color: CHART_COLORS.BLUE800,
            },
          },
          axisBorder: {
            show: true,
            color: CHART_COLORS.BLUE800,
            offsetX: -6,
          },
          opposite: true,
          labels: {
            style: {
              cssClass: 'yaxis-label-BLUE800',
            },
            formatter(val) {
              return formatterPercent(val);
            },
          },
          tickAmount: 5,
          min: getChartYAxisData(localSeries[3].data).min,
          max: getChartYAxisData(localSeries[3].data).max,
        },
      ],
      tooltip: {
        enabled: true,
        shared: true,
        followCursor: false,
        x: {
          formatter(_, { __, ___, dataPointIndex }) {
            const val = data[chartType].labels[dataPointIndex];
            return formatterDate(val);
          },
        },
        y: [
          {
            formatter(val) {
              switch (chartType) {
              case 'totalOrderCust': {
                return val && `${(val).toLocaleString()}명`;
              }
              default: {
                return val && (val).toLocaleString();
              }
              }
            },
          },
          {
            formatter(val) {
              return formatterPercent(val, true);
            },
          },
          {
            formatter(val) {
              switch (chartType) {
              case 'totalOrderCust': {
                return val && `${(val).toLocaleString()}명`;
              }
              default: {
                return val && (val).toLocaleString();
              }
              }
            },
          },
          {
            formatter(val) {
              return formatterPercent(val, true);
            },
          },
        ],
      },
    });
    chartRef.current.chart.ctx.updateSeries(localSeries);
  };

  const getTypeText = () => {
    switch (chartType) {
    case 'totalOrderCount': {
      return '전년대비 주문수의 변화를 GS SHOP 브랜드와 비교해보세요.';
    }
    case 'totalOrderCust': {
      return '전년대비 주문고객수의 변화를 GS SHOP 브랜드와 비교해보세요.';
    }
    default: {
      return '전년대비 주문금액의 변화를 GS SHOP 브랜드와 비교해보세요.';
    }
    }
  };
  useEffect(() => {
    makeChartData();
  }, [chartType, data]);

  return (
    <ResultChartBox title={getTypeText()}>
      <ReactApexChart options={options} series={[]} type="line" height={440} ref={chartRef} />
    </ResultChartBox>
  );
}

export default FirstResultChart;
