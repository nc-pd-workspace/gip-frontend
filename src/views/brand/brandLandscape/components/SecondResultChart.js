import ReactApexChart from 'react-apexcharts';

import { legendDefault } from '../../../../utils/chartOptions';

import ResultChartBox from './ResultChartBox';

const mockData = {
  series: [
    {
      name: 'GS SHOP',
      type: 'column',
      data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6],
    },
    {
      name: '브랜드명',
      type: 'column',
      data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5],
    },
  ],
  options: {
    colors: ['#8784d3', '#93c8a0'],
    chart: {
      zoom: {
        enabled: false,
      },
      toolbar: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [1, 1],
    },
    xaxis: {
      categories: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016],
    },
    yaxis: [
      {
        min: 0,
        max: 10,
        axisBorder: {
          show: true,
          color: '#8784d3',
        },
        labels: {
          style: {
            colors: '#8784d3',
          },
        },
      },
      {
        seriesName: 'Income',
        min: 0,
        max: 10,
        opposite: true,
        axisBorder: {
          show: true,
          color: '#93c8a0',
        },
        labels: {
          style: {
            colors: '#93c8a0',
          },
        },
      },
    ],
    legend: {
      ...legendDefault,
    },
  },
};

function SecondResultChart() {
  return (
    <ResultChartBox title="그래프설명이 추가되는 영역입니다.">
      <ReactApexChart options={mockData.options} series={mockData.series} type="line" height={350} />
    </ResultChartBox>
  );
}

export default SecondResultChart;
