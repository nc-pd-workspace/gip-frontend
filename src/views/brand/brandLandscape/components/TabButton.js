import styled from 'styled-components';
import isNil from 'lodash-es/isNil';

import { SvgArrowUp, SvgArrowDown } from '../../../../Images';

import Tooltip from '../../../../components/toolTip';

function YearArrow({ yaearArrowData }) {
  return (
    <>
      {
        // eslint-disable-next-line no-nested-ternary
        yaearArrowData > 0 ? (
          <SvgArrowUp width="16" height="16" fill="#0091FF" />
        ) : yaearArrowData < 0 ? (
          <SvgArrowDown width="16" height="16" fill="#FF6C63" />
        ) : (
          '-'
        )
      }
    </>
  );
}

function TabButton({ title, tooltipText, tooltipPosition, dataType, destData, srcData }) {
  const getContentNumber = (data) => {
    if (!data) return null;

    if (isNil(data[dataType])) return '-';
    switch (dataType) {
    case 'totalOrderAmount': {
      return `${(data[dataType]).toLocaleString()}`;
    }
    case 'totalOrderCount': {
      return `${(data[dataType]).toLocaleString()}`;
    }
    case 'totalOrderCust': {
      return `${(data[dataType]).toLocaleString()}명`;
    }
    default: {
      return null;
    }
    }
  };

  return (
    <Container>
      <Title>
        <p>{title}</p>
        {
          tooltipText && <Tooltip text={tooltipText} position={tooltipPosition} />
        }
      </Title>
      <ContentWrap>
        <ContentRow>
          <BrandName>{ destData && destData.brandNm}</BrandName>
          <ContentNumber>
            {getContentNumber(destData)}
          </ContentNumber>
          <ComparedYear>
            전년
            {' '}
            {
              !isNil(destData[`${dataType}ComparedYear`]) ? (
                <>
                  <YearArrow yaearArrowData={destData[`${dataType}ComparedYear`]} />

                  <YearArrowP yaearArrowData={destData[`${dataType}ComparedYear`]}>
                    {Math.abs(destData[`${dataType}ComparedYear`])}
                    %
                  </YearArrowP>
                </>
              ) : (
                <>-</>
              )
            }

          </ComparedYear>
        </ContentRow>
        <ContentRow>
          <BrandName>{srcData && srcData.brandNm}</BrandName>
          <ContentNumber>
            {getContentNumber(srcData)}
          </ContentNumber>
          <ComparedYear>
            전년
            {' '}
            {
              !isNil(srcData[`${dataType}ComparedYear`]) ? (
                <>
                  <YearArrow yaearArrowData={srcData[`${dataType}ComparedYear`]} />

                  <YearArrowP yaearArrowData={srcData[`${dataType}ComparedYear`]}>
                    {Math.abs(srcData[`${dataType}ComparedYear`])}
                    %
                  </YearArrowP>
                </>
              ) : (
                <>-</>
              )
            }
          </ComparedYear>
        </ContentRow>
      </ContentWrap>
    </Container>
  );
}

const Container = styled.div`
  cursor: pointer;
`;

const ContentWrap = styled.div`
  display: flex;
`;

const ContentRow = styled.div`
  flex: 1;
  /* margin: 0 8px; */

  &:first-child {
    position: relative;
    padding-right: 20px;
  }
  &:last-child {
    position: relative;
    padding-left: 20px;
  }
  &:first-child::after {
    content: '';
    background-color: #E9ECF3;
    width: 1px;
    height: 50px;
    position: absolute;
    right: 0;
    top: 50%;
    margin-top:-25px;
  }
`;

const Title = styled.h3`
  margin-bottom: 8px;
  color: var(--color-gray-700);
  font-size: 13px;
  font-weight: 700;
  line-height: 19px;
  /* vertical-align: center; */
  display: flex;
  align-items: center;
  position: relative;
`;

const BrandName = styled.p`
  margin-bottom: 4px;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 18px;
  letter-spacing: -0.5px;
  color: var(--color-gray-700);
  vertical-align: center;
`;
const ContentNumber = styled.p`
  font-family: Pretendard;
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  white-space: nowrap;
  color: var(--color-gray-900);
  letter-spacing: -0.75px;
  span {
    font-weight: 400;
    font-size: 12px;
    padding-left: 2px;
    color: var(--color-gray-900);
  }
`;
const ComparedYear = styled.p` 
  display: flex;
  font-size: 12px;
  color: var(--color-gray-700);
  font-weight: 400;
  align-items: center;
`;

const YearArrowP = styled.span`
  color:${(props) => {
    if (props.yaearArrowData > 0) {
      return 'var(--color-blue-500)';
    } if (props.yaearArrowData < 0) {
      return 'var(--color-red-500)';
    }
    return 'var(--color-gray-700)';
  }}
`;

export default TabButton;
