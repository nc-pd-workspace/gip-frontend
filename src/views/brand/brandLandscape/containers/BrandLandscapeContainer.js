import { useEffect, useState, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Radio } from 'antd';

import Table from '../../../../components/table';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import CategorySelects from '../../../../components/search/CategorySelects';
import TabButton from '../components/TabButton';
import Description from '../../../../components/paper/descripition';

import TabButtonList from '../../../../components/tabButtonList';
import Images from '../../../../Images';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';

import { columns } from '../constants';
import Paper from '../../../../components/paper';
import SectionHeader from '../../../../components/header/SectionHeader';
import EmptyList from '../../../../components/emptyList';
import ResultChartContainer from './ResultChartContainer';
import LoadingComponent from '../../../../components/loading';
import { getBrandLandscapeList, getBrandMainChart, getCategoryOptions, getBrandOptions } from '../redux/slice';

function BrandLandscapeContainer({ query }) {
  const searchRef = useRef();
  const dispatch = useDispatch();
  const currentSelectedInfoRef = useRef();
  const [search, setSearch] = useState({});
  const [currentSelectedRow, setCurrentSelectedRow] = useState(null);
  const [brand, setBrand] = useState(null);
  const [dataType, setDataType] = useState('avg'); // dataType avg, sum
  const [chartType, setChartType] = useState('totalOrderAmount'); // chartType, totalOrderAmount, totalOrderCount, totalOrderCust
  const [totalData, setTotalData] = useState(null);
  const [tableData, setTableData] = useState({
    tableKey: 'exampleTable',
    totalRow: 1000,
    pageCurrent: 1,
    pageTotal: 10,
    excelDownload: true,
    excelFileLink: 'excel.xlsx',
    rows: [],
  });

  const { selectPtnIdx } = useSelector((state) => state.common);
  const { brandLandScapeList, brandMainChart, categoryOption, brandOption } = useSelector((state) => state.brand.brandLandscape);

  const onChangeTabButton = (idx) => {
    switch (idx) {
    case 0: {
      setChartType('totalOrderAmount');
      break;
    }
    case 1: {
      setChartType('totalOrderCount');
      break;
    }
    case 2: {
      setChartType('totalOrderCust');
      break;
    }
    default: {
      setChartType('totalOrderAmount');
    }
    }
  };

  const onClickRow = useCallback(async (getData) => {
    setCurrentSelectedRow(getData);
  });

  const conditionHighlight = (row) => {
    if (currentSelectedRow && row.brandNm === currentSelectedRow.brandNm) {
      return 'currentSelectedRow';
    }
    return '';
  };

  const RadioOnchange = (e) => {
    setDataType(e.target.value);
  };
  const toolTip01 = (
    <>
      고객의 주문완료 시점의 주문금액(합계/평균)입니다.
      <br />
      <br />
      전년대비 성장률은 아래와 같이 계산됩니다.
      <br />
      &nbsp;- 전년대비 성장률=((올해년도 총 주문 금액 - 전년도 총 주문금액) / 전년도 총 주문금액) * 100%
      <br />
      &nbsp;- 예) 올해 기준일 2022. 1. 7.
      <br />
      &nbsp;&nbsp;&nbsp;
      {'{'}
      (2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~7 합계
      {'} '}
      * 100%
    </>
  );
  const toolTip02 = (
    <>
      고객의 주문완료 시점의 주문 건 수(합계/평균) 입니다.
      <br />
      <br />
      전년대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전년대비 성장률=((올해년도 총 주문수 -전년도 총 주문수) / 전년도 총 주문수) * 100%
      <br />
      - 예) 올해 기준일 2022. 1. 7.
      <br />
      &nbsp;&nbsp;&nbsp;
      {'{'}
      (2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~7 합계
      {'} '}
      * 100%
    </>
  );
  const toolTip03 = (
    <>
      조회기간동안 주문 완료한 고객수(합계/평균) 입니다.
      <br />
      <br />
      전년대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전년대비 성장률=((올해년도 총 주문고객수 -전년도 총 주문고객수) / 전년도 총 주문고객수) * 100%
      <br />
      - 예) 올해 기준일 2022. 1. 7.
      <br />
      &nbsp;&nbsp;&nbsp;
      {'{'}
      (2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~7 합계
      {'} '}
      * 100%
    </>
  );

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  useEffect(() => {
    if (query && Object.keys(query).length) {
      setSearch({ ...query });
      searchRef.current.setValue({ ...query });
    }
  }, [query]);

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    dispatch(getBrandOptions({ params: { ptnIdx: selectPtnIdx } }));
    searchRef.current.clickSearch();
  }, []);

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (Object.keys(search).length === 0) return;
    if (search.searchStartDate) {
      if (search.searchStartDate.length > 7) params.searchDateType = 'date';
      else params.searchDateType = 'month';

      params.searchStartDate = search.searchStartDate;
    }
    if (search.searchEndDate) params.searchEndDate = search.searchEndDate;

    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categoryLCode) params.categoryLCode = categoryLCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categorySCode) params.categorySCode = categorySCode;
    }
    if (search.brandCd) params.brandCd = search.brandCd;
    params.dataType = dataType;

    dispatch(getBrandLandscapeList({ params }));
  }, [search, selectPtnIdx, dataType]);

  useEffect(() => {
    if (brandLandScapeList.status === 'success' && brandLandScapeList.data.length > 0) {
      setTotalData(brandLandScapeList.data[0]);
      setCurrentSelectedRow(brandLandScapeList.data[1]);
      setTableData({ ...tableData, rows: brandLandScapeList.data });
    }
  }, [brandLandScapeList]);

  useEffect(() => {
    if (currentSelectedRow) {
      const params = {
        ptnIdx: selectPtnIdx,
      };
      if (Object.keys(search).length === 0) return;
      if (search.searchStartDate) {
        if (search.searchStartDate.length > 7) params.searchDateType = 'date';
        else params.searchDateType = 'month';

        params.searchStartDate = search.searchStartDate;
      }
      if (search.searchEndDate) params.searchEndDate = search.searchEndDate;

      if (search.categoryData) {
        const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
        if (categoryLCode) params.categoryLCode = categoryLCode;
        if (categoryMCode) params.categoryMCode = categoryMCode;
        if (categorySCode) params.categorySCode = categorySCode;
      }
      params.brandCd = currentSelectedRow.brandCode;
      params.brandNm = currentSelectedRow.brandNm;
      params.dataType = dataType;

      dispatch(getBrandMainChart({ params }));
      // currentSelectedInfoRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, [currentSelectedRow]);

  return (
    <Container>
      <PageHeader
        title="브랜드 현황"
        subTitle="GS SHOP과 내 브랜드의 실적 지표를 비교할 수 있습니다."
      />
      <Search search={search} setSearch={setSearch} ref={searchRef} onReset={onResetSearch}>
        <SingleRangePicker
          column={['searchStartDate', 'searchEndDate']}
          title="조회기간"
          maxSelectDate={30}
          maxSelectMonth={12}
          dropdownClassName="brandLandscapePickerPopup"
        />
        <SingleInputItem type="Select" column="brandCd" title="브랜드" width="50%" loading={brandOption.status === 'pending'} onChange={onChangeBrand} options={brandOption.data} placeholder="전체" />
        <CategorySelects column="categoryData" title="카테고리" depth={categoryOption.data.length} options={categoryOption.data} loading={categoryOption.status === 'pending'} placeholder="카테고리 선택" />
      </Search>
      {
        brandLandScapeList.status === 'pending' && (
          <SearchResult>
            <Title>
              <p>브랜드 실적조회</p>
            </Title>
            <LoadingWrap>
              <LoadingComponent isLoading />
            </LoadingWrap>
          </SearchResult>
        )
      }
      {
        brandLandScapeList.status === 'success' && (
          <>
            {
              brandLandScapeList.data.length === 0 ? (
                <SearchResult>
                  <Title>
                    <p>브랜드 실적조회</p>
                  </Title>
                  <EmptyList />
                </SearchResult>
              ) : (
                <>
                  <SearchResult>
                    <Title>
                      <p>브랜드 실적조회</p>
                    </Title>
                    <div className="searchResultToolBox">
                      <span className="caption-text">데이터 기준</span>
                      <div className="right-item set-section-toggle">
                        <Radio.Group value={dataType} buttonStyle="solid" onChange={RadioOnchange}>
                          <Radio.Button value="avg">평균</Radio.Button>
                          <Radio.Button value="sum">합계</Radio.Button>
                        </Radio.Group>
                      </div>
                      {/* <ButtonExcel /> */}
                    </div>
                    <Table
                      className="tableClassName"
                      columns={columns}
                      dataSource={tableData.rows}
                      conditionalHighlight={conditionHighlight}
                      fixedHeader="total"
                      onClickRow={onClickRow}
                      scrollY={239}
                    />
                  </SearchResult>
                  <Description text={['* 표시된 데이터는  실제 수치가 아닌  GIP만의 방식으로 계산된 지수화 데이터 입니다.']} />
                  {
                    currentSelectedRow && (
                      <PageSection ref={currentSelectedInfoRef} style={{ marginTop: '51px' }} border>
                        <SectionHeader title={`GS SHOP 브랜드와 ${currentSelectedRow.brandNm}의 실적분석 결과`} />
                        {
                          brandMainChart.status === 'pending' ? (
                            <>
                              <LoadingWrap>
                                <LoadingComponent isLoading />
                              </LoadingWrap>
                            </>
                          ) : (
                            <>
                              <TabButtonList gutter={10} defaultValue={0} onChange={onChangeTabButton}>
                                <TabButton title="*주문금액" tooltipText={toolTip01} dataType="totalOrderAmount" destData={totalData} srcData={currentSelectedRow} />
                                <TabButton title="*주문수" tooltipText={toolTip02} dataType="totalOrderCount" destData={totalData} srcData={currentSelectedRow} />
                                <TabButton title="주문고객수" tooltipText={toolTip03} dataType="totalOrderCust" tooltipPosition="right" destData={totalData} srcData={currentSelectedRow} />
                              </TabButtonList>
                              <ResultChartContainer chartType={chartType} data={brandMainChart.data} />
                            </>
                          )
                        }
                      </PageSection>
                    )
                  }
                </>
              )
            }
          </>
        )
      }
    </Container>
  );
}
const PageSection = styled.div`
  margin-top: 50px;
  background-color: #FFF;
  border-radius: 8px;
  border: 1px solid #e3e4e7;
`;
const LoadingWrap = styled.div`
  position: relative;
  height: 320px;
`;
const Container = styled(PageLayout)``;
const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;

  span.brand {
    padding-left: 20px;
  }
  .total{
    display:inline-block;
    background-image: url('${Images.gsShopLogo}');
    background-position: left 1px;
    background-repeat: no-repeat;
    background-size: 16px 16px;
    line-height:20px;
  }
  table tr td span {
    white-space: nowrap;
    max-width: 160px;
    width: 80px;
    display: inline-block;
  }
  table  {
    tr th:nth-of-type(1),
    tr td:nth-of-type(1) {
      width: 110px !important;
    }
    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      width: calc(25% - 160px) !important;
      padding-right: 30px;

    }
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      width: 140px !important;
      padding-right: 10px;

    }
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      width: 160px !important;
      padding-right: 30px;
    }
    tr th:nth-of-type(5),
    tr td:nth-of-type(5) {
      width: 140px !important;
      padding-right: 10px;
    }
    tr th:nth-of-type(6),
    tr td:nth-of-type(6) {
      width: 160px !important;
      padding-right: 30px;
    }
    tr th:nth-of-type(7),
    tr td:nth-of-type(7) {
      width: 130px !important;
    }
  }

  .fixedTable {
    table {
      tr th:nth-of-type(7),
      tr td:nth-of-type(7) {
        width: 136px !important;
        padding-right: 16px !important;
      }
    }
  }

  @media screen and (min-width: 1400px){
    table  {

    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      width: calc(25% - 200px) !important;
      padding-right: 70px;
    }
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      width: 170px !important;
      padding-right: 40px;
    }
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      width: 200px !important;
      padding-right: 70px;
    }
    tr th:nth-of-type(5),
    tr td:nth-of-type(5) {
      width: 170px !important;
      padding-right: 40px;
    }
    tr th:nth-of-type(6),
    tr td:nth-of-type(6) {
      width: 200px !important;
      padding-right: 70px;
    }

  }
  
  }

`;
const Title = styled.div`
  display:flex;
  font-weight: 700;
  position: relative;
  p {
    padding: 20px 0;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .right {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
  }
  .caption-text {
    color: var(--color-gray-500);
    margin-right: 5px;
    font-weight: 400;
    font-size: 12px;
  }
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
`;

export default BrandLandscapeContainer;
