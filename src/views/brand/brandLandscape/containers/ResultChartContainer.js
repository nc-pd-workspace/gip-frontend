import React from 'react';
import styled from 'styled-components';

import FirstResultChart from '../components/FirstResultChart';

// 브랜드 현황
function ResultChartContainer({ chartType, data }) {
  return (
    <Container>
      <FirstResultChart chartType={chartType} data={data} />
    </Container>
  );
}

const Container = styled.div`
    
`;

export default ResultChartContainer;
