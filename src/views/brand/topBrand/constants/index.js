import Tooltip from '../../../../components/toolTip';

export const sortOptions = [
  { label: '주문금액순', value: 'totalOrderAmount' },
  { label: '주문수량순', value: 'totalOrderCount' },
  { label: '주문고객수순', value: 'totalOrderCustCount' },
  { label: '1인당 주문수순', value: 'totalEachCustOrderCount' },
  { label: '객단가순', value: 'totalCustPrice' },
];

export const columns = [
  {
    title: '순위',
    dataIndex: 'rank',
    key: 'rank',
  },
  {
    title: '브랜드명',
    dataIndex: 'brandNm',
    key: 'brandNm',
  },
  {
    title: (
      <>
        *주문금액
        <Tooltip text={(<>조회기간 동안 내 브랜드에서 주문완료한 주문 금액(합계/평균) 입니다.</>)} />
      </>
    ),
    dataIndex: 'totalOrderAmount',
    key: 'totalOrderAmount',
    type: 'number',
  },
  {
    title: (
      <>
        *주문수
        <Tooltip text={(<>조회기간 동안 내 브랜드 상품을 주문한 건 수(합계/평균)입니다.</>)} />
      </>
    ),
    dataIndex: 'totalOrderCount',
    key: 'totalOrderCount',
    type: 'number',
  },
  {
    title: (
      <>
        주문고객수
        <Tooltip text={(<>조회기간 동안 내 브랜드 상품을 주문한 고객수(합계/평균)입니다.</>)} />
      </>
    ),
    dataIndex: 'totalOrderCust',
    key: 'totalOrderCust',
    type: 'number',
  },
  {
    title: (
      <>
        1인당 주문수
        <Tooltip
          text={(<>한 명의 고객이 내 브랜드의 상품을 주문한 건 수(합계/평균)입니다.</>)}
          position="right"
        />
      </>
    ),
    dataIndex: 'totalEachCustOrderCount',
    key: 'totalEachCustOrderCount',
    type: 'number',
  },
  {
    title: (
      <>
        *객단가
        <Tooltip
          text={
            (
              <>
                한 명의 고객의 평균 주문금액입니다.
                <br />
                객단가는 다음과 같이 계산됩니다.
                <br />
                계산식= 총 주문금액/주문고객수
              </>
            )
          }
          position="right"
        />
      </>
    ),
    dataIndex: 'totalCustPrice',
    key: 'totalCustPrice',
    type: 'number',
  },

];
