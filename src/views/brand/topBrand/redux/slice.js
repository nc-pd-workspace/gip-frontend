import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  topBrandList: asyncApiState.initial([]),
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'brand/topBrand',
  initialState,
  reducers: {
    updateStore: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getTopBrandList: (state, { payload }) => {
      state.topBrandList = asyncApiState.request([]);
    },
    getTopBrandListSuccess: (state, { payload }) => {
      const result = payload;
      // if need customize
      // result.data.apiArray.map((v) => {
      //   v.date = moment(v.date).format('YYYY-MM-DD');
      // });
      state.topBrandList = asyncApiState.success(result);
    },
    getTopBrandListFailure: (state, { payload }) => {
      state.topBrandList = asyncApiState.error(payload);
    },
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], [], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], [], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));

      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },
  },
});

export const { updateStore, getTopBrandList, getCategoryOptions, getBrandOptions } = actions;

export default reducer;
