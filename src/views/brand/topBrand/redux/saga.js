import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

import { createPromiseSaga } from '../../../../redux/lib';

import { getTopBrandList, getCategoryOptions, getBrandOptions } from './slice';
import API from '../../../../api';

const topBrandListSaga = createPromiseSaga(getTopBrandList, API.TopBrand.topBrandList);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

/* dispatch type 구독 설정, 종류에 따라 watch함수 분기해도 좋음 */
function* watchCommon() {
  yield takeLatest(getTopBrandList, topBrandListSaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
