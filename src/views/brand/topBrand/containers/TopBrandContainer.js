import { useEffect, useState, useRef } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Radio, Select } from 'antd';

import { updateStore, getTopBrandList, getCategoryOptions, getBrandOptions } from '../redux/slice';
import { asyncApiState } from '../../../../redux/constants';

import Table from '../../../../components/table';

import Search from '../../../../components/search';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import CategorySelects from '../../../../components/search/CategorySelects';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';
import { SvgArrowDropdown } from '../../../../Images';
import Description from '../../../../components/paper/descripition';
import EmptyList from '../../../../components/emptyList';

import { sortOptions, columns } from '../constants';
import LoadingComponent from '../../../../components/loading';

function TopBrandContainer() {
  const searchRef = useRef();
  const [search, setSearch] = useState({});
  const [brand, setBrand] = useState(null);
  const [dataType, setDataType] = useState('avg'); // dataType avg, sum
  const [sort, setSort] = useState('totalOrderAmount'); // dataType avg, sum
  const [tableData, setTableData] = useState({
    tableKey: 'exampleTable',
    totalRow: 1000,
    pageCurrent: 1,
    pageTotal: 10,
    excelDownload: true,
    excelFileLink: 'excel.xlsx',
    rows: [],
  });

  const { selectPtnIdx } = useSelector((state) => state.common);
  const { topBrandList, categoryOption, brandOption } = useSelector((state) => state.brand.topBrand);

  const dispatch = useDispatch();

  const RadioOnchange = (e) => {
    setDataType(e.target.value);
  };

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  const handleChangeSort = (value) => {
    setSort(value);
  };

  const conditionHighlight = (row) => {
    if (row.isMyBrand === 'Y') {
      return 'currentSelectedRow';
    }
    return '';
  };

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (Object.keys(search).length === 0 || (!search.categoryData || !search.categoryData[0])) {
      dispatch(updateStore({ topBrandList: asyncApiState.initial([]) }));
      setTableData({ ...tableData, rows: [] });
      return;
    }
    if (search.searchMonth) {
      params.searchMonth = search.searchMonth.replace(/\./g, '');
    }
    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categoryLCode) params.categoryLCode = categoryLCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categorySCode) params.categorySCode = categorySCode;
    }
    if (search.brandCd) params.brandCd = search.brandCd;
    params.dataType = dataType;
    params.sortOrder = sort;

    dispatch(getTopBrandList({ params }));
  }, [search, selectPtnIdx, dataType, sort]);

  useEffect(() => {
  }, [categoryOption]);

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    dispatch(getBrandOptions({ params }));
  }, []);

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  useEffect(() => {
    if (topBrandList.status === 'success') {
      setTableData({ ...tableData, rows: topBrandList.data });
    }
  }, [topBrandList]);

  return (
    <Container>
      <PageHeader
        title="TOP10 브랜드 현황"
        subTitle="카테고리별 GS SHOP의 TOP 10 브랜드와 내 브랜드의 실적을 비교할 수 있습니다."
      />

      <Search search={search} setSearch={setSearch} ref={searchRef} onReset={onResetSearch}>
        <SingleInputItem
          type="DatePicker"
          picker="month"
          column="searchMonth"
          title="조회 월"
          width="50%"
        />
        <SingleInputItem type="Select" column="brandCd" title="브랜드" width="50%" loading={brandOption.status === 'pending'} onChange={onChangeBrand} options={brandOption.data} placeholder="전체" />
        <CategorySelects
          column="categoryData"
          title="카테고리"
          required
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />
      </Search>
      {
        topBrandList.status === 'success' ? (
          <>
            {
              tableData?.rows.length > 0 ? (
                <>
                  <SearchResult>
                    <Title>
                      <p>브랜드 실적 순위</p>
                      <div className="searchResultToolBox">
                        <span className="caption-text">데이터 기준</span>
                        <div className="right-item set-section-toggle">
                          <Radio.Group value={dataType} buttonStyle="solid" onChange={RadioOnchange}>
                            <Radio.Button value="avg">평균</Radio.Button>
                            <Radio.Button value="sum">합계</Radio.Button>
                          </Radio.Group>
                        </div>
                        <div className="right-item set-select">
                          <Select onChange={handleChangeSort} style={{ width: 120 }} options={sortOptions} value={sort} suffixIcon={<SvgArrowDropdown />} />
                        </div>
                        {/* <ButtonExcel /> */}
                      </div>
                    </Title>
                    <Table
                      className="tableClassName tableNotHover"
                      columns={columns}
                      rowKey={(record) => record.rank}
                      dataSource={tableData.rows}
                      conditionalHighlight={conditionHighlight}
                    />
                  </SearchResult>
                  <Description text={['* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.', '내 브랜드가 아닌 타사의 브랜드명은 노출되지 않습니다.']} />
                </>
              ) : (
                <>
                  <SearchResult>
                    <EmptyList />
                  </SearchResult>
                </>
              )
            }
          </>
        ) : (
          <>
            {
              topBrandList.status === 'pending' ? (
                <SearchResult>
                  <Title>
                    <p>브랜드 실적 순위</p>
                  </Title>
                  <LoadingWrap>
                    <LoadingComponent isLoading />
                  </LoadingWrap>
                </SearchResult>
              ) : (
                (
                  <SearchResult>
                    <EmptyList warningTitle="실적순위 비교를 위해 카테고리를 선택해 주세요." />
                  </SearchResult>
                )
              )
            }
          </>
        )
      }
    </Container>
  );
}

const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;

  .set-select {
    margin-left:6px;
    .ant-select-single:not(.ant-select-customize-input) .ant-select-selector{
      height:28px;
    }
    .ant-select-single .ant-select-selector .ant-select-selection-item, .ant-select-single .ant-select-selector .ant-select-selection-placeholder{
      line-height:26px;
      font-size:12px;
    }
  }
  .ant-select-arrow {
    width:16px;
    height:16px;
    margin-top:-10px;
  }
`;
const Title = styled.div`
  display:flex;
  font-weight: 700;
  p{
    padding: 20px 0;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
`;

const Container = styled(PageLayout)``;

const LoadingWrap = styled.div`
  position: relative;
  height: 330px;
`;

export default TopBrandContainer;
