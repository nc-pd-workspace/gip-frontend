/* eslint-disable prefer-destructuring */
import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styled from 'styled-components';
import { Button, Input, Modal, Select } from 'antd';

// import PageLink from '../../pageTab/components/PageLink';
import SearchTreeContainer from '../../searchTree/containers/SearchTreeContainer';
// import { PageTypes } from '../../../../constants/pageType';
import { updateStore } from '../../../../redux/commonReducer';
import { usePageTab } from '../../pageTab/hooks/usePageTab';
import { ChevronRight, SvgArrowDropdown } from '../../../../Images';
import { alertMessage, confirmMessage } from '../../../../components/message';

const { Option } = Select;

function Nav() {
  const dispatch = useDispatch();

  const [empty, setEmpty] = useState(false);
  const [searchOption, setSearchOption] = useState('name');
  const [partnerModalVisible, setPartnerModalVisible] = useState(false);
  const [treeData, setTreeData] = useState([]);
  const [searchTreeData, setSearchTreeData] = useState([]);
  const [generateTreeData, setGenerateTreeData] = useState([]);
  const [searchTxt, setSearchTxt] = useState('');
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [viewFlag, setViewFlag] = useState();
  const [selectedNode, setSelectedNode] = useState();
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [treeBtnDisabled, setTreeBtnDisabled] = useState(false);

  const { closeAllPage } = usePageTab();
  const { userInfo, selectPtnIdx } = useSelector((state) => state.common);

  const findParentKeys = (key, tree) => {
    let parentKey = [];
    tree.forEach((data) => {
      const node = data;
      let arr = [];
      if (node.children && node.key.toString() !== key.toString()) {
        if (node.children.some((item) => item.key.toString() === key.toString())) {
          arr = [key];
        } else {
          arr = findParentKeys(key, node.children);
        }
      } else if (node.key.toString() === key.toString()) {
        parentKey = [node.key];
      }
      if (arr.length > 0) parentKey = [node.key, ...arr];
    });
    return parentKey;
  };

  useEffect(() => {
    if (treeData && treeData.length && treeData.length !== 0) {
      initSearch();
      const arr = [];
      generateData(treeData, arr);
      setGenerateTreeData(arr);
      if (!selectedNode && selectedKeys.length > 0) {
        const filterd = arr.filter((v) => v.key === selectedKeys[0]);
        if (filterd.length > 0) {
          setSelectedNode(filterd[0]);
        }
      }
    }
  }, [treeData]);

  useEffect(() => {
    if (userInfo && userInfo.partnerList && userInfo.partnerList.length !== 0 && partnerModalVisible) {
      initTreeData();
    }
  }, [userInfo.partnerList, selectPtnIdx, partnerModalVisible]);

  useEffect(() => {
  }, [expandedKeys]);

  const initSearch = useCallback(() => {
    setSearchOption('name');
    setSearchTxt();
  }, []);

  const initTreeData = useCallback(() => {
    setTreeData(userInfo.partnerList);
    setSearchTreeData(userInfo.partnerList);
    setSelectedKeys([selectPtnIdx.toString()]);

    const expandedArr = findParentKeys(selectPtnIdx.toString(), userInfo.partnerList);
    setExpandedKeys(expandedArr);
    setEmpty(false);
  }, [userInfo.partnerList, selectPtnIdx]);

  const generateData = useCallback(
    (param, arr) => {
      param.forEach((data) => {
        arr.push({ key: data.key, title: data.title, ptnNm: data.ptnNm, ptnId: data.ptnId });
        // setGenerateTreeData((prevData) => prevData.concat(tempArr));
        if (data.children) {
          generateData(data.children, arr);
        }
      });
    },
    [],
  );

  const onClickTreeButton = useCallback(() => {
    initTreeData();
    setPartnerModalVisible(true);
  }, [userInfo.partnerList, selectPtnIdx]);

  const onChangeSearchType = (value) => {
    setSearchOption(value);
  };

  const changePartner = (node) => {
    if (!node) {
      alertMessage('파트너를 선택해주세요.');
      return;
    }
    const name = `${node.ptnNm}(${node.ptnId})`;

    confirmMessage(`파트너를 변경하시면 현재 열어둔 모든 화면이 닫힙니다.\n${name} 메인으로 이동하시겠어요?`, () => {
      initSearch();
      setPartnerModalVisible(false);
      // 선택한 파트너 하위로 이동 시킬 수 없음
      // 목적 파트너의 10depth 가 넘어갈 수없음
      // if (selectedNode) {
      //   const title = getTitle(selectedNode.title);
      //   alert(`${title}로 변경 후 이용하시겠습니까?`);
      //   setPartnerModalVisible(false);
      // }
      // 트리 데이터 새로 호출
      dispatch(updateStore({ selectPtnIdx: node.key }));
      closeAllPage();
    });
  };
  const handleOk = useCallback(() => {
    if (selectedKeys.length === 0) {
      alertMessage('변경 할 파트너를 선택해주세요.');
      return;
    }
    changePartner(selectedNode);
  }, [selectedNode, selectedKeys]);

  const handleCancel = () => {
    confirmMessage('취소하시겠습니까?', () => {
      initSearch();
      initTreeData();
      setPartnerModalVisible(false);
    }, '예', '아니오');
  };

  const handleSearchChange = (e) => {
    setSearchTxt(e.target.value);
  };

  const onDoubleClickTree = (e, node) => {
    changePartner(node);
  };

  const getParentKey = (key, tree) => {
    let parentKey = '';
    tree.forEach((data) => {
      const node = data;
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          parentKey = node.key;
        } else if (getParentKey(key, node.children)) {
          parentKey = getParentKey(key, node.children);
        }
      }
    });
    return parentKey;
  };

  const handleSearchBtn = useCallback(() => {
    if (searchTxt === '') {
      const expandedArr = findParentKeys(selectPtnIdx.toString(), userInfo.partnerList);
      setExpandedKeys(expandedArr);
    } else {
      if (searchTxt.length === 1) {
        alertMessage('검색어는 최소 2글자 이상 입력해주세요');
        return;
      }
      // filter tree
      const expandedArr = generateTreeData
        .map((item) => {
          // const name = item.title.split('(')[0];
          // const id = item.title.split('(').length > 1 ? item.title.split('(')[1].substr(0, item.title.split('(')[1].length - 1) : '';
          const name = item.ptnNm;
          const id = item.ptnId;
          if (searchOption === 'id' ? id === searchTxt : name.indexOf(searchTxt) > -1) {
            return getParentKey(item.key, treeData);
          }
          return null;
        })
        .filter((item, i, self) => item && self.indexOf(item) === i);

      setExpandedKeys(expandedArr);
    }

    const searchItemList = [];
    // 검색된 텍스트 하이라이트
    const loopSearchData = (data) => data.map((item) => {
      const name = item.ptnNm;
      const id = item.ptnId;
      const isSearched = searchOption === 'id' ? id === searchTxt : name.indexOf(searchTxt) > -1;
      let title = null;
      if (isSearched && searchTxt !== '') {
        searchItemList.push(id);
        title = (
          <span>
            <span className="highlight">{item.title}</span>
          </span>
        );
      } else {
        title = (
          <span>{item.title}</span>
        );
      }

      if (item.children) {
        return { title, key: item.key, ptnNm: item.ptnNm, ptnId: item.ptnId, children: loopSearchData(item.children) };
      }

      return {
        title,
        key: item.key,
        ptnNm: item.ptnNm,
        ptnId: item.ptnId,
      };
    });

    setSearchTreeData(loopSearchData(treeData));
    if (searchItemList.length > 0 || searchTxt === '') {
      setEmpty(false);
    } else setEmpty(true);
  }, [searchTxt, selectPtnIdx, userInfo, treeData, generateTreeData, searchOption]);

  const handleChangeSelectedNode = useCallback((result) => {
    if (result === 'init') {
      setSelectedNode('');
      return;
    }
    setSelectedNode(result);
    // tree selected node 표현
    setSelectedKeys([result.key]);
    setViewFlag('detail');
    if (result.key === '0') {
      setTreeBtnDisabled(true);
    } else setTreeBtnDisabled(false);
  }, []);

  const getSelectedPatnerName = (key, tree) => {
    let partnerName = '';
    tree.forEach((data) => {
      const node = data;
      if (node.key === key) {
        partnerName = node.ptnNm;
      } else if (node.children) {
        if (getSelectedPatnerName(key, node.children) !== '') {
          partnerName = getSelectedPatnerName(key, node.children);
        }
      }
    });
    return partnerName;
  };

  return (
    <>
      <Container>
        <CustomTreeButton onClick={onClickTreeButton}>
          { selectPtnIdx !== null && getSelectedPatnerName(selectPtnIdx.toString(), userInfo.partnerList)}
          <ChevronRight width="20" height="20" fill="var(--color-gray-400)" />
        </CustomTreeButton>
      </Container>
      <PatnerTreeModal
        title="파트너 변경"
        visible={partnerModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{ disabled: treeBtnDisabled }}
        width={420}
        okText="선택"
        cancelText="취소"
      >
        {/* <div className="descText">
          <p>선택한 파트너 하위로 위치가 이동됩니다.</p>
          <p>이동할 위치의 상위 파트너를 선택해주세요.</p>
        </div> */}
        <TreeSearch>
          <Select
            value={searchOption}
            style={{ width: '100px', fontSize: '12px' }}
            onChange={onChangeSearchType}
            suffixIcon={<SvgArrowDropdown />}
          >
            <Option value="name">파트너명</Option>
            <Option value="id">파트너아이디</Option>
          </Select>
          <Input
            type="text"
            value={searchTxt}
            onChange={handleSearchChange}
            style={{ width: 'calc(100% - 163px)' }}
            allowClear
          />
          <CustomButton style={{ width: '51px' }} onClick={handleSearchBtn}>검색</CustomButton>
        </TreeSearch>

        <div className="tree-box">
          {
            searchTreeData && !empty ? (
              <>
                <SearchTreeContainer
                  height="300px"
                  searchTxt={searchTxt}
                  treeData={searchTreeData}
                  expandedKeys={expandedKeys}
                  viewFlag={viewFlag}
                  onDoubleClick={onDoubleClickTree}
                  onChangeSelected={handleChangeSelectedNode}
                  selectedKeys={selectedKeys}
                />
              </>
            ) : (
              <>
                <EmptyWrap>
                  검색결과가 없습니다.
                  <br />
                  파트너명과 파트너아이디를 확인해 주시기 바랍니다.
                </EmptyWrap>
              </>
            )
          }
        </div>
      </PatnerTreeModal>
    </>
  );
}

const Container = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  
`;

const TreeSearch = styled.div`
  font-size: 12px;
  display: flex;
  justify-content: space-between;
  // padding: 10px;

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    padding: 0 8px;
  }
`;

const CustomButton = styled(Button)`
  font-size: 12px;
`;

const CustomTreeButton = styled(Button)`
  display: flex;
  align-items: center;
  position: relative;
  width: 216px;
  height: 36px;
  font-weight: 400;
  color: var(--color-gray-900);
  background:var(--color-white);
  border: var(--border-default);
  border-radius: 64px !important;
  text-align: left;
  padding-right: 8px;
  &:hover {
    color: var(--color-gray-900);
    border:1px solid var(--color-gray-300);
    background-color:var(--color-gray-50)
    svg {
      fill: var(--color-gray-900);
    }
  }
  
  & > span {
    display: flex;
    flex: 1;
    font-size: 14px;
    line-height: 20px;
  }

  .anticon {
    flex: 0;
  }
`;

const PatnerTreeModal = styled(Modal)`
.disabled {
        color: rgba(0,0,0,.25);
      }
  max-width: fit-content;
  width: auto !important;
  .modal-helper {
    color: var(--color-gray-700);
    padding-bottom: 10px;
  }
  .ant-modal-body {
    padding: 0 20px 0;
  }
  .descText {
    p {
      color: var(--color-gray-500);
    }
  }
  .tree-box {
    margin-top: 10px;
    background:var(--color-white);
    border: 1px solid #E3E4E7;
    box-sizing: border-box;
    height: 90%;
    overflow: auto;
  }

  .ant-modal-footer {
    padding: 20px 0 20px;
    border-top: 0px;
    button {
      width:140px;
      height:40px;
    }
    button:disabled {
      background-color: #E3E4E7;
      border-color: #E3E4E7;
      color:var(--color-white);
    }
  }
`;

const EmptyWrap = styled.div`
  min-height: 250px;
  text-align: center;
  padding-top: 80px;
`;

export default Nav;
