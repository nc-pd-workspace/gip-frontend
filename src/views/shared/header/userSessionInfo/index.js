import { useCallback, useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Menu, Dropdown, Statistic } from 'antd';

import Button from '../../../../components/button';
import Images, { ChevronRight } from '../../../../Images';
import { logout, expiredTimeUpdate } from '../../../../redux/commonReducer';

import { PageTypes } from '../../../../constants/pageType';

import { usePageTab } from '../../pageTab/hooks/usePageTab';
import { confirmMessage } from '../../../../components/message';
import { onReissue } from '../../../../api';

function UserSessionInfo() {
  const dispatch = useDispatch();
  const { userInfo, expiredTime } = useSelector((state) => state.common);
  const { openPage } = usePageTab();
  const refCountdown = useRef();

  const [messageView, setMessageView] = useState(false);
  const [tokenTick, setTokenTick] = useState(true);
  const [remainingTime, setRemainingTime] = useState([]);

  const { Countdown } = Statistic;
  const onClickLogout = () => {
    confirmMessage('로그아웃 하시겠어요?', () => {
      dispatch(logout());
    });
  };

  // safari대응용 날짜 변환
  const parseDate = (s) => {
    const dateTime = s.split(' ');
    const dateBits = dateTime[0].split('-');
    const timeBits = dateTime[1].split(':');
    return new Date(dateBits[0], parseInt(dateBits[1], 10) - 1, dateBits[2], timeBits[0], timeBits[1], timeBits[2]).valueOf();
  };

  const onClickMenu = (e) => {
    if (e.key === 'mypage') {
      openPage(PageTypes.ACCOUNT_MY_PAGE);
    } else if (e.key === 'mypartner') {
      openPage(PageTypes.ACCOUNT_MY_PARTNER_PAGE);
    }
  };
  const timeChange = async () => {
    const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
    if (sessionUserInfo === null) {
      countdownFinish();
      return;
    }
    const userInfoObject = JSON.parse(sessionUserInfo);
    const updateTime = parseDate(userInfoObject.refreshTokenExpiredDate);
    dispatch(expiredTimeUpdate(updateTime));
  };

  const onBodyClickCheck = async (event) => {
    const elementName = event.target.tagName;
    const parentNodeName = event.target.parentElement?.nodeName;
    if (
      (
        elementName === 'A'
        || elementName === 'TD'
        || elementName === 'BUTTON'
        || elementName === 'a'
        || elementName === 'td'
        || elementName === 'button'
        || parentNodeName === 'BUTTON'
        || parentNodeName === 'button'
      ) && tokenTick
    ) {
      await setTokenTick(false);
      await onReissue();
      timeChange();
      await setTokenTick(true);
    }
  };
  const onExpired = async () => {
    await setTokenTick(false);
    confirmMessage('로그인 시간을 2시간 연장하시겠어요?', async () => {
      await onReissue();
      timeChange();
      await setTokenTick(true);
    }, '예', '아니오', () => {
      setTokenTick(true);
    });
  };

  const menu = (
    <PopMenu onClick={onClickMenu}>
      {
        Object.keys(userInfo).length > 0 && ['RL0003', 'RL0004', 'RL0005'].indexOf(userInfo.userInfo.roleCd) === -1 ? (
          <>
            <Menu.Item key="mypage">
              <p>내 정보</p>
              <ChevronRight width="16" height="16" fill="var(--color-gray-900)" />
            </Menu.Item>
            <Menu.Item key="mypartner">
              <p>파트너 정보</p>
              <ChevronRight width="16" height="16" fill="var(--color-gray-900)" />
            </Menu.Item>
          </>
        ) : (<></>)
      }
      <Menu.Item key="3"><Button onClick={onClickLogout} width="131">로그아웃</Button></Menu.Item>
    </PopMenu>
  );

  const countdownUpdate = (val) => {
    if (val > 7199000) {
      setMessageView(false);
    } else if (val > 299000 && val < 300000) {
      setMessageView(true);
    }
    if (val < 300000) {
      refCountdown.current.style.visibility = 'visible';
      const remainingCalculate = val / 1000;
      const remainingMinutes = Math.floor(remainingCalculate / 60);
      const remainingSeconds = Math.floor(remainingCalculate - (remainingMinutes * 60));
      setRemainingTime([remainingMinutes, remainingSeconds]);
    } else {
      refCountdown.current.style.visibility = 'hidden';
    }
  };

  const countdownFinish = () => {
    dispatch(logout('로그인 시간이 만료되어 자동으로 로그아웃 되었습니다.\n로그인 후 사용해 주세요.'));
  };

  useEffect(() => {
    const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
    if (sessionUserInfo !== null) {
      const userInfoObject = JSON.parse(sessionUserInfo);
      const updateTime = parseDate(userInfoObject.refreshTokenExpiredDate);
      dispatch(expiredTimeUpdate(updateTime));
    }
  }, []);

  useEffect(() => {
    document.body.addEventListener('click', onBodyClickCheck);
    return () => {
      document.body.removeEventListener('click', onBodyClickCheck);
    };
  });

  const onClickCancelBtn = async () => {
    await setTokenTick(false);
    setMessageView(false);
    await setTokenTick(true);
  };

  const TimeLeftBox = useCallback(() => (
    <UserLogoutCheck>
      <div className="countdownWrap" ref={refCountdown}>
        <Countdown
          value={expiredTime}
          onChange={countdownUpdate}
          format="mm:ss"
          onFinish={countdownFinish}
        />
        <button id="expiredButton" onClick={onExpired}>연장</button>
      </div>
    </UserLogoutCheck>
  ), [expiredTime]);

  return (
    <Container>
      <TimeLeftBox />
      <CustomDropdown overlay={menu} trigger={['click']} overlayStyle={{ marginTop: '20px' }}>
        <div>
          { userInfo && userInfo.userInfo ? `${userInfo.userInfo.usrNm}님` : '' }
          <img src={Images.arrow_user_down} alt="my info" />
        </div>
      </CustomDropdown>
      {
        messageView && (
          <MessageBox>
            <MessageBackground>
              <TextWrap>
                {remainingTime[0]}
                분
                {' '}
                {remainingTime[1]}
                초 후 자동으로 로그아웃됩니다.
                <br />
                로그인 시간을 2시간 연장하시겠어요?
              </TextWrap>
              <ButtonWrap>
                <CancelButton width="140" height="40" onClick={onClickCancelBtn}>아니오</CancelButton>
                <Button width="140" height="40" type="fillBlue">예</Button>
              </ButtonWrap>
            </MessageBackground>
          </MessageBox>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  display: flex;
`;
const CustomDropdown = styled(Dropdown)`
  position: relative;
  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 14px;
  display: flex;
  align-items: center;
  text-align: right;
  background: #fff;
  color: var(--color-gray-900);
  border: 0;
  cursor: pointer;
  img {
    margin-left: 7px;
  }
`;

const PopMenu = styled(Menu)`
  margin-top: 8px;
  margin-right: -6px;
  &.ant-dropdown-menu {
    padding: 10px 0 0;
    box-shadow: 0px 4px 8px rgba(55, 57, 61, 0.06);
    border-radius: 4px;
    border: 1px solid #EFF1F4;
  }
  .ant-dropdown-menu-item:last-child:hover,
  .ant-dropdown-menu-submenu-title:last-child:hover {
    background-color: transparent;
  }
  .ant-dropdown-menu-item {
    padding: 8px 16px;
    height: 48px;
  }
  .ant-dropdown-menu-item:last-child {
    padding: 10px 16px 16px;
    height: auto;
  }
  .ant-dropdown-menu-title-content {
    display: flex;
    align-items: center;
    p {
      flex:1 1 auto;
      font-size: 13px;
      color: #000000;
    }
    svg {
      margin-right:-5px;
    }
  }
`;
const UserLogoutCheck = styled.div`
  .countdownWrap{
    background-image: url(${Images.clock});
    background-position: left center;
    background-repeat: no-repeat;
    background-size: 16px 16px;
    padding-left: 18px;
    flex: 0 0 auto;
    display: flex;
    align-items: center;
    margin-right: 10px;
    visibility: hidden;
  }
  .ant-statistic-content {
    font-size: 12px;
    color: var(--color-gray-500);
  }
  button {
    border: none;
    background-color: transparent;
    text-decoration: underline;
    font-size:12px;
    color: var(--color-gray-500);
    padding: 0;
    margin:0 0 0 4px;
  }
`;
const MessageBox = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.45);
  z-index: 10000;
  display:flex;
  align-items: center;
  justify-content: center;
`;

const MessageBackground = styled.div`
  width: 360px;
  padding: 30px 20px 20px;
  background:var(--color-white);
  box-shadow: 0px 14px 12px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
`;

const TextWrap = styled.span`
  display: inline-block;
  font-size: 13px;
  line-height: 20px;
  color: #000000;
  margin-bottom: 30px;
  white-space: pre-line;
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: center;
`;

const CancelButton = styled(Button)`
  margin-right: 10px;
`;

export default UserSessionInfo;
