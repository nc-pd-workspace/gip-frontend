import styled from 'styled-components';

import Images from '../../../Images';
import PageLink from '../pageTab/components/PageLink';
import { PageTypes } from '../../../constants/pageType';

function Logo() {
  return (
    <Container>
      <PageLink to={PageTypes.MAIN}>
        <img src={Images.logo} alt="브랜드 로고" />
      </PageLink>
      <BetaLogo />
    </Container>
  );
}

const Container = styled.div`
  width: 258px;
  flex-shrink: 0;
  margin-top: 4px;
`;
const BetaLogo = styled.div`
  background-image:url(${Images.iconBeta});
  background-size:36px 16px;
  position:absolute;
  width: 36px;
  height: 16px;
  top: 22px;
  left: 167px;
`;
export default Logo;
