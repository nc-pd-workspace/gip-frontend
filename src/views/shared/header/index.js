import styled from 'styled-components';

import Logo from './Logo';
import Nav from './nav';
import UserSessionInfo from './userSessionInfo';

function Header({ className }) {
  return (
    <Container className={className}>
      <Logo />
      <Nav />
      <UserSessionInfo />
    </Container>
  );
}

const Container = styled.div`
  position: fixed;
  top: -60px;
  left: 0;
  right: 0;
  z-index: 999;
  display: flex;
  align-items: center;
  height: 60px;
  padding: 0 30px 0 22px;
  background-color: #fff;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.02);
  transition: var(--header-time);
  overflow: hidden;
  &.headerVisible {
    top: 0;
  }
`;

export default Header;
