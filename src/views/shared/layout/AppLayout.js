/* eslint-disable func-names */
import { useEffect, useLayoutEffect, useRef } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import Header from '../header';
import Sidebar from '../sidebar';
import PageTab from '../pageTab';

import { usePageTab } from '../pageTab/hooks/usePageTab';
import PageContainer from '../../viewFrame/PageContainer';

import { getHeaderPartnerList, updateDefaultDate, headerToggleChange } from '../../../redux/commonReducer';

let beforeScrollY = 0;
let currentScrollY = 0;
let setTimeScroll = null;
let headerOnOff = true;
let moveTopPosition = 0;

function AppLayout({ location }) {
  const dispatch = useDispatch();
  const { openedPages, headerToggle } = useSelector((state) => ({
    openedPages: state.common.openedPages,
    headerToggle: state.common.headerToggle,
    activePageId: state.common.activePageId,
  }));
  const headerVisible = !headerToggle && 'headerVisible';
  const refViewContents = useRef();

  const { id: locationPageId } = useParams();
  const { openFirstPage } = usePageTab();

  let ticking = false;
  function optimizeAnimation(callback) {
    return () => {
      if (!ticking) {
        ticking = true;
        requestAnimationFrame(() => {
          callback();
          ticking = false;
        });
      }
    };
  }
  function onScroll() {
    clearTimeout(setTimeScroll);

    // 스크롤된 화면의 크기가 header의 크기보다 적으면 스크롤 실행 x
    if ((refViewContents.current.scrollHeight - window.innerHeight) < 0) {
      headerOnOff = false;
      dispatch(headerToggleChange(headerOnOff));
      return;
    }

    currentScrollY = window.scrollY;
    setTimeScroll = setTimeout(() => {
      if (beforeScrollY === currentScrollY) return;
      if (beforeScrollY < currentScrollY) {
        headerOnOff = true;
        moveTopPosition = 60;
      } else {
        headerOnOff = false;
        moveTopPosition = 0;
      }
      if (currentScrollY < 60) {
        window.scrollTo({
          top: moveTopPosition,
          behavior: 'smooth',
        });
      }
      dispatch(headerToggleChange(headerOnOff));
      beforeScrollY = currentScrollY;
    }, 50);
  }

  useEffect(() => {
    dispatch(updateDefaultDate());
    if (location && location.search !== '') {
      // const query = queryString.parse(location.search);
      const arr = location.search.substr(1).split('&');
      const query = {};
      arr.forEach((item) => {
        const str = item.split('=');
        if (str.length > 1) {
          const [key, value] = str;
          query[key] = value;
        }
      });

      openFirstPage(locationPageId, query);
    } else openFirstPage(locationPageId);

    window.addEventListener(
      'scroll',
      optimizeAnimation(() => {
        onScroll();
      }),
      { passive: true },
    );
  }, []);

  useLayoutEffect(() => {
    dispatch(getHeaderPartnerList());
  }, []);

  return (
    <>
      <Header className={headerVisible} />
      <Sidebar className={headerVisible} />

      <Contents className={headerVisible} ref={refViewContents}>
        {
          openedPages.map(({ id, query }) => <PageContainer key={id} pageId={id} query={query} />)
        }
      </Contents>

      <PageTab />
    </>
  );
}

const ViewContainer = styled.div`
  -webkit-overflow-scrolling: auto;
  overscroll-behavior-y: none;
`;

const Contents = styled.div`

  flex: 1;
  padding-left: 250px;
  background-color: var(--color-lightGray01);
  -webkit-overflow-scrolling: auto;
  overscroll-behavior-y: none;
  /* &.headerVisible {
    padding-top: 0;
    margin-top: 60px;
  } */
`;

export default AppLayout;
