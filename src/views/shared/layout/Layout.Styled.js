import styled from 'styled-components';

export const PageLayout = styled.div`
  min-width: var(--contents-minWidth);
  max-width: var(--contents-maxWidth);
  padding: 30px 30px 74px 30px;
`;
