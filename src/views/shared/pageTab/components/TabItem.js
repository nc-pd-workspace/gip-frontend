import React, { useRef, useState } from 'react';

import styled from 'styled-components';
import cn from 'classnames';

import { SvgCloseX } from '../../../../Images';

function TabItem({
  id,
  name,
  selectPage,
  closePage,
  activePageId,
}) {
  const refName = useRef();
  const refTooltip = useRef();
  const refTooltipText = useRef();
  const [tooltipDisplay, setTooltipDisplay] = useState(false);
  const [tooltipAnimation, setTooltipAnimation] = useState();
  let hideAnimation;

  const showTooltip = async () => {
    await setTooltipDisplay(true);
    clearTimeout(hideAnimation);
    const rectElement = refName.current.getBoundingClientRect();
    const tooltipElement = refTooltipText.current.getBoundingClientRect();

    // 소숫점 차이로 인해 Tooltip 사이즈와 직접비교
    if (tooltipElement.width > rectElement.width) {
      setTooltipAnimation('show');
    }
  };

  const hideTooltip = () => {
    if (refTooltip !== null) {
      setTooltipAnimation('');
    }
    hideAnimation = setTimeout(() => setTooltipDisplay(false), 300);
  };

  return (
    <TabView
      key={id}
      className={cn({ isActiveTab: id === activePageId, mainTab: id === 'main' })}
      onMouseEnter={showTooltip}
      onMouseLeave={hideTooltip}
    >
      <Name onClick={() => selectPage(id)}>
        {
          tooltipDisplay && (
            <Tooltip ref={refTooltip} className={cn('Tooltip', tooltipAnimation)}>
              <span ref={refTooltipText}>{name}</span>
            </Tooltip>
          )
        }
        <a className="name" ref={refName}>
          {name}
        </a>
      </Name>
      {
        (id === 'main') ? false
          : (
            <ButtonClose onClick={() => closePage(id)}>
              <SvgCloseX className="svgCloseX" />
            </ButtonClose>
          )
      }
    </TabView>
  );
}

const TabView = styled.div`
  position: relative;
  display: flex;
  height: 40px;
  font-size: 12px;
  flex-basis: fit-content;
  cursor: pointer;
  background: var(--color-gray-200);
  min-width:20px;
  padding-right: 36px;
  &.mainTab {
    padding-right: 15px;
  }
  &:hover {
    background: rgba(255, 255, 255, 0.4);
  }
  .svgCloseX {
    width: 16px;
    height: 16px;
    fill: var(--color-gray-500);
  }
  div {
    a {
      color: #888;
    }
  }
  &.isActiveTab {
    background: #fff;
    flex-shrink: 0;
    flex-basis: fit-content !important;

    div a {
      color: #111;
      font-weight: 600;
    }
    .svgCloseX {
      width: 16px;
      height: 16px;
      fill: #111;
    }
    .Tooltip {
      display: none;
    }
  }
  &:last-child {
    .Tooltip {
      left: auto;
      right: 2px;
    }
  }
`;

const Name = styled.div`
  flex: 0 1 min-content;
  min-width: 10px;
  display: flex;
  padding-left: 15px;
  align-items: center;
  overflow:hidden;
  > a {
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

const ButtonClose = styled.div`
  position: absolute;
  right:9px;
  padding: 1px;
  top: 50%;
  margin-top: -9px;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  border-radius: 50%;
  height: 18px;
  width: 18px;
  &:hover, &:focus {
    background-color: var(--color-gray-200);
  }
`;

const Tooltip = styled.div`
  position: absolute;
  left:4px;
  top: -34px;
  border-radius:4px;
  line-height: 16px;
  padding:9px 12px 8px;
  white-space: nowrap;
  border: var(--border-default);
  box-shadow: 0px 2px 6px 0px #0000000D;
  background-color: #FFF;
  color: var(--color-gray-700);
  opacity: 0;
  transition: all .25s ease-out;
  &.show {
    top: -40px;
    opacity: 1;
  }
`;

export default TabItem;
