import styled from 'styled-components';

function Footer() {
  return (
    <Container>
      Footer
    </Container>
  );
}

const Container = styled.div`
  background:#ddd;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default Footer;
