import styled from 'styled-components';
import React, { useState, useEffect } from 'react';
import { Tree } from 'antd';

const { DirectoryTree } = Tree;

function TreeContainer({ treeData, expandedKeys, onChangeSelected }) {
  const [selectedItem, setSelectedItem] = useState();
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [expandedKey, setExpandedKey] = useState([]);
  const [highlight, setHighlight] = useState([]);

  useEffect(() => {
    setExpandedKey([...expandedKeys]);
    setAutoExpandParent(true);
  }, [expandedKeys]);

  useEffect(() => {
    onChangeSelected(selectedItem);
  }, [selectedItem]);

  const onSelect = (selectedKeys, info) => {
    setSelectedItem(info.selectedNodes[0]);
    setHighlight(selectedKeys);
  };

  const onExpand = (expandedKeysData) => {
    setAutoExpandParent(false);
    setExpandedKey(expandedKeysData);
  };

  return (
    <Container>
      <div className="ant-divider ant-divider-horizontal" role="separator" />
      <TreeWrap className="treeBoxWrap">
        <DirectoryTree
          showLine={{ showLeafIcon: false }}
          expandedKeys={expandedKey}
          autoExpandParent={autoExpandParent}
          defaultExpandAll
          onExpand={onExpand}
          onSelect={onSelect}
          treeData={treeData}
          showIcon={false}
          selectedKeys={highlight}
        />
      </TreeWrap>
    </Container>
  );
}

const Container = styled.div`
  height: ${(props) => props.height};
  overflow: auto;

  .ant-tree {
    white-space: nowrap;
    font-size: 13px;
  }

  .ant-tree-show-line .ant-tree-switcher {
    background-color: transparent;
  }

  .ant-tree.ant-tree-directory .ant-tree-treenode-selected .ant-tree-switcher {
    color: #000;
  }

  .ant-tree.ant-tree-directory
    .ant-tree-treenode
    .ant-tree-node-content-wrapper.ant-tree-node-selected {
    color: var(--color-blue-500);
    font-weight: 700;
  }

  .ant-tree.ant-tree-directory .ant-tree-treenode-selected:hover::before,
  .ant-tree.ant-tree-directory .ant-tree-treenode-selected::before {
    background: var(--color-blue-50);
    color: var(--color-blue-500);
    font-weight: 700;
  }
  .ant-tree-indent-unit {
    width: 20px;
    display: inline-block;
  }

  .ant-tree-switcher {
    width: 20px;
  }

  .site-tree-search-value {
    color: var(--color-yellow-500);
  }

  .highlight{
    font-weight: 700;
  }

  .ant-divider-horizontal {
    margin: 0 10px;
    width: unset;
    min-width: unset;
  }
`;

const TreeWrap = styled.div`
  height: 252px;
  overflow: auto;
  border-radius: 0 0 3px 3px;
`;

export default TreeContainer;
