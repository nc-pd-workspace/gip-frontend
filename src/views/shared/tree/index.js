import styled from 'styled-components';

import TreeContainer from './containers/TreeContainer';

function Tree({ treeData, expandedKeys, onChangeSelected }) {
  return (
    <Container>
      <TreeContainer
        treeData={treeData}
        expandedKeys={expandedKeys}
        onChangeSelected={onChangeSelected}
      />
    </Container>
  );
}

const Container = styled.div`
  h1 {
    white-space: pre-line;
  }
  overflow: scroll !important;
  height: 288px;
`;

export default Tree;
