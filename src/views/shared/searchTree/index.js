import styled from 'styled-components';

import SearchTreeContainer from './containers/SearchTreeContainer';

function Tree({ searchTxt, treeData, expandedKeys, onChangeSelected }) {
  return (
    <Container>
      <SearchTreeContainer
        searchTxt={searchTxt}
        treeData={treeData}
        expandedKeys={expandedKeys}
        onChangeSelected={onChangeSelected}
      />
    </Container>
  );
}

const Container = styled.div`
  h1 {
    white-space: pre-line;
  }
  overflow: scroll !important;
  height: 404px;
`;

export default Tree;
