import styled from 'styled-components';
import React, { useState, useEffect, useCallback } from 'react';
import { Tree } from 'antd';

import { treeStyle } from '../../../../styles/Tree';
import { alertMessage } from '../../../../components/message';

const { DirectoryTree } = Tree;

function SearchTreeContainer({ treeData, expandedKeys, viewFlag, onChangeSelected, onDoubleClick, selectedKeys, height = '300px' }) {
  const [selectedItem, setSelectedItem] = useState();
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [expandedKey, setExpandedKey] = useState([]);

  useEffect(() => {
    setExpandedKey([...expandedKeys]);
    setAutoExpandParent(true);
  }, [expandedKeys]);

  useEffect(() => {
    if (!selectedItem) {
      return;
    }
    onChangeSelected(selectedItem);
  }, [selectedItem]);

  useEffect(() => {
    // 초기화
    setSelectedItem(selectedItem);
  }, [viewFlag, selectedItem]);

  const onSelect = useCallback((keys, info) => {
    if (keys.some((key) => key.includes('disabled'))) {
      alertMessage('내 파트너와 하위 파트너 정보만 확인할 수 있습니다.');
      return;
    }
    onChangeSelected(info.selectedNodes[0]);
  }, [viewFlag]);

  const onExpand = (expandedKeysData) => {
    setAutoExpandParent(false);
    setExpandedKey(expandedKeysData);
  };

  return (
    <Container height={height} className="treeBoxContainer">
      <DirectoryTree
        showLine={{ showLeafIcon: false }}
        expandedKeys={expandedKey}
        autoExpandParent={autoExpandParent}
        onExpand={onExpand}
        onSelect={onSelect}
        onDoubleClick={onDoubleClick}
        selectedKeys={selectedKeys}
        treeData={treeData}
        showIcon={false}
      />
    </Container>
  );
}

const Container = styled.div`
  height: ${(props) => props.height};
  overflow: auto;

  ${treeStyle}
`;

export default SearchTreeContainer;
