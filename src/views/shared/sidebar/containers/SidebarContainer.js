import { useState, useEffect } from 'react';
import styled from 'styled-components';

import { useSelector } from 'react-redux';

import Nav from '../components/Nav';
import CopyRight from '../components/SideButton';

import { usePageTab } from '../../pageTab/hooks/usePageTab';

import { menuIcon } from '../constants';

function SidebarContainer() {
  const { activePageId, userInfo } = useSelector((state) => state.common);
  const [menu, setMenu] = useState([]);

  const { openPage } = usePageTab();

  const selectMenu = (id) => {
    openPage(id, {}, true);
  };

  const openMainPage = () => {
    openPage('main');
  };

  const loopFuncMenu = (itemList) => itemList.map((item) => ({
    id: item.linkUrl === null ? item.menuId : item.linkUrl.substr(1),
    name: item.menuNm,
    ...(menuIcon[item.menuId] ? { icon: menuIcon[item.menuId] } : {}),
    ...(item.linkUrl === null ? { openClose: (item.menuId === 'MN0004' || item.menuId === 'MN0005') ? 'close' : 'open' } : {}),
    ...(item.subMenu && item.subMenu.length > 0 ? { subMenu: loopFuncMenu(item.subMenu) } : {}),
  }));

  const createMenu = (menuList) => {
    let arr = [];

    menuList.forEach((item) => {
      arr = arr.concat(loopFuncMenu([item]));
    });
    setMenu(arr);
  };

  useEffect(() => {
    if (Object.keys(userInfo).length > 0) {
      createMenu(userInfo.menuList);
    }
  }, [userInfo]);

  return (
    <Container className="sideNav">
      <Nav
        menu={menu}
        openMainPage={openMainPage}
        activePageId={activePageId}
        onClickMenu={selectMenu}
      />
      <CopyRight />
    </Container>
  );
}

const Container = styled.div`
  min-height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  transition: var(--header-time);
`;

export default SidebarContainer;
