import Images from '../../../Images';

export const menu = [
  {
    id: 'brand',
    name: '브랜드 현황',
    icon: <img src={Images.performance} alt="" />,
    openClose: 'open',
    subMenu: [
      {
        id: 'brandLandscape',
        name: '브랜드 현황',
      },
      {
        id: 'topBrand',
        name: 'TOP10 브랜드 현황',
      },
    ],
  },
  {
    id: 'customer',
    name: '고객 활동 분석',
    icon: <img src={Images.customer_grey} alt="" />,
    openClose: 'open',
    subMenu: [
      {
        id: 'allActivityCustomer',
        name: '전체 활동고객 분석',
      },
      {
        id: 'newCustomer',
        name: '신규 활동고객 분석',
      },
      {
        id: 'stepCustomer',
        name: '단계별 활동고객 분석',
      },
      {
        id: 'top5BrandCustomer',
        name: 'TOP5 브랜드 활동고객 분석',
      },
    ],
  },
  {
    id: 'period',
    name: '기간별 성과 분석',
    icon: <img src={Images.activity} alt="" />,
    openClose: 'open',
    subMenu: [
      {
        id: 'brandPerformance',
        name: '브랜드 성과 분석',
      },
      {
        id: 'stepConversion',
        name: '단계별 고객 전환 분석',
      },
      {
        id: 'productCustomer',
        name: '상품별 고객 분석',
      },
      {
        id: 'inflowKeyword',
        name: '유입 검색어 분석',
      },
    ],
  },
  {
    id: 'account',
    name: '마이페이지',
    icon: <img src={Images.account} alt="" />,
    openClose: 'close',
    subMenu: [
      {
        id: 'myPage',
        name: '내 정보',
      },
      {
        id: 'myPartnerPage',
        name: '파트너 정보',
      },
    ],
  },
  {
    id: 'settings',
    name: '시스템 관리',
    icon: <img src={Images.setting} alt="" />,
    openClose: 'close',
    subMenu: [
      {
        id: 'partners',
        name: '파트너 관리',
      },
      {
        id: 'users',
        name: '사용자 관리',
      },
    ],
  },
];
