import styled from 'styled-components';

import React, { useEffect, useState } from 'react';

import SubNav from './SubNav';
import ArrowToggle from '../../../../components/arrowToggle';

function NavItem({
  name, icon, openClose, subMenu, activePageId, onClickMenu,
}) {
  const [openMenu, setOpenMenu] = useState(true);

  useEffect(() => {
    if (openClose === 'close') {
      setOpenMenu(false);
    }
  }, []);

  useEffect(() => {
    if (subMenu) {
      const arr = subMenu.filter((v) => v.id === activePageId);
      if (arr.length > 0) setOpenMenu(true);
    }
  }, [activePageId]);

  const toggleMenu = () => {
    setOpenMenu((v) => !v);
  };

  return (
    <Container>
      <Name onClick={toggleMenu}>
        <span>
          {icon && icon}
        </span>
        {name}

        {
          subMenu
          && <StyledArrowToggle open={openMenu} />
        }
      </Name>

      {
        subMenu && openMenu
        && (
          <SubNav
            subMenu={subMenu}
            activePageId={activePageId}
            onClickMenu={onClickMenu}
          />
        )
      }
    </Container>
  );
}

const Container = styled.div`
`;

const Name = styled.a`
  display: flex;
  align-items: center;
  color: var(--color-gray-500);
  padding: 0 20px;
  height: 40px;
  font-size: 12px;
  font-weight: 700;
  line-height: 20px;
  cursor:pointer;
  position: relative;
  span {
    display: flex;
    margin-right: 6px;
  }
  &:hover, &:focus, &:active {
    color: var(--color-gray-500);
  }
`;

const StyledArrowToggle = styled(ArrowToggle)`
  position:absolute;
  left: 214px;
  top: 0;
  bottom: 0;
  display: flex;
  align-items: center;
`;

export default NavItem;
