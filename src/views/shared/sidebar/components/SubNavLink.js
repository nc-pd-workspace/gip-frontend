import styled from 'styled-components';
import cn from 'classnames';

function SubNavLink({ name, isActive, onClickMenu }) {
  return (
    <Container
      className={cn({ isActive })}
      onClick={onClickMenu}
    >
      {name}
    </Container>
  );
}

const Container = styled.a`
  display: flex;
  align-items: center;
  height: 35px;
  padding: 0 42px;
  color: #fff;
  font-size: 13px;
  font-weight: 400;
  cursor:pointer;
  &:hover ,&:focus, &:active {
    color: #fff;
    background: linear-gradient(0deg, rgba(17, 17, 17, 0.25), rgba(17, 17, 17, 0.25));
  }
  &.isActive {
    font-weight: 700;
    background: linear-gradient(0deg, rgba(17, 17, 17, 0.25), rgba(17, 17, 17, 0.25));
  }
`;

export default SubNavLink;
