import styled from 'styled-components';

import SubNavLink from './SubNavLink';

function SubNav({ subMenu, activePageId, onClickMenu }) {
  return (
    <Container>
      {
        subMenu.map((item) => (
          <SubNavLink
            key={item.name}
            name={item.name}
            isActive={item.id === activePageId}
            onClickMenu={() => onClickMenu(item.id)}
          />
        ))
      }
    </Container>
  );
}

const Container = styled.div`
  padding-bottom: 10px;
`;

export default SubNav;
