import styled from 'styled-components';
import cn from 'classnames';

import React from 'react';

import NavItem from './NavItem';
import Images from '../../../../Images';

function Nav({
  menu, activePageId, openMainPage, onClickMenu, className,
}) {
  return (
    <Container className={className}>
      <HomeButton
        onClick={openMainPage}
        className={cn({ isActive: activePageId === 'main' })}
      >
        <span>
          <img src={Images.home} alt="button home" />
        </span>
        대시보드 메인
      </HomeButton>
      {
        menu.map(({ name, icon, openClose, subMenu }) => (
          <NavItem
            key={name}
            name={name}
            icon={icon}
            openClose={openClose}
            subMenu={subMenu}
            activePageId={activePageId}
            onClickMenu={onClickMenu}
          />
        ))
      }
    </Container>
  );
}

const Container = styled.div`
  padding-bottom: 12px;
`;

const HomeButton = styled.div`
  display: flex;
  align-items: center;
  color:#fff;
  font-size: 12px;
  font-weight: 700;
  line-height: 20px;
  padding: 10px 20px;
  cursor:pointer;
  &.isActive {
    background: linear-gradient(0deg, rgba(17, 17, 17, 0.25), rgba(17, 17, 17, 0.25));
  }
  span {
    display: flex;
    margin-right: 8px;
    img {
      width: 16px;
      height: 16px;
    }
  }
`;

export default Nav;
