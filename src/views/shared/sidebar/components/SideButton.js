import styled from 'styled-components';

import { useSelector } from 'react-redux';

import Button from '../../../../components/button';
import Images from '../../../../Images';

function CopyRight() {
  const { userInfo } = useSelector((state) => state.common);

  const onAdminManual = () => {
    window.open('/documents/관리자_매뉴얼_V1.0.pdf');
  };
  const onUserManual = () => {
    window.open('/documents/사용자_매뉴얼_V1.0.pdf');
  };

  return (
    <Container>
      {/* <Button type="outlineGray" width="210" size="small">
        <img src={Images.iconHelp} alt="" />
        GIP 도움말
      </Button> */}
      {
        Object.keys(userInfo).length > 0 && ['RL0003', 'RL0004', 'RL0005'].indexOf(userInfo?.userInfo.roleCd) !== -1
          && (
            <Button type="outlineGray" width="210" size="small" onClick={onAdminManual}>
              <img src={Images.iconFileMaster} alt="" />
              관리자 매뉴얼
            </Button>
          )
      }
      <Button type="outlineGray" width="210" size="small" onClick={onUserManual}>
        <img src={Images.iconFile} alt="" />
        사용자 매뉴얼
      </Button>
    </Container>
  );
}

const Container = styled.div`
  padding-bottom: 20px;
  line-height: 14px;
  padding-left: 20px;
  font-size: 12px;
  button + button {
    margin-top: 10px;
  }
`;
export default CopyRight;
