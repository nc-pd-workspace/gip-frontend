import Images from '../../../../Images';

export const menuIcon = {
  MN0001: (<img src={Images.performance} alt="" />),
  MN0002: (<img src={Images.customer_grey} alt="" />),
  MN0003: (<img src={Images.activity} alt="" />),
  MN0004: (<img src={Images.account} alt="" />),
  MN0005: (<img src={Images.setting} alt="" />),
};
