import styled from 'styled-components';
import { Modal } from 'antd';
import { useState, useCallback, useEffect } from 'react';

function Confirm(props) {
  const { title, visible, onOk, onClose, okText, cancelText, content, values } = props;

  const [confirmVisible, setConfirmVisible] = useState(false);

  const handleOk = useCallback(() => {
    onOk({ ptnIdx: values.ptnIdx, partnerStatus: values.partnerStatus });
  }, [values]);

  useEffect(() => {
    setConfirmVisible(visible);
  }, [visible]);

  return (
    <Container
      title={title}
      visible={confirmVisible}
      onOk={handleOk}
      onCancel={onClose}
      okText={okText}
      cancelText={cancelText}
    >
      <p>{content}</p>
    </Container>
  );
}

const Container = styled(Modal)`
    width: 300px !important;
    .ant-modal-footer{
        padding: 7px;
    }
`;

export default Confirm;
