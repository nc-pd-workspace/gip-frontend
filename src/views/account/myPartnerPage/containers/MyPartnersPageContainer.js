/* eslint-disable no-lonely-if */
import styled from 'styled-components';

import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Input, Select, Tabs } from 'antd';

import Images, { SvgArrowDropdown } from '../../../../Images';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';
import PartnerDetailInfo from '../components/partnerDetailInfo';
import { getPartnerTree, updatePartnerInfo, getSaleChannel, getInfo, updateState, initPartnerTree, initPartnerUpdateStatus } from '../redux/slice';
import UserList from '../components/userList';
import SearchTreeContainer from '../../../shared/searchTree/containers/SearchTreeContainer';
import Button from '../../../../components/button';
import { getHeaderUserInfo, getHeaderPartnerList } from '../../../../redux/commonReducer';

import { alertMessage } from '../../../../components/message';
import { resetStore } from '../../../main/redux/slice';

const { Option } = Select;
const { TabPane } = Tabs;

function MyPartnersPageContainer({ query }) {
  const { detailInfo, partnerTree, saleChannel, partnerUpdateStatus } = useSelector((state) => ({
    detailInfo: state.account.myPartnerPage.partnerInfo,
    partnerTree: state.account.myPartnerPage.partnerTree,
    saleChannel: state.account.myPartnerPage.saleChannel.data,
    partnerUpdateStatus: state.account.myPartnerPage.partnerUpdateStatus,
  }));

  const { activePageId, resetSettings } = useSelector((state) => ({
    activePageId: state.common.activePageId,
    resetSettings: state.common.resetSettings,
  }));

  const { selectPtnIdx, userInfo } = useSelector((state) => state.common);

  const dispatch = useDispatch();

  const [searchOption, setSearchOption] = useState('MC000070');
  const [searchTxt, setSearchTxt] = useState('');
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [treeData, setTreeData] = useState([]);
  const [searchTreeData, setSearchTreeData] = useState([]);
  const [generateTreeData, setGenerateTreeData] = useState([]);
  const [viewFlag, setViewFlag] = useState();
  const [selectedNode, setSelectedNode] = useState();
  const [selectedKeys, setSelectedKeys] = useState([]);

  useEffect(() => {
    if (activePageId === 'myPartnerPage' && resetSettings) {
      // 화면 초기화
      reset();
    } else {
      dispatch(resetStore());
    }
  }, [activePageId]);

  useEffect(() => {
    dispatch(getPartnerTree({ params: selectPtnIdx }));
    dispatch(getSaleChannel());
    return () => {
      dispatch(initPartnerTree());
    };
  }, []);

  useEffect(() => {
    dispatch(updateState({ selectPtnIdx: userInfo.partnerList[0].key }));
    dispatch(updateState({ userPtnIdx: userInfo.userInfo.ptnIdx }));
  }, [userInfo]);

  useEffect(() => {
    if (treeData && treeData.length !== 0) {
      initSearch();
      const arr = [];
      generateData(treeData, arr);
      setGenerateTreeData(arr);
    }
  }, [treeData, query]);

  useEffect(() => {
    if (partnerTree && partnerTree.data && partnerTree.data.length !== 0 && partnerTree.data.length !== undefined) {
      dispatch(getHeaderPartnerList());
      setTreeData(partnerTree.data);
      setSearchTreeData(partnerTree.data);
    }
  }, [partnerTree]);

  useEffect(() => {
    if (partnerUpdateStatus.status === 'success') {
      dispatch(getPartnerTree());
      const params = {
        partnerIdx: selectedNode.partnerIdx,
      };
      dispatch(getInfo({ params }));
      dispatch(getHeaderUserInfo());
      alertMessage('파트너 정보 수정이 완료되었습니다.');
    }
  }, [partnerUpdateStatus]);

  useEffect(() => {
    let ptnId = null;
    if (query && Object.keys(query).length) {
      ptnId = query.ptnId;
    } else {
      const myArr = generateTreeData.filter((v) => v.partnerIdx === userInfo.userInfo.ptnIdx);
      if (myArr.length) ptnId = myArr[0].key;
    }
    const filterdArr = generateTreeData.filter((v) => v.key === ptnId);
    if (filterdArr.length > 0) {
      if (ptnId) {
        if (partnerUpdateStatus.status !== 'success') {
          handleChangeSelectedNode(filterdArr[0]);
        }
        const expandedArr = findParentKeys(ptnId, partnerTree.data);
        if (expandedArr.filter((v) => v !== ptnId).length === 0) {
          if (partnerUpdateStatus.status === 'success') {
            dispatch(initPartnerUpdateStatus());
          } else {
            setExpandedKeys([generateTreeData[0].key]);
          }
        } else {
          setExpandedKeys([ptnId]);
        }
      } else {
        setExpandedKeys([generateTreeData[0].key]);
      }
    } else {
      if (generateTreeData.length !== 0) {
        setExpandedKeys([generateTreeData[0].key]);
      }
    }
  }, [generateTreeData]);

  const reset = () => {
    dispatch(getPartnerTree({ params: selectPtnIdx }));
    dispatch(getSaleChannel());

    setViewFlag();
    setSearchOption('MC000070');
    setSearchTxt('');
    setExpandedKeys([]);
    setTreeData([]);
    setSearchTreeData([]);
    setGenerateTreeData([]);
    setSelectedNode();
    setSelectedKeys([]);
  };

  const initSearch = useCallback(() => {
    setSearchOption('MC000070');
    setSearchTxt();
  }, []);

  const findParentKeys = (key, tree) => {
    let parentKey = [];
    tree?.forEach((data) => {
      const node = data;
      let arr = [];
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          arr = [key];
        } else {
          arr = findParentKeys(key, node.children);
        }
      }
      if (arr.length > 0) parentKey = [node.key, ...arr];
    });
    return parentKey;
  };

  const generateData = useCallback(
    (param, arr) => {
      param.forEach((data) => {
        arr.push({ key: data.key, title: data.title, partnerIdx: data.partnerIdx });
        if (data.children) {
          generateData(data.children, arr);
        }
      });
    },
    [generateTreeData],
  );

  const getParentKey = (key, tree, type) => {
    let parentKey = '';
    tree.forEach((data) => {
      const node = data;
      if (node.children) {
        if (node.children.some((item) => {
          if (type === 'MC000060') {
            // 아이디
            return item.key === key;
          }
          if (type === 'MC000070') {
            // 이름
            return item.title === key;
          }
          if (type === 'MC000130') {
            // 번호
            return item.partnerIdx === key;
          }
          return null;
        })) {
          parentKey = node.key;
        } else if (getParentKey(key, node.children, type)) {
          parentKey = getParentKey(key, node.children, type);
        }
      }
    });

    return parentKey;
  };

  const handleChangeSelectedNode = useCallback((result) => {
    if (result === 'init') {
      setSelectedNode('');
      return;
    }
    setSelectedNode(result);
    // tree selected node 표현
    setSelectedKeys([result.key]);
    const params = {
      partnerIdx: result.partnerIdx,
    };

    dispatch(getInfo({ params }));
    setViewFlag('detail');
  }, []);

  const handleSearchChange = (e) => {
    setSearchTxt(e.target.value);
  };

  const handleSearchBtn = useCallback(() => {
    if (searchTxt === '' || !searchTxt) {
      setSearchTreeData(treeData);
      setExpandedKeys([]);
      return;
    }
    // filter tree
    const expandedArr = generateTreeData
      .map((item) => {
        if (searchOption === 'MC000060') {
          // 파트너 아이디
          if (item.key === searchTxt) {
            return getParentKey(item.key, treeData, searchOption);
          }
          return null;
        }
        if (searchOption === 'MC000070') {
          // 파트너 이름
          let titleWrap = '';
          if (item.title.props.children[0].props) {
            titleWrap = item.title.props.children[0].props.children;
            const titleWrapReverse = titleWrap.split('').reverse().join('');
            const title = titleWrapReverse.split('(');
            if (title[1].split('').reverse().join('').indexOf(searchTxt) > -1) {
              return getParentKey(item.title, treeData, searchOption);
            }
          } else {
            // eslint-disable-next-line prefer-destructuring
            titleWrap = item.title.props.children[0];
            if (titleWrap.indexOf(searchTxt) > -1) {
              return getParentKey(item.title, treeData, searchOption);
            }
          }
          return null;
        }
        // 파트너 번호

        if (searchOption === 'MC000130') {
          if (item.partnerIdx === Number(searchTxt)) {
            return getParentKey(item.partnerIdx, treeData, searchOption);
          }
          return null;
        }
        return null;
      })
      .filter((item, i, self) => item && self.indexOf(item) === i);
    setExpandedKeys(expandedArr);

    // 검색된 텍스트 하이라이트
    const loopSearchData = (data) => data.map((item) => {
      let index = '';
      if (searchOption === 'MC000060') {
        // 파트너 아이디
        if (item.key === searchTxt) {
          index = item.partnerIdx;
        } else {
          index = -1;
        }
      } else if (searchOption === 'MC000070') {
        // 파트너 이름
        let titleWrap = '';
        if (item.title.props.children[0].props) {
          titleWrap = item.title.props.children[0].props.children;
          const titleWrapReverse = titleWrap.split('').reverse().join('');
          const title = titleWrapReverse.split('(');
          index = title[1].split('').reverse().join('').indexOf(searchTxt);
        } else {
          // eslint-disable-next-line prefer-destructuring
          titleWrap = item.title.props.children[0];
          index = titleWrap.indexOf(searchTxt);
        }
      } else {
        // 파트너 번호
        if (item.partnerIdx === Number(searchTxt)) {
          index = item.partnerIdx;
        } else {
          index = -1;
        }
      }

      const title = index > -1 ? (
        <span className="highlight">
          {item.title}
        </span>
      ) : (
        <span>
          {item.title}
        </span>
      );
      if (item.children) {
        return { title, key: item.key, children: loopSearchData(item.children), partnerIdx: item.partnerIdx };
      }
      return {
        title,
        key: item.key,
        partnerIdx: item.partnerIdx,
      };
    });
    setSearchTreeData(loopSearchData(treeData));
  }, [searchTxt, treeData, generateTreeData, searchOption]);

  // 파트너 상세 저장
  const handlePartnerInfoSave = useCallback((values) => {
    values.ptnIdx = selectedNode.partnerIdx;
    const params = {
      dto: values,
    };
    dispatch(updatePartnerInfo({ params }));
  }, [selectedNode]);

  const handleSearchOptionChange = (value) => {
    setSearchOption(value);
  };

  return (
    <>
      <Container>
        <PageHeader title="파트너 정보" subTitle="" />
        <Contents style={{ paddingTop: 12 }}>
          <Paper border className="treeContainer">
            <div>
              <TreeSearch>
                <Select defaultValue="MC000070" onChange={handleSearchOptionChange} style={{ width: '100px', fontSize: '12px' }} suffixIcon={<SvgArrowDropdown />}>
                  <Option value="MC000070">파트너명</Option>
                  <Option value="MC000060">파트너 아이디</Option>
                </Select>
                <Input
                  type="text"
                  value={searchTxt}
                  onChange={handleSearchChange}
                  style={{ width: '100px', flex: '1 1 auto', margin: '0 6px' }}
                />
                <CustomButton style={{ width: '37px', flex: '1 0 37px', padding: 0 }} onClick={handleSearchBtn}>검색</CustomButton>
              </TreeSearch>
            </div>
            <div className="ant-divider ant-divider-horizontal" role="separator" />

            {/* 검색 트리 */}
            {
              searchTreeData && (
                <SearchTreeContainer
                  height="444px"
                  searchTxt={searchTxt}
                  treeData={searchTreeData}
                  expandedKeys={expandedKeys}
                  viewFlag={viewFlag}
                  onChangeSelected={handleChangeSelectedNode}
                  selectedKeys={selectedKeys}
                />
              )
            }
          </Paper>
          {
            !viewFlag && (
              <Paper border className="sub-content">
                <EmptyContent>
                  <img src={Images.partner_empty} alt="미선택 파트너" />
                  <WarningTitle>선택된 파트너가 없습니다.</WarningTitle>
                  <WarningDetail>파트너 정보 확인을 위해 파트너를 선택해주세요.</WarningDetail>
                </EmptyContent>
              </Paper>
            )
          }
          {
            viewFlag === 'add' && (
              <Paper border className="sub-content">
                <Title>
                  <span>파트너 등록</span>
                </Title>
              </Paper>
            )
          }
          {
            viewFlag === 'detail' && detailInfo?.data && (
              <Paper className="sub-content" border>
                <Title>
                  <span>파트너 상세</span>
                </Title>
                {
                  userInfo?.userInfo.roleCd === 'RL0001'
                    && (
                      <PartnerDetailInfo detailInfo={detailInfo} onSave={handlePartnerInfoSave} saleChannel={saleChannel} roleCd={userInfo?.userInfo.roleCd} />

                    )
                }
                {
                  userInfo?.userInfo.roleCd !== 'RL0001' && (
                    <Tabs defaultActiveKey="3">
                      <TabPane tab="파트너 정보" key="1">
                        <PartnerDetailInfo detailInfo={detailInfo} onSave={handlePartnerInfoSave} saleChannel={saleChannel} roleCd={userInfo?.userInfo.roleCd} />
                      </TabPane>
                      {userInfo?.userInfo.roleCd === 'RL0002'
                  && (
                    <TabPane tab="소속 사용자" key="2">
                      <UserList selectedNode={selectedNode} />
                    </TabPane>
                  ) }
                    </Tabs>
                  )
                }

              </Paper>
            )
          }
        </Contents>
      </Container>
    </>
  );
}

const Container = styled(PageLayout)`
`;

const Title = styled.div`
  padding: 20px 0 20px 20px;
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
`;

const CustomButton = styled(Button)`
  font-size: 12px;
`;

const TreeSearch = styled.div`
  font-size: 12px;
  display: flex;
  justify-content: space-between;
  padding: 10px;

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    padding: 0 8px;
  }
`;

const Contents = styled.div`
  display: flex;
  margin-top: 16px;
  .disabled {
    color: rgba(0,0,0,.25);
  }
  .treeContainer {
    min-width:272px;
    flex: 1 1 auto;
    height: 498px;
    overflow: hidden;
    margin-right: 20px;
  }
  .sub-content {
    min-width: 600px;
    flex: 1 1 calc(70% - 20px);
    .ant-form-item-label > label::after {
      content: ''
    }
  }

  .category {
    &-wrap {
      padding: 16px 0;
      margin: 0 20px;
    }
    .row {
      display: flex;
      &:not(:last-child) {
        padding-bottom: 14px;
      }
    }
    .left {
      min-width: 80px;
      .tit {
        margin-top: 10px;
      }
    }
    .right {
      width: calc(100% - 76px);
    }
    .tit {
      display: inline-block;
      font-size: 14px;
    }
    .text-input-group {
      width: 100%;
      .text-input.calendar {
        width: 33.3334%;
      }
    }
    .ui-dropdown-group {
      display: flex;
      .ui-dropdown {
        width: 33.3334%;
        &:not(:last-child) {
          margin-right: 8px;
        }
      }
      &.num-2 {
        .ui-dropdown {
          width: 50%;
        }
      }
      &.num-3 {
        .ui-dropdown {
          width: 33.3334%;
        }
      }
    }
    .ui-dropdown-toggle {
      .ellipsis {
        display: inline-block;
        max-width: 60%;
        @include ellipsis;
        vertical-align: middle;
      }
      .count {
        display: inline-block;
        vertical-align: middle;
      }
    }
    .product-code {
      display: flex;
      .text-input {
        width: calc(100% - 156px);
      }
      .file-group {
        width: 140px;
        margin-left: 16px;
        li {
          &:not(:last-child) {
            margin-bottom: 10px;
          }
        }
        .ui-file {
          width: 100%;
        }
        .ui-btn {
          width: 100%;
          margin-right: 0;
        }
      }
    }
    &-detail {
      display: none;
      .category-wrap {
        border-top: 1px solid $gray20;
      }
      .ui-dropdown-group .ui-dropdown {
        width: 100%;
      }
      .row {
        margin-left: -20px;
        margin-right: -20px;
      }
      .col {
        display: flex;
        width: 33.3334%;
        padding: 0 20px;
      }
    }
    &.active {
      .category-detail {
        display: block;
      }
    }
    @media (max-width: 774px) {
      .ui-dropdown-toggle {
        .count {
          display: none;
        }
      }
    }
  }

  .ant-tabs-nav {
    margin:0 19px 6px;
  }
  .ant-tabs-nav:before {
      content: none !important;
  }
  .ant-tabs-ink-bar{
      display: none;
  }
  .ant-tabs-nav-list{
    flex: 1;
    background-color: #f7f8fa;
    border-radius: 4px;
  }
  .ant-tabs-tab-btn{
      width: 100%;
      height: 100%;
      text-align: center;
  }
  .ant-tabs{
    padding-top: 10px;
    margin-bottom: 20px;
  }
  .ant-tabs-tab{
      display: flex !important;
      padding:0px;
      height: 40px;
      line-height: 40px; 
      margin: 0px;
      justify-content: center;
      flex: 1;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
      border-radius: 4px;
      background-color: var(--color-steelGray-800);
      color: #fff;
  }

  .ant-form-item-label>label.ant-form-item-required:not(.ant-form-item-required-mark-optional):before {
    content: "";
    margin-right: 0;
  }

  .ant-divider-horizontal {
    margin: 0px 0;
  }
`;

const EmptyContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  flex-direction: column;

  .rectangle {
    width: 200px;
    height: 6px;
    left: 770px;
    top: 508px;
    
    background: #FF0000;
    opacity: 0.1;
  }
`;

const WarningTitle = styled.span`
  font-style: Bold;
  font-size: 16px;
  font-weight: 700;
  line-height: 24px;
  vertical-align: Center;
  color: var(--color-gray-900);
`;

const WarningDetail = styled.span`
  font-size: 14px;
  line-height: 150%;
  /* identical to box height, or 21px */

  text-align: center;

  /* GlayScale/Dark Gray 2 */

  color: var(--color-gray-700);
`;

export default MyPartnersPageContainer;
