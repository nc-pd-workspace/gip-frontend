import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  partnerInfo: asyncApiState.initial([]),
  accountList: asyncApiState.initial([]),
  partnerList: asyncApiState.initial([]),
  partnerTree: asyncApiState.initial([]),
  userId: asyncApiState.initial([]),
  saleChannel: asyncApiState.initial([]),
  partnerStatus: asyncApiState.initial([]),
  partnerUpdateStatus: asyncApiState.initial([]),
  supDuplicate: asyncApiState.initial([]),
  selectPtnIdx: null,
  userPtnIdx: null,

};

export const { actions, reducer } = createSlice({
  name: 'account/myPartners',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    // 파트너 상태 수정
    updatePartnerStatusChange: (state, { payload }) => {
      const result = payload;
      // state.partnerStatus = asyncApiState.request(result);
    },
    updatePartnerStatusChangeSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // const result = payload;
      state.partnerStatus = asyncApiState.success(result);
    },
    updatePartnerStatusChangeFailure: (state, { payload }) => {
      state.partnerStatus = asyncApiState.error(payload);
    },
    // 판매 채널
    getSaleChannel: (state, { payload }) => {
      const result = payload;
      // state.partnerInfo = asyncApiState.request(result);
    },
    getSaleChannelSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // const result = payload;
      state.saleChannel = asyncApiState.success(result);
    },
    getSaleChannelFailure: (state, { payload }) => {
      state.saleChannel = asyncApiState.error(payload);
    },
    // 수정
    updatePartnerInfo: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerUpdateStatus = asyncApiState.request(result);
      // const result = payload;
      // state.partnerInfo = asyncApiState.request(result);
    },
    updatePartnerInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerUpdateStatus = asyncApiState.success(result);
      // const result = { ...payload.data };
      // state.partnerInfo = asyncApiState.success(result);
    },
    updatePartnerInfoFailure: (state, { payload }) => {
      state.partnerUpdateStatus = asyncApiState.error(payload);
    },
    initPartnerUpdateStatus: (state, { payload }) => {
      state.partnerUpdateStatus = asyncApiState.initial([]);
    },
    getInfo: (state, { payload }) => {
      const result = payload;
      // state.partnerInfo = asyncApiState.request(result);
    },
    getInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      // const result = payload;
      state.partnerInfo = asyncApiState.success(result);
    },
    getInfoFailure: (state, { payload }) => {
      state.partnerInfo = asyncApiState.error(payload);
    },
    getAccount: (state, { payload }) => {
      const result = payload;
      state.accountList = asyncApiState.request(result);
    },
    getAccountSuccess: (state, { payload }) => {
      const result = { ...payload };
      state.accountList = asyncApiState.success(result);
    },
    getAccountFailure: (state, { payload }) => {
      state.accountList = asyncApiState.error(payload);
    },
    getPartner: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerList = asyncApiState.request(result);
    },
    getPartnerSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerList = asyncApiState.success(result);
    },
    getPartnerFailure: (state, { payload }) => {
      state.partnerList = asyncApiState.error(payload);
    },
    getPartnerTree: (state, { payload }) => {
      const result = payload;

      state.partnerTree = asyncApiState.request(result);
    },
    getPartnerTreeSuccess: (state, { payload }) => {
      const treeData = [...payload.data || []];
      const refinedData = [];

      const roopChildFunc = (data) => {
        if (data && data.length !== 0) {
          const childNode = data.map((childData) => {
            let badge = false;
            if (state.userPtnIdx === childData.ptnIdx) {
              badge = true;
            }

            return ({
              title: childData.accessYn === 'N' ? (
                <div className="disabled">
                  <span style={{ display: 'flex' }}>{`${childData.ptnNm}(${childData.ptnId})`}</span>
                  {
                    badge ? <span className="partnersMyBadge">마이</span> : ''
                  }
                </div>
              ) : (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  {childData.ptnNm}
                  (
                  {childData.ptnId}
                  )
                  {
                    badge ? <span className="partnersMyBadge">마이</span> : ''
                  }
                </div>
              ),
              // key: childData.ptnId,
              key: childData.accessYn === 'N' ? `${childData.ptnId}disabled` : childData.ptnId,
              ptnNm: childData.ptnNm,
              ptnId: childData.ptnId,
              partnerIdx: childData.ptnIdx,
              sortSeq: childData.sortSeq,
              depth: childData.ptnLvl,
              children: roopChildFunc(childData.subPtn),
              // disabled: childData.accessYn === 'N' && true,
              myPartner: state.selectPtnIdx === childData.ptnIdx,
            });
          });
          childNode.sort((a, b) => a.sortSeq - b.sortSeq);
          return childNode;
        }
        return '';
      };

      const roopFunc = (node) => {
        if (node && node.length !== 0) {
          let badge = false;
          if (Number(state.userPtnIdx) === node.ptnIdx) {
            badge = true;
          }
          refinedData.push({
            title: node.accessYn === 'N' ? (
              <div className="disabled">
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  {node.ptnNm}
                  (
                  {node.ptnId}
                  )
                  {
                    badge ? <span className="partnersMyBadge">마이</span> : ''
                  }
                </div>
              </div>
            ) : (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                {node.ptnNm}
                (
                {node.ptnId}
                )
                {
                  badge ? <span className="partnersMyBadge">마이</span> : ''
                }
              </div>
            ),
            key: node.accessYn === 'N' ? `${node.ptnId}disabled` : node.ptnId,
            partnerIdx: node.ptnIdx,
            ptnNm: node.ptnNm,
            ptnId: node.ptnId,
            sortSeq: node.sortSeq,
            depth: node.ptnLvl,
            children: roopChildFunc(node.subPtn),
            myPartner: state.selectPtnIdx === node.ptnIdx,
          });
          refinedData.sort((a, b) => a.sortSeq - b.sortSeq);
        }
      };

      treeData.forEach((element) => {
        roopFunc(element);
      });

      const result = {
        status: 200,
        data: refinedData,
      };

      state.partnerTree = asyncApiState.success(result);
    },
    initPartnerTree: (state, { payload }) => {
      state.partnerTree = asyncApiState.initial([]);
    },
    getPartnerTreeFailure: (state, { payload }) => {
      state.partnerTree = asyncApiState.error(payload);
    },
    // 거래처 중복
    getSupDuplicate: (state, { payload }) => {
      const result = payload;
      state.supDuplicate = asyncApiState.request(result);
    },
    getSupDuplicateSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.supDuplicate = asyncApiState.success(result);
    },
    getSupDuplicateFailure: (state, { payload }) => {
      state.supDuplicate = asyncApiState.error(payload);
    },
  },
});

export const { resetStore, updateState,
  updatePartnerInfo, getInfo, getAccount, getPartner, getPartnerTree, getSaleChannel, updatePartnerStatusChange, initPartnerUpdateStatus, getSupDuplicate, initPartnerTree } = actions;

export default reducer;
