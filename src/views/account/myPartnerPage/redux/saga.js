import {
  all, fork,
  takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getAccount, getInfo, getPartner, getPartnerTree, updatePartnerInfo, getSaleChannel, updatePartnerStatusChange, getSupDuplicate } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const myPartnerInfoSaga = createPromiseSaga(getInfo, API.MyPartners.getMyPatnerInfo);
const myPartnerAccountListSaga = createPromiseSaga(getAccount, API.MyPartners.getMyPatnerAccountList);
const myPartnerListSaga = createPromiseSaga(getPartner, API.MyPartners.getMyPatnerList);
const myPartnerTreeSaga = createPromiseSaga(getPartnerTree, API.MyPartners.getMyPatnerTree);
const myPartnerInfoUpdateSaga = createPromiseSaga(updatePartnerInfo, API.MyPartners.updatePatnerInfo);
const commonSaleChannel = createPromiseSaga(getSaleChannel, API.Common.getSaleChannelList);
const commonPartnerStatusChangeChange = createPromiseSaga(updatePartnerStatusChange, API.Common.putPartnerStatusChange);
const commonSupDuplicate = createPromiseSaga(getSupDuplicate, API.Common.supDuplicate);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getInfo, myPartnerInfoSaga);
  yield takeLatest(getAccount, myPartnerAccountListSaga);
  yield takeLatest(getPartner, myPartnerListSaga);
  yield takeLatest(getPartnerTree, myPartnerTreeSaga);
  yield takeLatest(updatePartnerInfo, myPartnerInfoUpdateSaga);
  yield takeLatest(getSaleChannel, commonSaleChannel);
  yield takeLatest(updatePartnerStatusChange, commonPartnerStatusChangeChange);
  yield takeLatest(getSupDuplicate, commonSupDuplicate);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
