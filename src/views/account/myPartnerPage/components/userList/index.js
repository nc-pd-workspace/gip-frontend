import styled from 'styled-components';
import { useState, useCallback, useRef, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';

import PagingTable from '../../../../../components/pagingTable';

import Paper from '../../../../../components/paper';

import { getPartner } from '../../redux/slice';
import Button from '../../../../../components/button';
import { SvgIconRegister } from '../../../../../Images';
import { putUserType } from '../../../../settings/users/redux/slice';
import UserAddModal from '../../../../settings/partners/components/Modal/UserAddModal';
import { confirmMessage } from '../../../../../components/message';

function UserList(props) {
  const { selectedNode } = props;

  const columnOptions = [
    {
      title: '사용자 아이디',
      dataIndex: 'usrId',
      width: 150,
    },
    {
      title: '사용자 이름',
      dataIndex: 'usrNm',
      width: 100,
    },
    {
      title: '휴대폰 번호',
      dataIndex: 'cphoneNo',
      width: 150,
    },
    {
      title: '이메일',
      dataIndex: 'email',
      width: 200,
    },
    {
      title: '사용자 구분',
      dataIndex: 'roleNm',
      render: (roleNm, record) => {
        if (record.roleCd !== 'RL0003') {
          if ((record.roleCd === 'RL0002' && record.joinTypeCd === 'MC000010') || userInfo.userInfo.usrIdx === record.usrIdx) {
            return (
              <span>
                { roleNm }
              </span>
            );
          }
          return (
            <Link to="#" className="test" onClick={(e) => onClickUserType(record)}>
              { roleNm }
            </Link>
          );
        }
        return roleNm;
      },
      width: 100,
    },
    {
      title: '상태',
      dataIndex: 'stCd',
      render: (stCd) => {
        let str = '';
        if (stCd === 'MC000040') str = '정상';
        else if (stCd === 'MC000050') str = '중지';
        else if (stCd === 'MC000030') str = '승인 전';

        return str;
      },
      width: 100,
    },
  ];

  const initialState = {
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };

  const tableRef = useRef(null);
  const dispatch = useDispatch();

  const { getUserList } = useSelector((state) => ({
    getUserList: state.account.myPartnerPage.partnerList,
  }));
  const { userTypeModify } = useSelector((state) => state.settings.users);
  const { userInfo } = useSelector((state) => state.common);

  const [state, setState] = useState(initialState);
  const [userAddModalVisible, setUserAddModalVisible] = useState(false);

  useEffect(() => {
    const params = {
      idx: selectedNode.partnerIdx,
      page: 0,
      size: 10,
    };
    dispatch(getPartner({ params }));
  }, [selectedNode]);

  useEffect(() => {
    setState({
      ...state,
      data: getUserList.data && getUserList.data.items ? getUserList.data.items : [],
      loading: getUserList.status === 'pending',
      pagination: {
        ...state.pagination,
        total: getUserList.data && getUserList.data.totalItem
          ? getUserList.data.totalItem : 0,
      },
    });
  }, [getUserList]);

  useEffect(() => {
    if (userTypeModify.status === 'success') {
      const params = {
        idx: selectedNode.partnerIdx,
        page: state.pagination.current - 1,
        size: 10,
      };
      dispatch(getPartner({ params }));
    }
  }, [userTypeModify]);

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleOk = useCallback(() => {
    setUserAddModalVisible(false);

    setState({ ...state, pagination: { ...state.pagination, current: 1 } });
    // 초기화
    const params = {
      idx: selectedNode.partnerIdx,
      page: 0,
      size: 10,
    };

    dispatch(getPartner({ params }));
  }, [selectedNode]);

  const handleCancel = () => {
    setUserAddModalVisible(false);
  };

  const handleUserModalBtn = () => {
    setUserAddModalVisible(true);
  };

  const onClickUserType = useCallback((record) => {
    // 로그인한 계정의 권한 변경 불가능
    if (userInfo.userInfo.usrIdx === record.usrIdx) {
      return;
    }

    if (record.roleCd === 'RL0001') {
      confirmMessage('일반 사용자를 마스터로 변경하시겠어요?', () => {
        const params = {
          usrIdx: record.usrIdx,
          roleCd: 'RL0002',
        };
        dispatch(putUserType({ params }));
      }, '예', '아니요');
    } else if (record.roleCd === 'RL0002' && record.joinTypeCd !== 'MC000010') {
      confirmMessage('마스터를 일반 사용자로 변경하시겠어요?', () => {
        const params = {
          usrIdx: record.usrIdx,
          roleCd: 'RL0001',
        };
        dispatch(putUserType({ params }));
      }, '예', '아니요');
    }
  }, [userInfo]);

  const handleTableChange = (pagination, filters, sorter) => {
    const params = {
      idx: selectedNode.partnerIdx,
      page: pagination.current - 1,
      size: state.pagination.pageSize,
    };
    dispatch(getPartner({ params }));
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  return (
    <>
      <Container>
        <div className="user-header">
          <Button onClick={handleUserModalBtn}>
            <SvgIconRegister fill="var(--color-gray-700)" />
            사용자 등록
          </Button>
        </div>
        <div>
          <TableWrap>
            {/* <StyledTitle
              level="3"
              title="조회 결과"
              subTitle="총 50개"
            /> */}

            <PagingTable
              ref={tableRef}
              columns={columnOptions}
              data={state.data}
              pagination={state.pagination}
              loading={state.loading}
              rowKey={(record) => record.usrIdx}
              showRowIndex
              onChange={handleTableChange}
            />

          </TableWrap>
        </div>
      </Container>
      {userAddModalVisible
      && <UserAddModal visible={userAddModalVisible} selectedNode={selectedNode} onOk={handleOk} onClose={handleCancel} /> }
    </>
  );
}

const Container = styled.div`
  .user-header {
    display: flex;
    padding-top: 17px;
    button:first-child {
      margin: 0 20px 0 auto
    }
  }
`;

const TableWrap = styled(Paper)`
  padding: 20px;
`;

const LoadingWrap = styled.div`
  height: 240px;
`;

export default UserList;
