import styled from 'styled-components';
import { Input, Button, Modal, Select } from 'antd';
import { useCallback, useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getPartner } from '../../redux/slice';
import Paper from '../../../../../components/paper';
import Title from '../../../../../components/title';
import PagingTable from '../../../../../components/pagingTable';

const { Option } = Select;

function partnerInqueryModal(props) {
  const { visible, onOk, onClose } = props;

  const columnOptions = [
    {
      title: '파트너번호',
      dataIndex: 'partnerNo',
      width: 100,
    },
    {
      title: '파트너 아이디',
      dataIndex: 'partnerId',
      width: 150,
    },
    {
      title: '파트너명',
      dataIndex: 'partnerName',
      width: 100,
    },
    {
      title: '상태',
      dataIndex: 'status',
      width: 100,
    },
  ];

  const { partnerList } = useSelector((state) => ({
    partnerList: state.settings.partners.partnerList,
  }));

  const dispatch = useDispatch();

  const initialState = {
    search: {
      searchType: 'partnerId',
      searchText: '',
    },
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };

  const tableRef = useRef(null);

  const [state, setState] = useState(initialState);
  const [searchParams, setSearchParams] = useState({ searchType: 'partnerId', searchText: '' });

  useEffect(() => {
    if (visible) {
      dispatch(getPartner(state));
    } else {
      setSearchParams({ searchType: 'partnerId', searchText: '' });
    }
  }, [visible]);

  useEffect(() => {
    setState({
      ...state,
      data: partnerList.data && partnerList.data.result ? partnerList.data.result : [],
      loading: partnerList.status === 'pending',
      pagination: {
        ...state.pagination,
        total: partnerList.data && partnerList.data.totalItemSize
          ? partnerList.data.totalItemSize : 0,
      },
    });
  }, [partnerList]);

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleSearchBtn = useCallback(() => {
    dispatch(getPartner(state));
  }, [state]);

  const handleSearchTxtChange = useCallback((e) => {
    setSearchParams({ ...searchParams, searchText: e.target.value });
  }, [searchParams]);

  const handleSearchTypeChange = useCallback((value) => {
    setSearchParams({ ...searchParams, searchType: value });
  }, [searchParams]);

  const handleTableChange = (pagination, filters, sorter) => {
    // search param 넘겨줘야함
    dispatch(getPartner());
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const handleOkBtn = () => {
    onOk(tableRef.current.getSelectedRowData());
  };

  return (
    <PartnerModal
      title="파트너 조회"
      visible={visible}
      onOk={handleOkBtn}
      onCancel={onClose}
      okText="선택 완료"
      cancelText="취소"
    >
      <div className="modal-search">
        <div className="modal-search-input">
          <span className="search-title">검색어</span>
          <Select
            className="search-select"
            onChange={handleSearchTypeChange}
            value={searchParams.searchType}
          >
            <Option value="partnerId">파트너 아이디</Option>
            <Option value="partnerName">파트너명</Option>
            <Option value="partnerNumber">파트너 번호</Option>
          </Select>
          <Input
            className="search-input"
            type="text"
            value={searchParams.searchText}
            onChange={handleSearchTxtChange}
            style={{ width: '100px' }}
          />
        </div>
        <div className="search-button">
          <Button onClick={handleSearchBtn} type="fillSteelGray" width="200" height="40">검색</Button>
        </div>
      </div>
      <TableWrap>
        <PagingTable
          ref={tableRef}
          columns={columnOptions}
          data={state.data}
          pagination={state.pagination}
          loading={state.loading}
          rowKey={(record) => record.userId}
          showRowIndex
          rowSelect
          onChange={handleTableChange}
        />
      </TableWrap>
    </PartnerModal>
  );
}

const PartnerModal = styled(Modal)`
  .modal-search {
    background-color: #F7F8FA;
    height: 134px;
  }

  .search-title {
    flex: 2;
    display: inline-flex;
    align-items: center;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }
  
  .ant-select {
      font-size: 13px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-footer {
    border-top: 0px;
    text-align: center;
  }

  .ant-modal-footer button:last-child {
    width: 250px;
    height: 40px;
  }

  .ant-modal-footer > .ant-btn {
    width: 80px;
    height: 40px;
  }

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
      width: 121px;
  }

  .ant-table-tbody {
    height: 400px;
  }
`;

const SearchBtn = styled(Button)`
  width: 200px;
  height: 40px;

  background-color: var(--color-steelGray-800);
  color:var(--color-white);
`;

const TableWrap = styled(Paper)`
  margin: 20px;
`;

const StyledTitle = styled(Title)`
  padding: 20px 0;

  & > p {
    font-family: Pretendard;
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 20px;
    margin-left: 6px;

    text-align: justify;
    color: var(--color-blue-500);
  }`;
export default partnerInqueryModal;
