/* eslint-disable no-nested-ternary */
import styled from 'styled-components';
import { Input, Modal, Select } from 'antd';
import { useCallback, useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getAccount, getSupDuplicate } from '../../redux/slice';
import Paper from '../../../../../components/paper';
import Title from '../../../../../components/title';
import Button from '../../../../../components/button';
import { SvgArrowDropdown } from '../../../../../Images';
import LoadingComponent from '../../../../../components/loading';
import PagingTable from '../../../../../components/pagingTable';
import EmptyList from '../../../../../components/emptyList';
import { alertMessage } from '../../../../../components/message';

const { Option } = Select;

function AccountInqueryModal(props) {
  const { visible, onOk, onClose, detailInfoPtnIdx } = props;

  const columnOptions = [
    {
      title: '거래처 코드',
      dataIndex: 'supCode',
      width: 70,
    },
    {
      title: '하위거래처 코드',
      dataIndex: 'subSupCode',
      width: 70,
    },
    {
      title: '거래처명',
      dataIndex: 'supName',
      width: 100,
    },
  ];

  const { accountList, supDuplicate } = useSelector((state) => ({
    accountList: state.account.myPartnerPage.accountList,
    supDuplicate: state.account.myPartnerPage.supDuplicate,

  }));

  const dispatch = useDispatch();

  const initialState = {
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };

  const tableRef = useRef(null);

  const [state, setState] = useState(initialState);
  const [searchParams, setSearchParams] = useState({ searchType: 'supCd', searchValue: '' });

  useEffect(() => {
    const params = {
      searchType: 'supCd',
      searchValue: '',
      page: initialState.pagination.current - 1,
      size: initialState.pagination.pageSize,
    };
    if (visible) {
      dispatch(getAccount({ params }));
    } else {
      setSearchParams({ searchType: 'supCd', searchValue: '' });
      setState(initialState);
    }
  }, [visible]);

  useEffect(() => {
    setState({
      ...state,
      data: accountList.data && accountList.data.items ? accountList.data.items : [],
      loading: accountList.status === 'pending',
      pagination: {
        ...state.pagination,
        total: accountList.data && accountList.data.totalItem
          ? accountList.data.totalItem : 0,
      },
    });
  }, [accountList]);

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleSearchBtn = useCallback(() => {
    const params = {
      searchType: searchParams.searchType,
      searchValue: searchParams.searchValue ? searchParams.searchValue : '',
      page: '0',
      size: state.pagination.pageSize,

    };
    dispatch(getAccount({ params }));
  }, [state, searchParams]);

  const handleSearchTxtChange = useCallback((e) => {
    setSearchParams({ ...searchParams, searchValue: e.target.value });
  }, [searchParams]);

  const handleSearchTypeChange = useCallback((value) => {
    setSearchParams({ ...searchParams, searchType: value });
  }, [searchParams]);

  const handleTableChange = (pagination, filters, sorter) => {
    const params = {
      searchType: searchParams.searchType,
      searchValue: searchParams.searchValue ? searchParams.searchValue : '',
      page: pagination.current - 1,
      size: state.pagination.pageSize,
    };

    dispatch(getAccount({ params }));
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const handleOkBtn = useCallback(() => {
    if (tableRef.current.getSelectedRowData().length === 0) {
      alertMessage('목록을 선택해주세요.');
    } else if (supDuplicate?.status === 'error') {
      const ptnData = supDuplicate.data.data.data;
      alertMessage(`이미 등록된 거래처 코드 입니다.\n다시 확인해 주세요.\n매핑된 파트너 : ${ptnData.ptnNm}(${ptnData.ptnIdx})`);
    } else {
      setSearchParams({ ...searchParams, searchValue: '' });
      onOk(tableRef.current.getSelectedRowData());
    }
  }, [supDuplicate]);

  const onRowClick = useCallback((v) => {
    if (v.supCode) {
      const params = {
        ptnIdx: detailInfoPtnIdx,
        supCd: v.supCode,
        subSupCd: v.subSupCode,
      };
      dispatch(getSupDuplicate({ params }));
    }
  }, [detailInfoPtnIdx]);

  return (
    <AccountModal
      title="거래처 조회"
      visible={visible}
      onOk={handleOkBtn}
      onCancel={onClose}
      okText="선택 완료"
      cancelText="취소"
    >
      <div className="modal-search">
        <div className="modal-search-input">
          <span className="search-title">검색어</span>
          <Select
            className="search-select"
            onChange={handleSearchTypeChange}
            value={searchParams.searchType}
            suffixIcon={<SvgArrowDropdown />}
          >
            <Option value="supCd">거래처 코드</Option>
            <Option value="supNm">거래처명</Option>
          </Select>
          <Input
            className="search-input"
            type="text"
            value={searchParams.searchValue}
            onChange={handleSearchTxtChange}
            style={{ width: '100px' }}
          />
        </div>
        <div className="search-button">
          <SearchBtn type="fillSteelGray" onClick={handleSearchBtn} width="200" height="40">검색</SearchBtn>
        </div>
      </div>
      <TableWrap>
        {
          accountList.status === 'pending' ? (
            <LoadingWrap>
              <LoadingComponent isLoading />
            </LoadingWrap>
          ) : (
            state.data.length > 0 ? (
              <PagingTable
                ref={tableRef}
                columns={columnOptions}
                data={state.data}
                pagination={state.pagination}
                loading={state.loading}
                rowKey={(record) => record.userId}
                rowSelect
                onChange={handleTableChange}
                onRowClick={onRowClick}
              />
            ) : (
              <EmptyList warningSubTitle="조회 기준을 다시 설정해보세요." height={240} />
            )
          )
        }
      </TableWrap>
    </AccountModal>
  );
}

const AccountModal = styled(Modal)`
  .modal-search {
    background-color: #F7F8FA;
    height: 134px;
  }

  .search-title {
    flex: 2;
    display: inline-flex;
    align-items: center;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-footer {
    border-top: 0px;
    text-align: center;
  }

  .ant-modal-footer button:last-child {
    width: 250px;
    height: 40px;
  }

  .ant-modal-footer > .ant-btn {
    width: 80px;
    height: 40px;
  }

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
      width: 121px;
  }

  .ant-table-tbody {
    height: 400px;
  }
`;

const SearchBtn = styled(Button)`
  width: 200px;
  height: 40px;

  background-color: var(--color-steelGray-800);
  color:var(--color-white);
`;

const TableWrap = styled(Paper)`
  margin: 20px;
  .ant-table-tbody{height:auto !important}
`;

const StyledTitle = styled(Title)`
  padding: 20px 0;

  & > p {
    font-family: Pretendard;
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 20px;
    margin-left: 6px;

    text-align: justify;
    color: var(--color-blue-500);
  }`;

const LoadingWrap = styled.div`
  height: 240px;
`;

export default AccountInqueryModal;
