/* eslint-disable prefer-regex-literals */
import styled from 'styled-components';
import { Form, Input, Checkbox } from 'antd';
import { useEffect, useCallback, useState } from 'react';

import Button from '../../../../../components/button';
import AccountInqueryModal from '../Modal/AccountInqueryModal';
import Descripition from '../../../../../components/paper/descripition';

import { getPhoneValidationTransform, getOnlyNumberValidation } from '../../../../../utils/utils';

function PartnerDetailInfo(props) {
  const [form] = Form.useForm();
  const { detailInfo, onSave, saleChannel, roleCd } = props;
  const [detailData, setDetailData] = useState({
    ptnId: '',
  });
  const [accountModalVisible, setAccountModalVisible] = useState(false);
  const [checkedArr, setCheckedArr] = useState([]);

  const initialStateSalesChnl = {
    Cd: '',
    Nm: '',
  };
  const [salesChnl, setSalesChnl] = useState(initialStateSalesChnl);
  const [salesChnlSupCode, setSalesChnlSupCode] = useState('');
  const [salesChnlSupName, setSalesChnlSupName] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const [salesChnlSubSupCode, setSalesChnlSubSupCode] = useState('');
  const [salesChnlSubSupName, setSalesChnlSubSupName] = useState('');
  const [channelOutline, setChannelOutline] = useState();

  useEffect(() => {
    if (detailInfo.status === 'success') {
      setDetailData({
        ptnId: detailInfo.data.ptnId,
      });
      form.setFieldsValue({
        ptnIdx: detailInfo.data.ptnIdx,
        ptnNm: detailInfo.data.ptnNm,
        ptnId: detailInfo.data.ptnId,
        ptnDesc: detailInfo.data.ptnDesc,
        upperPtnIdx: detailInfo.data.upperPtnIdx,
        upperPtnNm: detailInfo.data.upperPtnNm,
        sortSeq: detailInfo.data.sortSeq,
        picNm: detailInfo.data.picNm,
        picDept: detailInfo.data.picDept,
        picCphoneNo: detailInfo.data.picCphoneNo,
        picEmail: detailInfo.data.picEmail,
        stCd: detailInfo.data.stCd,
      });
      setSalesChnl({ Cd: detailInfo.data.salesChnlCd, Nm: detailInfo.data.salesChnlNm });
      setErrorMsg('');
      setChannelOutline(false);
      setCheckedArr([detailInfo.data.salesChnlNm]);
      setSalesChnlSupName(detailInfo.data.subSupNm ? detailInfo.data.subSupNm : detailInfo.data.supNm);
      setSalesChnlSupCode(detailInfo.data.subSupCd ? detailInfo.data.subSupCd : detailInfo.data.supCd);
      setSalesChnlSubSupName(detailInfo.data.subSupNm ? detailInfo.data.subSupNm : '');
      setSalesChnlSubSupCode(detailInfo.data.subSupCd ? detailInfo.data.subSupCd : '');
    }
  }, [detailInfo]);

  const handleFormSubmitSuccess = useCallback((values) => {
    if (checkedArr && checkedArr.length > 0) {
      if (salesChnl.Cd && !salesChnlSupCode && salesChnl.Nm !== 'GS 편의점') {
        setErrorMsg(<span className="ant-form-item-explain-error">GS SHOP의 거래처 코드를 입력해주세요.</span>);
        setChannelOutline(true);
      } else {
        const params = {
          ptnNm: values.ptnNm ? values.ptnNm : '',
          ptnId: values.ptnId ? values.ptnId : '',
          ptnDesc: values.ptnDesc ? values.ptnDesc : '',
          sortSeq: values.sortSeq ? values.sortSeq : '',
          salesChnlCd: salesChnl.Cd,
          salesChnlNm: salesChnl.Nm,
          supCd: salesChnlSupCode || '',
          supNm: salesChnlSupName || '',
          subSupCd: salesChnlSubSupCode || '',
          subSupNm: salesChnlSubSupName || '',
          // picNm: values.picNm ? values.picNm : '',
          picDept: values.picDept ? values.picDept : '',
          // picCphoneNo: values.picCphoneNo ? values.picCphoneNo : '',
          // picEmail: values.picEmail ? values.picEmail : '',
        };

        Object.keys(values).forEach((items) => {
          if (values[items] !== '' && values[items] !== detailInfo.data[items]) {
            params[items] = values[items];
          }
        });

        onSave(params);
      }
    } else {
      setErrorMsg(<span className="ant-form-item-explain-error">채널을 선택해주세요.</span>);
      setChannelOutline(true);
    }
  }, [detailInfo, salesChnlSupCode, salesChnlSupName, checkedArr, salesChnlSubSupCode, salesChnlSubSupName]);

  const handleOk = useCallback((value) => {
    // supCode ,supName
    setSalesChnlSupName(value[0].supName);
    setSalesChnlSupCode(value[0].supCode);
    setSalesChnlSubSupName(value[0].subSupName);
    setSalesChnlSubSupCode(value[0].subSupCode);

    setChannelOutline(false);
    setErrorMsg('');
    setAccountModalVisible(false);
  }, []);

  const handleCancel = () => {
    setAccountModalVisible(false);
  };

  const handleCheck = useCallback((value, index) => {
    setErrorMsg('');
    setChannelOutline(false);
    setSalesChnl({ Cd: saleChannel[index].salesChnlCd, Nm: saleChannel[index].salesChnlNm });

    if (detailInfo.data.salesChnlCd === saleChannel[index].salesChnlCd) {
      setSalesChnlSupName(detailInfo.data.supNm);
      setSalesChnlSupCode(detailInfo.data.supCd);
      setSalesChnlSubSupName(detailInfo.data.subSupName ? detailInfo.data.subSupName : '');
      setSalesChnlSubSupCode(detailInfo.data.subSupCode ? detailInfo.data.subSupCode : '');
    } else {
      setSalesChnlSupName('');
      setSalesChnlSupCode('');
      setSalesChnlSubSupName('');
      setSalesChnlSubSupCode('');
    }

    // 하드코딩
    if (saleChannel[index].salesChnlNm === 'GS 편의점') {
      setSalesChnlSupName('');
      setSalesChnlSupCode('');
      setSalesChnlSubSupName('');
      setSalesChnlSubSupCode('');
    }
    let tempArr = [...checkedArr];

    const idx = tempArr.indexOf(value.target.value);

    if (idx === -1 && tempArr.length === 0) {
      tempArr.push(value.target.value);
    } else if (idx === 0) {
      tempArr = [];
    } else {
      tempArr = [];
      tempArr.push(value.target.value);
    }

    setCheckedArr(tempArr);
  }, [checkedArr, saleChannel]);

  const onFormValueChange = (data) => {
    // 파트너명 15자 이상 자동 입력방지
    if (data.ptnNm) {
      if (data.ptnNm && data.ptnNm.length > 15) {
        form.setFieldsValue({
          ptnNm: data.ptnNm.substr(0, 15),
        });
      }
    }
    if (data.sortSeq) {
      form.setFieldsValue({
        sortSeq: getOnlyNumberValidation(data.sortSeq),
      });
    }

    if (data.picCphoneNo) {
      if (getPhoneValidationTransform(data.picCphoneNo) !== data.picCphoneNo) {
        form.setFieldsValue({
          picCphoneNo: getPhoneValidationTransform(data.picCphoneNo),
        });
      }
    }
  };

  const onFocusItem = (e, name) => {
    if (e.target.value === detailInfo.data[name]) {
      form.setFieldsValue({
        [name]: '',
      });
    }
  };

  const onBlurItem = (e, name) => {
    if (e.target.value === '') {
      form.setFieldsValue({
        [name]: detailInfo.data[name],
      });
    }
  };

  const handleChangeSortSeq = useCallback((event) => {
    if (event.target.value === '0') {
      form.setFieldsValue({ sortSeq: '' });
    }
  });

  return (
    <>
      <Container>
        <PartnerForm
          name="basic"
          labelAlign="left"
          labelCol={{ span: 5,
            style: {
              maxWidth: '112px',
            },
          }}
          initialValues={{ remember: true }}
          autoComplete="off"
          form={form}
          onFinish={handleFormSubmitSuccess}
          onValuesChange={onFormValueChange}
        >
          <PartnerFormHeader>
            <span>
              기본 정보
            </span>
          </PartnerFormHeader>
          <Form.Item
            label="파트너 아이디"
            name="ptnId"
          >
            <span>
              {detailData.ptnId}
            </span>
          </Form.Item>
          <Form.Item
            label="파트너명"
            name="ptnNm"
            deafaultvalue={detailInfo.data.name}
            rules={[
              { required: true, message: '파트너명을 입력해주세요.' },
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 15) {
                      form.setFieldsValue({
                        ptnNm: value.substring(0, 15),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 15자" disabled={roleCd === 'RL0001'} />
          </Form.Item>
          <Form.Item
            label="파트너 설명"
            name="ptnDesc"
            rules={[
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 50) {
                      form.setFieldsValue({
                        ptnDesc: value.substring(0, 50),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            {
              roleCd === 'RL0001' ? (
                <Input disabled />

              ) : (
                <Input placeholder="최대 50자" />
              )
            }
          </Form.Item>
          <Form.Item
            label="상위 파트너"
            name="upperPtnNm"
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            label="우선 순위"
            name="sortSeq"
          >
            <Input onChange={handleChangeSortSeq} maxLength={3} min={1} max={999} placeholder="숫자 1~999" disabled={roleCd === 'RL0001'} />
          </Form.Item>

          <Form.Item
            label="판매 채널"
            className="formSalesChannel"
            extra={errorMsg}
          >
            <>
              {Object.keys(saleChannel).map((value, index) => (
                <SubChannelGroup key={index}>
                  <Checkbox value={saleChannel[value].salesChnlNm} checked={checkedArr.indexOf(saleChannel[value].salesChnlNm) > -1} onChange={(v) => { handleCheck(v, index); }} disabled>
                    <div className="check-label">{saleChannel[value].salesChnlNm}</div>
                  </Checkbox>
                  {
                    (checkedArr.indexOf(saleChannel[value].salesChnlNm) !== -1 && saleChannel[value].salesChnlNm !== 'GS 편의점') && (
                      <AccountInquiry>
                        <Input disabled className={channelOutline ? 'ptn-error' : ''} value={`${salesChnlSupName}${salesChnlSupCode ? `(${salesChnlSupCode})` : ''}`} />
                      </AccountInquiry>
                    )
                  }
                </SubChannelGroup>
              ))}

            </>
          </Form.Item>
          <div className="ant-divider ant-divider-horizontal" role="separator" />
          <PartnerFormHeader><span>담당자 정보</span></PartnerFormHeader>
          <Form.Item
            label="이름"
            name="picNm"
            rules={[
              { required: true, message: '이름을 입력해주세요.' },
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 15) {
                      form.setFieldsValue({
                        picNm: value.substring(0, 15),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input disabled={roleCd === 'RL0001'} placeholder="최대 15자" onFocus={(e) => onFocusItem(e, 'picNm')} onBlur={(e) => onBlurItem(e, 'picNm')} />
          </Form.Item>
          <Form.Item
            label="부서"
            name="picDept"
            rules={[
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 50) {
                      form.setFieldsValue({
                        picDept: value.substring(0, 50),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            {
              roleCd === 'RL0001' ? (
                <Input disabled={roleCd === 'RL0001'} />

              ) : (
                <Input disabled={roleCd === 'RL0001'} placeholder="최대 50자" />
              )
            }
          </Form.Item>
          <Form.Item
            label="휴대폰 번호"
            name="picCphoneNo"
            rules={[
              {
                validator: (_, value) => {
                  if (value !== detailInfo.data.picCphoneNo && /^\d{3}-\d{3,4}-\d{4}$/.test(value) === false) {
                    return Promise.reject(new Error('올바른 휴대폰 번호가 아닙니다.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            {
              roleCd === 'RL0001' ? (
                <Input disabled={roleCd === 'RL0001'} onFocus={(e) => onFocusItem(e, 'picCphoneNo')} onBlur={(e) => onBlurItem(e, 'picCphoneNo')} />

              ) : (
                <Input disabled={roleCd === 'RL0001'} placeholder="‘-’ 없이 입력" onFocus={(e) => onFocusItem(e, 'picCphoneNo')} onBlur={(e) => onBlurItem(e, 'picCphoneNo')} />
              )
            }
          </Form.Item>
          <Form.Item
            label="이메일"
            name="picEmail"
            required
            rules={[
              {
                validator: (_, value) => {
                  if (value.trim() === '') {
                    return Promise.reject(new Error('이메일을 입력해주세요.'));
                  }
                  if (value !== detailInfo.data.picEmail && !/^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/.test(value)) {
                    return Promise.reject(new Error('이메일 형식에 맞지 않습니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
            extra={roleCd !== 'RL0001' && (<Descripition text="최초 마스터 계정의 이메일로 등록되며, 로그인 인증번호가 발송됩니다." padding="6px 0 0 " />)}
          >
            <Input disabled={roleCd === 'RL0001'} onFocus={(e) => onFocusItem(e, 'picEmail')} onBlur={(e) => onBlurItem(e, 'picEmail')} />
          </Form.Item>
          {
            roleCd !== 'RL0001' && (
              <>
                <div className="ant-divider ant-divider-horizontal" role="separator" />
                <SubmitWrap>
                  <SubmitButton type="fillBlue" htmlType="submit" width="250" height="40">
                    수정
                  </SubmitButton>
                </SubmitWrap>
              </>
            )
          }
        </PartnerForm>
      </Container>
      {accountModalVisible
      && <AccountInqueryModal visible={accountModalVisible} onOk={handleOk} onClose={handleCancel} detailInfoPtnIdx={detailInfo.data.ptnIdx} />}
    </>
  );
}

const Container = styled.div`
`;

const PartnerFormHeader = styled.div`
  font-weight: 700;
  font-size: 16px;
  line-height: 24px;
  height: 74px;
  line-height: 74px;
`;

const PartnerForm = styled(Form)`
  margin: 0 20px 20px;
  .check-label {
    min-width: 65px;
    white-space: nowrap; 
    width: 30px; 
    overflow: hidden;
    text-overflow: ellipsis; 
  }

  .ant-form-item {
    margin: 0 0 14px;
  }

  .row-margin {
    margin-bottom: 5px;
  }

  .margin-right {
    margin-right: 10px;
  }

  .ptn-error {
    border-color: #ff4d4f !important;
  }
`;

const SubChannelGroup = styled.div`
  display: flex;
  flex: 1;
  height: 33px;
  align-items: center;
`;

const SubmitButton = styled(Button)`
  margin-top: 26px;
  width: 250px;
  height: 40px;
`;

const SubmitWrap = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  padding-top: 11px;
`;
const AccountInquiry = styled.div`
  display: flex;
  flex: 1;
  input {
    flex: 1 1 auto;
  }
  button {
    flex:0 0 80px;
    margin-left: 6px;
  }
`;
export default PartnerDetailInfo;
