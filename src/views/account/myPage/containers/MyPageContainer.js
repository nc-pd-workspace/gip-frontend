import { useEffect } from 'react';
import styled from 'styled-components';
import {
  Form,
  Input,
  Button,
} from 'antd';

import { useDispatch, useSelector } from 'react-redux';

import { PageTypes } from '../../../../constants/pageType';

import PageHeader from '../../../../components/header/PageHeader';
import Paper from '../../../../components/paper';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import { getMyInfo, putMyInfo, resetStore } from '../redux/slice';
import { getHeaderUserInfo } from '../../../../redux/commonReducer';

import { getPhoneValidationTransform } from '../../../../utils/utils';
import { usePageTab } from '../../../shared/pageTab/hooks/usePageTab';
import { alertMessage } from '../../../../components/message';

function MyPageContainer() {
  const dispatch = useDispatch();
  const { myInfo, modifyMyInfo } = useSelector((state) => state.account.myPage);
  const { userInfo, activePageId, resetSettings } = useSelector((state) => state.common);
  const [form] = Form.useForm();
  const { openPage } = usePageTab();

  const onFormValueChange = (data) => {
    if (data.cphoneNo) {
      if (getPhoneValidationTransform(data.cphoneNo) !== data.cphoneNo) {
        form.setFieldsValue({
          cphoneNo: getPhoneValidationTransform(data.cphoneNo),
        });
      }
    }
    if (data.usrNm) {
      if (data.usrNm && data.usrNm.length > 15) {
        form.setFieldsValue({
          usrNm: data.usrNm.substr(0, 15),
        });
      }
    }
    // setComponentSize(size);
  };

  const onFinishForm = (values) => {
    if (!myInfo.data || !myInfo.data.roleCd) {
      // roleCd 정보가 있어야함.
      return;
    }
    const params = {
      roleCd: myInfo.data.roleCd,
    };
    Object.keys(values).forEach((items) => {
      if (values[items] !== '' && values[items] !== myInfo.data[items]) {
        params[items] = values[items];
      }
    });
    if (Object.keys(params).length > 0) {
      dispatch(putMyInfo({ params }));
    }
  };

  const onFocusItem = (e, name) => {
    if (e.target.value === myInfo.data[name]) {
      form.setFieldsValue({
        [name]: '',
      });
    }
  };

  const onBlurItem = (e, name) => {
    if (e.target.value === '') {
      form.setFieldsValue({
        [name]: myInfo.data[name],
      });
    }
  };

  const onClickPartner = (e) => {
    openPage(PageTypes.ACCOUNT_MY_PARTNER_PAGE, { ptnId: myInfo.data.ptnId });
  };

  useEffect(() => {
    dispatch(getMyInfo());

    return () => {
      dispatch(resetStore());
    };
  }, []);

  useEffect(() => {
    if (myInfo.status === 'success') {
      // 내 정보가 수정되었는데 *마스킹 표시가 없다면 이름을 업데이트 한 것.
      form.setFieldsValue({
        ...myInfo.data,
      });
    }
  }, [myInfo]);

  useEffect(() => {
    if (modifyMyInfo.status === 'success') {
      alertMessage('내 정보 수정이 완료되었습니다.', () => {
        dispatch(getMyInfo());
        dispatch(getHeaderUserInfo());
      });
    }
  }, [modifyMyInfo]);

  useEffect(() => {
    if (activePageId === 'myPage' && resetSettings) {
      form.setFieldsValue({
        usrNm: '',
        cphoneNo: '',
        email: '',
      });
      dispatch(resetStore());
      dispatch(getMyInfo());
    }
  }, [activePageId]);

  return (
    <Container>
      <PageHeader
        title="내 정보"
        subTitle=""
      />
      <Paper border>
        <CustomForm
          form={form}
          layout="horizontal"
          initialValues={{ remember: true }}
          onFinish={onFinishForm}
          onValuesChange={onFormValueChange}
        >
          <Form.Item label="사용자 구분">
            <Text>{myInfo.data ? myInfo.data.roleNm : ''}</Text>
          </Form.Item>
          <Form.Item label="파트너">
            <CustomPartnerBtn ghost onClick={onClickPartner}>
              {myInfo.data && myInfo.data.ptnNm ? `${myInfo.data.ptnNm}(${myInfo.data.ptnId})` : ''}
            </CustomPartnerBtn>
          </Form.Item>
          <Form.Item label="사용자 아이디">
            <Text>{myInfo.data && myInfo.data.usrId ? myInfo.data.usrId : ''}</Text>
          </Form.Item>
          <CustomForimItem
            label="이름"
            name="usrNm"
            deafaultvalue=""
            rules={[
              {
                validator: (_, value) => {
                  if (value.trim() === '') {
                    return Promise.reject(new Error('이름을 입력해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'usrNm')} onBlur={(e) => onBlurItem(e, 'usrNm')} />
          </CustomForimItem>
          <CustomForimItem
            label="휴대폰 번호"
            name="cphoneNo"
            deafaultvalue=""
            rules={[
              {
                validator: (_, value) => {
                  if (value !== myInfo.data.cphoneNo && /^\d{3}-\d{3,4}-\d{4}$/.test(value) === false) {
                    return Promise.reject(new Error('올바른 휴대폰 번호가 아닙니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'cphoneNo')} onBlur={(e) => onBlurItem(e, 'cphoneNo')} />
          </CustomForimItem>
          <CustomForimItem
            label="이메일"
            name="email"
            deafaultvalue=""
            rules={[
              {
                validator: (_, value) => {
                  if (value.trim() === '') {
                    return Promise.reject(new Error('이메일을 입력해주세요.'));
                  }
                  if (value !== myInfo.data.email && /^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/.test(value) === false) {
                    return Promise.reject(new Error('이메일 형식에 맞지 않습니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'email')} onBlur={(e) => onBlurItem(e, 'email')} />
          </CustomForimItem>
          <Form.Item label="가입 구분">
            <Text>{myInfo.data && myInfo.data.joinTypeNm ? myInfo.data.joinTypeNm : ''}</Text>
          </Form.Item>
          <Form.Item label="가입일">
            <Text>{myInfo.data && myInfo.data.regDtm ? myInfo.data.regDtm : null}</Text>
          </Form.Item>
          <ButtonWrap>
            <Button type="primary" htmlType="submit">수정</Button>
          </ButtonWrap>
        </CustomForm>
      </Paper>
    </Container>
  );
}

const Container = styled(PageLayout)``;
const CustomForm = styled(Form)`
  padding: 20px;

  .ant-form-item-label {
    flex: 0 0 112px;

    font-size: 14px;
    line-height: 150%;
    color: #333;
  }
  .ant-form-item-control {
    max-width: initial;
    padding-left: 10px;
  }
  .ant-form-item-label {
    flex: 0 0 101px;
  }
  .ant-form-item {
    margin-bottom: 16px !important;
  }
  .ant-row {
    &:last-child {
      margin-bottom: 0;
    }
  }
`;
const StatusBtn = styled(Button)`
  padding: 7px 10px;
  margin-left: 20px;
  width: 65px;
  height: 34px;

  font-size: 13px;
  line-height: 20px;
`;

const Text = styled.span`
  font-size: 14px;
  line-height: 20px;
  font-weight: 400;

  color: #333;
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 40px;

  .ant-btn {
    width: 80px;
    height: 40px;
    border: 1px solid #E3E4E7;
    box-sizing: border-box;
    border-radius: 4px;
  }

  .ant-btn.ant-btn-primary {
    width: 250px;
    margin-left: 10px;
    border: 0;
  }
`;

const CustomForimItem = styled(Form.Item)`
  .ant-form-item-label>label.ant-form-item-required:not(.ant-form-item-required-mark-optional):after {
    display: none;
  }
`;

const CustomPartnerBtn = styled(Button)`
  cursor: 'pointer' !important;
  border: 0 !important;
  color: #000 !important;
  padding: 0 !important;

  span {
    text-decoration: underline;
  }
`;
export default MyPageContainer;
