import { all, fork, takeLatest } from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import {
  getMyInfo,
  putMyInfo,
} from './slice';
/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const getMyInfoSaga = createPromiseSaga(getMyInfo, API.Common.getMyInfo);
const putMyInfoSaga = createPromiseSaga(putMyInfo, API.Common.putMyInfo);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getMyInfo, getMyInfoSaga);
  yield takeLatest(putMyInfo, putMyInfoSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
