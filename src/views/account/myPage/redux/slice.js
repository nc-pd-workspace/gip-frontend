import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  myInfo: asyncApiState.initial(),
  modifyMyInfo: asyncApiState.initial(),
};

export const { actions, reducer } = createSlice({
  name: 'account/myPage',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    // get mypage userinfo flow
    getMyInfo: (state, { payload }) => {
      const result = { ...payload };
      state.myInfo = asyncApiState.request(result);
    },
    getMyInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };

      state.myInfo = asyncApiState.success(result);
    },
    getMyInfoFailure: (state, { payload }) => {
      state.myInfo = asyncApiState.error({ ...payload });
    },
    // get mypage userinfo flow
    putMyInfo: (state, { payload }) => {
      const result = { ...payload };
      state.modifyMyInfo = asyncApiState.request(result);
    },
    putMyInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.modifyMyInfo = asyncApiState.success(result);
    },
    putMyInfoFailure: (state, { payload }) => {
      state.modifyMyInfo = asyncApiState.error({ ...payload });
    },
  },
});

export const { resetStore, updateState, getMyInfo, putMyInfo } = actions;

export default reducer;
