import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import { Button } from 'antd';

import { alertMessage } from '../../../../components/message';

function IdSearchList({ list }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { changePassword } = useSelector((state) => state.account.login);

  const onClickGoLogin = (e) => {
    history.push('/');
  };

  const onClickSuspensedIdBtn = (e) => {
  };

  const onClickPartnerIdSearch = (e) => {
    alertMessage('파트너 아이디는 대시보드 대표 이메일로 문의 부탁 드립니다. (doyun.koo@gsretail.com)');
  };

  return (
    <Container>
      <InputContainer>
        <StyledTitle>사용자 아이디 안내</StyledTitle>
        <StyledText>
          아이디 찾기를 완료하였습니다.
          <br />
          가입한 아이디를 확인 후 로그인 해주세요.
        </StyledText>
        <IdSearchWrap>
          <IdList>
            {
              list.map((v, index) => (
                <IdListItem key={index}>
                  <span>{v.usrId}</span>
                  {
                    v.usrStatus !== 'ACTIVATE' && (
                      <SuspensedIdBtn onClick={onClickSuspensedIdBtn}>사용중지 계정</SuspensedIdBtn>
                    )
                  }
                </IdListItem>
              ))
            }

          </IdList>
        </IdSearchWrap>
      </InputContainer>
      <LoginButton onClick={onClickGoLogin}>로그인하기</LoginButton>
      <ForgotList>
        <li><CustomLink to="#" onClick={onClickPartnerIdSearch}>파트너 아이디 찾기</CustomLink></li>
        <li><CustomLink to="/find-password">비밀번호 찾기</CustomLink></li>
      </ForgotList>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  align-items: top;
  justify-content: start;
`;
const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 50px;
  justify-content: center;
`;
const StyledTitle = styled.h1`
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;

  text-align: center;

  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;
  color: var(--color-gray-700);

  text-align: center;
`;

const IdSearchWrap = styled.div`
  overflow: hidden;
  overflow-y: auto;
  margin-top: 50px;

  background: #FFFFFF;
  opacity: 0.6;

  border: 1px solid #E3E4E7;
  box-sizing: border-box;
  border-radius: 4px;
`;

const IdList = styled.ul`
  width: 350px;
  height: 140px;
  padding: 11px 20px;
`;
const IdListItem = styled.li`
  display: flex;
  height: 34px;
  font-size: 14px;
  line-height: 150%;
  
  color: #111111;

  span {
    flex: 1;
  }
`;
const SuspensedIdBtn = styled(Button)`
  width: 76px;
  height: 20px;
  padding: 1px 6px;
  background: #FFFFFF;
  flex-basis: 76px;

  font-size: 11px;
  line-height: 16px;
  color: #F04953;
  text-align: center;
  border: 1px solid #F04953;
  box-sizing: border-box;
  border-radius: 31px;
`;

const LoginButton = styled(Button)`
  width: 350px;
  height: 50px;
  margin-top: 30px;
  color: #fff;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;
`;

const ForgotList = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;

  font-size: 13px;
  line-height: 20px;
  text-align: center;
  color: var(--color-gray-900);

  li {
    position: relative;
  }

  li:last-child > a:after {
    display: none;
  }
`;

const CustomLink = styled(Link)`
  padding: 0;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;

  &:after {
    content: "";
    display: inline-block;
    width: 1px;
    height: 10px;
    margin-left: 12px;
    margin-right: 12px;
    background-color: #e3e4e7;
  }
`;

export default IdSearchList;
