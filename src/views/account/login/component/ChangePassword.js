import styled from 'styled-components';
import { Button } from 'antd';

import { useDispatch, useSelector } from 'react-redux';

import { useHistory } from 'react-router-dom';

import { useEffect, useState } from 'react';

import { putChangePassword } from '../redux/slice';
import LoginInputForm from './LoginInputForm';
import LoginInputFormItem from './LoginInputFormItem';

import { idSearchPw } from '../../../../utils/utils';

function ChangePassword({ onCancel, token, usrId }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const [error, setError] = useState([]);
  const { changePassword } = useSelector((state) => state.account.login);

  const onClickPasswordChange = (values) => {
    const params = {
      ...values,
    };
    const config = {
      headers: {},
    };
    config.headers['Authorization-gip-access-token'] = token;
    dispatch(putChangePassword({ params, config }));
  };

  const onFocusCurrPassword = () => {
    setError([]);
  };

  const onClickCancel = (e) => {
    onCancel();
  };

  useEffect(() => {
    if (changePassword.status === 'error') {
      const errorCode = changePassword.data && changePassword.data.error && changePassword.data.error.errorCode ? changePassword.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0209') {
          setError([{ target: 'currentPassword', errorFocus: ['currentPassword'], message: changePassword.data.error.errorDescription }]);
        } else if (errorCode === '0208') {
          setError([{ target: 'password', errorFocus: ['password'], message: changePassword.data.error.errorDescription }]);
        }
      }
    } else {
      setError([]);
    }
  }, [changePassword]);

  return (
    <Container>
      <CustomLoginInputForm onClickOk={onClickPasswordChange}>
        <StyledTitle>비밀번호 변경 안내</StyledTitle>
        <StyledText>
          6개월 이내 비밀번호를 변경하지 않은 경우 회원님의
          <br />
          소중한 개인정보를 위해
          {' '}
          <BlueText>비밀번호 변경</BlueText>
          을 권장하고 있습니다.
        </StyledText>
        <FormWrap>
          <CustomLoginInputFormItem
            label="현재 비밀번호"
            type="password"
            name="currentPassword"
            placeholder=""
            onFocusItem={onFocusCurrPassword}
            error={error}
            rules={[
              {
                validator: (_, value) => {
                  if (value === '') {
                    return Promise.reject(new Error('비밀번호를 입력해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          />
          <LineDiv />

          <CustomLoginInputFormItem
            label="새 비밀번호"
            type="password"
            name="password"
            placeholder="영문,숫자,특수문자 포함 8자 이상 입력"
            error={error}
            rules={[
              {
                validator: (_, value) => {
                  const regThree = /(.)\1\1/;
                  // eslint-disable-next-line no-useless-escape
                  const regSpecial = /[<>\\\/|&\'\"\` ]/gi;
                  const regOne = /^(?=.*[a-zA-z])(?=.*[0-9])(?=.*[$`~!@$!%*#^?&\\(\\)\-_=+])./gi;
                  if (value === '') {
                    return Promise.reject(new Error('새 비밀번호를 입력해주세요.'));
                  } if (regThree.test(value)) {
                    return Promise.reject(new Error('3자리 연속 반복된 문자나 숫자는 입력할 수 없습니다.'));
                  } if (regSpecial.test(value)) {
                    return Promise.reject(new Error('공백이나 제한된 특수문자는 사용하실 수 없습니다.'));
                  } if (value.length < 8 || value.length > 15) {
                    return Promise.reject(new Error('8~15자 이내로 설정해주세요.'));
                  } if (!regOne.test(value)) {
                    return Promise.reject(new Error('영문, 숫자, 특수문자를 각각 1자리 이상 포함해주세요.'));
                  } if (idSearchPw(usrId, value)) {
                    return Promise.reject(new Error('사용자 아이디와 4자 이상 동일할 수 없습니다.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          />
          <CustomLoginInputFormItem
            label="새 비밀번호 확인"
            type="password"
            name="confirm"
            placeholder="비밀번호를 다시 한번 입력해주세요."
            rules={[
              {
                required: true,
                message: '새 비밀번호 확인은 필수값입니다!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('동일한 비밀번호를 입력해주세요.'));
                },
              }),
            ]}
          />
        </FormWrap>
        <ButtonWrap>
          <CancelBtn onClick={onClickCancel}>30일 후 변경하기</CancelBtn>
          <OkBtn type="primary" htmlType="submit">변경하기</OkBtn>
        </ButtonWrap>
      </CustomLoginInputForm>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  align-items: top;
  justify-content: start;
`;
const StyledTitle = styled.h1`
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;

  text-align: center;

  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;
  color: var(--color-gray-700);

  text-align: center;
`;

const BlueText = styled.span`
  color: var(--color-blue-500);
`;

const FormWrap = styled.div`
  margin-top: 50px;
`;

const LineDiv = styled.div`
  margin-bottom: 20px;
  border-bottom: 1px solid #E3E4E7;
`;

const CustomLoginInputForm = styled(LoginInputForm)`
  margin-bottom: 20px;
`;

const CustomLoginInputFormItem = styled(LoginInputFormItem)`
  .ant-form-item-label > label {
    color: var(--color-blue-500) !important;
  }
  .ant-form-item-label>label.ant-form-item-required:not(.ant-form-item-required-mark-optional):after {
    display: none;
  }
`;

const ButtonWrap = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
`;

const CancelBtn = styled(Button)`
  width: 140px;
  height: 50px;
  border-radius: 4px;
  font-weight: 700;
  color: var(--color-gray-700);
`;
const OkBtn = styled(Button)`
  width: 200px;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;
`;
export default ChangePassword;
