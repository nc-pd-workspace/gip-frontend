import styled from 'styled-components';
import { useEffect, useState, useCallback, useRef } from 'react';
import { useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { Button } from 'antd';

import Images from '../../../../Images';

import Paper from '../../../../components/paper';
import Title from '../../../../components/title';
import LoginInputForm from './LoginInputForm';
import LoginInputFormItem from './LoginInputFormItem';

import { alertMessage } from '../../../../components/message';

function Login({ onClickLogin }) {
  const history = useHistory();
  const formRef = useRef(null);
  const [error, setError] = useState([]); // { target: 에러메세지가 표기될 input name, errorFocus: 표기된 인풋과 에러 테두리 표시해야할 인풋이 다를때 input name, message: 에러 내용 }
  const [formValues, setFormValues] = useState({});
  const { login } = useSelector((state) => state.account.login);

  const onClickPartnerIdSearch = (e) => {
    alertMessage('파트너 아이디는 대시보드 대표 이메일로 문의 부탁 드립니다.\n\n(doyun.koo@gsretail.com)');
  };

  const onClickLoginButton = (values) => {
    if (login.status !== 'pending') {
      onClickLogin(values);
    }
  };

  const onValuesChange = (values) => {
    setFormValues({ ...formValues, ...values });
    setError([]);
  };

  const getLoginDisabled = useCallback(() => {
    const { ptnId, usrId, password } = formValues;
    if ((ptnId && ptnId.length > 0 && usrId && usrId.length > 0 && password && password.length > 0)) return false;
    return true;
  }, [formValues, login]);

  useEffect(() => {
    if (login.status === 'error') {
      const errorCode = login.data && login.data.error && login.data.error.errorCode ? login.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0201' || errorCode === '0202' || errorCode === '0203' || errorCode === '0205' || errorCode === '0206') {
          setError([{ target: 'password',
            errorFocus: ['ptnId', 'usrId', 'password'],
            message: login.data.error.errorDescription }]);
        } else if (errorCode === '0207') {
          alertMessage('비밀번호가 5회 틀렸습니다.\n\n비밀번호 찾기를 통해 비밀번호를 재설정해주세요.', () => {
            history.push('/find-password');
          });
        }
      }
    } else {
      setError([]);
    }
  }, [login]);

  useEffect(() => {
    const savedUserId = window.localStorage.getItem('GIPADMIN_SAVE_USERID');
    if (savedUserId) {
      const jsonParse = JSON.parse(savedUserId);
      setFormValues({ ptnId: jsonParse.ptnId, usrId: jsonParse.usrId, save: jsonParse.save });
      formRef.current.setFieldsValue({
        ptnId: jsonParse.ptnId,
        usrId: jsonParse.usrId,
        save: jsonParse.save,
      });
    }
  }, []);

  return (
    <Container>
      <CustomLoginWrap>
        <Header>
          <img src={Images.login_logo} alt="login logo" />
          <CustomTitle
            level={1}
            title="파트너 로그인"
          />
        </Header>
        <CustomLoginInputForm ref={formRef} onClickOk={onClickLoginButton} onValuesChange={onValuesChange} okText="로그인">
          <CustomLoginInputFormItem
            name="ptnId"
            placeholder="파트너 아이디"
            style={{ marginBottom: '0' }}
            error={error}
            rules={[
              {
                required: true,
                message: '파트너 아이디를 입력해주세요.',
              },
            ]}
          />

          <CustomLoginInputFormItem
            name="usrId"
            placeholder="사용자 아이디"
            style={{ marginTop: '10px' }}
            error={error}
            rules={[
              {
                required: true,
                message: '사용자 아이디를 입력해주세요.',
              },
            ]}
          />
          <CustomLoginInputFormItem
            type="password"
            name="password"
            placeholder="비밀번호"
            style={{ marginTop: '10px' }}
            error={error}
            rules={[
              {
                required: true,
                message: '비밀번호를 입력해주세요.',
              },
            ]}
          />
          <CustomCheckBoxItem
            type="checkbox"
            name="save"
            checkText="파트너 아이디/ 사용자 아이디 저장"
          />
          <ButtonWrap>
            <OkBtn type="primary" htmlType="submit" className={getLoginDisabled() ? 'disabled' : ''} disabled={getLoginDisabled()}>로그인</OkBtn>
          </ButtonWrap>
        </CustomLoginInputForm>
        <ForgotList>
          <li><CustomLink to="#" onClick={onClickPartnerIdSearch}>파트너 아이디 찾기</CustomLink></li>
          <li><CustomLink to="/forgot-id">사용자 아이디 찾기</CustomLink></li>
          <li><CustomLink to="/find-password">비밀번호 찾기</CustomLink></li>
        </ForgotList>
      </CustomLoginWrap>
      <BottomButtonWrap>
        <CustomGrayLink>로그인이 처음이신가요?</CustomGrayLink>
        <CustomPasswordLink to="/first-login">비밀번호 설정하러가기</CustomPasswordLink>
      </BottomButtonWrap>
      <Link to="/about">
        <img src={Images.loginBanner} alt="매출로 연결되는 데이터기반 인사이트:지금 바로 GIP에서 확인하세요(소개사이트로 이동)" />
      </Link>
    </Container>
  );
}

const Container = styled.div`
  width: 440px;
  margin: 0 auto;
  padding: 60px 0;
`;
const CustomLoginWrap = styled(Paper)`
  width: 440px;
  padding: 40px 0;

  border: 0;
  border-radius: 8px;
  box-shadow: 0px 0px 12px rgb(190 209 231 / 40%), 0px 14px 12px rgb(222 232 243 / 50%);
  background-color: #fff;
`;

const Header = styled.div`
  text-align: center;

  img {
    margin-bottom: 16px;
  }
`;

const CustomTitle = styled(Title)`
  margin-bottom: 30px;
  justify-content: center;
`;

const CustomLoginInputForm = styled(LoginInputForm)`
  padding: 0 45px;
  input {
    background: #fff !important;
  }
  button{
    margin: 0 45px;
    width: 100%;
  }
`;

const CustomCheckBoxItem = styled(LoginInputFormItem)`
  width: 100%;
  margin-top: 14px;
  margin-bottom: 0px;
`;

const CustomLoginInputFormItem = styled(LoginInputFormItem)`
  margin-bottom: 0;
`;

const ForgotList = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;

  font-size: 13px;
  line-height: 20px;
  text-align: center;
  color: var(--color-gray-900);

  li {
    position: relative;
  }

  li:last-child > a:after {
    display: none;
  }
`;

const CustomLink = styled(Link)`
  padding: 0;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;
  color: var(--color-gray-900);

  &:after {
    content: "";
    display: inline-block;
    width: 1px;
    height: 10px;
    margin-left: 12px;
    margin-right: 12px;
    background-color: #e3e4e7;
  }
`;

const BottomButtonWrap = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: 30px;
  margin-bottom: 60px;
`;

const CustomGrayLink = styled.span`
  padding: 0 20px;
  border: 0;
  background: transparent;
  box-shadow: none !important;
  font-size: 13px;
  line-height: 20px;
  color: var(--color-gray-500);
`;

const CustomPasswordLink = styled(Link)`
  padding: 0 20px;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;
  color: var(--color-blue-500);
`;

const ButtonWrap = styled.div`
  display: flex;
  width: 100%;
  margin-top: 30px;
  justify-content: center;
`;

const OkBtn = styled(Button)`
  width: 100%;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;

  &.disabled {
    color: rgba(0,0,0,.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
  }
  &.disabled:hover {
    color: rgba(0,0,0,.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
  }
`;

export default Login;
