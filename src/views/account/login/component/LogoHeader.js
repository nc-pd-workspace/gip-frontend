import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Images from '../../../../Images';

function LogoHeader() {
  return (
    <Container>
      <Link to="/">
        <img src={Images.logo} alt="브랜드 로고" />
      </Link>
      <BetaLogo />
    </Container>
  );
}

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 999;
  display: flex;
  align-items: center;
  height: 60px;
  padding: 0 22px;
  background-color: #fff;
  > a {
    margin-top: 4px;
    display: inline-block;
  }
`;

const BetaLogo = styled.div`
  background-image:url(${Images.iconBeta});
  background-size:36px 16px;
  position:absolute;
  width: 36px;
  height: 16px;
  top: 22px;
  left: 167px;
`;

export default LogoHeader;
