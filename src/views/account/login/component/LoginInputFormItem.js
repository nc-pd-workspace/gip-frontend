import styled from 'styled-components';
import { Input, Checkbox, Form } from 'antd';
import { forwardRef, useImperativeHandle, useRef } from 'react';

function LoginInputFormItem({ label, type, name, placeholder, rules, className, style, checkText, error, onFocusItem, onBlurItem }, ref) {
  const itemRef = useRef(null);
  useImperativeHandle(ref, () => ({
    setFocus: () => itemRef.current.focus(),
  }));

  const renderType = (itemName) => {
    const isErrorFocus = error && error.filter((v) => v.errorFocus.indexOf(itemName) > -1).length > 0;

    switch (type) {
    case 'password': {
      return (
        <CustomFormItem
          className={`${isErrorFocus ? 'error' : ''}`}
          name={name}
          rules={rules}
          label={label}
          style={style}
        >
          <PasswordInput ref={itemRef} placeholder={placeholder} onFocus={onFocusItem} onBlur={onBlurItem} visibilityToggle={false} />
        </CustomFormItem>
      );
    }
    case 'checkbox': {
      return (
        <CustomFormItem
          className={`${isErrorFocus ? 'error' : ''}`}
          name={name}
          rules={rules}
          label={label}
          style={style}
          valuePropName="checked"
        >
          <Checkbox>{checkText}</Checkbox>
        </CustomFormItem>
      );
    }
    default: {
      return (
        <CustomFormItem
          className={`${isErrorFocus ? 'error' : ''}`}
          name={name}
          rules={rules}
          label={label}
          style={style}
        >
          <TextInput placeholder={placeholder} onFocus={onFocusItem} onBlur={onBlurItem} ref={itemRef} />
        </CustomFormItem>
      );
    }
    }
  };

  const renderError = () => {
    const errorMsg = error && error.filter((v) => v.target === name).length > 0 ? error.filter((v) => v.target === name) : [];

    if (errorMsg.length > 0) {
      return (
        <ErrorWrap role="alert" className="ant-form-item-explain-error">
          {errorMsg[0].message}
        </ErrorWrap>
      );
    }
    return (<></>);
  };

  return (
    <FormWrap className={className}>
      {renderType(name)}
      {renderError()}
    </FormWrap>
  );
}

const FormWrap = styled.div`
  margin-bottom: 20px;
`;
const CustomFormItem = styled(Form.Item)`

  &.ant-form-item {
    margin: 0;
  }

  .ant-form-item-label > label {
    margin-bottom: 2px;
    align-items: center;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
  
    color: var(--color-gray-900);
  }

  .ant-form-item-label > label:before {
    display: none !important;
  } 

  & input {
    font-size: 16px;
    line-height: 19px;
    display: flex;
    align-items: center;
    color: var(--color-gray-700);
    &::placeholder {
      color: var(--color-var-400) !important;
      opacity: 1;
    }
  }

  .ant-form-item-explain-error {
    font-size: 12px;
    line-height: 18px;
  }

  &.error {
    .ant-input {
      border: 1px solid #F04953 !important;
    }
  }

  .ant-form-item-label>label.ant-form-item-required:not(.ant-form-item-required-mark-optional):after {
    display: none;
  }
  .ant-form-item-control-input-content {
    font-size: 16px;
  }
  input:-webkit-autofill::first-line {
    font-size: 16px !important;
  }
`;

const TextInput = styled(Input)`
  display: flex;
  width: 350px;
  height: 50px;
  padding: 15px 14px;
  font-size: initial;
  line-height: 19px;
  align-items: center;
  box-sizing: border-box;
  color: var(--color-gray-700);
  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 1000px transparent inset;
  }
  &:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
    color: var(--color-gray-700) !important;
    -webkit-text-fill-color: var(--color-gray-700) !important;
  }
`;

const PasswordInput = styled(Input.Password)`
  display: flex;
  width: 350px;
  height: 50px;
  padding: 15px 14px;
  font-size: 16px;
  line-height: 19px;
  align-items: center;
  box-sizing: border-box;

  color: var(--color-gray-500);

  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 1000px transparent inset;
  }
  &:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
    color: var(--color-gray-700) !important;
    -webkit-text-fill-color: var(--color-gray-700) !important;
  }
`;

const ErrorWrap = styled.div`
  margin-top:4px;
  width: 100%;
  height: auto;
  min-height: 18px;
  opacity: 1;
  color: #ff4d4f;
  font-size: 12px;
  line-height: 18px;

  span {
    padding-left: 1px;
    img {
      width: 14px;
    }
    svg {
      margin-right: 2px;
    }
  }
`;

export default forwardRef(LoginInputFormItem);
