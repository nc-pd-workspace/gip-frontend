/* eslint-disable no-useless-escape */
import styled from 'styled-components';

import { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Button } from 'antd';

import { putSetPassword } from '../redux/slice';
import LoginInputForm from './LoginInputForm';
import LoginInputFormItem from './LoginInputFormItem';
import { idSearchPw } from '../../../../utils/utils';

function SetPassword({ token, idData }) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [error, setError] = useState([]); // { target: 에러메세지가 표기될 input name, errorFocus: 표기된 인풋과 에러 테두리 표시해야할 인풋이 다를때 input name, message: 에러 내용 }
  const [formValues, setFormValues] = useState({});
  const { setPassword } = useSelector((state) => state.account.login);

  const onClickSetPassword = (formData) => {
    const params = {
      password: formData.password,
    };
    const config = {
      headers: {},
    };
    config.headers['Authorization-temp-access-token'] = token;
    dispatch(putSetPassword({ params, config }));
  };

  const onClickCancel = (e) => {
    history.goBack();
  };

  const getLoginDisabled = useCallback(() => {
    const { password, confirm } = formValues;
    if ((password && password.length > 0 && confirm && confirm.length > 0)) return false;
    return true;
  }, [formValues]);

  const onValuesChange = (data, allData) => {
    setFormValues(allData);
  };

  useEffect(() => {
    if (setPassword.status === 'error') {
      const errorCode = setPassword.data && setPassword.data.error && setPassword.data.error.errorCode ? setPassword.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0209') {
          setError([{ target: 'currentPassword', errorFocus: ['password'], message: setPassword.data.error.errorDescription }]);
        } else if (errorCode === '0208') {
          setError([{ target: 'password', errorFocus: ['password'], message: setPassword.data.error.errorDescription }]);
        }
      }
    } else {
      setError([]);
    }
  }, [setPassword]);

  return (
    <Container>
      <LoginInputForm onClickOk={onClickSetPassword} onValuesChange={onValuesChange}>
        <StyledTitle>비밀번호 설정</StyledTitle>
        <StyledText>
          회원님의 소중한 개인정보 보호를 위해
          <br />
          새로운 비밀번호 설정 후 로그인해주세요.
        </StyledText>
        <FormWrap>
          <LoginInputFormItem
            label="새 비밀번호"
            type="password"
            name="password"
            placeholder="영문,숫자,특수문자 포함 8자 이상 입력"
            style={{ marginBottom: '0' }}
            rules={[
              {
                validator: (_, value) => {
                  const regThree = /(.)\1\1/;
                  // eslint-disable-next-line no-useless-escape
                  const regSpecial = /[<>\\\/|&\'\"\` ]/gi;
                  const regOne = /^(?=.*[a-zA-z])(?=.*[0-9])(?=.*[$`~!@$!%*#^?&\\(\\)\-_=+])./gi;
                  if (value === '') {
                    return Promise.reject(new Error('비밀번호를 입력해주세요.'));
                  } if (regThree.test(value)) {
                    return Promise.reject(new Error('3자리 연속 반복된 문자나 숫자는 입력할 수 없습니다.'));
                  } if (regSpecial.test(value)) {
                    return Promise.reject(new Error('공백이나 제한된 특수문자는 사용하실 수 없습니다.'));
                  } if (value.length < 8 || value.length > 15) {
                    return Promise.reject(new Error('8~15자 이내로 설정해주세요.'));
                  } if (!regOne.test(value)) {
                    return Promise.reject(new Error('영문, 숫자, 특수문자를 각각 1자리 이상 포함해주세요.'));
                  } if (idSearchPw(idData.usrId, value)) {
                    return Promise.reject(new Error('사용자 아이디와 4자 이상 동일할 수 없습니다.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
            error={error}
          />
          <LoginInputFormItem
            label="새 비밀번호 확인"
            type="password"
            name="confirm"
            placeholder="비밀번호를 다시 한번 입력해주세요."
            style={{ marginTop: '20px', marginBottom: '0' }}
            rules={[
              {
                required: true,
                message: '비밀번호를 한번 더 입력해주세요.',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('동일한 비밀번호를 입력해주세요.'));
                },
              }),
            ]}
            error={error}
          />
        </FormWrap>
        <ButtonWrap>
          <CancelBtn onClick={onClickCancel}>취소</CancelBtn>
          <OkBtn type="primary" htmlType="submit" className={getLoginDisabled() ? 'disabled' : ''} disabled={getLoginDisabled()}>완료</OkBtn>
        </ButtonWrap>
      </LoginInputForm>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  align-items: top;
  justify-content: start;
`;
const StyledTitle = styled.h1`
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;

  text-align: center;

  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;
  color: var(--color-gray-700);

  text-align: center;
`;

const BlueText = styled.span`
  color: var(--color-blue-500);
`;

const FormWrap = styled.div`
  margin-top: 50px;
`;

const ButtonWrap = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
`;

const CancelBtn = styled(Button)`
  width: 140px;
  height: 50px;
  border-radius: 4px;
  font-weight: 700;
  color: var(--color-gray-700);
`;
const OkBtn = styled(Button)`
  width: 200px;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;

  &.disabled {
    color: rgba(0,0,0,.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
  }
  &.disabled:hover {
    color: rgba(0,0,0,.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
  }
`;

export default SetPassword;
