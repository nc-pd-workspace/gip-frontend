import styled from 'styled-components';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import { Button } from 'antd';

import LoginInputForm from './LoginInputForm';
import LoginInputFormItem from './LoginInputFormItem';
import { alertMessage } from '../../../../components/message';

function FindPassword({ onClickPasswordSearch }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { findPassword } = useSelector((state) => state.account.login);
  const [error, setError] = useState([]);

  const onClickCancel = (e) => {
    history.push('/');
  };

  const onClickPartnerIdSearch = (e) => {
    alertMessage('파트너 아이디는 대시보드 대표 이메일로 문의 부탁 드립니다. (doyun.koo@gsretail.com)');
  };

  useEffect(() => {
    if (findPassword.status === 'error') {
      const errorCode = findPassword.data && findPassword.data.error && findPassword.data.error.errorCode ? findPassword.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0201') {
          setError([{ target: 'ptnId', errorFocus: ['ptnId'], message: '일치하는 아이디가 없습니다. 다시 입력해주세요.' }]);
        } else if (errorCode === '0202') {
          setError([{ target: 'usrId', errorFocus: ['usrId'], message: '일치하는 아이디가 없습니다. 다시 입력해주세요.' }]);
        } else if (errorCode === '0205') {
          setError([{ target: 'usrId', errorFocus: ['usrId'], message: '사용이 중지 된 사용자 아이디입니다.' }]);
        } else {
          setError([{ target: 'ptnId', errorFocus: ['ptnId'], message: findPassword.data.error.errorDescription }]);
        }
      }
    } else {
      setError([]);
    }
  }, [findPassword]);

  return (
    <Container>
      <CustomLoginInputForm onClickOk={onClickPasswordSearch}>
        <StyledTitle>비밀번호 설정</StyledTitle>
        <StyledText>
          비밀번호 설정을 위해 아이디를 입력해주세요.
          <br />
          회원정보가 확인되면 등록된 이메일로 인증번호가 발송됩니다.
        </StyledText>
        <FormWrap>
          <LoginInputFormItem
            label="파트너 아이디"
            name="ptnId"
            style={{ marginBottom: '0' }}
            rules={[
              {
                required: true,
                message: '파트너 아이디를 입력해주세요',
              },
            ]}
            error={error}
          />

          <LoginInputFormItem
            label="사용자 아이디"
            name="usrId"
            style={{ marginTop: '20px', marginBottom: '0' }}
            rules={[
              {
                required: true,
                message: '사용자 아이디를 입력해주세요',
              },
            ]}
            error={error}
          />
        </FormWrap>
        <ButtonWrap>
          <CancelBtn onClick={onClickCancel}>취소</CancelBtn>
          <OkBtn type="primary" htmlType="submit">확인</OkBtn>
        </ButtonWrap>
      </CustomLoginInputForm>
      <ForgotList>
        <li><CustomLink to="#" onClick={onClickPartnerIdSearch}>파트너 아이디 찾기</CustomLink></li>
        <li><CustomLink to="/forgot-id">사용자 아이디 찾기</CustomLink></li>
      </ForgotList>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  align-items: top;
  justify-content: start;
`;
const StyledTitle = styled.h1`
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;

  text-align: center;

  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;
  color: var(--color-gray-700);

  text-align: center;
`;

const FormWrap = styled.div`
  margin-top: 50px;
`;

const CustomLoginInputForm = styled(LoginInputForm)`
`;

const ForgotList = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;

  font-size: 13px;
  line-height: 20px;
  text-align: center;
  color: var(--color-gray-900);

  li {
    position: relative;
  }

  li:last-child > a:after {
    display: none;
  }
`;

const CustomLink = styled(Link)`
  padding: 0;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;

  &:after {
    content: "";
    display: inline-block;
    width: 1px;
    height: 10px;
    margin-left: 12px;
    margin-right: 12px;
    background-color: #e3e4e7;
  }
`;

const ButtonWrap = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
`;

const CancelBtn = styled(Button)`
  width: 140px;
  height: 50px;
  border-radius: 4px;
  font-weight: 700;
  color: var(--color-gray-700);
`;
const OkBtn = styled(Button)`
  width: 200px;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;
`;
export default FindPassword;
