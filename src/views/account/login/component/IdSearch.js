import styled from 'styled-components';

import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import { Button } from 'antd';

import LoginInputForm from './LoginInputForm';
import LoginInputFormItem from './LoginInputFormItem';

import { alertMessage } from '../../../../components/message';

function IdSearch({ onClickSearch }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const [error, setError] = useState([]);
  const { idSearch } = useSelector((state) => state.account.login);

  const onClickSearchButton = (values) => {
    onClickSearch(values);
  };

  const onClickCancel = (e) => {
    history.push('/');
  };

  const onClickPartnerIdSearch = (e) => {
    alertMessage('파트너 아이디는 대시보드 대표 이메일로 문의 부탁 드립니다. (doyun.koo@gsretail.com)');
  };

  const onValuesChange = (values) => {
    setError([]);
  };

  useEffect(() => {
    if (idSearch.status === 'error') {
      const errorCode = idSearch.data && idSearch.data.error && idSearch.data.error.errorCode ? idSearch.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0201' || errorCode === '0110' || errorCode === '0210') {
          setError([{ target: 'email', errorFocus: ['email'], message: '일치하는 회원정보가 없습니다. 다시 입력해주세요.' }]);
        } else if (errorCode === '0206') {
          setError([{ target: 'ptnId', errorFocus: ['ptnId'], message: idSearch.data.error.errorDescription }]);
        }
      }
    } else {
      setError([]);
    }
  }, [idSearch]);

  return (
    <Container>
      <CustomLoginInputForm onClickOk={onClickSearchButton} onValuesChange={onValuesChange} onClickCancel={onClickCancel} okText="완료" cancelText="취소">
        <StyledTitle>사용자 아이디 찾기</StyledTitle>
        <StyledText>
          가입한 이메일로 인증 후 사용자 아이디를 확인하세요.
        </StyledText>
        <FormWrap>
          <LoginInputFormItem
            label="파트너 아이디"
            name="ptnId"
            rules={[
              {
                required: true,
                message: '파트너 아이디를 입력해주세요.',
              },
            ]}
            error={error}
          />

          <LoginInputFormItem
            label="이메일"
            name="email"
            rules={[
              {
                validator: (_, value) => {
                  const regEmail = /^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/;
                  if (value === '') {
                    return Promise.reject(new Error('이메일 주소를 입력해주세요.'));
                  } if (!regEmail.test(value)) {
                    return Promise.reject(new Error('이메일 형식에 맞지 않습니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
            error={error}
          />
        </FormWrap>
        <ButtonWrap>
          <CancelBtn onClick={onClickCancel}>취소</CancelBtn>
          <OkBtn type="primary" htmlType="submit">메일발송</OkBtn>
        </ButtonWrap>
      </CustomLoginInputForm>
      <ForgotList>
        <li><CustomLink to="#" onClick={onClickPartnerIdSearch}>파트너 아이디 찾기</CustomLink></li>
        <li><CustomLink to="/find-password">비밀번호 찾기</CustomLink></li>
      </ForgotList>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  align-items: top;
  justify-content: start;
`;
const StyledTitle = styled.h1`
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;

  text-align: center;

  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;
  color: var(--color-gray-700);

  text-align: center;
`;

const FormWrap = styled.div`
  margin-top: 50px;
`;

const CustomLoginInputForm = styled(LoginInputForm)`
`;

const ForgotList = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;

  font-size: 13px;
  line-height: 20px;
  text-align: center;
  color: var(--color-gray-900);

  li {
    position: relative;
  }

  li:last-child > a:after {
    display: none;
  }
`;

const CustomLink = styled(Link)`
  padding: 0;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;

  &:after {
    content: "";
    display: inline-block;
    width: 1px;
    height: 10px;
    margin-left: 12px;
    margin-right: 12px;
    background-color: #e3e4e7;
  }
`;

const ButtonWrap = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
`;

const CancelBtn = styled(Button)`
  width: 140px;
  height: 50px;
  border-radius: 4px;
  font-weight: 700;
  color: var(--color-gray-700);
`;
const OkBtn = styled(Button)`
  width: 200px;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;
`;
export default IdSearch;
