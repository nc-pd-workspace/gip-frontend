import { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { Input, Button } from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { getEmailResend, updateStore } from '../redux/slice';
import { asyncApiState } from '../../../../redux/constants';
import { alertMessage } from '../../../../components/message';

let timeId = null;

function EmailAuth({ token, expiredAt, email, onClickSend, onClickCancel, error }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const timeRef = useRef(300);
  const { emailAuth, emailResend } = useSelector((state) => state.account.login);
  const [time, setTime] = useState(300);
  const [authCode, setAuthCode] = useState('');
  const [localError, setLocalError] = useState('');

  const showTime = (t) => {
    const pad = (v) => (v < 10 ? `0${v}` : v);
    const min = pad(parseInt(t / 60, 10));
    const sec = pad(t % 60);
    return `${min}:${sec}`;
  };

  const onClickEmailAuth = (e) => {
    onClickSend(authCode);
  };

  const onClickResend = (e) => {
    if (emailResend.status === 'pending') return;
    const params = {};
    const config = { headers: {} };
    if (token) {
      config.headers['Authorization-temp-access-token'] = token;
    } else if (email) {
      params.email = email;
    }
    dispatch(getEmailResend({ params, config }));
  };

  const onChangeAuthCode = (e) => {
    setAuthCode(e.target.value);
  };
  const registerInterval = () => {
    if (timeId !== null) return;
    timeId = setInterval(() => {
      if (timeRef.current > 0) {
        timeRef.current -= 1;
      } else {
        alertMessage('인증번호 입력 시간이 초과 하였습니다. 다시 인증해주세요.');
        clearInterval(timeId);
        timeId = null;
      }
      setTime(timeRef.current);
    }, 1000);
  };

  useEffect(() => {
    if (emailResend.status === 'success') {
      alertMessage('인증번호를 발송하였습니다.');
      timeRef.current = 300;
      setTime(300);
      registerInterval();

      dispatch(updateStore({ emailResend: asyncApiState.initial() }));
    } else if (emailResend.status === 'error') {
      const errorCode = emailResend.data && emailResend.data.error && emailResend.data.error.errorCode ? emailResend.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0002') {
          alertMessage('이메일 인증시간이 종료되었습니다. 로그인 페이지로 이동합니다.', () => {
            window.location.href('/');
          });
        }
      }
    }
  }, [emailResend]);

  useEffect(() => {
    setLocalError(error);
  }, [error]);

  useEffect(() => {
    registerInterval();
    return () => {
      if (timeId !== null) {
        clearInterval(timeId);
        timeId = null;
      }
    };
  }, []);

  return (
    <Container>
      <StyledTitle>이메일 인증</StyledTitle>
      <StyledText>
        <EmailText>{email}</EmailText>
        {' '}
        이메일로 인증번호를 발송하였습니다.
        <br />
        6자리 인증번호를 입력해 주세요.
      </StyledText>
      <InputContainer>
        <InputWrap>
          <InputTitle>인증번호</InputTitle>
          <TimeStapInputWrap>
            <Input placeholder="인증번호 6자리 입력" value={authCode} onChange={onChangeAuthCode} />
            <span>{showTime(time)}</span>
          </TimeStapInputWrap>
        </InputWrap>
        <StyledSendButton onClick={onClickResend}>
          재전송
        </StyledSendButton>
      </InputContainer>
      {
        localError && (
          <ErrorWrap role="alert" className="ant-form-item-explain-error">
            {localError}
          </ErrorWrap>
        )
      }
      <ButtonWrap>
        <CancelBtn onClick={(e) => history.replace('/')}>취소</CancelBtn>
        <OkBtn type="primary" onClick={onClickEmailAuth}>확인</OkBtn>
      </ButtonWrap>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding-top: 60px;
  flex-direction: column;
  height: 100vh;
  background: #F7F8FA;
  align-items: top;
  justify-content: start;
`;
const StyledTitle = styled.h1`
  margin-top: 50px;
  font-weight: 700;
  font-size: 32px;
  line-height: 48px;
  text-align: center;
  color: #000000;
`;

const StyledText = styled.p`
  padding: 0;
  margin: 20px 0 0;
  font-size: 16px;
  line-height: 150%;

  text-align: center;
`;

const EmailText = styled.span`
  color: var(--color-blue-500);
`;

const InputContainer = styled.div`
  display: flex;
  margin-top: 50px;
  justify-content: center;
  align-items: flex-end;
`;
const InputWrap = styled.div`
  
`;
const TimeStapInputWrap = styled.div`
  position:relative;
  display: flex;
  flex-direction: column;

  input {
    display: flex;
    width: 238px;
    height: 50px;
    padding: 15px 64px 15px 14px;
    font-size: 16px;
    line-height: 19px;
    align-items: center;
    color: var(--color-gray-700);
  }

  span {
    position: absolute;
    right: 14px;
    top: 16px;

    font-size: 14px;
    line-height: 17px;
    
    display: flex;
    align-items: center;
    text-align: right;

    color: #F04953;
  }
`;
const InputTitle = styled.p`
  display: flex;
  align-items: center;
  font-weight: 400;
  font-size: 12px;
  line-height: 18px;
  color: var(--color-gray-900);
  margin-bottom: 2px;
`;

const StyledSendButton = styled(Button)`
  width: 102px;
  height: 50px;
  flex-basis: 102px;
  box-sizing: border-box;
  margin-left: 10px;
  

  font-size: 16px;
  line-height: 150%;

  text-align: center;

  color: var(--color-gray-700);
`;

const ButtonWrap = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
`;

const CancelBtn = styled(Button)`
  width: 140px;
  height: 50px;
  border-radius: 4px;
  font-weight: 700;
  color: var(--color-gray-700);
`;
const OkBtn = styled(Button)`
  width: 200px;
  height: 50px;
  margin-left: 10px;
  background: var(--color-blue-500);
  border-radius: 4px;
  font-weight: 700;
`;

const ErrorWrap = styled.div`
  width: 352px;
  margin:4px auto 0;
  height: auto;
  min-height: 18px;
  opacity: 1;
  color: #ff4d4f;
  font-size: 12px;
  line-height: 18px;
  span {
    padding-left: 1px;
    img {
      width: 14px;
    }
    svg {
      margin-right: 2px;
    }
  }
`;
export default EmailAuth;
