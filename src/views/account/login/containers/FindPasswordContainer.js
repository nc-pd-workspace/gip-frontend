import { useEffect, useState } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { resetStore, getFindPassword, getFindPasswordCert } from '../redux/slice';

import EmailAuth from '../component/EmailAuth';
import LogoHeader from '../component/LogoHeader';
import FindPassword from '../component/FindPassword';
import SetPassword from '../component/SetPassword';

import { alertMessage } from '../../../../components/message';

function FindPasswordContainer() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [step, setStep] = useState('find-password'); // find-password(아이디입력), email-auth(이메일인증), set-password(password 설정)
  const [emailError, setEmailError] = useState('');
  const [findPasswordData, setFindPasswordData] = useState({
    ptnId: '',
    usrId: '',
  });

  const { findPassword, findPasswordCert, setPassword } = useSelector((state) => state.account.login);

  const reset = () => {
    dispatch(resetStore());
    setFindPasswordData({ ptnId: '', usrId: '' });
    setStep('find-password');
  };

  /** 이메일 인증코드 입력 확인 버튼 */
  const onClickSend = (authCode) => {
    const params = {
      ...findPasswordData,
      authenticationCode: authCode,
    };
    dispatch(getFindPasswordCert({ params }));
  };

  /** 아이디 입력 후 비밀번호 찾기 버튼 클릭 */
  const onClickPasswordSearch = (values) => {
    const params = {
      ...values,
    };
    setFindPasswordData(params);
    dispatch(getFindPassword({ params }));
  };

  const getRenderPageStep = (v) => {
    switch (v) {
    case 'email-auth': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <EmailAuth token="" expiredAt="" email={findPassword.data.email} onClickSend={onClickSend} error={emailError} />
        </LogoLayoutWrap>
      );
    }
    case 'set-password': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <SetPassword token={findPasswordCert.data.tempAccessToken} idData={findPasswordData} />
        </LogoLayoutWrap>
      );
    }
    default: {
      return (
        <FindPassword onClickPasswordSearch={onClickPasswordSearch} />
      );
    }
    }
  };

  useEffect(() => {
    // 여기서 서버 결과값에 의해 플로우 조절
    if (findPassword.status === 'success' && findPasswordCert.status === 'success') {
      setStep('set-password');
    } else if (findPassword.status === 'success') {
      setStep('email-auth');
    } else {
      setStep('find-password');
    }
  }, [findPassword, findPasswordCert]);

  useEffect(() => {
    if (findPasswordCert.status === 'error') {
      const errorCode = findPasswordCert.data && findPasswordCert.data.error && findPasswordCert.data.error.errorCode ? findPasswordCert.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0110') {
          alertMessage(findPasswordCert.data.error.errorDescription);
        } else {
          setEmailError(findPasswordCert.data.error.errorDescription);
        }
      }
    }
  }, [findPasswordCert]);

  useEffect(() => {
    if (setPassword.status === 'success') {
      alertMessage('비밀번호가 변경 되었습니다.', () => {
        history.push('/');
      });
    }
  }, [setPassword]);

  useEffect(() => () => {
    reset();
  }, []);

  return (
    <Container>
      {
        getRenderPageStep(step)
      }
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: start;
  justify-content: center;
  background: #F7F8FA;
`;

const LogoLayoutWrap = styled.div`
  display: flex;
  flex: 1 1 1;
  width: 100%;
  justify-content: center;;
  background-color: #F7F8FA;
`;

export default FindPasswordContainer;
