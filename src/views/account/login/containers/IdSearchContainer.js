import { useEffect, useState } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { resetStore, getIdSearch, getIdSearchCert } from '../redux/slice';

import LogoHeader from '../component/LogoHeader';
import IdSearch from '../component/IdSearch';
import EmailAuth from '../component/EmailAuth';
import IdSearchList from '../component/IdSearchList';
import { alertMessage } from '../../../../components/message';

function IdSearchContainer() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [step, setStep] = useState('forgot'); // forgot(아이디입력), email-auth(이메일인증), id-list(id 목록)
  const [emailError, setEmailError] = useState('');
  const [idSearchData, setIdSearchData] = useState({
    ptnId: '',
    email: '',
  });

  const { idSearch, idSearchCert } = useSelector((state) => state.account.login);

  const reset = () => {
    dispatch(resetStore());
    setIdSearchData({ ptnId: '', email: '' });
    setStep('forgot');
  };

  const onClickSend = (authCode) => {
    const params = {
      ...idSearchData,
      authenticationCode: authCode,
    };
    dispatch(getIdSearchCert({ params }));
  };

  const onClickSearch = (values) => {
    const params = {
      ...values,
    };
    setIdSearchData(params);
    dispatch(getIdSearch({ params }));
  };

  const getRenderPageStep = (v) => {
    switch (v) {
    case 'email-auth': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <EmailAuth token={null} expiredAt="" email={idSearch.data.email} onClickSend={onClickSend} error={emailError} />
        </LogoLayoutWrap>
      );
    }
    case 'id-list': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <IdSearchList list={idSearchCert.data} />
        </LogoLayoutWrap>
      );
    }
    default: {
      return (
        <IdSearch onClickSearch={onClickSearch} />
      );
    }
    }
  };

  useEffect(() => {
    // 여기서 서버 결과값에 의해 플로우 조절
    if (idSearch.status === 'success' && idSearchCert.status === 'success') {
      setStep('id-list');
    } else if (idSearch.status === 'success') {
      setStep('email-auth');
    } else {
      setStep('forgot');
    }
  }, [idSearch, idSearchCert]);

  useEffect(() => {
    if (idSearchCert.status === 'error') {
      const errorCode = idSearchCert.data && idSearchCert.data.error && idSearchCert.data.error.errorCode ? idSearchCert.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0110') {
          alertMessage(idSearchCert.data.error.errorDescription);
        } else {
          setEmailError(idSearchCert.data.error.errorDescription);
        }
      }
    }
  }, [idSearchCert]);

  useEffect(() => () => {
    reset();
  }, []);

  return (
    <Container>
      {
        getRenderPageStep(step)
      }
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: start;
  justify-content: center;
  background: #F7F8FA;
`;

const LogoLayoutWrap = styled.div`
  display: flex;
  flex: 1 1 1;
  width: 100%;
  justify-content: center;;
  background-color: #F7F8FA;
`;

export default IdSearchContainer;
