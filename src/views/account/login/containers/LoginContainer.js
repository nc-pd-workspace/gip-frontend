import { useEffect, useState } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { resetStore, postLogin, postLoginCert, postLoginCertFailure, putNextCharge, postLoginPass } from '../redux/slice';
import { setUserInfo } from '../../../../redux/commonReducer';

import EmailAuth from '../component/EmailAuth';
import LogoHeader from '../component/LogoHeader';
import Login from '../component/Login';
import Terms from '../component/Terms';
import ChangePassword from '../component/ChangePassword';
import { alertMessage } from '../../../../components/message';

function LoginContainer() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [step, setStep] = useState('login'); // login(로그인), email-auth(이메일인증), change-password(패스워드 변경), terms(약관동의)
  const [emailError, setEmailError] = useState('');
  const [loginData, setLoginData] = useState({
    ptnId: '',
    usrId: '',
    password: '',
  });
  const [tempToken, setTempToken] = useState(null);

  const { login, loginCert, emailResend, nextCharge, changePassword, terms, loginPass } = useSelector((state) => state.account.login);
  const { userInfo } = useSelector((state) => state.common);

  const reset = () => {
    dispatch(resetStore());
    setEmailError('');
    setStep('login');
  };

  const onCancelChangePassword = () => {
    const config = { headers: {} };
    config.headers['Authorization-gip-access-token'] = loginCert.data.accessToken;

    dispatch(putNextCharge({ params: {}, config }));
  };

  const onClickSend = (authCode) => {
    if (!login.data || !login.data.tempAccessToken) {
      alertMessage('인증정보를 확인할 수 없습니다.');
      return;
    }
    const config = { headers: {} };
    config.headers['Authorization-temp-access-token'] = login.data.tempAccessToken;
    if (authCode === '0000') {
      dispatch(postLoginCertFailure({ params: { authenticationCode: authCode }, config }));
      return;
    }
    dispatch(postLoginCert({ params: { authenticationCode: authCode }, config }));
  };

  const onClickLogin = (values) => {
    const params = {
      ...values,
    };

    setLoginData(params);
    dispatch(postLogin({ params }));
  };

  useEffect(() => {
    if (login.status === 'success') {
      setTempToken(login.data.tempAccessToken);
    } else if (emailResend.status === 'success') {
      setTempToken(emailResend.data.tempAccessToken);
    }
  }, [login, emailResend]);

  const getRenderPageStep = (v) => {
    switch (v) {
    case 'email-auth': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <EmailAuth token={tempToken} expiredAt={login.data.expiredDate} onClickSend={onClickSend} email={login.data.email} error={emailError} />
        </LogoLayoutWrap>
      );
    }
    case 'change-password': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <ChangePassword onCancel={onCancelChangePassword} usrId={loginData.usrId} token={loginCert.data.accessToken} loginData={loginData} />
        </LogoLayoutWrap>
      );
    }
    case 'terms': {
      return (
        <LogoLayoutWrap>
          <LogoHeader />
          <Terms token={loginCert.data.accessToken} />
        </LogoLayoutWrap>
      );
    }
    default: {
      return (
        <Login onClickLogin={onClickLogin} />
      );
    }
    }
  };

  const saveAndSetUserInfo = () => {
    window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(loginCert.data));
    dispatch(setUserInfo(loginCert.data));
    window.localStorage.setItem('GIPADMIN_RESPONSE_LOGIN_INFO', JSON.stringify(loginCert.data));
    window.localStorage.removeItem('GIPADMIN_RESPONSE_LOGIN_INFO');
  };

  useEffect(() => {
    // 여기서 서버 결과값에 의해 플로우 조절
    switch (step) {
    case 'login': {
      if (login.status === 'success') {
        if (login?.data?.authUseYn === 'N') {
          const params = {};
          const config = { headers: {} };
          config.headers['Authorization-temp-access-token'] = login.data.tempAccessToken;

          dispatch(postLoginPass({ params, config }));
        } else {
          setStep('email-auth');
        }
      }
      break;
    }
    case 'email-auth': {
      if (login.status === 'success' && loginCert.status === 'success') {
        setEmailError('');
        if (loginCert.data.initLoginYn === 'Y') {
          setStep('terms');
        } else if (loginCert.data.passwordExpiredYn === 'Y') {
          setStep('change-password');
        } else {
          saveAndSetUserInfo();
        }
      }
      break;
    }
    case 'terms': {
      if (terms.status === 'success') {
        alertMessage('약관에 동의 하셨습니다', () => {
          saveAndSetUserInfo();
        });
      }
      break;
    }
    case 'change-password': {
      if (changePassword.status === 'success' || nextCharge.status === 'success') {
        if (changePassword.status === 'success') {
          alertMessage('비밀번호가 변경되었습니다.', () => {
            saveAndSetUserInfo();
          });
        } else {
          saveAndSetUserInfo();
        }
      }
      break;
    }
    default: break;
    }
  }, [login, loginCert, nextCharge, changePassword, terms]);

  useEffect(() => {
    if (loginPass.status === 'success') {
      // 2차 인증 플로우 우회하는 계정추가
      window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(loginPass.data));
      dispatch(setUserInfo(loginPass.data));
      window.localStorage.setItem('GIPADMIN_RESPONSE_LOGIN_INFO', JSON.stringify(loginPass.data));
      window.localStorage.removeItem('GIPADMIN_RESPONSE_LOGIN_INFO');
    }
  }, [loginPass]);

  useEffect(() => {
    if (loginCert.status === 'error') {
      const errorCode = loginCert.data && loginCert.data.error && loginCert.data.error.errorCode ? loginCert.data.error.errorCode : null;

      if (errorCode) {
        if (errorCode === '0110') {
          alertMessage(loginCert.data.error.errorDescription);
        } else {
          setEmailError(loginCert.data.error.errorDescription);
        }
      }
    }
  }, [loginCert]);

  useEffect(() => {
    if (userInfo && userInfo.accessToken) {
      if (loginData.save) {
        window.localStorage.setItem('GIPADMIN_SAVE_USERID', JSON.stringify({ ptnId: loginData.ptnId, usrId: loginData.usrId, save: loginData.save }));
      } else {
        window.localStorage.removeItem('GIPADMIN_SAVE_USERID');
      }
      history.replace('/');
    }
  }, [userInfo]);

  useEffect(() => () => {
    reset();
  }, []);

  return (
    <Container>
      {
        getRenderPageStep(step)
      }
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: start;
  justify-content: center;
  background: #F7F8FA;
`;

const LogoLayoutWrap = styled.div`
  display: flex;
  flex: 1 1 1;
  width: 100%;
  justify-content: center;
  background-color: #F7F8FA;
`;

export default LoginContainer;
