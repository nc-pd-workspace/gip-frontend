import { useEffect, useState, useRef } from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { useDispatch, useSelector } from 'react-redux';

import { pageComponents } from './pageComponents';
import ScrollButton from './ScrollButton';

function PageContainer({ pageId, query, onScroll }) {
  const activePage = useRef();
  const dispatch = useDispatch();
  const { activePageId, headerToggle } = useSelector((state) => ({
    activePageId: state.common.activePageId,
    headerToggle: state.common.headerToggle,
  }));
  const PageComponent = pageComponents[pageId];
  // move to button visible
  const [moveTopVisible, setMoveTopVisible] = useState(false);
  const topMoveButtonVisiblePosition = 80;

  const onScrolled = (e) => {
    const scrolled = activePage.current.scrollTop;
    setMoveTopVisible(topMoveButtonVisiblePosition < scrolled);

    if (activePageId === pageId) {
      onScroll(pageId, e);
    }
  };

  useEffect(() => {
    // dispatch(headerToggleChange(false));
    setMoveTopVisible(false);
    // if (pageId === activePageId) {
    //   onScroll(pageId, { target: {
    //     scrollTop: activePage.current.scrollTop,
    //     offsetHeight: activePage.current.offsetHeight,
    //     scrollHeight: activePage.current.scrollHeight,
    //   } });
    // }
    // activePage.current.removeEventListener('scroll', onScroll);
    // if (activePageId === pageId) {
    //   activePage.current.addEventListener('scroll', onScroll);
    // }
  }, [activePageId]);
  return (
    <Container ref={activePage} className={cn({ isActivePage: activePageId === pageId })} onScroll={onScrolled}>
      <PageComponent query={query} />
      <ScrollButton visible={moveTopVisible} />
    </Container>
  );
}

const Container = styled.div`
  background-color: var(--background-default);
  -webkit-overflow-scrolling: touch;
  overscroll-behavior-y: none;
  height: 0;
  overflow: hidden;
  &.isActivePage {
    padding-top: 60px;
    z-index: 99;
    height: auto;
    overflow: unset !important
  }
`;

export default PageContainer;
