import styled, { keyframes } from 'styled-components';

import Images from '../../Images';

function ScrollButton({ visible }) {
  const scrollToTop = () => {
    document.querySelector('.isActivePage').scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <>
      <SetToTop
        onClick={scrollToTop}
        className={visible ? 'show' : 'hide'}
      />
    </>
  );
}

const viewAnimation = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;
const viewAnimation02 = keyframes`
  0% { opacity: 1; }
  100% { opacity: 0; }
`;

const SetToTop = styled.button`
  position: fixed;
  z-index: 9999;
  right: 14px;
  bottom: 44px;
  border: none;
  background-image: url(${Images.btnMoveToTop});
  background-color: transparent;
  width: 70px;
  height: 70px;
  cursor: pointer;
  bottom: 44px;
  &.show {
    bottom: 44px;
    display: block;
    animation: ${viewAnimation} .5s;
    &:hover, &:active {
      transition: .3s;
      bottom: 46px;
    }
  }
  &.hide {
    transition-delay: .4s;
    opacity: 0;
    bottom: -70px;
    animation: ${viewAnimation02} .3s;
  }
`;

export default ScrollButton;
