import styled from 'styled-components';

import { useSelector } from 'react-redux';

import { useParams } from 'react-router-dom';

import { useEffect } from 'react';

import { usePageTab } from '../shared/pageTab/hooks/usePageTab';

function ViewContainer({ children }) {
  const { id: activePageId } = useParams();
  const { openedPages } = useSelector((state) => ({
    openedPages: state.common.openedPages,
  }));
  const { openPage } = usePageTab();
  useEffect(() => {
    openPage(activePageId);
  }, []);

  return (
    <Container>
      {
        children
      }
    </Container>
  );
}

const Container = styled.div`
  z-index: -10;
  background-color: #f7f8fa;
`;

export default ViewContainer;
