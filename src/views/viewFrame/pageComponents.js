import MainContainer from '../main/containers/MainContainer';
import BrandLandscapeContainer from '../brand/brandLandscape/containers/BrandLandscapeContainer';
import TopBrandContainer from '../brand/topBrand/containers/TopBrandContainer';

import AllActivtyCustomerContainer from '../customer/allActivityCustomer/containers/AllActivityCustomerContainer';
import NewCustomerContainer from '../customer/newCustomer/containers/NewCustomerContainer';
import StepCustomerContainer from '../customer/stepCustomer/containers/StepCustomerContainer';
import Top5BrandCustomerContainer from '../customer/top5BrandCustomer/containers/Top5BrandCustomerContainer';

import BrandPerformanceContainer from '../period/brandPerformance/containers/BrandPerformanceContainer';
import StepConversionContainer from '../period/stepConversion/containers/StepConversionContainer';
import ProductCustomerContainer from '../period/productCustomer/containers/ProductCustomerContainer';
import InflowKeywordContainer from '../period/inflowKeyword/containers/InflowKeywordContainer';

import MyPageContainer from '../account/myPage/containers/MyPageContainer';
import PartnersContainer from '../settings/partners/containers/PartnersContainer';
import UsersContainer from '../settings/users/containers/UsersContainer';
import UserDetailContainer from '../settings/users/containers/UserDetailContainer';
import DevComponent from '../_DEV/component';
import MyPartnerContainer from '../account/myPartnerPage/containers/MyPartnersPageContainer';

export const pageComponents = {
  main: MainContainer,
  brandLandscape: BrandLandscapeContainer,
  topBrand: TopBrandContainer,

  allActivityCustomer: AllActivtyCustomerContainer,
  newCustomer: NewCustomerContainer,
  stepCustomer: StepCustomerContainer,
  top5BrandCustomer: Top5BrandCustomerContainer,

  brandPerformance: BrandPerformanceContainer,
  stepConversion: StepConversionContainer,
  productCustomer: ProductCustomerContainer,
  inflowKeyword: InflowKeywordContainer,

  myPage: MyPageContainer,
  myPartnerPage: MyPartnerContainer,
  partners: PartnersContainer,
  users: UsersContainer,
  userDetail: UserDetailContainer,
  component: DevComponent,
};
