import React from 'react';
import styled from 'styled-components';

import { COLORS } from '../../../styles/Colors';

import Paper from '../../../components/paper';

function ColorPalette() {
  const colorView = (color, colorName) => (
    <ViewBox>
      <ViewColor style={{ background: color }} />
      <ViewDescription>
        <h4>{colorName}</h4>
        <p>{color}</p>
      </ViewDescription>
    </ViewBox>
  );

  const colorTest = (color) => {
    const colorArray = Object.entries(COLORS[color]);
    const output = colorArray.map((item) => (
      <li>
        <div style={{ backgroundColor: item[1] }} />
        <p>
          {color}
          <br />
          {item[0]}
        </p>
      </li>
    ));
    return (
      <ColorPaletteWrap>
        {output}
      </ColorPaletteWrap>
    );
  };

  return (
    <Container>
      <ColorHeader>
        <h2>Color Palette</h2>
      </ColorHeader>
      <ColorBody>
        <TypeHeader>FIX COLOR</TypeHeader>
        <ColorSet>
          <ColorSetTitle>
            고객 유형별 지정 컬러
          </ColorSetTitle>
          {colorView(COLORS.BLUE[500], '활동고객')}
          {colorView(COLORS.GREEN[300], '방문고객')}
          {colorView(COLORS.BLUE[600], '관심고객')}
          {colorView(COLORS.VIOLET[300], '주문고객')}
          {colorView(COLORS.BLUE[400], '충성고객')}
          {colorView(COLORS.YELLOW[400], '신규고객')}
          {colorView(COLORS.VIOLET[500], '주문고객')}
        </ColorSet>
        <ColorSet>
          <ColorSetTitle>
            성별 컬러
          </ColorSetTitle>
          {colorView(COLORS.GREEN[400], '남자')}
          {colorView(COLORS.VIOLET[400], '여자')}
        </ColorSet>
        <ColorSet>
          <ColorSetTitle>
            브랜드 컬러
          </ColorSetTitle>
          {colorView(COLORS.GRAY[400], 'GS SHOP')}
          {colorView(COLORS.BLUE[500], '비교 브랜드')}
        </ColorSet>
        <ColorSet>
          <ColorSetTitle>
            빈데이터 컬러
          </ColorSetTitle>
          {colorView(COLORS.GRAY[200], '알수없음')}
        </ColorSet>

        <TypeHeader>Color palettes</TypeHeader>
        <ColorSet>
          <h3>Sequential palettes</h3>
          <TypeHeaderDescription>
            Sequential (순차) 데이터는 단일 색조 내의 단계를 사용하고 색상 밝기를 사용하여 낮음에서 높음까지의 범위를 나타냅니다.
            <br />
            BLUE는 단일 데이터 범위가 있는 차트의 기본 색조이며, 비슷한 패턴을 따라 다른 색조를 대신 사용할 수 있습니다. 누적막대 차트, 히트맵, 피라미드 차트 등에서 활용할 수 있습니다.
            <br />
            (BLUE → GREEN → VIOLET 의 순으로 색조 사용을 권장합니다)
          </TypeHeaderDescription>
        </ColorSet>
        {colorTest('BLUE')}
        {colorTest('VIOLET')}
        {colorTest('GREEN')}
        {colorTest('RED')}
        {colorTest('YELLOW')}
        {colorTest('GRAY')}
      </ColorBody>
    </Container>
  );
}

const Container = styled(Paper)`
  padding:48px 40px;
  border: var(--border-default);
  border-radius: 0;
`;

const ColorSet = styled.div`
  display:flex;
  flex-wrap:wrap;
  padding:40px 0;
`;
const ColorSetTitle = styled.div`
  padding: 0 0 8px;
  flex:0 0 100%;
`;
const ColorHeader = styled.div`
  h2 {
    font-size: var(--font-header01);
    font-weight: 700;
    color: var(--color-steelGray-900);
  }
`;
const TypeHeaderDescription = styled.div`
  flex:1 1 100%;
  padding: 16px 0 0 12px;
`;
const ColorBody = styled.article`
  display: flex;
  flex-wrap: wrap;
  h3 {
    flex: 1 0 100%;
  }
`;

const ViewBox = styled.div`
  width: 100px;
  border: var(--border-default);
  border-radius: var(--border-radius-default);
  overflow: hidden;
  flex: 0 0 100px;
  margin:10px 20px 10px 0;
`;
const ViewColor = styled.div`
  width: 100px;
  height: 90px;
`;
const ViewDescription = styled.div`
  padding: 12px;
  background-color: var(--color-white);
  p {
    font-size: 10px;
    color: var(--color-gray-600);
  }
`;
const TypeHeader = styled.h3`
  font-size: 12px;
  font-weight: 600;
  padding:60px 0 12px;
  border-bottom: 1px solid var(--color-gray-200);
`;
const ColorPaletteWrap = styled.ul`
  padding-top: 32px;
  display: flex;
  width: 100%;
  li {
    flex: 1 1 auto;
    div {
      height: 64px;
    }
    p {
      text-align:center;
      padding-top: 4px;
      font-size: 11px;
      font-weight: 400;
      line-height: 14px;

    }
  }
`;
export default ColorPalette;
