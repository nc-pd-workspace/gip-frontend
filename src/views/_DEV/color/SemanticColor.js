import styled from 'styled-components';

// import Paper from '../../../components/paper';
import { COLORS } from '../../../styles/Colors';

import Descripition from '../../../components/paper/descripition';
import Paper from '../../../components/paper';

function SemanticColor() {
  const colorView = (color, colorName, opacity) => (
    <ViewBox>
      <ViewColor style={{ background: color, opacity }} />
      <ViewDescription>
        <h4>{colorName}</h4>
        <p>
          {color}
          {
            opacity && (
              <>
                &nbsp;/&nbsp;
                {opacity}
              </>
            )
          }
        </p>
      </ViewDescription>
    </ViewBox>
  );

  const colorTest = (color) => {
    const colorArray = Object.entries(COLORS[color]);
    const output = colorArray.map((item) => (
      <li>
        <div style={{ backgroundColor: item[1] }} />
        <p>
          {color}
          <br />
          {item[0]}
        </p>
      </li>
    ));
    return (
      <ColorPalette>
        {output}
      </ColorPalette>
    );
  };

  return (
    <Container>
      <ColorHeader>
        <h2>SEMANTIC COLOR</h2>
        <HeaderDescription>
          Semantic 컬러는 컬러의 목적과 적용하는 인터페이스에 따라 정의합니다.
        </HeaderDescription>
      </ColorHeader>
      <ColorBody>
        <TypeHeader>BACKGROUND COLOR</TypeHeader>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[50], 'GRAY-50')
            }
            <ColorText>
              <h4>DEFAULT</h4>
              <Descripition
                text={[
                  '기본 배경 컬러입니다.',
                  '테이블 리스트 오버시 배경, 본문 카드 내 하위 차트 카드의 배경에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.BLACK_DIMM[50], 'BLACK')
            }
            <ColorText>
              <h4>DIM</h4>
              <Descripition
                text={[
                  '화면 위에서 뜨는 팝업이 노출됐을 때 사용하는 배경 컬러입니다.',
                  '#000000 / opacity 50%',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.BLUE[50], 'BLUE-50')
            }
            <ColorText>
              <h4>TABLE HIGHLIGHT</h4>
              <Descripition
                text={[
                  '테이블 리스트에서 내용을 강조하기 위해 사용하는 배경 컬러입니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.BLUE_GRAY, 'BLUE GRAY')
            }
            <ColorText>
              <h4>EXCEPTIONAL</h4>
              <Descripition
                text={[
                  '메인 상단 정보 영역의 배경 컬러입니다.',
                  'GRAY-50 컬러와 상하 그라데이션 형태로 사용되며,메인 상단 영역에서만 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <TypeHeader>BORDER COLOR</TypeHeader>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[200], 'GRAY-200')
            }
            <ColorText>
              <h4>DEFAULT</h4>
              <Descripition
                text={[
                  '기본 라인 컬러입니다.',
                  '본문 카드의 외곽선과 영역간 구분 라인에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[200], 'GRAY-200', '50%')
            }
            <ColorText>
              <h4>CELL / LIST</h4>
              <Descripition
                text={[
                  '테이블 또는 리스트 구분 라인에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <TypeHeader>TEXT COLOR</TypeHeader>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[900], 'GRAY-900')
            }
            <ColorText>
              <h4>PRIMARY</h4>
              <Descripition
                text={[
                  '기본 라인 컬러입니다.',
                  '본문 카드의 외곽선과 영역간 구분 라인에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[700], 'GRAY-700')
            }
            <ColorText>
              <h4>SECONDARY</h4>
              <Descripition
                text={[
                  '서브 타이틀 또는 테이블 내 헤더텍스트에 사용합니다.',
                  '텍스트 필드 내 아이콘 컬러에도 함께 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GRAY[500], 'GRAY-500')
            }
            <ColorText>
              <h4>DISABLE</h4>
              <Descripition
                text={[
                  '텍스트필드 내 플레이스 홀더 텍스트에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

        <TypeHeader>FUNCTIONAL COLOR</TypeHeader>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.RED[500], 'RED-500')
            }
            <ColorText>
              <h4>DANGER / ERROR</h4>
              <Descripition
                text={[
                  '오류, 실패, 하락과 같은 부정적인 메시지에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.YELLOW[500], 'YELLOW-500')
            }
            <ColorText>
              <h4>WARNING</h4>
              <Descripition
                text={[
                  '주의, 경고와 같은 메세지에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.GREEN[500], 'GREEN-500')
            }
            <ColorText>
              <h4>SUCCESS</h4>
              <Descripition
                text={[
                  '확인, 성공과 같은 긍정적인 메세지에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>
        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.BLUE[500], 'BLUE-500')
            }
            <ColorText>
              <h4>INFO</h4>
              <Descripition
                text={[
                  '정보를 전달하는 메세지에 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
        </ColorSet>

      </ColorBody>
    </Container>
  );
}

const Container = styled(Paper)`
  padding:48px 40px;
  border: var(--border-default);
  border-radius: 0;
`;

const ColorSet = styled.div`
  padding:40px 0;
`;
const ColorType = styled.div`
  display: flex;
`;
const ColorText = styled.div`
  flex: 1 1 auto;
  padding: 0 0 0 20px;
  h4 {
    font-size: 12px;
    line-height: 14px;
    color: var(--color-gray-800);
    font-weight: 700;
  }
`;
const ColorHeader = styled.div`
  h2 {
    font-size: var(--font-header01);
    font-weight: 700;
    color: var(--color-steelGray-900);
  }
`;
const HeaderDescription = styled.div`
  color: var(--color-gray-800);
  font-weight: 400;
  font-size: 16px;
  padding-top: 24px;
`;
const ColorBody = styled.article`
  display: flex;
  flex-wrap: wrap;
  h3 {
    flex: 1 0 100%;
  }
  > div {
    flex: 1 1 50%;
  }
`;

const ViewBox = styled.div`
  width: 100px;
  border: var(--border-default);
  border-radius: var(--border-radius-default);
  overflow: hidden;
`;
const ViewColor = styled.div`
  width: 100px;
  height: 90px;
`;
const ViewDescription = styled.div`
  padding: 12px;
  background-color: var(--color-white);
  p {
    font-size: 10px;
    color: var(--color-gray-600);
  }
`;
const TypeHeader = styled.h3`
  font-size: 12px;
  font-weight: 600;
  padding:60px 0 12px;
  border-bottom: 1px solid var(--color-gray-200);
`;
const ColorPalette = styled.ul`
  padding-top: 32px;
  display: flex;
  width: 100%;
  li {
    flex: 1 1 auto;
    div {
      height: 64px;
    }
    p {
      text-align:center;
      padding-top: 4px;
      font-size: 11px;
      font-weight: 400;
      line-height: 14px;

    }
  }
`;
export default SemanticColor;
