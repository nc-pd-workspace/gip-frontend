import styled from 'styled-components';

import { Tabs } from 'antd';

import BasicColor from './BasicColor';
import SemanticColor from './SemanticColor';
import ColorPalette from './ColorPalette';

const { TabPane } = Tabs;

function ColorSystem() {
  return (
    <Container>
      <Tabs type="card" className="tabs">
        <TabPane tab="Basic Color" key="basicColor">
          <BasicColor />
        </TabPane>
        <TabPane tab="Semantic Color" key="sementicColor">
          <SemanticColor />
        </TabPane>
        <TabPane tab="Color Palette" key="colorPalette">
          <ColorPalette />
        </TabPane>
      </Tabs>
    </Container>
  );
}

const Container = styled.div`
  padding:40px 0;
  .ant-tabs-nav {
    margin-bottom: 0;
  }
`;

export default ColorSystem;
