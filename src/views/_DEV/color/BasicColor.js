import styled from 'styled-components';

// import Paper from '../../../components/paper';
import { COLORS } from '../../../styles/Colors';
import Paper from '../../../components/paper';

import Descripition from '../../../components/paper/descripition';

function BasicColor() {
  const colorView = (color, colorName) => (
    <ViewBox>
      <ViewColor style={{ background: color }} />
      <ViewDescription>
        <h4>{colorName}</h4>
        <p>{color}</p>
      </ViewDescription>
    </ViewBox>
  );

  const colorTest = (color) => {
    const colorArray = Object.entries(COLORS[color]);
    const output = colorArray.map((item) => (
      <li>
        <div style={{ backgroundColor: item[1] }} />
        <p>
          {color}
          <br />
          {item[0]}
        </p>
      </li>
    ));
    return (
      <ColorPalette>
        {output}
      </ColorPalette>
    );
  };

  return (
    <Container>
      <ColorHeader>
        <h2>BASIC COLOR</h2>
        <HeaderDescription>
          Basic 컬러는 명도 혹은 기능과 서비스에 따라 정의하고, 크게 Tint Color와 Grey Color가 있습니다.
        </HeaderDescription>
      </ColorHeader>
      <ColorBody>
        <TypeHeader>TINT COLOR</TypeHeader>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.BLUE[500], 'BLUE-500')
            }
            <ColorText>
              <h4>MAIN BLUE</h4>
              <Descripition
                text={[
                  '브랜드를 대표하는 컬러로, 포인트 컬러와 긍정의 의미를 전달할 때 주로 사용합니다.',
                  'Blue-500이 브랜드 컬러이며, 필요에 따라 다른 명도의 컬러를 적절하게 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
          {
            colorTest('BLUE')
          }
        </ColorSet>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.STEELGRAY[800], 'SteelGray-800')
            }
            <ColorText>
              <h4>MAIN STEEL GRAY</h4>
              <Descripition
                text={[
                  '메인 블루 컬러를 보조해주는 컬러로, CTA 하위 뎁스의 버튼 등에 주로 사용합니다.',
                  'Steel Gray-800이 브랜드 컬러이며, 필요에 따라 다른 명도의 컬러를 적절하게 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
          {
            colorTest('STEELGRAY')
          }
        </ColorSet>

        <ColorSet>
          <ColorType>
            {
              colorView(COLORS.RED[500], 'Red-500')
            }
            <ColorText>
              <h4>SUB RED</h4>
              <Descripition
                text={[
                  '메인 블루 컬러를 보조해주는 컬러로, CTA 하위 뎁스의 버튼 등에 주로 사용합니다.',
                  'Steel Gray-800이 브랜드 컬러이며, 필요에 따라 다른 명도의 컬러를 적절하게 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
          {
            colorTest('RED')
          }
        </ColorSet>

        <TypeHeader>GRAY COLOR</TypeHeader>
        <ColorSet>
          <ColorType>
            <ColorText>
              <Descripition
                text={[
                  '텍스트, 라인, 배경 등에 주로 사용합니다.',
                ]}
                color="#111"
              />
            </ColorText>
          </ColorType>
          {
            colorTest('GRAY')
          }
        </ColorSet>

      </ColorBody>
    </Container>
  );
}

const Container = styled(Paper)`
  padding:48px 40px;
  border: var(--border-default);
  border-radius: 0;
`;

const ColorSet = styled.div`
  padding:40px 0;
`;
const ColorType = styled.div`
  display: flex;
`;
const ColorText = styled.div`
  flex: 1 1 auto;
  padding: 0 0 0 20px;
  h4 {
    font-size: 12px;
    line-height: 14px;
    color: var(--color-gray-800);
    font-weight: 700;
  }
`;
const ColorHeader = styled.div`
  h2 {
    font-size: var(--font-header01);
    font-weight: 700;
    color: var(--color-steelGray-900);
  }
`;
const HeaderDescription = styled.div`
  color: var(--color-gray-800);
  font-weight: 400;
  font-size: 16px;
  padding-top: 24px;
`;
const ColorBody = styled.article`

`;

const ViewBox = styled.div`
  width: 100px;
  border: var(--border-default);
  border-radius: var(--border-radius-default);
  overflow: hidden;
`;
const ViewColor = styled.div`
  width: 100px;
  height: 90px;
`;
const ViewDescription = styled.div`
  padding: 12px;
  p {
    font-size: 10px;
    color: var(--color-gray-600);
  }
`;
const TypeHeader = styled.h3`
  font-size: 12px;
  font-weight: 600;
  padding:60px 0 12px;
  border-bottom: 1px solid var(--color-gray-200);
`;
const ColorPalette = styled.ul`
  padding-top: 32px;
  display: flex;
  width: 100%;
  li {
    flex: 1 1 auto;
    div {
      height: 64px;
    }
    p {
      text-align:center;
      padding-top: 4px;
      font-size: 11px;
      font-weight: 400;
      line-height: 14px;

    }
  }
`;
export default BasicColor;
