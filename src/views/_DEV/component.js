import React from 'react';
import styled from 'styled-components';

import { Tabs } from 'antd';

import PageHeader from '../../components/header/PageHeader';
import { PageLayout } from '../shared/layout/Layout.Styled';
import ColorSystem from './color';

import Table from './Table';

const { TabPane } = Tabs;

function DevComponent() {
  return (
    <Container>
      <PageHeader
        title="개발용 컴포넌트"
      />
      <Tabs defaultActiveKey="Table">
        <TabPane tab="Table" key="Table">
          <Table />
        </TabPane>
        <TabPane tab="Search" key="Search">
          Search
        </TabPane>
        <TabPane tab="Form" key="Form">
          Form
        </TabPane>
        <TabPane tab="Button" key="Button">
          Button
        </TabPane>
        <TabPane tab="Color" key="Color">
          <ColorSystem />
        </TabPane>
      </Tabs>
    </Container>
  );
}

const Container = styled(PageLayout)``;

export default DevComponent;
