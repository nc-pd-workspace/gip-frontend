import React, { useState, useCallback, useRef } from 'react';
import styled from 'styled-components';

import Paper from '../../../../components/paper';

import Description from '../../../../components/paper/descripition';

import { columns, exampleTableData } from './constans';
import Table from '../../../../components/table';

function TableType01() {
  const [currentSelectedRow, setCurrentSelectedRow] = useState();
  const fixedHeader = 'gsShop';

  /*
  * Table Component 관련 추가
  * @function
  */
  const currentSelectedInfoRef = useRef();
  const onClickRow = useCallback(async (getData) => {
    await setCurrentSelectedRow(getData.brand);
    currentSelectedInfoRef.current.scrollIntoView({ behavior: 'smooth' });
  });

  return (
    <Container>
      <Paper border="true" padding="20" style={{ paddingBottom: 0 }}>
        <PaperHeader>
          <PaperTitle>
            <h3>브랜드 신규고객 조회</h3>
            {
              exampleTableData.totalRow && (
                <TotalRow>
                  총
                  {exampleTableData.totalRow}
                  개
                </TotalRow>
              )
            }
          </PaperTitle>
        </PaperHeader>
        <Table
          className="tableClassName"
          columns={columns}
          dataSource={exampleTableData.rows}
          fixedHeader={fixedHeader}
          onClickRow={onClickRow}
        />
      </Paper>
      <Description text={
        [
          '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
          '브랜드명을 클릭 하시면 , 하단에  GS SHOP과 브랜드의 전년대비 성장률을 비교한 그래프가 제공됩니다.',
        ]
      }
      />
      {
        currentSelectedRow && (
          <PageSection ref={currentSelectedInfoRef}>
            <PaperTitle>
              <h3>{currentSelectedRow}</h3>
            </PaperTitle>
            <SectionDetail>
              콘텐츠 들어갈 곳 ...
            </SectionDetail>
          </PageSection>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  table {
    width: 100%;
    table-layout: auto !important;
  }
  table tr {
    th:nth-of-type(1), td:nth-of-type(1) {
      padding-left: 30px;
      width: auto;
    }
    th:nth-of-type(2), td:nth-of-type(2) {
      width: calc(25% - 91px) !important;
      white-space: nowrap
    }
    th:nth-of-type(3), td:nth-of-type(3) {
      width: 91px;
    }
    th:nth-of-type(4), td:nth-of-type(4) {
      width: calc(25% - 91px) !important;
      white-space: nowrap
    }
    th:nth-of-type(5), td:nth-of-type(5) {
      width: 91px;
    }
    th:nth-of-type(6), td:nth-of-type(6) {
      width: calc(25% - 91px) !important;
    }
    th:nth-of-type(7), td:nth-of-type(7) {
      width: 91px;
    }
  }
`;
const PaperTitle = styled.div`
  display: flex;
  h3 {
    flex:1 1 auto;
    padding-bottom: 18px;
    font-size: 20px;
    line-height: 30px;
    font-weight: 700;
  }
  div {
    text-align:right;
    flex:1 1 auto;
  }
`;
const PageSection = styled.div`
  margin-top: 50px;
  background-color: #FFF;
  border-radius: 8px;
  padding: 20px 20px 0;
  border: 1px solid #e3e4e7;
`;
const SectionDetail = styled.div`
  padding: 100px;
  text-align: center;
`;
const PaperHeader = styled.div`

`;
const TotalRow = styled.div`
  
`;

export default TableType01;
