import React, { useState, useCallback, useRef } from 'react';
import styled from 'styled-components';
import ResizeObserver from 'rc-resize-observer';

import Paper from '../../../../components/paper';
import Images from '../../../../Images';

import Description from '../../../../components/paper/descripition';

import { columns, exampleTableData } from './constans';
import Table from '../../../../components/table';

/*
*
*  Table Props
*    @params className="tableClassName"  // 상속되는 classname
*    @params columns={columns}  // headerData
*    @params dataSource={exampleTableData.rows}  // 실제 데이터 Row
*    @params onClickRow={onClickRow}  // Row항목 선택시 입력
*    @params onCheckedRow={onCheckedRow}  // Checked 항목 선택시 입력
*    @params rowSelected={selectedRowKeys}  // Checked 항목 선택시 입력
*    @params scrollY={325}  // scroll 크기 입력 (미사용시 해당 param 미입력)
*    @params fixedHeader={fixedHeader}  // 상단 고정 형태 테이블 (사용시 입력, 입력값 : 해당 Row.key)
*
*    - CSS로 column의 width설정
*    - 현재 FixedHeader와 CheckedRow 같이 사용 불가
*
*  Table State
*    fixHeader(string) : 상단 고정 Table 사용시 지정할 Row의 key값 입력
*    currentSelectedRow(string) : 현재 선택된 Row
*    selectedRowKeys(array) : CheckBox로 선택된 Row
*
*  Table Fuctions
*    onCheckedRow : Table컴포넌트에서 선택된 checkbox박스 전달하여 현재 화면에 업데이트
*    onCheckedDelete : CheckBox선택 리스트UI에서 Row data삭제
*    onClickRow : Table의 Row선택시 UI업데이트
*
*/

function TableContainer() {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [currentSelectedRow, setCurrentSelectedRow] = useState();
  const [tableWidth, setTableWidth] = useState(0);
  const fixedHeader = 'gsShop';

  const TableWidth = styled.div`
    table {
      width: ${tableWidth}px;
    }
    table tr {
      th:nth-of-type(1), td:nth-of-type(1) {
        width: ${tableWidth / 4}px;
      }
      th:nth-of-type(2), td:nth-of-type(2) {
        width: ${(tableWidth * 0.75) / 6}px;
      }
      th:nth-of-type(3), td:nth-of-type(3) {
        width: ${(tableWidth * 0.75) / 6}px;
      }
      th:nth-of-type(4), td:nth-of-type(4) {
        width: ${(tableWidth * 0.75) / 6}px;
      }
      th:nth-of-type(5), td:nth-of-type(5) {
        width: ${(tableWidth * 0.75) / 6}px;
      }
      th:nth-of-type(6), td:nth-of-type(6) {
        width: ${(tableWidth * 0.75) / 6}px;
      }
    }
  `;

  const onCheckedRow = useCallback((selectedCheckRow) => {
    setSelectedRowKeys(selectedCheckRow);
  });

  const onCheckedDelete = useCallback((rowKey) => {
    setSelectedRowKeys(selectedRowKeys.filter((data) => data !== rowKey));
  });

  const currentSelectedInfoRef = useRef();
  const onClickRow = useCallback(async (getData) => {
    await setCurrentSelectedRow(getData.brand);
    currentSelectedInfoRef.current.scrollIntoView({ behavior: 'smooth' });
  });

  return (
    <Container>
      <Paper border="true" padding="20" style={{ paddingBottom: 0 }}>
        <PaperHeader>
          <PaperTitle>
            <h3>브랜드 신규고객 조회</h3>
          </PaperTitle>
        </PaperHeader>
        <ResizeObserver onResize={({ width }) => { setTableWidth(width); }}>
          <TableWidth>
            <Table
              className="tableClassName"
              columns={columns}
              scrollY={325}
              fixedHeader={fixedHeader}
              dataSource={exampleTableData.rows}
              onClickRow={onClickRow}
              onCheckedRow={onCheckedRow}
              rowSelected={selectedRowKeys}
            />
          </TableWidth>
        </ResizeObserver>
      </Paper>
      <Description text={
        [
          '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
          '브랜드명을 클릭 하시면 , 하단에  GS SHOP과 브랜드의 전년대비 성장률을 비교한 그래프가 제공됩니다.',
        ]
      }
      />
      {
        selectedRowKeys.length > 0 && (
          <Paper>
            <ListSelectData>
              {
                selectedRowKeys.map((data) => (
                  <li key={data}>
                    <button onClick={() => onCheckedDelete(data)}>{data}</button>
                  </li>
                ))
              }
            </ListSelectData>
          </Paper>
        )
      }
      {
        currentSelectedRow && (
          <PageSection ref={currentSelectedInfoRef}>
            <PaperTitle>
              <h3>{currentSelectedRow}</h3>
            </PaperTitle>
            <SectionDetail>
              콘텐츠 들어갈 곳 ...
            </SectionDetail>
          </PageSection>
        )
      }
    </Container>
  );
}
const Container = styled.div`
  span.brand {
    padding-left: 20px;
  }
  .gsShop {
    display:inline-block;
    background-image: url(${Images.gsShopLogo});
    background-position: left 1px;
    background-repeat: no-repeat;
    background-size: 16px 16px;
    line-height:20px;
  }
`;
const PaperTitle = styled.div`
  display: flex;
  h3 {
    flex:0 0 auto;
    padding-top: 10px;
    padding-bottom: 16px;
    font-size: 20px;
    font-weight: 700;
  }
`;
const PageSection = styled.div`
  margin-top: 50px;
  background-color: #FFF;
  border-radius: 8px;
  padding: 20px 20px 0;
  border: 1px solid #e3e4e7;
`;
const SectionDetail = styled.div`
  padding: 100px;
  text-align: center;
`;
const PaperHeader = styled.div`
`;

const ListSelectData = styled.ul`
  padding: 10px;
  text-align: center;
  display: flex;
  li {
    border:1px solid #ccc;
    border-radius: 3px;
  }
`;

export default TableContainer;
