import React, { useState, useCallback } from 'react';
import styled from 'styled-components';

import Paper from '../../../../components/paper';

import Description from '../../../../components/paper/descripition';

import { columns, exampleTableData } from './constans';
import Table from '../../../../components/table';

function TableType01() {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const onCheckedRow = useCallback((selectedCheckRow) => {
    setSelectedRowKeys(selectedCheckRow);
  });

  const onCheckedDelete = useCallback((rowKey) => {
    setSelectedRowKeys(selectedRowKeys.filter((data) => data !== rowKey));
  });

  return (
    <Container>
      <Paper border="true" padding="20" style={{ paddingBottom: 0 }}>
        <PaperHeader>
          <PaperTitle>
            <h3>브랜드 신규고객 조회</h3>
            {
              exampleTableData.totalRow && (
                <TotalRow>
                  총
                  {exampleTableData.totalRow}
                  개
                </TotalRow>
              )
            }
          </PaperTitle>
        </PaperHeader>
        <Table
          className="tableClassName"
          columns={columns}
          dataSource={exampleTableData.rows}
          onCheckedRow={onCheckedRow}
          rowSelected={selectedRowKeys}
        />
      </Paper>
      <Description text={
        [
          '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
          '브랜드명을 클릭 하시면 , 하단에  GS SHOP과 브랜드의 전년대비 성장률을 비교한 그래프가 제공됩니다.',
        ]
      }
      />
      {
        selectedRowKeys.length > 0 && (
          <Paper>
            <ListSelectData>
              {
                selectedRowKeys.map((data) => (
                  <li key={data}>
                    <button onClick={() => onCheckedDelete(data)}>{data}</button>
                  </li>
                ))
              }
            </ListSelectData>
          </Paper>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  /* table tr {
    th:nth-of-type(2), 
    td:nth-of-type(2) {
      width: auto;
    }
    th:nth-of-type(3), 
    td:nth-of-type(3) {
      width: 120px;
    }
    th:nth-of-type(4), 
    td:nth-of-type(4) {
      width: 140px;
    }
    th:nth-of-type(5), 
    td:nth-of-type(5) {
      width: 150px;
    }
    th:nth-of-type(6), 
    td:nth-of-type(6) {
      width: 150px;
    }
    th:nth-of-type(7), 
    td:nth-of-type(7) {
      width: 150px;
    }
    th:nth-of-type(8), 
    td:nth-of-type(8) {
      width: 150px;
    }
  } */
`;
const PaperTitle = styled.div`
  display: flex;
  h3 {
    flex:1 1 auto;
    padding-bottom: 18px;
    font-size: 20px;
    line-height: 30px;
    font-weight: 700;
  }
  div {
    text-align:right;
    flex:1 1 auto;
  }
`;
const PaperHeader = styled.div`

`;
const TotalRow = styled.div`
  
`;

const ListSelectData = styled.ul`
  padding: 10px;
  text-align: center;
  display: flex;
  li {
    border:1px solid #ccc;
    border-radius: 3px;
  }
`;

export default TableType01;
