import React from 'react';
import styled from 'styled-components';

import Paper from '../../../../components/paper';

import Description from '../../../../components/paper/descripition';

import { columns, exampleTableData } from './constans';
import Table from '../../../../components/table';

function TableType01() {
  return (
    <Container>
      <Paper border="true" padding="20" style={{ paddingBottom: 0 }}>
        <PaperHeader>
          <PaperTitle>
            <h3>브랜드 신규고객 조회</h3>
            {
              exampleTableData.totalRow && (
                <TotalRow>
                  총
                  {exampleTableData.totalRow}
                  개
                </TotalRow>
              )
            }
          </PaperTitle>
        </PaperHeader>
        <Table
          className="tableClassName"
          columns={columns}
          dataSource={exampleTableData.rows}
        />
      </Paper>
      <Description text={
        [
          '* 표시된 데이터는  실제 수치가 아닌  GIP만의 방식으로 계산된 지수화 데이터 입니다.',
        ]
      }
      />
    </Container>
  );
}

const Container = styled.div`
  table tr {
    th:nth-of-type(1), td:nth-of-type(1) {
      width: 50px;
    }
    th:nth-of-type(2), td:nth-of-type(2) {
      width: calc(25% - 50px);
    }
    th:nth-of-type(3), td:nth-of-type(3) {
      width: calc(75% / 5);
    }
    th:nth-of-type(4), td:nth-of-type(4) {
      width: calc(75% / 5);
    }
    th:nth-of-type(5), td:nth-of-type(5) {
      width: calc(75% / 5);
    }
    th:nth-of-type(6), td:nth-of-type(6) {
      width: calc(75% / 5);
    }
    th:nth-of-type(7), td:nth-of-type(7) {
      width: calc(75% / 5);
    }
  }
`;
const PaperTitle = styled.div`
  display: flex;
  h3 {
    flex:1 1 auto;
    padding-bottom: 18px;
    font-size: 20px;
    line-height: 30px;
    font-weight: 700;
  }
  div {
    text-align:right;
    flex:1 1 auto;
  }
`;
const PaperHeader = styled.div`

`;
const TotalRow = styled.div`
  
`;

export default TableType01;
