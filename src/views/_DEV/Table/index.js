import styled from 'styled-components';
import { Tabs } from 'antd';

import TableSample from './sample/container';
import TableType01 from './TableType01/container';
import TableType02 from './TableType02/container';
import TableType03 from './TableType03/container';
// import TableType05 from './TableType01/TableType05';

const { TabPane } = Tabs;

function Table() {
  return (
    <Container>
      <Tabs type="card">
        <TabPane tab="Sample" key="sample">
          <TableSample />
        </TabPane>
        <TabPane tab="1열 고정" key="1">
          <TableType01 />
        </TabPane>
        <TabPane tab="단순 리스트" key="2">
          <TableType02 />
        </TabPane>
        <TabPane tab="체크박스" key="3">
          <TableType03 />
        </TabPane>
      </Tabs>
    </Container>
  );
}

const Container = styled.div`
  padding-top: 32px;
`;
export default Table;
