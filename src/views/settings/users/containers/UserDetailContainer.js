import moment from 'moment';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import {
  Form,
  Input,
  Button,
  Radio,
  Select,
} from 'antd';

import { PageTypes } from '../../../../constants/pageType';

import PageHeader from '../../../../components/header/PageHeader';
import Paper from '../../../../components/paper';
import { getPhoneValidationTransform } from '../../../../utils/utils';
import { PageLayout } from '../../../shared/layout/Layout.Styled';

import { getUserInfo, putUserInfo, putUserStatus, updateStore } from '../redux/slice';
import { getHeaderUserInfo } from '../../../../redux/commonReducer';
import { asyncApiState } from '../../../../redux/constants';
import { usePageTab } from '../../../shared/pageTab/hooks/usePageTab';
import { alertMessage, confirmMessage } from '../../../../components/message';

function UserDetailContainer({ query }) {
  const { usrIdx } = query;
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.common);
  const { userDetail, userInfoModify, userStatus } = useSelector((state) => state.settings.users);
  const [form] = Form.useForm();
  const { openPage } = usePageTab();

  const reload = () => {
    const params = {
      usrIdx,
    };
    dispatch(getUserInfo({ params }));
  };

  const onFormValueChange = (data) => {
    if (data.cphoneNo) {
      if (getPhoneValidationTransform(data.cphoneNo) !== data.cphoneNo) {
        form.setFieldsValue({
          cphoneNo: getPhoneValidationTransform(data.cphoneNo),
        });
      }
    }
    if (data.usrNm) {
      if (data.usrNm && data.usrNm.length > 15) {
        form.setFieldsValue({
          usrNm: data.usrNm.substr(0, 15),
        });
      }
    }
    // setComponentSize(size);
  };

  const onFinishForm = (values) => {
    const params = {
      usrIdx: userDetail.data.usrIdx,
    };

    Object.keys(values).forEach((items) => {
      if (values[items] !== '' && values[items] !== userDetail.data[items]) {
        params[items] = values[items];
      }
    });
    if (Object.keys(params).length > 0) {
      dispatch(putUserInfo({ params }));
    }
  };

  const onFocusItem = (e, name) => {
    if (e.target.value === userDetail.data[name]) {
      form.setFieldsValue({
        [name]: '',
      });
    }
  };

  const onBlurItem = (e, name) => {
    if (e.target.value === '') {
      form.setFieldsValue({
        [name]: userDetail.data[name],
      });
    }
  };

  const onClickChangeStatus = () => {
    let message = '사용자 상태를 \'중지(사용 안함)\'로 변경하시겠어요?';
    if (userDetail.data.stCd === 'MC000050') {
      message = '사용자 상태를 \'사용\'으로 변경하시겠어요?';
    }
    confirmMessage(message, () => {
      const params = {
        usrIdx: userDetail.data.usrIdx,
        stCd: userDetail.data.stCd === 'MC000050' ? 'MC000040' : 'MC000050',
      };

      dispatch(putUserStatus({ params }));
    }, '예', '아니오');
  };

  const getMasterRender = () => {
    if (!userDetail.data) {
      return (
        <Form.Item label="사용자 구분">
          <Text />
        </Form.Item>
      );
    }
    // 자동생성 마스터
    if (userDetail.data && userDetail.data.joinTypeCd && userDetail.data.joinTypeCd === 'MC000010' && userDetail.data.roleCd === 'RL0002') {
      return (
        <Form.Item label="사용자 구분">
          <Text>마스터</Text>
        </Form.Item>
      );
    } if (userDetail.data && userDetail.data.roleCd && ['RL0004', 'RL0005'].indexOf(userDetail.data.roleCd) > -1) {
      // select는 Form.Item 내부에 다른 element를 넣으면 동작하지 않으므로. Form.Item 안에 Form.Item을 다시 선언해서 name이 있는 Select에서만 값을 뽑아오도록 함.
      return (
        <CustomSelectForm label="사용자 구분">
          <SelectText>관리자</SelectText>
          <Form.Item name="roleCd" className="custom-select">
            <Select>
              <Select.Option value="RL0004">시스템 관리자</Select.Option>
              <Select.Option value="RL0005">GS SHOP 관리자</Select.Option>
            </Select>
          </Form.Item>
        </CustomSelectForm>
      );
    }
    return (
      <Form.Item label="사용자 구분" name="roleCd">
        <Radio.Group>
          <Radio value="RL0001">일반</Radio>
          <Radio value="RL0002">마스터</Radio>
        </Radio.Group>
      </Form.Item>
    );
  };

  const onClickPartner = () => {
    openPage(PageTypes.SETTINGS_PARTNERS, { ptnId: userDetail.data.ptnId }, true);
  };

  useEffect(() => {
    if (userDetail.status === 'success') {
      form.setFieldsValue({
        roleCd: userDetail.data.roleCd ? userDetail.data.roleCd : null,
        usrNm: userDetail.data.usrNm ? userDetail.data.usrNm : null,
        cphoneNo: userDetail.data.cphoneNo ? userDetail.data.cphoneNo : null,
        email: userDetail.data.email ? userDetail.data.email : null,
      });
    }
  }, [userDetail]);

  useEffect(() => {
    if (userInfoModify.status === 'success') {
      alertMessage('사용자 정보 수정이 완료되었습니다.', () => {
        if (userDetail.data.usrIdx === userInfo.userInfo.usrIdx) {
          dispatch(getHeaderUserInfo());
        }
        form.setFieldsValue({
          usrNm: null,
          cphoneNo: null,
          email: null,
        });
        reload();
      });
    }
  }, [userInfoModify]);

  useEffect(() => {
    if (userStatus.status === 'success') {
      dispatch(updateStore({
        userStatus: asyncApiState.initial([]),
      }));
      reload();
    }
  }, [userStatus]);

  useEffect(() => {
    reload();
  }, [query]);

  useEffect(() => () => {
    dispatch(updateStore({
      userDetail: asyncApiState.initial([]),
      userInfoModify: asyncApiState.initial([]),
      userStatus: asyncApiState.initial([]),
    }));
  }, []);

  return (
    <Container>
      <PageHeader
        title="사용자 상세"
        subTitle=""
      />
      <Paper border>
        <CustomForm
          form={form}
          layout="horizontal"
          initialValues={{ remember: true }}
          onFinish={onFinishForm}
          onValuesChange={onFormValueChange}
        >
          {
            getMasterRender()
          }
          {
            (userDetail.data && ['RL0003', 'RL0004', 'RL0005'].indexOf(userDetail.data.roleCd) !== -1) ? (
              <></>
            ) : (
              <Form.Item label="파트너">
                <CustomPartnerBtn ghost onClick={onClickPartner}>
                  {userDetail.data && userDetail.data.ptnNm ? `${userDetail.data.ptnNm}(${userDetail.data.ptnId})` : ''}
                </CustomPartnerBtn>
              </Form.Item>
            )
          }

          <Form.Item label="사용자 아이디">
            <Text>{userDetail.data && userDetail.data.usrId ? userDetail.data.usrId : ''}</Text>
          </Form.Item>
          <Form.Item
            label="이름"
            name="usrNm"
            deafaultvalue=""
            required
            rules={[
              {
                validator: (_, value) => {
                  if (value.trim() === '') {
                    return Promise.reject(new Error('이름을 입력해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'usrNm')} onBlur={(e) => onBlurItem(e, 'usrNm')} />
          </Form.Item>
          <Form.Item
            label="휴대폰 번호"
            name="cphoneNo"
            deafaultvalue=""
            rules={[
              {
                validator: (_, value) => {
                  if (value && value !== userDetail.data.cphoneNo && /^\d{3}-\d{3,4}-\d{4}$/.test(value) === false) {
                    return Promise.reject(new Error('올바른 휴대폰 번호가 아닙니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'cphoneNo')} onBlur={(e) => onBlurItem(e, 'cphoneNo')} />
          </Form.Item>
          <Form.Item
            label="이메일"
            name="email"
            deafaultvalue=""
            required
            rules={[
              {
                validator: (_, value) => {
                  if (value.trim() === '') {
                    return Promise.reject(new Error('이메일을 입력해주세요.'));
                  }
                  if (value !== userDetail.data.email && /^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/.test(value) === false) {
                    return Promise.reject(new Error('이메일 형식에 맞지 않습니다. 다시 확인해주세요.'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input onFocus={(e) => onFocusItem(e, 'email')} onBlur={(e) => onBlurItem(e, 'email')} />
          </Form.Item>
          <Form.Item label="가입 구분">
            <Text>{userDetail.data && userDetail.data.joinTypeNm ? userDetail.data.joinTypeNm : ''}</Text>
          </Form.Item>
          <Form.Item label="가입일">
            <Text>{userDetail.data ? moment(userDetail.data.regDtm ? userDetail.data.regDtm : null).format('YYYY. M. D. HH:mm:SS') : ''}</Text>
          </Form.Item>
          <Form.Item label="상태">
            {
              userDetail.data && (
                <>
                  <Text>{ userDetail.data.stCd === 'MC000050' ? '중지' : '정상'}</Text>
                  {
                    (userDetail.data.joinTypeCd === 'MC000010' && userDetail.data.roleCd === 'RL0002') ? (<></>) : (
                      <StatusBtn onClick={() => onClickChangeStatus()}>{ userDetail.data.stCd === 'MC000050' ? '사용하기' : '중지하기'}</StatusBtn>
                    )
                  }
                </>
              )
            }

          </Form.Item>
          <ButtonWrap>
            <Button type="primary" htmlType="submit">수정</Button>
          </ButtonWrap>
        </CustomForm>
      </Paper>
    </Container>
  );
}

const Container = styled(PageLayout)``;
const CustomForm = styled(Form)`
  padding: 20px;

  .ant-form-item-label {
    flex: 0 0 112px;

    font-size: 14px;
    line-height: 150%;
    color: #333;
  }
  .ant-form-item-control {
    max-width: initial;
    padding-left: 10px;
  }
  .ant-form-item-label {
    flex: 0 0 101px;
  }
  .ant-form-item {
    margin-bottom: 16px !important;
  }
  .ant-row {
    &:last-child {
      margin-bottom: 0;
    }
  }
`;
const StatusBtn = styled(Button)`
  padding: 7px 10px;
  margin-left: 20px;
  width: 65px;
  height: 34px;

  font-size: 13px;
  line-height: 20px;
`;

const Text = styled.span`
  font-size: 14px;
  line-height: 20px;
  font-weight: 400;

  color: #333;
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 40px;

  .ant-btn {
    width: 80px;
    height: 40px;
    border: 1px solid #E3E4E7;
    box-sizing: border-box;
    border-radius: 4px;
  }

  .ant-btn.ant-btn-primary {
    width: 250px;
    margin-left: 10px;
    border: 0;
  }
`;

const SelectText = styled.div`
  display: flex;
  height: 32px;
  flex-basis: 80px;
  align-items: center;
`;

const CustomSelectForm = styled(Form.Item)`
  .ant-form-item-control-input-content {
    display: flex !important;
  }

  & .custom-select {
    flex-basis: 200px;
    margin-bottom: 0 !important;
  }
`;

const CustomPartnerBtn = styled(Button)`
  cursor: 'pointer' !important;
  border: 0 !important;
  color: #000 !important;
  padding: 0 !important;

  span {
    text-decoration: underline;
  }
`;
export default UserDetailContainer;
