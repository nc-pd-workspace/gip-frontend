import { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { Link } from 'react-router-dom';

import { asyncApiState } from '../../../../redux/constants';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import MultipleRow from '../../../../components/search/MultipleRow';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import TypeText from '../../../../components/search/TypeText';
import PartnerSearch from '../../../../components/search/PartnerSearch';

import Paper from '../../../../components/paper';
import Title from '../../../../components/title';
import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import { PageTypes } from '../../../../constants/pageType';

import { userTypeOptions, statusOptions, wordTypeOptions } from '../constants';
import { usePageTab } from '../../../shared/pageTab/hooks/usePageTab';
import PagingTable from '../../../../components/pagingTable';
import { getUsers, putUserType, updateStore } from '../redux/slice';
import Button from '../../../../components/button';
import { SvgIconRegister } from '../../../../Images';
import UserAddModal from '../../partners/components/Modal/UserAddModal';
import { confirmMessage } from '../../../../components/message';

function UsersContainer() {
  const columnOptions = [
    {
      title: '파트너 번호',
      dataIndex: 'ptnIdx',
      key: 'ptnIdx',
      width: 70,
    },
    {
      title: '파트너 아이디',
      dataIndex: 'ptnId',
      key: 'ptnId',
    },
    {
      title: '파트너명',
      dataIndex: 'ptnNm',
      key: 'ptnNm',
    },
    {
      title: '사용자 아이디',
      dataIndex: 'usrId',
      key: 'usrId',
    },
    {
      title: '이름',
      dataIndex: 'usrNm',
      key: 'usrNm',
    },
    {
      title: '휴대폰 번호',
      dataIndex: 'cphoneNo',
      key: 'cphoneNo',
    },
    {
      title: '이메일',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '사용자 구분',
      dataIndex: 'roleNm',
      render: (roleNm, record) => {
        if (record.roleCd === 'RL0001' || (record.joinTypeCd !== 'MC000010' && record.roleCd === 'RL0002')) {
          return (
            <Link to="#" onClick={() => onClickUserType(record)}>{ roleNm }</Link>
          );
        }
        return (
          <><CustomLink to="#" onClick={(e) => { e.preventDefault(); }}>{roleNm}</CustomLink></>
        );
      },
      key: 'roleNm',
    },
    {
      title: '가입 구분',
      dataIndex: 'joinTypeNm',
      key: 'joinTypeNm',
    },
    {
      title: '가입일',
      dataIndex: 'regDtm',
      key: 'regDtm',
    },
    {
      title: '상태',
      dataIndex: 'stNm',
      key: 'stNm',
      width: 70,
    },
  ];

  const searchRef = useRef(null);
  const tableRef = useRef(null);
  const dispatch = useDispatch();
  const { openPage } = usePageTab();
  const { userList, userTypeModify } = useSelector((state) => state.settings.users);
  const { userInfo } = useSelector((state) => state.common.userInfo);
  const { activePageId, resetSettings } = useSelector((state) => state.common);
  const initialState = {
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };
  const [userAddModalVisible, setUserAddModalVisible] = useState(false);
  const [state, setState] = useState(initialState);
  const [search, setSearch] = useState({});

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const onSetSearch = (v) => {
    setSearch({ ...v });
  };

  const handleOk = (usrIdx) => {
    fetchUsers(1);
    setUserAddModalVisible(false);
    openPage(PageTypes.SETTINGS_USER_DETAIL, { usrIdx });
  };

  const handleCancel = () => {
    setUserAddModalVisible(false);
  };

  const handleUserModalBtn = () => {
    setUserAddModalVisible(true);
  };

  const handleTableChange = (pagination, _, __) => {
    fetchUsers(pagination.current);
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const onClickUserType = (record) => {
    if (record.roleCd === 'RL0001') {
      confirmMessage('일반 사용자를 마스터로 변경하시겠어요?', () => {
        const params = {
          usrIdx: record.usrIdx,
          roleCd: 'RL0002',
        };
        dispatch(putUserType({ params }));
      }, '예', '아니요');
    } else if (record.roleCd === 'RL0002' && record.joinTypeCd !== 'MC000010') {
      confirmMessage('마스터를 일반 사용자로 변경하시겠어요?', () => {
        const params = {
          usrIdx: record.usrIdx,
          roleCd: 'RL0001',
        };
        dispatch(putUserType({ params }));
      }, '예', '아니요');
    }
  };

  const handleRowClick = (data) => {
    openPage(PageTypes.SETTINGS_USER_DETAIL, { usrIdx: data.usrIdx });
  };

  const fetchUsers = (page) => {
    const params = {
      searchType: 'MC000100',
      ...search,
    };
    if (page) params.page = page - 1 > -1 ? page - 1 : 0;

    if (params.regStartDtm) params.regStartDtm = params.regStartDtm.replace(/\./g, '-');
    if (params.regEndDtm) params.regEndDtm = params.regEndDtm.replace(/\./g, '-');

    setState({ ...state, pagination: { ...state.pagination, current: page } });
    dispatch(getUsers({ params }));
  };

  useEffect(() => {
    fetchUsers(1);
  }, [search]);

  useEffect(() => {
    setState({
      ...state,
      data: userList.data && userList.data.items ? userList.data.items : [],
      loading: userList.status === 'pending',
      pagination: {
        ...state.pagination,
        total: userList.data && userList.data.totalItem
          ? userList.data.totalItem : 0,
      },
    });
  }, [userList]);

  useEffect(() => {
    if (userTypeModify.status === 'success') {
      fetchUsers(1);
    }
  }, [userTypeModify]);

  useEffect(() => () => {
    dispatch(updateStore({
      userList: asyncApiState.initial([]),
      userStatus: asyncApiState.initial([]),
      userTypeModify: asyncApiState.initial({}),
    }));
  }, []);
  useEffect(() => {
    if (activePageId === 'users' && resetSettings) {
      dispatch(updateStore({
        userList: asyncApiState.initial([]),
        userStatus: asyncApiState.initial([]),
        userTypeModify: asyncApiState.initial({}),
      }));
      searchRef.current.clickReset();
      setState(initialState);
      setSearch({});
    }
  }, [activePageId]);

  return (
    <>
      <Container>
        <PageHeader
          title="사용자 관리"
          subTitle=""
        />
        <Search ref={searchRef} setSearch={onSetSearch}>
          <MultipleRow>
            <CustomRangePicker
              column={['regStartDtm', 'regEndDtm']}
              title="가입일"
              showDateType={false}
              setDefaultDate={false}
              disabledToday
              dropdownClassName="usersPickerPopup"
            />
            <SingleInputItem defaultValue={['RL0001', 'RL0002', 'RL0003']} type="CheckGroup" column="roleCds" title="사용자 구분" options={userTypeOptions} />
          </MultipleRow>
          <MultipleRow>
            <PartnerSearch column="ptnIdxs" title="파트너" />
            <SingleInputItem type="Select" defaultValue="" column="stCd" title="상태" options={statusOptions} />
          </MultipleRow>
          <MultipleRow>
            <CustomTypeText
              selectColumn="searchType"
              column="searchKeyword"
              title="검색어"
              options={wordTypeOptions}
              defaultSelectValue=""
            />
            <EmptyWrap />
          </MultipleRow>
        </Search>

        <TableWrap>
          <StyledTitle
            level="3"
            title="조회 결과"
            subTitle={`총 ${state.pagination.total}개`}
          />
          {
            userInfo && userInfo.roleCd && ['RL0003', 'RL0004'].indexOf(userInfo.roleCd) !== -1 ? (
              <CustomUserButton onClick={handleUserModalBtn}>
                <SvgIconRegister fill="var(--color-gray-700)" />
                사용자 등록
              </CustomUserButton>
            ) : (<></>)
          }

          <PagingTable
            ref={tableRef}
            columns={columnOptions}
            data={state.data}
            pagination={state.pagination}
            loading={state.loading}
            rowKey={(record) => record.usrIdx}
            onChange={handleTableChange}
            onRowClick={handleRowClick}
          />
        </TableWrap>
      </Container>
      {
        userAddModalVisible && (
          <UserAddModal visible={userAddModalVisible} onOk={handleOk} onClose={handleCancel} />
        )
      }
    </>
  );
}

const Container = styled(PageLayout)``;

const TableWrap = styled(Paper)`
  position: relative;
  margin-top: 50px;
  padding-left: 20px;
  padding-bottom: 50px;
  border: var(--border-default);
  table {
    th:nth-of-type(1), td:nth-of-type(1) {
      width: 50px;
    }
    th:nth-of-type(2), td:nth-of-type(2) {
      width: 100px;
    }
    th:nth-of-type(3), td:nth-of-type(3) {
      width: 100px;
    }
    th:nth-of-type(4), td:nth-of-type(4) {
      width: 90px;
    }
    th:nth-of-type(5), td:nth-of-type(5) {
      width: 100px;
    }
    th:nth-of-type(6), td:nth-of-type(6) {
      width: 100px;
    }
    th:nth-of-type(7), td:nth-of-type(7) {
      width: 120px;
    }
    th:nth-of-type(8), td:nth-of-type(8) {
      width: 170px;
    }
    th:nth-of-type(9), td:nth-of-type(9) {
      width: 100px;
    }
    th:nth-of-type(10), td:nth-of-type(10) {
      width: 100px;
    }
    th:nth-of-type(11), td:nth-of-type(11) {
      width: 160px;
    }
  }
`;

const StyledTitle = styled(Title)`
  padding: 20px 0;

  & > p {
    font-family: Pretendard;
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 16px;
    margin-left: 6px;
    text-align: justify;
    color: var(--color-blue-500);
  }
`;

const CustomUserButton = styled(Button)`
  position: absolute;
  right: 20px;
  top: 19px;
`;

const CustomLink = styled(Link)`
  padding: 0;
  border: 0;
  background: transparent;
  box-shadow: none !important;

  font-size: 13px;
  line-height: 20px;
  cursor: pointer;
  color: var(--color-gray-900);
`;

const CustomTypeText = styled(TypeText)`
  flex: 1;
  padding-right: 20px;
`;

const EmptyWrap = styled.div`
  flex: 1;
`;

const CustomRangePicker = styled(SingleRangePicker)`
  .ant-picker-range .ant-picker-input {
    width: initial !important;
  }
`;

export default UsersContainer;
