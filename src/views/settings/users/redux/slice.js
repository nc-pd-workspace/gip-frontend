import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  userList: asyncApiState.initial([]),
  userDetail: asyncApiState.initial([]),
  userStatus: asyncApiState.initial([]),
  userInfoModify: asyncApiState.initial([]),
  userTypeModify: asyncApiState.initial({}),
};

export const { actions, reducer } = createSlice({
  name: 'settings/users',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateStore: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getUsers: (state) => {
      state.userList = asyncApiState.request(null);
    },
    getUsersSuccess: (state, { payload }) => {
      const result = payload;
      state.userList = asyncApiState.success(result);
    },
    getUsersFailure: (state, { payload }) => {
      state.userList = asyncApiState.error(payload);
    },
    getUserInfo: (state) => {
      state.userDetail = asyncApiState.request(null);
    },
    getUserInfoSuccess: (state, { payload }) => {
      const result = payload;
      state.userDetail = asyncApiState.success(result);
    },
    getUserInfoFailure: (state, { payload }) => {
      state.userDetail = asyncApiState.error(payload);
    },
    putUserInfo: (state) => {
      state.userInfoModify = asyncApiState.request(null);
    },
    putUserInfoSuccess: (state, { payload }) => {
      const result = payload;
      state.userInfoModify = asyncApiState.success(result);
    },
    putUserInfoFailure: (state, { payload }) => {
      state.userInfoModify = asyncApiState.error(payload);
    },
    putUserStatus: (state) => {
      state.userStatus = asyncApiState.request(null);
    },
    putUserStatusSuccess: (state, { payload }) => {
      const result = payload;
      state.userStatus = asyncApiState.success(result);
    },
    putUserStatusFailure: (state, { payload }) => {
      state.userStatus = asyncApiState.error(payload);
    },
    putUserType: (state) => {
      state.userTypeModify = asyncApiState.request(null);
    },
    putUserTypeSuccess: (state, { payload }) => {
      const result = payload;
      state.userTypeModify = asyncApiState.success(result);
    },
    putUserTypeFailure: (state, { payload }) => {
      state.userTypeModify = asyncApiState.error(payload);
    },
  },
});

export const { resetStore, updateStore, getUsers, getUserInfo, putUserInfo, putUserStatus, putUserType } = actions;

export default reducer;
