import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getUsers, getUserInfo, putUserInfo, putUserStatus, putUserType } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const getUserListSaga = createPromiseSaga(getUsers, API.Users.getUserList);
const getUserInfoSaga = createPromiseSaga(getUserInfo, API.Users.getUserInfo);
const putUserInfoSaga = createPromiseSaga(putUserInfo, API.Users.putUserInfo);
const putUserStatusSaga = createPromiseSaga(putUserStatus, API.Common.putUserStatus);
const putUserTypeSaga = createPromiseSaga(putUserType, API.Common.putUserType);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getUsers, getUserListSaga);
  yield takeLatest(getUserInfo, getUserInfoSaga);
  yield takeLatest(putUserInfo, putUserInfoSaga);
  yield takeLatest(putUserStatus, putUserStatusSaga);
  yield takeLatest(putUserType, putUserTypeSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
