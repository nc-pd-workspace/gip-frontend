export const userTypeOptions = [
  { label: '일반', value: 'RL0001' },
  { label: '마스터', value: 'RL0002' },
  { label: '관리자', value: 'RL0003' },
];

export const statusOptions = [
  { label: '전체', value: '' },
  { label: '정상', value: 'MC000040' },
  { label: '중지', value: 'MC000050' },
];

export const wordTypeOptions = [
  { label: '선택', value: '' },
  { label: '이름', value: 'MC000090' },
  { label: '사용자 아이디', value: 'MC000080' },
  { label: '휴대폰 번호', value: 'MC000110' },
  { label: '이메일', value: 'MC000120' },
];
