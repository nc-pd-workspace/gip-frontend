/* eslint-disable no-lonely-if */
import styled from 'styled-components';

import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Input, Select, Tabs, Modal } from 'antd';

import Images, { SvgArrowDropdown, SvgIconRegister } from '../../../../Images';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import TreeContainer from '../../../shared/tree/containers/TreeContainer';
import SearchTreeContainer from '../../../shared/searchTree/containers/SearchTreeContainer';
import Paper from '../../../../components/paper';
import {
  getInfo,
  getPartnerTree,
  updatePartnerDetailInfo,
  initPartnerIdCheck,
  updatePatnerDepth,
  insertPartnerDetailInfo,
  initPartnerAddStatus,
  initPartnerUpdateStatus,
  updateState,
  resetStore,
} from '../redux/slice';
import UserList from '../components/userList';
import Button, { ButtonExcel } from '../../../../components/button';
import PartnerAdd from '../components/partnerAdd';
import PartnerDetailInfo from '../components/partnerDetailInfo';
import { getSaleChannel } from '../../../account/myPartnerPage/redux/slice';
import { treeStyle } from '../../../../styles/Tree';
import { getHeaderPartnerList, getUserRole } from '../../../../redux/commonReducer';
import { alertMessage, confirmMessage } from '../../../../components/message';
import { excelDownload } from '../../../../utils/utils';

const { Option } = Select;
const { TabPane } = Tabs;

function PartnersContainer({ query }) {
  const { activePageId, saleChannel, resetSettings } = useSelector((state) => ({
    activePageId: state.common.activePageId,
    saleChannel: state.account.myPartnerPage.saleChannel.data,
    resetSettings: state.common.resetSettings,
  }));

  const [fristFlag, setFristFlag] = useState('');
  const { userRole, userInfo } = useSelector((state) => state.common);

  const { detailInfo, partnerTree, partnerAddStatus, partnerUpdateStatus, updatePatnerDepthStatus } = useSelector((state) => ({
    detailInfo: state.settings.partners.partnerDetailInfo,
    partnerTree: state.settings.partners.partnerTree,
    partnerAddStatus: state.settings.partners.partnerAddStatus,
    partnerUpdateStatus: state.settings.partners.partnerUpdateStatus,
    updatePatnerDepthStatus: state.settings.partners.updatePatnerDepthStatus,
  }));

  const dispatch = useDispatch();

  const [searchOption, setSearchOption] = useState('MC000070');
  const [searchTxt, setSearchTxt] = useState('');
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [treeData, setTreeData] = useState([]);
  const [searchTreeData, setSearchTreeData] = useState([]);
  const [generateTreeData, setGenerateTreeData] = useState([]);
  const [viewFlag, setViewFlag] = useState();
  const [partnerModalVisible, setPartnerModalVisible] = useState(false);
  const [selectedNode, setSelectedNode] = useState();
  const [selectedMoveNode, setSelectedMoveNode] = useState();
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [userRoleState, setUserRoleState] = useState();
  const [buttonExcelDisabled, setButtonExcelDisabled] = useState(false);
  const [localQuery, setLocalQuery] = useState(null);

  useEffect(() => {
    if (activePageId === 'partners' && resetSettings) {
      // 화면 초기화
      dispatch(resetStore());
      reset();
    }
  }, [activePageId]);

  useEffect(() => () => {
    dispatch(resetStore());
    reset();
  }, []);

  useEffect(() => {
    if (userRole.status === 'success') {
      setUserRoleState(userRole.data.isAdmin);
    }
  }, [userRole]);

  useEffect(() => {
    dispatch(updateState({ selectPtnIdx: userInfo.userInfo.ptnIdx }));
    if (fristFlag === '') {
      setFristFlag('init');
    }
  }, [userInfo]);

  useEffect(() => {
    if (partnerTree && partnerTree.data && partnerTree.data.length !== 0 && partnerTree.data.length !== undefined) {
      initSearch();
      dispatch(getHeaderPartnerList());
      setTreeData(partnerTree.data); // 원본 데이터 저장
      setSearchTreeData(partnerTree.data); // 실제 뿌리는 트리 데이터
      const arr = [];
      generateData(partnerTree.data, arr);
      setGenerateTreeData(arr);

      // 최초 진입 시 gip 열림 처리
      const filterdTempArr = partnerTree.data.filter((v) => v.key === 'gip');
      if (filterdTempArr[0].key === 'gip' && fristFlag === 'init') {
        setExpandedKeys(['gip']);
        setFristFlag('update');
      }

      if (query?.ptnId && arr?.length) {
        const filterdArr = arr.filter((v) => v.key === query.ptnId);
        if (filterdArr.length > 0) {
          handleChangeSelectedNode(filterdArr[0]);
          const expandedArr = findParentKeys(query.ptnId, partnerTree.data);
          setExpandedKeys(expandedArr.filter((v) => v !== query.ptnId));
        }
      }
    }
  }, [partnerTree]);

  useEffect(() => {
    if (partnerAddStatus.status === 'success') {
      // 상세페이지로 변경
      alertMessage('파트너 등록이 완료되었습니다.', () => {
        dispatch(getPartnerTree());
        setSelectedNode({ partnerIdx: detailInfo.data.ptnIdx, key: detailInfo.data.ptnId, ptnNm: detailInfo.data.ptnNm });
        setViewFlag('detail');
        setSelectedKeys([detailInfo.data.ptnId]);
        if (!selectedNode) {
          setExpandedKeys(['gip']);
        } else {
          setExpandedKeys([selectedNode.key]);
        }
        dispatch(initPartnerAddStatus());
      });
    } else if (partnerAddStatus.status === 'error') {
      alertMessage(partnerAddStatus.data.error.errorDescription);
    }
  }, [partnerAddStatus, selectedNode]);

  useEffect(() => {
    if (partnerUpdateStatus.status === 'success') {
      dispatch(getPartnerTree());
      const params = {
        idx: selectedNode.partnerIdx,
      };
      dispatch(getInfo({ params }));
      // SelectedNode
      dispatch(initPartnerUpdateStatus());
      alertMessage('파트너 정보 수정이 완료되었습니다.');
    }
  }, [partnerUpdateStatus]);

  useEffect(() => {
    if (updatePatnerDepthStatus.status === 'success') {
      dispatch(getPartnerTree());
      setPartnerModalVisible(false);
      setExpandedKeys([selectedMoveNode.key]);
    } else if (updatePatnerDepthStatus.status === 'error') {
      alertMessage(updatePatnerDepthStatus.data.error.errorDescription);
    }
  }, [updatePatnerDepthStatus]);

  useEffect(() => {
    if (viewFlag === 'add') {
      dispatch(initPartnerIdCheck());
    }
  }, [viewFlag]);

  useEffect(() => {
    if (JSON.stringify(query) !== JSON.stringify(localQuery)) {
      dispatch(resetStore());
      reset();
      setLocalQuery(query);
    }
  }, [query]);

  const reset = () => {
    setViewFlag();
    setSearchOption('MC000070');
    setSearchTxt('');
    setExpandedKeys([]);
    setTreeData([]);
    setSearchTreeData([]);
    setGenerateTreeData([]);
    setPartnerModalVisible(false);
    setSelectedNode();
    setSelectedMoveNode();
    setSelectedKeys([]);
    setUserRoleState();
    setButtonExcelDisabled(false);
    setFristFlag('init');
    setLocalQuery(null);

    dispatch(getSaleChannel());
    dispatch(getPartnerTree());
    dispatch(getUserRole());
  };

  const findParentKeys = (key, tree) => {
    let parentKey = [];
    tree.forEach((data) => {
      const node = data;
      let arr = [];
      if (node.children) {
        if (node.children.some((item) => item.key === key)) {
          arr = [key];
        } else {
          arr = findParentKeys(key, node.children);
        }
      }
      if (arr.length > 0) parentKey = [node.key, ...arr];
    });
    return parentKey;
  };

  const initSearch = useCallback(() => {
    setSearchOption('MC000070');
    setSearchTxt();
  }, []);

  const generateData = useCallback(
    (param, arr) => {
      param.forEach((data) => {
        arr.push({ key: data.key, title: data.title, partnerIdx: data.partnerIdx });
        if (data.children) {
          generateData(data.children, arr);
        }
      });
    },
    [generateTreeData],
  );

  const getParentKey = (key, tree, type) => {
    let parentKey = '';
    tree.forEach((data) => {
      const node = data;
      if (node.children) {
        if (node.children.some((item) => {
          if (type === 'MC000060') {
            // 아이디
            return item.key === key;
          }
          if (type === 'MC000070') {
            // 이름
            return item.title === key;
          }
          if (type === 'MC000130') {
            // 번호
            return item.partnerIdx === key;
          }
          return null;
        })) {
          parentKey = node.key;
        } else if (getParentKey(key, node.children, type)) {
          parentKey = getParentKey(key, node.children, type);
        }
      }
    });

    return parentKey;
  };

  const handleChangeSelectedNode = useCallback((result) => {
    if (result === 'init') {
      setSelectedNode('');
      return;
    }

    setSelectedNode(result);
    // tree selected node 표현
    setSelectedKeys([result.key]);

    const params = {
      idx: result.partnerIdx,
    };
    dispatch(getInfo({ params }));
    if (result.key !== 'gip') {
      setViewFlag('detail');
    } else {
      setViewFlag('gipEmpty');
    }
  }, []);

  const handleSearchChange = (e) => {
    setSearchTxt(e.target.value);
  };

  const handleSearchBtn = useCallback(() => {
    if (searchTxt === '' || !searchTxt) {
      setSearchTreeData(treeData);
      setExpandedKeys([]);
      return;
    }
    // filter tree
    const expandedArr = generateTreeData
      .map((item) => {
        if (searchOption === 'MC000060') {
          // 파트너 아이디
          if (item.key === searchTxt) {
            return getParentKey(item.key, treeData, searchOption);
          }
          return null;
        }
        if (searchOption === 'MC000070') {
          // 파트너 이름
          const titleWrap = item.title.props.children.props.children;
          const titleWrapReverse = titleWrap.split('').reverse().join('');
          const title = titleWrapReverse.split('(');
          if (title[1].split('').reverse().join('').indexOf(searchTxt) > -1) {
            return getParentKey(item.title, treeData, searchOption);
          }
          return null;
        }
        // 파트너 번호

        if (searchOption === 'MC000130') {
          if (item.partnerIdx === Number(searchTxt)) {
            return getParentKey(item.partnerIdx, treeData, searchOption);
          }
          return null;
        }
        return null;
      })
      .filter((item, i, self) => item && self.indexOf(item) === i);
    setExpandedKeys(expandedArr);

    // 검색된 텍스트 하이라이트
    const loopSearchData = (data) => data.map((item) => {
      let index = '';
      if (searchOption === 'MC000060') {
        // 파트너 아이디
        if (item.key === searchTxt) {
          index = item.partnerIdx;
        } else {
          index = -1;
        }
        // index = item.key === searchTxt;
      } else if (searchOption === 'MC000070') {
        // 파트너 이름
        const titleWrap = item.title.props.children.props.children;
        const titleWrapReverse = titleWrap.split('').reverse().join('');
        const title = titleWrapReverse.split('(');
        index = title[1].split('').reverse().join('').indexOf(searchTxt);
      } else {
        // 파트너 번호
        if (item.partnerIdx === Number(searchTxt)) {
          index = item.partnerIdx;
        } else {
          index = -1;
        }
      }

      const title = index > -1 ? (
        <span className="highlight">
          {item.title}
        </span>
      ) : (
        <span>
          {item.title}
        </span>
      );
      if (item.children) {
        return { title, key: item.key, children: loopSearchData(item.children), partnerIdx: item.partnerIdx };
      }
      return {
        title,
        key: item.key,
        partnerIdx: item.partnerIdx,
      };
    });
    setSearchTreeData(loopSearchData(treeData));
  }, [searchTxt, treeData, generateTreeData, searchOption]);

  const handleAddBtnClick = useCallback(() => {
    if (selectedNode && selectedNode.depth > 9) {
      alertMessage('하위 파트너는 최대 10 Depth까지만 등록 가능합니다.');
      return;
    }
    // 파트너 미선택 상태에서 ‘등록’ 버튼 클릭 시 노출되는 안내 문구 삭제 - 파트너 미선택 상태에서 등록 버튼 클릭 시 바로 파트너 등록 영역 노출
    // if (!selectedNode) {
    //   if (window.confirm('파트너를 선택하지 않으면 최상위 파트너가 등록됩니다. 최상위 파트너를 등록하시겠어요?')) {
    //     setViewFlag('add');
    //   }
    // } else {
    //   setViewFlag('add');
    // }
    setViewFlag('add');
  }, [selectedNode]);

  const handleMoveBtnClick = useCallback(() => {
    if (!selectedNode || selectedKeys.length === 0) {
      alertMessage('이동시킬 파트너를 선택해주세요.');
      return;
    }
    setPartnerModalVisible(true);
  }, [selectedNode, selectedMoveNode, selectedKeys]);

  const handleOk = useCallback(() => {
    if (!selectedMoveNode) {
      alertMessage('이동할 위치의 상위 파트너를 선택해주세요.');
      return;
    }
    confirmMessage('파트너를 이동하시겠어요?', () => {
      const params = {
        ptnIdx: selectedNode.partnerIdx,
        upperPtnIdx: selectedMoveNode.partnerIdx,
      };
      dispatch(updatePatnerDepth({ params }));
    }, '예', '아니요');
    // 트리 데이터 새로 호출
  }, [selectedNode, selectedMoveNode]);

  const handleCancel = () => {
    setPartnerModalVisible(false);
  };

  const handleMoveSelectedNode = (result) => {
    setSelectedMoveNode(result);
  };

  const handleDetailInfoSave = useCallback((data) => {
    const params = {
      detailData: data,
      ptnIdx: selectedNode.partnerIdx,
    };
    dispatch(updatePartnerDetailInfo({ params }));
  }, [selectedNode]);

  const handleDetailInfoAdd = useCallback((data) => {
    const params = {
      detailData: data,
    };
    dispatch(insertPartnerDetailInfo({ params }));
    // 트리 선택 초기화
    setSelectedKeys([]);
  }, [selectedNode]);

  const handleSearchOptionChange = (value) => {
    setSearchOption(value);
  };

  const excelButtonOnClick = () => {
    setButtonExcelDisabled(true);
    excelDownload('/api/my/partner/excel/list', userInfo.accessToken, '파트너관리')
      .then((result) => {
        if (result === 'success') {
          setButtonExcelDisabled(false);
        } else {
          setButtonExcelDisabled(false);
        }
      });
  };

  return (
    <>
      <Container>
        <PageHeader title="파트너 관리" subTitle="" />
        <Contents style={{ paddingTop: 12 }}>
          <Paper border className="treeContainer">
            <TreeSearch>
              <Select defaultValue="MC000070" onChange={handleSearchOptionChange} style={{ width: '100px', fontSize: '12px' }} suffixIcon={<SvgArrowDropdown />}>
                <Option value="MC000070">파트너명</Option>
                <Option value="MC000060">파트너 아이디</Option>
                <Option value="MC000130">파트너 번호</Option>
              </Select>
              <Input
                type="text"
                value={searchTxt}
                onChange={handleSearchChange}
                style={{ width: '100px', flex: '1 1 auto', margin: '0 6px' }}
              />
              <CustomButton style={{ width: '37px', flex: '1 0 37px', padding: 0 }} onClick={handleSearchBtn}>검색</CustomButton>
            </TreeSearch>
            <TreeButtons>
              <Button onClick={handleAddBtnClick} width="71" size="small" type="outlineBlue">
                <SvgIconRegister fill="var(--color-blue-500)" />
                등록
              </Button>
              <Button onClick={handleMoveBtnClick} width="70" size="small">이동</Button>
              <ButtonExcel disabled={buttonExcelDisabled} onClick={excelButtonOnClick} width="97">엑셀 다운로드</ButtonExcel>
            </TreeButtons>
            {/* 검색 트리 */}
            {
              searchTreeData && (
                <SearchTreeContainer
                  height="396px"
                  searchTxt={searchTxt}
                  treeData={searchTreeData}
                  expandedKeys={expandedKeys}
                  viewFlag={viewFlag}
                  onChangeSelected={handleChangeSelectedNode}
                  selectedKeys={selectedKeys}
                />
              )
            }
          </Paper>
          {
            !viewFlag && (
              <Paper className="sub-content" border>
                <EmptyContent>
                  <img src={Images.partner_empty} alt="미선택 파트너" />
                  <WarningTitle>선택된 파트너가 없습니다.</WarningTitle>
                  <WarningDetail>파트너 정보 확인을 위해 파트너를 선택해주세요.</WarningDetail>
                </EmptyContent>
              </Paper>
            )
          }
          {
            viewFlag === 'gipEmpty' && (
              <Paper className="sub-content" border>
                <EmptyContent />
              </Paper>
            )
          }
          {
            viewFlag === 'add' && (
              <Paper className="sub-content" border>
                <Title>
                  <span>파트너 등록</span>
                </Title>
                <PartnerAdd partnerAddStatus={partnerAddStatus} saleChannel={saleChannel} onSave={handleDetailInfoAdd} selectedNode={selectedNode} />
              </Paper>
            )
          }
          {
            viewFlag === 'detail' && (
              <Paper className="sub-content" border>
                <Title>
                  <span>파트너 상세</span>
                </Title>
                <Tabs defaultActiveKey="3">
                  <TabPane tab="파트너 정보" key="1">
                    {
                      detailInfo !== undefined && (
                        <PartnerDetailInfo detailInfo={detailInfo} saleChannel={saleChannel} onSave={handleDetailInfoSave} />
                      )
                    }
                  </TabPane>
                  <TabPane tab="소속 사용자" key="2">
                    <UserList userRoleState={userRoleState} selectedNode={selectedNode} />
                  </TabPane>
                </Tabs>
              </Paper>
            )
          }
        </Contents>
      </Container>
      <PatnerTreeModal title="파트너 이동" visible={partnerModalVisible} okText="이동" onOk={handleOk} onCancel={handleCancel} width={430}>
        <span className="modal-helper">
          선택한 파트너 하위로 위치가 이동됩니다.
          <br />
          이동할 위치의 상위 파트너를 선택해주세요.
        </span>
        <div className="tree-box">
          {
            searchTreeData && (
              <TreeContainer
                searchTxt={searchTxt}
                treeData={searchTreeData}
                expandedKeys={expandedKeys}
                onChangeSelected={handleMoveSelectedNode}
              />
            )
          }
        </div>
      </PatnerTreeModal>
    </>
  );
}

const Container = styled(PageLayout)`
`;

const Title = styled.div`
  padding: 20px 0 20px 20px;
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  border-radius: 8px;
`;

const CustomButton = styled(Button)`
  font-size: 12px;
`;

const TreeSearch = styled.div`
  font-size: 12px;
  display: flex;
  justify-content: space-between;
  padding: 10px;

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    padding: 0 0 0 8px;
  }
  .ant-select-arrow {
    right: 8px;
  }
`;

const TreeButtons = styled.div`
  font-size: 12px;
  display: flex;
  justify-content: space-between;
  padding: 10px;
  background: #F7F8FA;
  button {
    flex: 1 1 auto;
  }
  button + button {
    margin-left: 5px;
  }
`;

const Contents = styled.div`
  display: flex;
  margin-top: 16px;
  .treeContainer {
    min-width:272px;
    flex: 1 1 auto;
    height: 498px;
    overflow: hidden;
    margin-right: 20px;
  }
  .sub-content {
    min-width: 600px;
    flex: 1 1 calc(70% - 20px);
    .ant-form-item-label > label::after {
      content: ''
    }
  }

  .category {
    &-wrap {
      padding: 16px 0;
      margin: 0 20px;
    }
    .row {
      display: flex;
      &:not(:last-child) {
        padding-bottom: 14px;
      }
    }
    .left {
      min-width: 80px;
      .tit {
        margin-top: 10px;
      }
    }
    .right {
      width: calc(100% - 76px);
    }
    .tit {
      display: inline-block;
      font-size: 14px;
    }
    .text-input-group {
      width: 100%;
      .text-input.calendar {
        width: 33.3334%;
      }
    }
    .ui-dropdown-group {
      display: flex;
      .ui-dropdown {
        width: 33.3334%;
        &:not(:last-child) {
          margin-right: 8px;
        }
      }
      &.num-2 {
        .ui-dropdown {
          width: 50%;
        }
      }
      &.num-3 {
        .ui-dropdown {
          width: 33.3334%;
        }
      }
    }
    .ui-dropdown-toggle {
      .ellipsis {
        display: inline-block;
        max-width: 60%;
        @include ellipsis;
        vertical-align: middle;
      }
      .count {
        display: inline-block;
        vertical-align: middle;
      }
    }
    .product-code {
      display: flex;
      .text-input {
        width: calc(100% - 156px);
      }
      .file-group {
        width: 140px;
        margin-left: 16px;
        li {
          &:not(:last-child) {
            margin-bottom: 10px;
          }
        }
        .ui-file {
          width: 100%;
        }
        .ui-btn {
          width: 100%;
          margin-right: 0;
        }
      }
    }
    &-detail {
      display: none;
      .category-wrap {
        border-top: 1px solid $gray20;
      }
      .ui-dropdown-group .ui-dropdown {
        width: 100%;
      }
      .row {
        margin-left: -20px;
        margin-right: -20px;
      }
      .col {
        display: flex;
        width: 33.3334%;
        padding: 0 20px;
      }
    }
    &.active {
      .category-detail {
        display: block;
      }
    }
    @media (max-width: 774px) {
      .ui-dropdown-toggle {
        .count {
          display: none;
        }
      }
    }
  }
  .ant-tabs-nav {
    margin:0 20px 20px;
  }
  .ant-tabs-nav:before {
      content: none !important;
  }
  .ant-tabs-ink-bar{
      display: none;
  }
  .ant-tabs-nav-list{
      flex:1;
      background-color: #f7f8fa;
      border-radius: 4px;
  }
  .ant-tabs-tab-btn{
      width: 100%;
      height: 100%;
      text-align: center;
  }
  .ant-tabs{
    padding-top: 10px;
    margin-bottom: 20px;
  }
  .ant-tabs-tab{
      display: flex !important;
      padding:0px;
      height: 40px;
      line-height: 40px; 
      margin: 0px;
      justify-content: center;
      flex: 1;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
      border-radius: 4px;
      background-color: var(--color-steelGray-800);
      color: #fff;
  }
  .ant-form-item-label>label.ant-form-item-required:not(.ant-form-item-required-mark-optional):before {
    content: "";
    margin-right: 0;
  }
  .ant-divider-horizontal {
    margin: 20px 0 0;
  }
`;

const EmptyContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  flex-direction: column;
  .rectangle {
    width: 200px;
    height: 6px;
    left: 770px;
    top: 508px;
    background: #FF0000;
    opacity: 0.1;
  }
`;

const WarningTitle = styled.span`
  font-style: Bold;
  font-size: 16px;
  font-weight: 700;
  line-height: 24px;
  vertical-align: Center;
  color: var(--color-gray-900);
`;

const WarningDetail = styled.span`
  font-size: 14px;
  line-height: 150%;
  /* identical to box height, or 21px */

  text-align: center;

  /* GlayScale/Dark Gray 2 */

  color: var(--color-gray-700);
`;

const PatnerTreeModal = styled(Modal)`
  width: 430px;
  .modal-helper {
    color: var(--color-gray-700);
    padding-bottom: 10px;
  }
  .ant-modal-body {
    padding: 0 20px 20px;
  }
  .tree-box {
    margin-top: 10px;
    border-radius: 4px;
    background:var(--color-white);
    border: 1px solid #E3E4E7;
  }

  .ant-modal-footer {
    border-top: 0px;
    padding:0 0 20px;
    .ant-btn {
      width: 140px;
      height: 40px;
    }
  }
  ${treeStyle}
`;

export default PartnersContainer;
