/* eslint-disable consistent-return */
/* eslint-disable prefer-regex-literals */
/* eslint-disable arrow-body-style */
import styled from 'styled-components';
import { Form, Input, Checkbox } from 'antd';
import { useCallback, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import AccountInqueryModal from '../../../../account/myPartnerPage/components/Modal/AccountInqueryModal';
import { getPartnerIdCheck, initPartnerIdCheck } from '../../redux/slice';
import Images from '../../../../../Images';
import Descripition from '../../../../../components/paper/descripition';
import Button from '../../../../../components/button';

import { getPhoneValidationTransform, getOnlyNumberValidation } from '../../../../../utils/utils';

function PartnerAdd(props) {
  const { partnerAddStatus, onSave, saleChannel, selectedNode } = props;

  const { partnerIdDuplicateFlag } = useSelector((state) => ({
    partnerIdDuplicateFlag: state.settings.partners.partnerIdDuplicateFlag,
  }));

  const dispatch = useDispatch();

  const [form] = Form.useForm();
  const [accountModalVisible, setAccountModalVisible] = useState(false);
  const [checkedArr, setCheckedArr] = useState([]);
  const [partnerIdValue, setPartnerIdValue] = useState();
  const [errorMsg, setErrorMsg] = useState('');
  const [duplicateFlag, setDuplicateFlag] = useState(true);

  const initialStateSalesChnl = {
    Cd: '',
    Nm: '',
  };
  const [salesChnl, setSalesChnl] = useState(initialStateSalesChnl);
  const [salesChnlSupCode, setSalesChnlSupCode] = useState('');
  const [salesChnlSupName, setSalesChnlSupName] = useState('');
  const [salesChnlSubSupCode, setSalesChnlSubSupCode] = useState('');
  const [salesChnlSubSupName, setSalesChnlSubSupName] = useState('');
  const [channelErrorMsg, setChannelErrorMsg] = useState('');
  const [upperPtnIdx, setUpperPtnIdx] = useState('');
  const [partnerIdOutline, setPartnerIdOutline] = useState();
  const [channelOutline, setChannelOutline] = useState();
  const regExp = /^[a-z|A-Z|0-9|]+$/;

  const [formOriginValue, setFormOriginValue] = useState({
    ptnId: '',
    ptnNm: '',
    ptnDesc: '',
    picNm: '',
    picDept: '',
  });

  const handleFormSubmitFailed = useCallback(({ values }) => {
    if (checkedArr && checkedArr.length > 0) {
      // 판매채널 선택 했을시
      if (salesChnl.Cd && !salesChnlSupCode) {
        setChannelErrorMsg(<span className="ant-form-item-explain-error">GS SHOP의 거래처 코드를 입력해주세요.</span>);
        setChannelOutline(true);
      }
    }
    if (values.ptnId === '' || values.ptnId === undefined) {
      setErrorMsg(<span className="ant-form-item-explain-error">파트너 아이디를 입력해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
    }
  }, [partnerIdDuplicateFlag, checkedArr, salesChnl, salesChnlSupCode]);

  const validation = useCallback((values) => {
    if (values.ptnId === '') {
      setErrorMsg(<span className="ant-form-item-explain-error">파트너 아이디를 입력해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
      return false;
    }
    if (values.ptnId.length < 6 || values.ptnId.length > 30) {
      setErrorMsg(<span className="ant-form-item-explain-error">6자 이상 ~ 30자 이내로 입력해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
      return false;
    }

    if (!partnerIdregExp(form.getFieldValue('ptnId'))) {
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
      return false;
    }

    if (partnerIdDuplicateFlag.status !== 'success') {
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);

      return false;
    }

    if (!duplicateFlag) {
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);

      return false;
    }
    setErrorMsg('');
    return true;
  }, [duplicateFlag, partnerIdDuplicateFlag]);

  const handleFormSubmitSuccess = useCallback((values) => {
    if (!validation(values)) {
      return;
    }
    if (checkedArr && checkedArr.length > 0) {
      // 판매채널 선택 했을시
      const params = {
        ptnNm: values.ptnNm ? values.ptnNm : '',
        ptnId: values.ptnId ? values.ptnId : '',
        ptnDesc: values.ptnDesc ? values.ptnDesc : '',
        upperPtnIdx: values.upperPtnIdx ? values.upperPtnIdx : '',
        sortSeq: values.sortSeq ? values.sortSeq : '',
        salesChnlCd: salesChnl.Cd,
        salesChnlNm: salesChnl.Nm,
        supCd: salesChnlSubSupCode || salesChnlSupCode,
        supNm: salesChnlSubSupName || salesChnlSupName,
        picNm: values.picNm ? values.picNm : '',
        picDept: values.picDept ? values.picDept : '',
        picCphoneNo: values.picCphoneNo ? values.picCphoneNo : '',
        picEmail: values.picEmail ? values.picEmail : '',
      };

      if (salesChnl.Cd && !salesChnlSupCode) {
        setChannelErrorMsg(<span className="ant-form-item-explain-error">GS SHOP의 거래처 코드를 입력해주세요.</span>);
        setChannelOutline(true);
      } else { onSave(params); }
    } else {
      // 판매채널 선택 안했을시
      const params = {
        ptnNm: values.ptnNm ? values.ptnNm : '',
        ptnId: values.ptnId ? values.ptnId : '',
        ptnDesc: values.ptnDesc ? values.ptnDesc : '',
        upperPtnIdx: upperPtnIdx || '',
        sortSeq: values.sortSeq ? values.sortSeq : '',
        salesChnlCd: salesChnl.Cd,
        salesChnlNm: salesChnl.Nm,
        supCd: salesChnlSubSupCode || salesChnlSupCode,
        supNm: salesChnlSubSupName || salesChnlSupName,
        picNm: values.picNm ? values.picNm : '',
        picDept: values.picDept ? values.picDept : '',
        picCphoneNo: values.picCphoneNo ? values.picCphoneNo : '',
        picEmail: values.picEmail ? values.picEmail : '',
      };
      onSave(params);
    }
  }, [checkedArr, partnerIdDuplicateFlag, duplicateFlag, salesChnlSupCode, salesChnlSupName, salesChnlSubSupCode, salesChnlSubSupName, upperPtnIdx]);

  useEffect(() => {
    // eslint-disable-next-line no-console
    return () => {
      dispatch(initPartnerIdCheck());
    };
  }, []);

  useEffect(() => {
    if (selectedNode) {
      if (selectedNode.key === 'gip') {
        return false;
      }
      form.setFieldsValue({
        upperPtnNm: `${selectedNode.upperPtnNm ? selectedNode.upperPtnNm : ''}${selectedNode.upperPtnNm ? ' > ' : ''}${selectedNode.ptnNm}`,
      });
      setUpperPtnIdx(selectedNode.partnerIdx);
    }
  }, [selectedNode]);

  useEffect(() => {
    if (partnerIdDuplicateFlag.status === 'success') {
      setErrorMsg(<span style={{ color: '#0091FF' }}>사용 가능한 아이디입니다.</span>);
      setDuplicateFlag(true);
    } else if (partnerIdDuplicateFlag.data.data && partnerIdDuplicateFlag.data.data.data) {
      setErrorMsg(<span className="ant-form-item-explain-error">{partnerIdDuplicateFlag.data.data.error.errorDescription}</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
    }
  }, [partnerIdDuplicateFlag]);

  const handleInitBtn = useCallback(() => {
    // todo. 등록시 상위파트너는 초기화 하지말아야함
    form.resetFields();
    // 파트너 아이디 초기화
    setPartnerIdValue('');
    setErrorMsg('');
    setDuplicateFlag(true);

    // 판매채널 초기화
    setCheckedArr([]);
    setPartnerIdOutline(false);
    // 채널 필수값 초기화
    setChannelErrorMsg('');
    if (selectedNode) {
      form.setFieldsValue({
        upperPtnNm: `${selectedNode.upperPtnNm ? selectedNode.upperPtnNm : ''}${selectedNode.upperPtnNm ? ' > ' : ''}${selectedNode.ptnNm}`,
      });
    }
  }, [selectedNode]);

  const handleCheck = useCallback((value, index) => {
    setChannelOutline(false);
    setChannelErrorMsg('');
    if (value.target.checked) {
      setSalesChnl({ Cd: saleChannel[index].salesChnlCd, Nm: saleChannel[index].salesChnlNm });
    } else {
      setSalesChnl({ Cd: '', Nm: '' });
    }

    setSalesChnlSupName('');
    setSalesChnlSupCode('');
    setSalesChnlSubSupName('');
    setSalesChnlSubSupCode('');

    let tempArr = [...checkedArr];

    const idx = tempArr.indexOf(value.target.value);

    if (idx === -1 && tempArr.length === 0) {
      tempArr.push(value.target.value);
    } else if (idx === 0) {
      tempArr = [];
    } else {
      tempArr = [];
      tempArr.push(value.target.value);
    }

    setCheckedArr(tempArr);
  }, [checkedArr, saleChannel]);

  const handleAccountModalBtn = () => {
    setAccountModalVisible(true);
  };

  const handleOk = useCallback((value) => {
    setSalesChnlSupName(value[0].supName);
    setSalesChnlSupCode(value[0].subSupCode ? value[0].subSupCode : value[0].supCode);
    setSalesChnlSubSupName(value[0].subSupName);
    setSalesChnlSubSupCode(value[0].subSupCode);
    setAccountModalVisible(false);

    setChannelOutline(false);
    setChannelErrorMsg('');
  }, []);

  const handleCancel = () => {
    setAccountModalVisible(false);
  };

  const partnerIdregExp = useCallback((data) => {
    return regExp.test(data);
  }, []);

  const onClickDuplicateBtn = useCallback(() => {
    if (partnerIdValue === '' || partnerIdValue === undefined) {
      setErrorMsg(<span className="ant-form-item-explain-error">파트너 아이디를 입력해주세요.</span>);
      setPartnerIdOutline(true);
      setDuplicateFlag(false);
    } else if (partnerIdValue.length < 6 || partnerIdValue.length > 30) {
      setErrorMsg(<span className="ant-form-item-explain-error">6자 이상 ~ 30자 이내로 입력해주세요.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
    } else if (!partnerIdregExp(partnerIdValue)) {
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
    } else {
      const params = {
        ptnId: partnerIdValue,
      };
      // 파트너 아이디 중복조회 api
      dispatch(getPartnerIdCheck({ params }));
      setPartnerIdOutline(false);
    }
  }, [errorMsg, partnerIdValue]);

  function replaceWithAddingBrace() {
    return '';
  }

  const handleChangePtnId = useCallback((event) => {
    if (!event.target.value) {
      // 전체 삭제한 경우
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setPartnerIdValue(event.target.value);
      setDuplicateFlag(false);
      setPartnerIdOutline(true);
      return;
    }
    if (!(regExp.test(event.target.value))) {
      const thisValue = event.target.value.replace(/[^a-zA-Z0-9]/gi, replaceWithAddingBrace);
      setPartnerIdValue(thisValue);
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setPartnerIdOutline(true);
      return;
    }
    setPartnerIdValue(event.target.value);
    setDuplicateFlag(false);
    setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
    setPartnerIdOutline(true);
  }, [errorMsg]);

  const onFormValueChange = (data) => {
    if (data.sortSeq) {
      form.setFieldsValue({
        sortSeq: getOnlyNumberValidation(data.sortSeq),
      });
    }

    if (data.picCphoneNo) {
      if (getPhoneValidationTransform(data.picCphoneNo) !== data.picCphoneNo) {
        form.setFieldsValue({
          picCphoneNo: getPhoneValidationTransform(data.picCphoneNo),
        });
      }
    }

    if (data.ptnId) {
      if (data.ptnId.length <= 15) {
        setFormOriginValue({ ...formOriginValue, ptnId: data.ptnId });
      }
      if (data.ptnId.length > 15) {
        form.setFieldsValue({
          ptnId: formOriginValue.ptnId,
        });
      }
    }

    if (data.ptnNm) {
      if (data.ptnNm.length <= 15) {
        setFormOriginValue({ ...formOriginValue, ptnNm: data.ptnNm });
      }
      if (data.ptnNm && data.ptnNm.length > 15) {
        form.setFieldsValue({
          ptnNm: formOriginValue.ptnNm,
        });
      }
    }

    if (data.ptnDesc) {
      if (data.ptnDesc.length <= 50) {
        setFormOriginValue({ ...formOriginValue, ptnDesc: data.ptnDesc });
      }
      if (data.ptnDesc.length > 50) {
        form.setFieldsValue({
          ptnDesc: formOriginValue.ptnDesc,
        });
      }
    }

    if (data.picNm) {
      if (data.picNm.length <= 15) {
        setFormOriginValue({ ...formOriginValue, picNm: data.picNm });
      }
      if (data.picNm.length > 15) {
        form.setFieldsValue({
          picNm: formOriginValue.picNm,
        });
      }
    }

    if (data.picDept) {
      if (data.picDept.length <= 50) {
        setFormOriginValue({ ...formOriginValue, picDept: data.picDept });
      }
      if (data.picDept.length > 50) {
        form.setFieldsValue({
          picDept: formOriginValue.picDept,
        });
      }
    }
  };

  const handleChangeSortSeq = useCallback((event) => {
    if (event.target.value === '0') {
      form.setFieldsValue({ sortSeq: '' });
    }
  });

  return (
    <>
      <Container>
        <PartnerForm
          form={form}
          name="partnerForm"
          labelAlign="left"
          labelCol={{ span: 5 }}
          // initialValues={{ remember: true }}
          autoComplete="off"
          onFinish={handleFormSubmitSuccess}
          onValuesChange={onFormValueChange}
          onFinishFailed={handleFormSubmitFailed}
        >
          <PartnerFormHeader><span>기본 정보</span></PartnerFormHeader>
          <CustomFormItem
            label="파트너 아이디"
            name="ptnId"
            extra={errorMsg}
            required
            // rules={[
            //   { required: true, message: '파트너 아이디를 입력해주세요.' },
            // ]}
          >
            <PartnerIdGroup>
              <Input maxLength={30} onChange={handleChangePtnId} placeholder="영문, 숫자 6~30자" className={partnerIdOutline ? 'ptn-error' : ''} value={partnerIdValue} />
              <Button onClick={onClickDuplicateBtn} type="outlineBlue" width="80" htmlType="button">중복 확인</Button>
            </PartnerIdGroup>
          </CustomFormItem>
          <Form.Item
            label="파트너명"
            name="ptnNm"
            rules={[
              { required: true, message: '파트너명을 입력해주세요.' },
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 15) {
                      form.setFieldsValue({
                        ptnNm: value.substring(0, 15),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 15자" maxLength={15} />
          </Form.Item>
          <Form.Item
            label="파트너 설명"
            name="ptnDesc"
            rules={[
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 50) {
                      form.setFieldsValue({
                        ptnDesc: value.substring(0, 50),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 50자" maxLength={50} />
          </Form.Item>
          <Form.Item
            label="상위 파트너"
            name="upperPtnNm"
          >
            <Input value={selectedNode?.upperPtnNm || ''} disabled />
          </Form.Item>
          <Form.Item
            label="우선 순위"
            name="sortSeq"
          >
            <Input onChange={handleChangeSortSeq} maxLength={3} min={1} max={999} placeholder="숫자 1~999" />
          </Form.Item>
          <Form.Item
            label="판매 채널"
            className="formSalesChannel"
            extra={channelErrorMsg}
          >
            <>
              {Object.keys(saleChannel).map((value, index) => (
                <SubChannelGroup key={index}>
                  <Checkbox value={saleChannel[value].salesChnlNm} checked={checkedArr.indexOf(saleChannel[value].salesChnlNm) > -1} onChange={(v) => { handleCheck(v, index); }}>
                    <div className="check-label">{saleChannel[value].salesChnlNm}</div>
                  </Checkbox>
                  {
                    (checkedArr.indexOf(saleChannel[value].salesChnlNm) !== -1) && (
                      <AccountInquiry>
                        <Input disabled className={channelOutline ? 'ptn-error' : ''} value={`${salesChnlSupName}${salesChnlSupCode ? `(${salesChnlSupCode})` : ''}`} />
                        <Button htmlType="button" onClick={handleAccountModalBtn} type="outlineBlue" width="80">거래처 조회</Button>
                      </AccountInquiry>
                    )
                  }
                </SubChannelGroup>
              ))}
            </>
          </Form.Item>
          <div className="ant-divider ant-divider-horizontal" role="separator" />
          <PartnerFormHeader><span>담당자 정보</span></PartnerFormHeader>
          <Form.Item
            label="이름"
            name="picNm"
            rules={[
              { required: true, message: '이름을 입력해주세요.' },
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 15) {
                      form.setFieldsValue({
                        picNm: value.substring(0, 15),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 15자" maxLength={15} />
          </Form.Item>
          <Form.Item
            label="부서"
            name="picDept"
            rules={[
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 50) {
                      form.setFieldsValue({
                        picDept: value.substring(0, 50),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 50자" maxLength={50} />
          </Form.Item>
          <Form.Item
            label="휴대폰 번호"
            name="picCphoneNo"
            rules={[
              {
                pattern: /^\d{3}-\d{3,4}-\d{4}$/,
                message: '올바른 휴대폰 번호가 아닙니다. 다시 확인해주세요.',
              },
            ]}
          >
            <Input placeholder="‘-’ 없이 입력" />
          </Form.Item>
          <Form.Item
            label="이메일"
            name="picEmail"
            rules={[
              { required: true, message: '이메일을 입력해주세요.',
              },
              {
                pattern: /^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/,
                message: '이메일 형식에 맞지 않습니다. 다시 확인해주세요.',
              },
            ]}
            extra={(<Descripition text="최초 마스터 계정의 이메일로 등록되며, 로그인 인증번호가 발송됩니다." padding="6px 0 0 " />)}
          >
            <Input />
          </Form.Item>
          <SubmitWrap>
            <InitButton htmlType="button" onClick={handleInitBtn} width="80" height="40">
              <img src={Images.iconRefresh} alt="초기화" />
              초기화
            </InitButton>
            {
              partnerAddStatus.status === 'pending'
                ? (
                  <SubmitButton type="fillBlue" width="250" height="40" htmlType="submit" disabled>
                    등록
                  </SubmitButton>
                )
                : (
                  <SubmitButton type="fillBlue" width="250" height="40" htmlType="submit">
                    등록
                  </SubmitButton>
                )
            }
          </SubmitWrap>
        </PartnerForm>
      </Container>
      <AccountInqueryModal visible={accountModalVisible} onOk={handleOk} onClose={handleCancel} />
    </>
  );
}

const Container = styled.div`
`;

const PartnerFormHeader = styled.div`
  font-weight: 700;
  font-size: 16px;
  line-height: 24px;
  margin: 0px 0 25px;
`;

const PartnerForm = styled(Form)`
  padding: 0 20px 40px;

  .check-label {
    min-width: 65px;
    white-space: nowrap; 
    width: 30px; 
    overflow: hidden;
    text-overflow: ellipsis; 
  }

  .ant-form-item {
    margin: 0 0 14px;
  }
  .ant-form-item-label {
    min-width:90px;
  }
  .ant-form-item-label>label {
    max-width: auto;
  }

  .margin-right {
    margin-right: 10px;
  }
  .ant-checkbox-group {
    flex: 1;
  }

  .ptn-error {
    border-color: #ff4d4f !important;
  }

`;

const PartnerIdGroup = styled.div`
  display: flex;
  flex: 1;
  input {
    flex: 1 1 auto;
  }
  button {
    flex:0 0 auto;
    margin-left: 6px;
  }
`;

const SubChannelGroup = styled.div`
  display: flex;
  height: 33px;
  flex: 1 1 100%;
  margin-bottom: 5px;
  align-items: center;
`;

const SubmitWrap = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  padding-top: 11px;
  margin-top: 26px;
  button + button {
    margin-left: 10px;
  }
`;

const InitButton = styled(Button)`
  width: 80px;
  height: 40px;
`;

const SubmitButton = styled(Button)`
  width: 250px;
  height: 40px;
`;

const CustomFormItem = styled(Form.Item)`

  .ptn-error {
    border-color: #ff4d4f !important;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }

  .ant-modal-body {
    height: 350px;
    padding: 0;
  }

  .ant-modal-footer {
    text-align: center;
    border-top: 0px;
    display:none;
  }
`;

const AccountInquiry = styled.div`
  display: flex;
  flex: 1;
  input {
    flex: 1 1 auto;
  }
  button {
    flex:0 0 auto;
    margin-left: 6px;
  }
`;
export default PartnerAdd;
