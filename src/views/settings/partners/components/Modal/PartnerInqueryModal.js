import styled from 'styled-components';
import { Input, Modal, Select } from 'antd';
import { useCallback, useEffect, useState, useRef, forwardRef, useImperativeHandle } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getCommonPartnerList } from '../../redux/slice';
import Paper from '../../../../../components/paper';
import PagingTable from '../../../../../components/pagingTable';
import Button from '../../../../../components/button';

const { Option } = Select;

function partnerInqueryModal(props, ref) {
  const { visible, onOk, onClose } = props;

  const columnOptions = [
    {
      title: '파트너 번호',
      dataIndex: 'ptnIdx',
      width: 60,
    },
    {
      title: '파트너 아이디',
      dataIndex: 'ptnId',
      width: 130,
    },
    {
      title: '파트너명',
      dataIndex: 'ptnNm',
      width: 130,
    },
    {
      title: '상태',
      dataIndex: 'stNm',
      width: 40,
    },
  ];

  const { commonPartnerList } = useSelector((state) => ({
    commonPartnerList: state.settings.partners.commonPartnerList,
  }));

  const dispatch = useDispatch();

  const initialState = {
    search: {
      searchType: 'MC000070',
      searchKeyword: '',
      page: 0,
    },
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };

  const tableRef = useRef(null);

  const [state, setState] = useState(initialState);

  useImperativeHandle(ref, () => ({
    setReset: () => {
      setState(initialState);
      if (tableRef.current)tableRef.current.setReset();
    },
  }));

  useEffect(() => {
    if (visible) {
      const params = {
        params: state.search,
      };
      dispatch(getCommonPartnerList(params.search));
    } else {
      setState(initialState);
    }
  }, [visible]);

  useEffect(() => {
    if (commonPartnerList.status === 'success') {
      setState({
        ...state,
        data: commonPartnerList.data && commonPartnerList.data.items ? commonPartnerList.data.items : [],
        loading: commonPartnerList.status === 'pending',
        pagination: {
          ...state.pagination,
          total: commonPartnerList.data && commonPartnerList.data.totalItem
            ? commonPartnerList.data.totalItem : 0,
        },
      });
    }
  }, [commonPartnerList]);

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleSearchBtn = useCallback(() => {
    const params = {
      params: state.search,
    };

    dispatch(getCommonPartnerList(params));
  }, [state]);

  const handleSearchTxtChange = useCallback((e) => {
    setState({ ...state, search: { searchKeyword: e.target.value, searchType: state.search.searchType } });
  }, [state]);

  const handleSearchTypeChange = useCallback((value) => {
    setState({ ...state, search: { searchType: value, searchKeyword: state.search.searchKeyword } });
  }, [state]);

  const handleTableChange = (pagination) => {
    const params = {
      params: {
        searchType: state.search.searchType,
        searchKeyword: state.search.searchKeyword,
        page: pagination.current - 1,
      },
    };

    dispatch(getCommonPartnerList(params));
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const handleOkBtn = () => {
    onOk(tableRef.current.getSelectedRowData());
  };

  return (
    <PartnerModal
      title="파트너 조회"
      visible={visible}
      onOk={handleOkBtn}
      onCancel={onClose}
      okText="선택 완료"
      cancelText="취소"
    >
      <div className="modal-search">
        <div className="modal-search-input">
          <span className="search-title">검색어</span>
          <Select
            className="search-select"
            onChange={handleSearchTypeChange}
            value={state.search.searchType}
          >
            <Option value="MC000070">파트너명</Option>
            <Option value="MC000060">파트너 아이디</Option>
            <Option value="MC000130">파트너 번호</Option>
          </Select>
          <Input
            className="search-input"
            type="text"
            value={state.search.searchKeyword}
            onChange={handleSearchTxtChange}
            style={{ width: '100px' }}
          />
        </div>
        <div className="search-button">
          <Button onClick={handleSearchBtn} type="fillSteelGray" width="200" height="40">검색</Button>
        </div>
      </div>
      <TableWrap>
        <PagingTable
          ref={tableRef}
          columns={columnOptions}
          data={state.data}
          pagination={state.pagination}
          loading={commonPartnerList.status === 'pending'}
          rowKey={(record) => record.ptnId}
          // showRowIndex
          rowSelect
          onChange={handleTableChange}
        />
      </TableWrap>
    </PartnerModal>
  );
}

const PartnerModal = styled(Modal)`
  left: calc(50% - 260px);
  margin: 0;
  
  .modal-search {
    background-color: #F7F8FA;
    height: 134px;
  }

  .search-title {
    flex: 2;
    display: inline-flex;
    align-items: center;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }
  
  .ant-select {
      font-size: 13px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-footer {
    border-top: 0px;
    text-align: center;
  }

  .ant-modal-footer button:last-child {
    width: 250px;
    height: 40px;
  }

  .ant-modal-footer > .ant-btn {
    width: 80px;
    height: 40px;
  }

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
      width: 121px;
  }

  // .ant-table-tbody {
  //   height: 400px;
  // }
`;

const TableWrap = styled(Paper)`
  margin: 20px;
`;

export default forwardRef(partnerInqueryModal);
