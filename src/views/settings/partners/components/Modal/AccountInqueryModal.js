import styled from 'styled-components';
import { Input, Button, Modal, Select } from 'antd';
import { useCallback, useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getAccount, getSupDuplicate } from '../../redux/slice';
import Paper from '../../../../../components/paper';
import Title from '../../../../../components/title';
import PagingTable from '../../../../../components/pagingTable';
import { SvgArrowDropdown } from '../../../../../Images';
import { alertMessage } from '../../../../../components/message';

const { Option } = Select;

function AccountInqueryModal(props) {
  const { visible, onOk, onClose, detailInfoPtnIdx } = props;

  const columnOptions = [
    {
      title: '거래처 코드',
      dataIndex: 'code',
      width: 100,
    },
    {
      title: '하위거래처 코드',
      dataIndex: 'subSupCode',
      width: 150,
    },
    {
      title: '거래처명',
      dataIndex: 'name',
      width: 100,
    },
  ];

  const { accountList, supDuplicate } = useSelector((state) => ({
    accountList: state.settings.partners.accountList,
    supDuplicate: state.settings.partners.supDuplicate,

  }));

  const dispatch = useDispatch();

  const initialState = {
    data: [],
    pagination: {
      current: 0,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
    loading: false,
  };

  const tableRef = useRef(null);

  const [state, setState] = useState(initialState);
  const [searchParams, setSearchParams] = useState({ searchType: 'supCd', searchText: '' });

  useEffect(() => {
    if (visible) {
      dispatch(getAccount(state));
    } else {
      setSearchParams({ searchType: 'supCd', searchText: '' });
      setState(initialState);
    }
  }, [visible]);

  useEffect(() => {
    setState({
      ...state,
      data: accountList.data && accountList.data.result ? accountList.data.result : [],
      loading: accountList.status === 'pending',
      pagination: {
        ...state.pagination,
        total: accountList.data && accountList.data.totalItemSize
          ? accountList.data.totalItemSize : 0,
      },
    });
  }, [accountList]);

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleSearchBtn = useCallback(() => {
    dispatch(getAccount(searchParams));
  }, [state, searchParams]);

  const handleSearchTxtChange = useCallback((e) => {
    setSearchParams({ ...searchParams, searchText: e.target.value });
  }, [searchParams]);

  const handleSearchTypeChange = useCallback((value) => {
    setSearchParams({ ...searchParams, searchType: value });
  }, [searchParams]);

  const handleTableChange = (pagination, filters, sorter) => {
    const params = {
      searchType: searchParams.searchType,
      searchValue: searchParams.searchValue ? searchParams.searchValue : '',
      page: pagination.current - 1,
      size: state.pagination.pageSize,
    };
    dispatch(getAccount({ params }));
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const handleOkBtn = useCallback(() => {
    if (tableRef.current.getSelectedRowData().length === 0) {
      alertMessage('목록을 선택해주세요.');
    } else if (supDuplicate?.status === 'error') {
      const ptnData = supDuplicate.data.data.data;
      alertMessage(`이미 등록된 거래처 코드 입니다.\n다시 확인해 주세요.\n매핑된 파트너 : ${ptnData.ptnNm}(${ptnData.ptnIdx})`);
    } else {
      setSearchParams({ ...searchParams, searchValue: '' });
      onOk(tableRef.current.getSelectedRowData());
    }
  }, [supDuplicate]);

  const onRowClick = useCallback((v) => {
    if (v.supCode) {
      const params = {
        ptnIdx: detailInfoPtnIdx,
        supCd: v.supCode,
        subSupCd: v.subSupCode,
      };
      dispatch(getSupDuplicate({ params }));
    }
  }, [detailInfoPtnIdx]);

  return (
    <AccountModal
      title="거래처 조회"
      visible={visible}
      onOk={handleOkBtn}
      onCancel={onClose}
      okText="선택 완료"
      cancelText="취소"
    >
      <div className="modal-search">
        <div className="modal-search-input">
          <span className="search-title">검색어</span>
          <Select
            className="search-select"
            onChange={handleSearchTypeChange}
            value={searchParams.searchType}
            suffixIcon={<SvgArrowDropdown />}
          >
            <Option value="id">거래처 코드</Option>
            <Option value="name">거래처명</Option>
          </Select>
          <Input
            className="search-input"
            type="text"
            value={searchParams.searchText}
            onChange={handleSearchTxtChange}
            style={{ width: '100px' }}
          />
        </div>
        <div className="search-button">
          <SearchBtn onClick={handleSearchBtn}>검색</SearchBtn>
        </div>
      </div>
      <TableWrap>
        <PagingTable
          ref={tableRef}
          columns={columnOptions}
          data={state.data}
          pagination={state.pagination}
          loading={state.loading}
          rowKey={(record) => record.userId}
          showRowIndex
          rowSelect
          onChange={handleTableChange}
          onRowClick={onRowClick}

        />
      </TableWrap>
    </AccountModal>
  );
}

const AccountModal = styled(Modal)`
  .modal-search {
    background-color: #F7F8FA;
    height: 134px;
  }

  .search-title {
    flex: 2;
    display: inline-flex;
    align-items: center;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-footer {
    border-top: 0px;
    text-align: center;
  }

  .ant-modal-footer button:last-child {
    width: 250px;
    height: 40px;
  }

  .ant-modal-footer > .ant-btn {
    width: 80px;
    height: 40px;
  }

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
      width: 121px;
  }

  .ant-table-tbody {
    height: 400px;
  }
`;

const SearchBtn = styled(Button)`
  width: 200px;
  height: 40px;
  background-color: var(--color-steelGray-800);
  color:var(--color-white);
`;

const TableWrap = styled(Paper)`
  margin: 20px;
`;

const StyledTitle = styled(Title)`
  padding: 19px 0;

  & > p {
    font-family: Pretendard;
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 20px;
    margin-left: 6px;

    text-align: justify;
    color: var(--color-blue-500);
  }`;
export default AccountInqueryModal;
