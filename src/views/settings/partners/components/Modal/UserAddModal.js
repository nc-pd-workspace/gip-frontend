/* eslint-disable no-shadow */
/* eslint-disable no-lonely-if */
/* eslint-disable prefer-regex-literals */
/* eslint-disable arrow-body-style */
import React, { useState, useCallback, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { Modal, Form, Input, Radio, Select, Row, Col } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import PartnerInqueryModal from './PartnerInqueryModal';
import Button from '../../../../../components/button';
import Images, { SvgArrowDropdown } from '../../../../../Images';
import { getUserIdCheck, initUserIdCheck, insertUser, updateStore } from '../../redux/slice';
import { getPhoneValidationTransform, getOnlyNumberAndEngTransform } from '../../../../../utils/utils';
import { getUserRole } from '../../../../../redux/commonReducer';
import { asyncApiState } from '../../../../../redux/constants';
import { alertMessage } from '../../../../../components/message';

const { Option } = Select;
// 시스템관리
function UserAddModal(props) {
  const { visible, selectedNode, onOk, onClose } = props;

  const { userIdDuplicateFlag, insertUserData } = useSelector((state) => ({
    userIdDuplicateFlag: state.settings.partners.userIdDuplicateFlag,
    insertUserData: state.settings.partners.insertUserData,
  }));

  const { userRole } = useSelector((state) => state.common);
  const modalRef = useRef(null);
  const dispatch = useDispatch();

  const [form] = Form.useForm();
  const [userAddModalVisible, setUserAddModalVisible] = useState(false);
  const [partnerInqueryModalVisible, setPartnerInqueryModalVisible] = useState(false);
  const [roleCd, setRoleCd] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const [partnerIdErrorMsg, setPartnerIdErrorMsg] = useState('');
  const [userTypeErrorMsg, setUserTypeErrorMsg] = useState('');
  const [userTypeFlag, setUserTypeFlag] = useState(true);
  const [duplicateFlag, setDuplicateFlag] = useState(true);
  const [partnerIdFlag, setPartnerIdFlag] = useState(true);
  const [selectedPtnIdx, setSeletedPtnIdx] = useState();
  const [selectedPtnTitle, setSeletedPtnTitle] = useState();
  const [userRoleState, setUserRoleState] = useState();
  const [partnerIdValue, setPartnerIdValue] = useState();
  const [userIdOutline, setUserIdOutline] = useState();
  const [partnerIdOutline, setPartnerIdOutline] = useState();

  const [formOriginValue, setFormOriginValue] = useState({
    usrId: '',
    usrNm: '',
  });

  useEffect(() => {
    if (visible) {
      setUserAddModalVisible(visible);
      setErrorMsg('');
      setPartnerIdErrorMsg('');
      dispatch(getUserRole());
      dispatch(initUserIdCheck());
    } else if (!visible) {
      setUserAddModalVisible(visible);
      handleInit();
      dispatch(initUserIdCheck());
      // 사용자 구분 초기화
      setRoleCd('');
      setErrorMsg('');
      setPartnerIdErrorMsg('');
      form.resetFields();
    }
  }, [visible]);

  useEffect(() => {
    if (!selectedNode) {
      return;
    }
    if (!selectedNode.title) {
    // 파트너 등록 직후 사용자등록 할 경우
      setSeletedPtnTitle(`${selectedNode.ptnNm}(${selectedNode.key})`);
      setSeletedPtnIdx(selectedNode.partnerIdx);
    } else if (selectedNode.title.props.children.props) {
      const childrenType = typeof (selectedNode.title.props.children.props.children);
      if (childrenType !== 'string') {
        setSeletedPtnTitle(selectedNode.title.props.children.props.children.props.children);
      } else {
        setSeletedPtnTitle(selectedNode.title.props.children.props.children);
      }
      setSeletedPtnIdx(selectedNode.partnerIdx);
    } else {
      const array = selectedNode.title.props.children.slice(0, -1);
      const titleString = array.join('');
      setSeletedPtnTitle(titleString);
      setSeletedPtnIdx(selectedNode.partnerIdx);
    }
  }, [selectedNode]);

  useEffect(() => {
    if (insertUserData.status === 'success') {
      setUserAddModalVisible(visible);
      handleInit();
      dispatch(initUserIdCheck());
      // 사용자 구분 초기화
      setRoleCd('');
      form.resetFields();
      alertMessage('사용자 등록이 완료되었습니다.', () => { onOk(insertUserData.data.usrIdx); });
    }
  }, [insertUserData]);

  useEffect(() => {
    if (userRole.status === 'success') {
      setUserRoleState(userRole.data.isAdmin);
    }
  }, [userRole]);

  const handleCancel = () => {
    onClose(false);
  };

  const handlePartnerModal = () => {
    setPartnerInqueryModalVisible(true);
  };

  const handlePartnerOk = useCallback((param) => {
    form.setFieldsValue({ partner: `${param[0].ptnNm}(${param[0].ptnId})` });
    setSeletedPtnIdx(param[0].ptnIdx);
    setPartnerInqueryModalVisible(false);
    setPartnerIdErrorMsg('');
    setPartnerIdOutline(false);
  }, []);

  const handlePartnerCancel = () => {
    setPartnerInqueryModalVisible(false);
  };

  const handleuUserChange = () => {
    setRoleCd(form.getFieldValue('roleCd'));
  };

  const partnerIdregExp = useCallback((data) => {
    const regExp = /^[a-z|A-Z|0-9|]+$/;
    return regExp.test(data);
  }, []);

  const handleDuplicateCheck = () => {
    if (!form.getFieldValue('partner') && roleCd !== 'RL0003') {
      setErrorMsg(<span className="ant-form-item-explain-error">파트너를 먼저 선택해 주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (form.getFieldValue('usrId') === '' || form.getFieldValue('usrId') === undefined) {
      setErrorMsg(<span className="ant-form-item-explain-error">사용자 아이디를 입력해주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (form.getFieldValue('usrId').length < 6 || form.getFieldValue('usrId').length > 30) {
      setErrorMsg(<span className="ant-form-item-explain-error">6자 이상 ~ 30자 이내로 입력해주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (!partnerIdregExp(form.getFieldValue('usrId'))) {
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else {
      const params = {};
      params.usrId = form.getFieldValue('usrId');
      if (selectedPtnIdx && (roleCd !== 'RL0003')) {
        params.ptnIdx = selectedPtnIdx;
      }
      setDuplicateFlag(true);
      dispatch(getUserIdCheck({ params }));
      setUserIdOutline(false);
    }
  };

  useEffect(() => {
    if (userIdDuplicateFlag.status === 'success') {
      if (userIdDuplicateFlag.data) {
        setErrorMsg(<span className="ant-form-item-explain-error">이미 사용중인 아이디입니다.</span>);
        setUserIdOutline(true);
        setDuplicateFlag(false);
      } else if (userIdDuplicateFlag && userIdDuplicateFlag.data === false) {
        setErrorMsg(<span className="success-msg">사용 가능한 아이디입니다.</span>);
        setUserIdOutline(false);
        setDuplicateFlag(true);
      } else {
        setErrorMsg();
        setUserTypeFlag(false);
        setDuplicateFlag(false);
      }
    } else if (userIdDuplicateFlag.status === 'error') {
      setErrorMsg(<span className="ant-form-item-explain-error">이미 사용중인 아이디입니다.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (userIdDuplicateFlag.status === 'initial') {
      setErrorMsg('');
      setUserIdOutline(false);
      setDuplicateFlag(false);
    }
  }, [userIdDuplicateFlag]);

  useEffect(() => {
    return () => {
      dispatch(updateStore({
        insertUserData: asyncApiState.initial([]),
        partnerIdDuplicateFlag: asyncApiState.initial([]),
      }));
    };
  }, []);

  // 초기화
  const handleInit = () => {
    form.resetFields();
    setPartnerIdValue('');
    setUserIdOutline(false);
    setPartnerIdOutline(false);
    setUserTypeErrorMsg('');
    setUserTypeFlag(true);
    setErrorMsg('');
    setPartnerIdErrorMsg('');
    // 권한 초기화
    setRoleCd('');
    modalRef.current.setReset();
  };

  function replaceWithAddingBrace() {
    return '';
  }
  const regExp = /^[a-z|A-Z|0-9|]+$/;

  const validation = useCallback((values) => {
    if (values.usrId === '') {
      setErrorMsg(<span className="ant-form-item-explain-error">사용자 아이디를 입력해주세요.</span>);
      setDuplicateFlag(false);
      return false;
    }
    if (values.usrId.length < 6 || values.usrId.length > 30) {
      setErrorMsg(<span className="ant-form-item-explain-error">6자 이상 ~ 30자 이내로 입력해주세요.</span>);
      setDuplicateFlag(false);
      return false;
    }

    if (!partnerIdregExp(form.getFieldValue('usrId'))) {
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setDuplicateFlag(false);
      return false;
    }

    if (userIdDuplicateFlag.status !== 'success') {
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setDuplicateFlag(false);

      return false;
    }

    if (!duplicateFlag) {
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setDuplicateFlag(false);

      return false;
    }

    if (!partnerIdFlag) {
      return false;
    }

    // 관리자인경우
    if (values.roleCd === 'RL0003') {
      if (values.adminRoleCd === 'default') {
        setUserTypeFlag(false);
        setUserTypeErrorMsg(<span className="ant-form-item-explain-error">관리자를 선택해주세요.</span>);
        return false;
      }
    } else {
      if (values.roleCd !== 'RL0003') {
        if (values.adminRoleCd === 'default') {
          setUserTypeFlag(false);
          setUserTypeErrorMsg(<span className="ant-form-item-explain-error">파트너를 먼저 선택해 주세요.</span>);
          return false;
        }
      }
    }
    setErrorMsg('');
    return true;
  }, [duplicateFlag, userIdDuplicateFlag, partnerIdFlag]);

  const handleChangeUsrId = useCallback((event) => {
    if (event.target.value === '') {
      setPartnerIdValue();
      setErrorMsg(<span className="ant-form-item-explain-error">사용자 아이디를 입력해주세요.</span>);
      setUserIdOutline(true);
      return;
    }
    if (!event.target.value) {
      // 전체 삭제한 경우
      setErrorMsg();
      setPartnerIdValue(event.target.value);
      setDuplicateFlag(false);
      return;
    }
    if (!(regExp.test(event.target.value))) {
      const thisValue = event.target.value.replace(/[^a-zA-Z0-9]/gi, replaceWithAddingBrace);
      setPartnerIdValue(thisValue);
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setUserIdOutline(true);
      return;
    }
    setPartnerIdValue(event.target.value);
    setDuplicateFlag(false);
    setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
    setUserIdOutline(true);
  }, [errorMsg]);

  const handleFormSubmitSuccess = useCallback((values) => {
    if (!validation(values)) {
      return;
    }

    if (values.roleCd === 'RL0003') {
      const params = {
        cphoneNo: values.cphoneNo,
        email: values.email,
        roleCd: values.adminRoleCd,
        usrId: values.usrId,
        usrNm: values.usrNm,
      };
      dispatch(insertUser({ params }));
    } else {
      const params = {
        cphoneNo: values.cphoneNo,
        email: values.email,
        ptnIdx: selectedPtnIdx,
        roleCd: values.roleCd,
        usrId: values.usrId,
        usrNm: values.usrNm,
      };
      dispatch(insertUser({ params }));
    }
  }, [userIdDuplicateFlag, selectedPtnIdx]);

  const handleFormSubmitFailed = useCallback(({ values }) => {
    if (values.usrId === '' || values.usrId === undefined) {
      setErrorMsg(<span className="ant-form-item-explain-error">사용자 아이디를 입력해주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (values.usrId.length < 6 || values.usrId.length > 30) {
      setErrorMsg(<span className="ant-form-item-explain-error">6자 이상 ~ 30자 이내로 입력해주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (!partnerIdregExp(form.getFieldValue('usrId'))) {
      setErrorMsg(<span className="ant-form-item-explain-error">영문, 숫자만 입력 가능합니다.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    } else if (userIdDuplicateFlag.status !== 'success') {
      setErrorMsg(<span className="ant-form-item-explain-error">아이디 중복 확인을 해주세요.</span>);
      setDuplicateFlag(false);
      setUserIdOutline(true);
    }

    if (values.roleCd === 'RL0003') {
      if (values.adminRoleCd === 'default') {
        setUserTypeFlag(false);
        setUserTypeErrorMsg(<span className="ant-form-item-explain-error">관리자 유형을 선택해 주세요.</span>);
      }
    } else {
      if (values.partner === '' || values.partner === undefined) {
        setPartnerIdErrorMsg(<span className="ant-form-item-explain-error">파트너를 선택해주세요.</span>);
        setPartnerIdFlag(false);
        setPartnerIdOutline(true);
      }
    }
  }, [userIdDuplicateFlag]);

  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 18,
        offset: 6,
      },
    },
  };

  const handleAdminRoleChange = useCallback(() => {
    setPartnerIdValue('');
    setUserIdOutline(false);
    setPartnerIdOutline(false);
    setUserTypeErrorMsg('');
    setUserTypeFlag(true);
    setErrorMsg('');
    setPartnerIdErrorMsg('');
  }, []);

  const onFormValueChange = (data) => {
    if (data.cphoneNo) {
      if (getPhoneValidationTransform(data.cphoneNo) !== data.cphoneNo) {
        form.setFieldsValue({
          cphoneNo: getPhoneValidationTransform(data.cphoneNo),
        });
      }
    }
    if (data.usrId) {
      if (getOnlyNumberAndEngTransform(data.usrId) !== data.usrId) {
        form.setFieldsValue({
          usrId: getOnlyNumberAndEngTransform(data.usrId),
        });
      }

      if (data.usrId.length <= 15) {
        setFormOriginValue({ ...formOriginValue, usrId: data.usrId });
      }
      if (data.usrId.length > 15) {
        form.setFieldsValue({
          usrId: formOriginValue.usrId,
        });
      }
    }

    if (data.usrNm) {
      if (data.usrNm.length <= 15) {
        setFormOriginValue({ ...formOriginValue, usrNm: data.usrNm });
      }
      if (data.usrNm.length > 15) {
        form.setFieldsValue({
          usrNm: formOriginValue.usrNm,
        });
      }
    }
  };

  return (
    <>
      <UserModal
        forceRender
        title="사용자 등록"
        visible={userAddModalVisible}
        onCancel={handleCancel}
        cancelButtonProps={{ style: { display: 'none' } }}
        okButtonProps={{ style: { display: 'none' } }}
      >
        <UserForm
          form={form}
          name="userForm"
          labelAlign="left"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          initialValues={{ remember: true }}
          autoComplete="off"
          onValuesChange={onFormValueChange}
          onFinish={handleFormSubmitSuccess}
          onFinishFailed={handleFormSubmitFailed}
        >
          <Form.Item
            label="사용자 구분"
            name="roleCd"
            rules={[{ required: true, message: '담당자 정보를 입력해주세요.' }]}
            className="userMargin"
            initialValue="RL0001"
          >
            <Radio.Group onChange={handleuUserChange}>
              <Radio value="RL0001">
                일반
              </Radio>
              <Radio value="RL0002">마스터</Radio>
              {
                userRoleState === 'Y' ? (
                  <Radio value="RL0003">
                    관리자
                  </Radio>
                ) : ''
              }
            </Radio.Group>
          </Form.Item>

          {
            roleCd === 'RL0003' && (
              <>
                <CustomPartnerItem
                  name="adminRoleCd"
                  className={userTypeErrorMsg ? 'ant-form-item-has-error userMargin' : 'userMargin'}
                  rules={[{ required: true, message: '담당자 정보를 입력해주세요.' }]}
                  initialValue="default"
                  {...tailFormItemLayout}
                  style={{ paddingTop: '4px' }}
                >
                  <Select onChange={handleAdminRoleChange} suffixIcon={<SvgArrowDropdown />}>
                    <Option value="default">선택</Option>
                    <Option value="RL0004">시스템 관리자</Option>
                    <Option value="RL0005">GS SHOP 관리자</Option>
                  </Select>
                </CustomPartnerItem>
                {
                  (!userTypeFlag) && (
                    <ErrorUserWrap role="alert">
                      <span>
                        {userTypeErrorMsg}
                      </span>
                    </ErrorUserWrap>
                  )
                }
              </>
            )
          }
          <DescriptionUserWrap role="alert">
            <span>
              마스터는 파트너 정보 수정, 소속 사용자 등록 등 관리 권한이 있는 사용자입니다.
            </span>
          </DescriptionUserWrap>
          {
            selectedNode && selectedPtnTitle && (
              // 파트너관리, 내파트너정보
              roleCd !== 'RL0003' ? (
                <Form.Item
                  label="파트너"
                  required
                >
                  <Row gutter={8}>
                    <Col span={18}>
                      <Form.Item
                        name="partner"
                        noStyle
                        rules={[{ required: true, message: '파트너를 선택해주세요.' }]}
                        initialValue={`${selectedPtnTitle}`}
                      >
                        <Input disabled />
                      </Form.Item>
                    </Col>
                  </Row>
                </Form.Item>
              ) : (
                ''
              )
            )
          }
          {!selectedNode && (
          // 사용자정보
            roleCd !== 'RL0003' && (
              <CustomPartnerItem
                label="파트너"
                required
                extra={partnerIdErrorMsg}
              >
                <Row gutter={8}>
                  <Col span={18}>
                    <Form.Item
                      name="partner"
                      noStyle
                      // rules={[{ required: true, message: '파트너를 선택해주세요.' }]}
                    >
                      <Input disabled className={partnerIdOutline ? 'ptn-error' : ''} />
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <InnerButton type="outlineBlue" htmlType="button" width="80" onClick={handlePartnerModal}>파트너 조회</InnerButton>
                  </Col>
                </Row>
              </CustomPartnerItem>
            )

          )}
          <CustomFormItem
            label="사용자 아이디"
            name="usrId"
            extra={errorMsg}
            required
            rules={[
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 30) {
                      form.setFieldsValue({
                        usrId: value.substring(0, 30),
                      });
                      setPartnerIdValue(value.substring(0, 30));
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <PartnerIdGroup>
              <Input maxLength={30} onChange={handleChangeUsrId} placeholder="영문, 숫자 6~30자" className={userIdOutline ? 'ptn-error' : ''} value={partnerIdValue} />
              <InnerButton onClick={handleDuplicateCheck} type="outlineBlue" width="80" htmlType="button">
                중복 확인
              </InnerButton>
            </PartnerIdGroup>
          </CustomFormItem>
          <DescriptionPtnNmWrap role="alert">
            <span>
              파트너가 다를 경우 동일한 아이디 사용이 가능합니다.
            </span>
          </DescriptionPtnNmWrap>

          <Form.Item
            label="이름"
            name="usrNm"
            rules={[
              { required: true, message: '이름을 입력해주세요.' },
              {
                validator: (_, value) => {
                  if (value) {
                    if (value.length > 15) {
                      form.setFieldsValue({
                        usrNm: value.substring(0, 15),
                      });
                    }
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="최대 15자" maxLength={15} />
          </Form.Item>
          <Form.Item
            label="휴대폰 번호"
            name="cphoneNo"
            rules={[
              {
                required: false,
                message: '휴대폰 번호를 입력해주세요.',
              },
              {
                pattern: /^\d{3}-\d{3,4}-\d{4}$/,
                message: '올바른 휴대폰 번호가 아닙니다. 다시 확인해주세요.',
              },
            ]}
          >
            <Input placeholder="‘-’ 없이 입력" />
          </Form.Item>
          <Form.Item
            label="이메일"
            name="email"
            rules={[
              {
                required: true,
                message: '이메일을 입력해주세요.',
              },
              {
                pattern: /^[0-9a-zA-Z!#&*+,./=?^_~-]([-_\d.]?[0-9a-zA-Z!#&*+,./=?^_~-])*@[0-9a-zA-Z]([-_\d.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/,
                message: '이메일 형식에 맞지 않습니다. 다시 확인해주세요.',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <SubmitWrap>
            <Button onClick={onClose} htmlType="button" width="80" height="40">취소</Button>
            <Button onClick={handleInit} htmlType="button" width="80" height="40">
              <img src={Images.iconRefresh} alt="초기화" />
              초기화
            </Button>
            <Button type="fillBlue" htmlType="submit" width="250" height="40" disabled={insertUserData.status === 'pending'}>
              등록
            </Button>
          </SubmitWrap>
        </UserForm>
      </UserModal>
      <PartnerInqueryModal
        forceRender
        ref={modalRef}
        visible={partnerInqueryModalVisible}
        onOk={handlePartnerOk}
        onClose={handlePartnerCancel}
        getContainer={false}

      />
    </>

  );
}

const UserModal = styled(Modal)`
    .ant-modal-content {
        width: 500px;
    }

    .ant-modal-footer {
      display:none;

    }

    .ant-modal-footer button:last-child {
      width: 250px;
      height: 40px;
    }

    .ant-modal-footer > .ant-btn {
      width: 80px;
      height: 40px;
    }
`;

const UserForm = styled(Form)`
  .check-label {
    min-width: 50px;
    white-space: nowrap; 
    width: 30px; 
    overflow: hidden;
    text-overflow: ellipsis; 
  }

  .ant-form-item {
    margin: 0 0 14px;
  }
  .ant-form-item .ant-form-item {
    margin-bottom: 0;
  }
  .row-margin {
    margin-bottom: 5px;
  }

  .ant-form-item-extra {
    transition: 0.3s;
  }

  .form-div-inner {
    display: flex;
  }
  .ant-form-item-explain {
    // transition: none;
  }
  .userMargin {
    margin-bottom: 0px;
  }
  .success-msg {
    background-repeat: no-repeat;
    background-position: left center;
    // padding-left: 16px;
    height: 18px;
    line-height: 18px;
    font-size: 12px;
    margin-top: 4px;
    color: var(--color-blue-500) !important
  }
`;

const InnerButton = styled(Button)`
  width: 80px;
  font-size: 13px;
`;

const SubmitWrap = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  padding: 26px 0 20px;
  button + button {
    margin-left: 10px;
  }
`;

const ErrorUserWrap = styled.div`
  margin-left: 25%;
  // margin-top: -15px;
  width: 100%;
  height: auto;
  min-height: 24px;
  opacity: 1;
  color: #ff4d4f;

  span {
    svg {
      margin-right: 5px;
    }
  }
`;

const DescriptionUserWrap = styled.div`
// margin-top: -15px;
margin-bottom: 14px;
margin-left: 25%;
font-size: 12px;
line-height: 18px;
text-align: left;
padding: 6px 0 0;
color: var(--color-gray-500);
&:before {
  content: "";
  margin-top: -2px;
  margin-right: 6px;
  display: inline-block;
  width: 2px;
  height: 2px;
  border-radius: 3px;
  vertical-align: middle;
  background-color: var(--color-gray-500);
}
`;

const DescriptionPtnNmWrap = styled.div`
// margin-top: -15px;
margin-bottom: 14px;
margin-left: 25%;
font-size: 12px;
line-height: 18px;
text-align: left;
padding: 0 0 0;
color: var(--color-gray-500);
&:before {
  content: "";
  margin-top: -2px;
  margin-right: 6px;
  display: inline-block;
  width: 2px;
  height: 2px;
  border-radius: 3px;
  vertical-align: middle;
  background-color: var(--color-gray-500);
}
`;

const CustomFormItem = styled(Form.Item)`
  margin-bottom: 7px !important;
  .ptn-error {
    border-color: #ff4d4f !important;
  }

  .search-select {
    flex: 2;
  }

  .search-input {
    flex: 8;
    margin-left: 6px;
  }

  .search-button {
    text-align: center;
  }

  .modal-search-input {
    font-size: 12px;
    display: flex;
    justify-content: space-between;
    padding: 20px;
  }

  .ant-modal-body {
    height: 350px;
    padding: 0;
  }

  .ant-modal-footer {
    text-align: center;
    border-top: 0px;
    display:none;
  }
`;

const PartnerIdGroup = styled.div`
  display: flex;
  flex: 1;
  input {
    flex: 1 1 auto;
  }
  button {
    flex:0 0 auto;
    margin-left: 6px;
  }
`;

const CustomPartnerItem = styled(Form.Item)`
  &.ant-form-item-has-error {
    input, .ant-select-selector {
      border-color: #ff4d4f !important;
    }
  }

  .ptn-error {
    border-color: #ff4d4f !important;
  }
`;

export default UserAddModal;
