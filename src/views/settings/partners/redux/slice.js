import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  partnerDetailInfo: asyncApiState.initial([]),
  accountList: asyncApiState.initial([]),
  partnerList: asyncApiState.initial([]),
  partnerTree: asyncApiState.initial([]),
  partnerIdDuplicateFlag: asyncApiState.initial([]),
  partnerAddStatus: asyncApiState.initial([]),
  partnerUpdateStatus: asyncApiState.initial([]),
  updatePatnerDepthStatus: asyncApiState.initial([]),
  userList: asyncApiState.initial([]),
  userIdDuplicateFlag: asyncApiState.initial([]),
  commonPartnerList: asyncApiState.initial([]),
  insertUserData: asyncApiState.initial([]),
  userRole: asyncApiState.initial([]),
  supDuplicate: asyncApiState.initial([]),
  selectPtnIdx: null,
  partnerStatusCheck: asyncApiState.initial([]),

};

export const { actions, reducer } = createSlice({
  name: 'settings/partners',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateStore: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getInfo: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerDetailInfo = asyncApiState.request(result);
    },
    getInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerDetailInfo = asyncApiState.success(result);
    },
    getInfoFailure: (state, { payload }) => {
      state.partnerDetailInfo = asyncApiState.error(payload);
    },
    getAccount: (state, { payload }) => {
      const result = { ...payload || {} };
      state.accountList = asyncApiState.request(result);
    },
    getAccountSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.accountList = asyncApiState.success(result);
    },
    getAccountFailure: (state, { payload }) => {
      state.accountList = asyncApiState.error(payload);
    },
    getPartner: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerList = asyncApiState.request(result);
    },
    getPartnerSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerList = asyncApiState.success(result);
    },
    getPartnerFailure: (state, { payload }) => {
      state.partnerList = asyncApiState.error(payload);
    },
    getPartnerTree: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerTree = asyncApiState.request(result);
    },
    getPartnerTreeSuccess: (state, { payload }) => {
      const treeData = [...payload.data];
      const refinedData = [];

      const roopChildFunc = (data) => {
        if (data && data.length !== 0) {
          const childNode = data.map((childData) => ({
            title: (
              <div className="disabled">
                <span>{`${childData.ptnNm}(${childData.ptnId})`}</span>
              </div>
            ),
            key: childData.ptnId,
            partnerIdx: childData.ptnIdx,
            sortSeq: childData.sortSeq,
            depth: childData.ptnLvl,
            ptnNm: childData.ptnNm,
            ptnId: childData.ptnId,
            upperPtnNm: childData.upperPtnNm,
            upperPtnIdx: childData.upperPtnIdx,
            children: roopChildFunc(childData.subPtn),
          }));
          childNode.sort((a, b) => a.sortSeq - b.sortSeq);
          return childNode;
        }
        return '';
      };

      const roopFunc = (node) => {
        if (node && node.length !== 0) {
          refinedData.push({
            title: (
              <div className="disabled">
                <span>{`${node.ptnNm}(${node.ptnId})`}</span>
              </div>
            ),
            key: node.ptnId,
            partnerIdx: node.ptnIdx,
            sortSeq: node.sortSeq,
            depth: node.ptnLvl,
            ptnNm: node.ptnNm,
            ptnId: node.ptnId,
            upperPtnNm: node.upperPtnNm,
            upperPtnIdx: node.upperPtnIdx,
            children: roopChildFunc(node.subPtn),
          });
          refinedData.sort((a, b) => a.sortSeq - b.sortSeq);
        }
      };

      treeData.forEach((element) => {
        roopFunc(element);
      });

      const result = {
        ...payload,
        data: refinedData,
      };

      state.partnerTree = asyncApiState.success(result);
    },
    getPartnerTreeFailure: (state, { payload }) => {
      state.partnerTree = asyncApiState.error(payload);
    },
    updatePartnerDetailInfo: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerUpdateStatus = asyncApiState.request(result);
    },
    updatePartnerDetailInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerUpdateStatus = asyncApiState.success(result);
    },
    updatePartnerDetailInfoFailure: (state, { payload }) => {
      state.partnerUpdateStatus = asyncApiState.error(payload);
    },
    initPartnerUpdateStatus: (state) => {
      state.partnerUpdateStatus = asyncApiState.initial([]);
    },
    insertPartnerDetailInfo: (state, { payload }) => {
      const result = { ...payload || {} };

      state.partnerAddStatus = asyncApiState.request(result);
    },
    insertPartnerDetailInfoSuccess: (state, { payload }) => {
      const result = { ...payload || {} };

      state.partnerAddStatus = asyncApiState.success(result);
      state.partnerDetailInfo = asyncApiState.success(result);
    },
    insertPartnerDetailInfoFailure: (state, { payload }) => {
      state.partnerAddStatus = asyncApiState.error(payload);
    },
    initPartnerAddStatus: (state) => {
      state.partnerAddStatus = asyncApiState.initial([]);
    },
    getPartnerIdCheck: (state, { payload }) => {
      const result = { ...payload || {} };

      state.partnerIdDuplicateFlag = asyncApiState.request(result);
    },
    getPartnerIdCheckSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerIdDuplicateFlag = asyncApiState.success(result);
    },
    getPartnerIdCheckFailure: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerIdDuplicateFlag = asyncApiState.error(result);
    },
    initPartnerIdCheck: (state) => {
      state.partnerIdDuplicateFlag = asyncApiState.initial(false);
    },
    updatePatnerDepth: (state, { payload }) => {
      const result = { ...payload || {} };
      state.updatePatnerDepthStatus = asyncApiState.request(result);
    },
    updatePatnerDepthSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.updatePatnerDepthStatus = asyncApiState.success(result);
    },
    updatePatnerDepthFailure: (state, { payload }) => {
      state.updatePatnerDepthStatus = asyncApiState.error(payload);
    },
    getUsers: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userList = asyncApiState.request(result);
    },
    getUsersSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userList = asyncApiState.success(result);
    },
    getUsersFailure: (state, { payload }) => {
      state.userList = asyncApiState.error(payload);
    },
    // 사용자 id 중복 체크
    getUserIdCheck: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userIdDuplicateFlag = asyncApiState.request(result);
    },
    getUserIdCheckSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userIdDuplicateFlag = asyncApiState.success(result);
    },
    getUserIdCheckFailure: (state, { payload }) => {
      state.userIdDuplicateFlag = asyncApiState.error(payload);
    },
    // 내파트너 소속 사용자 id 중복 체크
    getMyPartnerUserIdCheck: (state, { payload }) => {
      const result = { ...payload || {} };
      state.myPartnerUserIdDuplicateFlag = asyncApiState.request(result);
    },
    getMyPartnerUserIdCheckSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.myPartnerUserIdDuplicateFlag = asyncApiState.success(result);
    },
    getMyPartnerUserIdCheckFailure: (state, { payload }) => {
      state.myPartnerUserIdDuplicateFlag = asyncApiState.error(payload);
    },
    initUserIdCheck: (state) => {
      state.userIdDuplicateFlag = asyncApiState.initial('init');
    },
    getCommonPartnerList: (state, { payload }) => {
      const result = { ...payload || {} };
      state.commonPartnerList = asyncApiState.request(result);
    },
    getCommonPartnerListSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.commonPartnerList = asyncApiState.success(result);
    },
    getCommonPartnerListFailure: (state, { payload }) => {
      state.commonPartnerList = asyncApiState.error(payload);
    },
    insertUser: (state, { payload }) => {
      const result = { ...payload || {} };
      state.insertUserData = asyncApiState.request(result);
    },
    insertUserSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.insertUserData = asyncApiState.success(result);
    },
    insertUserFailure: (state, { payload }) => {
      state.insertUserData = asyncApiState.error(payload);
    },
    getUserRole: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userRole = asyncApiState.request(result);
    },
    getUserRoleSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userRole = asyncApiState.success(result);
    },
    getUserRoleFailure: (state, { payload }) => {
      state.userRole = asyncApiState.error(payload);
    },
    // 거래처 중복
    getSupDuplicate: (state, { payload }) => {
      const result = payload;
      state.supDuplicate = asyncApiState.request(result);
    },
    getSupDuplicateSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.supDuplicate = asyncApiState.success(result);
    },
    getSupDuplicateFailure: (state, { payload }) => {
      state.supDuplicate = asyncApiState.error(payload);
    },
    // 파트너 상태 변경전 유효성 검사
    getPartnerStatusCheck: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerStatusCheck = asyncApiState.request(result);
    },
    getPartnerStatusCheckSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerStatusCheck = asyncApiState.success(result);
    },
    getPartnerStatusCheckFailure: (state, { payload }) => {
      const result = { ...payload || {} };
      state.partnerStatusCheck = asyncApiState.error(result);
    },
    initPartnerStatusCheck: (state) => {
      state.partnerStatusCheck = asyncApiState.initial([]);
    },
  },
});

export const {
  resetStore,
  updateStore,
  updateState,
  getInfo,
  getAccount,
  getPartner,
  getPartnerTree,
  updatePartnerDetailInfo,
  insertPartnerDetailInfo,
  getPartnerIdCheck,
  updatePatnerDepth,
  initPartnerIdCheck,
  getUsers,
  getUserIdCheck,
  getMyPartnerUserIdCheck,
  getCommonPartnerList,
  initUserIdCheck,
  insertUser,
  initPartnerAddStatus,
  initPartnerUpdateStatus,
  getUserRole,
  getSupDuplicate,
  getPartnerStatusCheck,
  initPartnerStatusCheck,

} = actions;

export default reducer;
