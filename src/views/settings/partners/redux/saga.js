import {
  all, fork,
  takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import {
  getInfo,
  getPartnerTree,
  updatePartnerDetailInfo,
  insertPartnerDetailInfo,
  getPartnerIdCheck,
  updatePatnerDepth,
  getUsers,
  getUserIdCheck,
  getMyPartnerUserIdCheck,
  getCommonPartnerList,
  insertUser,
  getUserRole,
  getSupDuplicate,
  getPartnerStatusCheck,
} from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

const { getPatnerInfo } = API.Partners;

/** createPromiseSaga로 api공통 로직 적용 */
const partnerInfoSaga = createPromiseSaga(getInfo, getPatnerInfo);
const partnerTreeSaga = createPromiseSaga(getPartnerTree, API.Partners.getPatnerTree);
const updatePatnerDetailInfoSaga = createPromiseSaga(updatePartnerDetailInfo, API.Partners.updatePatnerDetailInfo);
const insertPatnerDetailInfoSaga = createPromiseSaga(insertPartnerDetailInfo, API.Partners.insertPatnerDetailInfo);
const PartnerIdCheckSaga = createPromiseSaga(getPartnerIdCheck, API.Partners.getPartnerIdCheck);
const updatePatnerDepthSaga = createPromiseSaga(updatePatnerDepth, API.Partners.updatePatnerDepth);
const getUserListSaga = createPromiseSaga(getUsers, API.Partners.getUserList);
const getUserIdCheckSaga = createPromiseSaga(getUserIdCheck, API.Partners.getUserIdCheck);
const getMyPartnerUserIdCheckSaga = createPromiseSaga(getMyPartnerUserIdCheck, API.Partners.getMyPartnerUserIdCheck);
const getCommonPartnerListSaga = createPromiseSaga(getCommonPartnerList, API.Partners.getCommonPartnerList);
const insertUserSaga = createPromiseSaga(insertUser, API.Partners.insertUser);
const getUserRoleSaga = createPromiseSaga(getUserRole, API.Partners.getUserRole);
const commonSupDuplicate = createPromiseSaga(getSupDuplicate, API.Common.supDuplicate);
const commonPartnerStatusCheck = createPromiseSaga(getPartnerStatusCheck, API.Common.partnerStatusCheck);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getInfo, partnerInfoSaga);
  yield takeLatest(getPartnerTree, partnerTreeSaga);
  yield takeLatest(updatePartnerDetailInfo, updatePatnerDetailInfoSaga);
  yield takeLatest(insertPartnerDetailInfo, insertPatnerDetailInfoSaga);
  yield takeLatest(getPartnerIdCheck, PartnerIdCheckSaga);
  yield takeLatest(updatePatnerDepth, updatePatnerDepthSaga);
  yield takeLatest(getUsers, getUserListSaga);
  yield takeLatest(getUserIdCheck, getUserIdCheckSaga);
  yield takeLatest(getCommonPartnerList, getCommonPartnerListSaga);
  yield takeLatest(insertUser, insertUserSaga);
  yield takeLatest(getMyPartnerUserIdCheck, getMyPartnerUserIdCheckSaga);
  yield takeLatest(getUserRole, getUserRoleSaga);
  yield takeLatest(getSupDuplicate, commonSupDuplicate);
  yield takeLatest(getPartnerStatusCheck, commonPartnerStatusCheck);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
