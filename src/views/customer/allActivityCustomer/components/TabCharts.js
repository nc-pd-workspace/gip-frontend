import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import moment from 'moment';

import ReactApexChart from 'react-apexcharts';

import { Tabs, TabPane } from '../../../../components/tabs';

import { COLORS } from '../../../../styles/Colors';

import { ReactComponent as SvgArrowUp } from '../../../../Images/arrow_up.svg';
import { ReactComponent as SvgArrowDown } from '../../../../Images/arrow_down.svg';
import { CHART_COLORS } from '../../../../styles/chartColors';
import ToolTip from '../../../../components/toolTip';
import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import { legendDefault } from '../../../../utils/chartOptions';
import { TabHeadTitle } from '../constants';
import PercentView from '../../../../components/percentView';

function YearArrow({ yaearArrowData }) {
  return (
    <>
      {yaearArrowData > 0 ? <SvgArrowUp width="16" height="16" fill="#0091FF" /> : <SvgArrowDown width="16" height="16" fill="#FF6C63" /> || '-'}
    </>
  );
}
const TooltipText = [
  (
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에서 활동(방문, 관심, 주문, 충성)한 고객수 입니다.
      <br />
      <br />
      전년대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 -전년도  전체 활동고객수)/전년도 전체 활동고객수 * 100%
      <br />
      - 예) 올해 기준일 2022. 1. 7.
      <br />
      &nbsp;&nbsp;&nbsp;전년대비 성장률=(2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~ 7 합계 * 100%
    </>
  ),
  (
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품을 새롭게 방문한 고객이
      <br />
      활동(신규방문, 신규관심, 신규주문, 신규충성)한 고객수 입니다.
      <br />
      <br />
      전년대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전년대비 성장률=(올해년도 신규 활동고객수 -전년도 신규 활동고객수)/전년도 신규 활동고객수 * 100%
      <br />
      - 예) 올해 기준일 2022. 1. 7.
      <br />
      &nbsp;&nbsp;&nbsp;(2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~7 합계 * 100%
    </>
  ),
];

function TabCharts({ setSelectedTab, customerType, statisticsData, lineAndColumn }) {
  const [allchartData, setAllChartData] = useState('');

  useEffect(() => {
    if (lineAndColumn?.data?.series) {
      const changeArr = lineAndColumn.data.series.map(({ data, name, type }) => ({ data, name, type }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '', type: 'line' });

      const lineAndColumnChartData = {
        series: changeArr,
        options: {
          chart: {
            zoom: {
              enabled: false,
            },
            height: 350,
            type: 'line',
            toolbar: {
              show: false,
            },
          },
          stroke: {
            width: [0, 0, 3],
          },
          colors: [CHART_COLORS.UNKNOWN, CHART_COLORS.CUSTOMER_ACTIVITY, CHART_COLORS.VARIATION_12],
          dataLabels: {
            enabled: false,
            enabledOnSeries: [1],
            formatter(val) {
              return getChartValuePeople(val);
            },
          },
          labels: lineAndColumn.data.labels,
          xaxis: {
            type: 'string',
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
            tooltip: {
              enabled: false,
            },
          },
          tooltip: {
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = lineAndColumn.data.labels[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return val && `${(val).toLocaleString()}명`;
              },
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
          legend: {
            ...legendDefault,
          },
        },
      };
      setAllChartData(lineAndColumnChartData);
    } else {
      setAllChartData('');
    }
  }, [lineAndColumn]);
  const tabHead = (idx, total, changeRate = '') => (
    <>
      <Title>
        {TabHeadTitle[idx]}
        <ToolTip text={TooltipText[idx]} position={(idx === 1) ? 'right' : false} />
      </Title>
      <Total>
        <div className="count">{total ? total.toLocaleString('ko-KR') : 0}</div>
        <div className="unit">명</div>
      </Total>
      <ChangeRate>
        <div className="item">
          <div className="text">
            전년 대비
          </div>
          <PercentView data={changeRate} />
        </div>
      </ChangeRate>
    </>
  );

  return (
    <Container>
      {statisticsData.status === 'success' && statisticsData.data
        && (
          <Tabs activeKey={customerType} onChange={(v) => { setSelectedTab(v); }}>

            <TabPane tab={tabHead(0, statisticsData.data.active_cust_count, statisticsData.data.active_cust_count_compared_year)} key="total">
              {/* <LoadingComponent isLoading={lineAndColumn?.status === 'pending'} />
            {allchartData
              ? <ReactApexChart options={allchartData.options} series={allchartData.series} type="line" height={350} />
              : <EmptyGraph />} */}

            </TabPane>
            <TabPane tab={tabHead(1, statisticsData.data.new_cust_count, statisticsData.data.new_cust_count_compared_year)} key="new" />
          </Tabs>

        ) }
      <>
        <div className="chart-name-wrap">
          {allchartData && statisticsData.data
          && (
            <p className="chart-name">
              내 브랜드
              {' '}
              {customerType === 'total' ? '전체' : '신규'}
              {' '}
              활동고객수의 일별 변화를 확인해보세요.
            </p>
          ) }
        </div>
        <div className="loadingDiv apexcharts-legend-first-none">
          <LoadingComponent isLoading={lineAndColumn?.status === 'pending' || statisticsData.status === 'pending'} />
          {allchartData && statisticsData.data
            ? <ReactApexChart options={allchartData.options} series={allchartData.series} type="line" height={350} />
            : <EmptyGraph />}
        </div>
      </>
    </Container>
  );
}

const Container = styled.div`
  .ant-tabs-nav:before {
    content: none !important;
  }
  .ant-tabs-ink-bar{
    display: none;
  }
  .ant-tabs-nav-list{
    flex:1;
  }
  .ant-tabs-tab-btn{
    width: 100%;
  }
  .ant-tabs{
    margin-bottom: 5px;
  }
  .ant-tabs-tab{
    display: flex !important;
    padding:0px;
    line-height: 40px; 
    margin: 0px;
    justify-content: center;
    flex: 1;
    margin-left: 10px;
    background-color: #fff;
  }
  .ant-tabs-tab:first-child{
    margin-left: 0px !important;
  }
  .ant-tabs-tab .ant-tabs-tab-btn{
    border-radius: 8px;
    border: var(--border-default);
    color: #333;
    transition: none;
    padding: 16px;
    height: 116px;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
    border-radius: 8px;
    border: 2px solid var(--color-blue-500);
    color: #333;
    padding: 15px;
    text-shadow: none;
  }
  .ant-tabs-tab:hover {
    color: #333;
  }
`;

const Title = styled.h3`
  margin-bottom: 6px;
  color: var(--color-gray-700);
  font-size: 13px;
  font-weight: 700;
  line-height: 18px;
  display: flex;
  align-items: center;
  position: relative;
  & > button {
    border: none;
    background-color: transparent;
    padding: 0;
    width: 16px;
    height: 16px;
    margin-left: 2px;
    cursor: help;
  }
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  height: 36px;
  .count {
    color: ${COLORS.DARK_GRAY_1};
    font-size: 20px;
    font-weight: 700;
    line-height: 36px;
    letter-spacing: -0.75px;
  }
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 30px;
    margin-left: 4px;
  }
`;

const ChangeRate = styled.div`
  display: flex;
  align-items: center;
  height: 12px;
  margin-top: 4px;
  .item {
    display: flex;
    align-items: center;
    font-size: 12px;
    font-weight: 400;
  }
  .text{
    color: ${COLORS.DARK_GRAY_2};
  }
`;

export default TabCharts;
