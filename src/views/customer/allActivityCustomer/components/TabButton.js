import React from 'react';
import styled from 'styled-components';

import Images from '../../../../Images';

function TabButton({
  title, amount, changeRateByYear,
}) {
  return (
    <Container className="test">
      <Title>
        {title}
        <img className="circle" src={Images.exclamation_circle} alt="exclamation_circle" width={16} height={16} />
      </Title>
      <Total>
        {amount.value.toLocaleString('ko-KR')}
        <div className="unit">{amount.unit}</div>
      </Total>
      {changeRateByYear
      && (
        <ChangeRate>
          <div className="item">
            <div className="text">
              전년
            </div>
            <div className="arrow up">
              {changeRateByYear}
              %
            </div>
          </div>
        </ChangeRate>
      ) }
    </Container>
  );
}

const Container = styled.div`
flex:1;
position: relative;
:not(:last-child)::after {
    content: "";
    display: block;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 22px;
    width: 1px;
    height: 48px;
    background-color: #e3e4e7;
}
`;

const Title = styled.div`
  color: var(--color-gray-900);
  font-size: 12px;
  line-height: 18px;
  font-weight: 700;
  margin-bottom: 6px;
  .circle{
    padding-left: 2px;
  }
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  color: var(--color-gray-900);
  font-size: 24px;
  font-weight: 700;
  line-height: 36px;
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 18px;
    padding-bottom: 7px;
    margin-left: 2px;
  }
`;

const ChangeRate = styled.div`
  display: flex;
  align-items: center;
  margin-top: 2px;
  .item {
    display: flex;
    align-items: center;
    font-size: 12px;
    font-weight: 400;
  }
  .text{
    color: var(--color-gray-700);
  }
  .arrow{
    margin-left: 4px;
  }
  .up{
    color: #e05a42;
  }
  .up::before {
    content: "";
    display: inline-block;
    width: 0;
    height: 0;
    margin-right: 4px;
    border-radius: 2px;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-bottom: 4px solid #e05a42;
    vertical-align: middle;}
  .down{
    color: var(--color-blue-500);
  }
  .down::before {
    content: "";
    display: inline-block;
    width: 0;
    height: 0;
    margin-right: 4px;
    border-radius: 2px;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-bottom: 4px solid #e05a42;
    vertical-align: middle;}
`;

export default TabButton;
