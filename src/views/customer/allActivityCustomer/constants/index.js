import { CHART_COLORS } from '../../../../styles/chartColors';

export const TabHeadTitle = ['전체 활동고객수', '신규 활동고객수'];
export const AlertTabHeadTitle = { total: '전체 활동고객수', new: '신규 활동고객수' };

export const defaultCategoryOptions = [
  [
    { label: 'al', value: 'al' },
    { label: 'al1', value: 'al1' },
    { label: 'al2', value: 'al2' },
  ],
  [
    { label: '2al', value: '2al', parent: 'al' },
    { label: '20al', value: '20al', parent: 'al' },
    { label: '200al', value: '200al', parent: 'al' },
    { label: '21al1', value: '21al2', parent: 'al1' },
    { label: '211al1', value: '211al2', parent: 'al1' },
    { label: '2111al1', value: '2111al2', parent: 'al1' },
    { label: '22al2', value: '22al2', parent: 'al2' },
    { label: '222al2', value: '222al2', parent: 'al2' },
    { label: '2222al2', value: '2222al2', parent: 'al2' },
  ],
];

export const genderOptions = [
  { label: '남자', value: 'male' },
  { label: '여자', value: 'female' },
];

export const membershipOptions = [
  { label: '골드', value: 'gold' },
  { label: '실버', value: 'silver' },
];

export const locationOptions = [
  { label: '서울', value: 'seoul' },
  { label: '과천', value: 'gwachon' },
  { label: '합정', value: 'hapjung' },
];

const mockData = {
  series: [{
    name: '남성',
    data: [
      8, 13, 15.2, 17.6, 30, 42, 58, 76, 78, 84, 80, 86, 82, 84, 90, 78, 70, 60,
    ],
  },
  {
    name: '여성',
    data: [
      -16, -21, -21, -23, -28, -44, -57, -74, -79.2, -84, -86, -88, -82, -80, -82, -68, -62, -56,
    ],
  }],
  yaxis: {

  },
  xaxis: {

  },
};

export const chartData = {
  series: [{
    name: '활동고객수',
    type: 'column',
    data: [440, 505, 414, 671, 227, 413, 201, 352, 752, 320, 257, 160],
  }, {
    name: '신규 고객수',
    type: 'line',
    data: [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16],
  }],
  options: {
    chart: {
      height: 350,
      type: 'line',
      toolbar: {
        show: false,
      },
    },
    stroke: {
      width: [0, 3],
    },
    colors: [CHART_COLORS.CUSTOMER_ACTIVITY, CHART_COLORS.CUSTOMER_LINE],
    dataLabels: {
      enabled: false,
      enabledOnSeries: [1],
    },
    labels: ['01 Jan 2001', '02 Jan 2001', '03 Jan 2001', '04 Jan 2001', '05 Jan 2001', '06 Jan 2001', '07 Jan 2001', '08 Jan 2001', '09 Jan 2001', '10 Jan 2001', '11 Jan 2001', '12 Jan 2001'],
    xaxis: {
      type: 'datetime',
      axisTicks: false,
      labels: {
        show: false,
      },
    },
    yaxis: [
      {}, {
        opposite: true,
        labels: {
          show: true,
        },
      },
    ],
  },
};

export const barData = {
  series: mockData.series,
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
      stacked: true,
    },
    colors: [CHART_COLORS.GENDER_MALE, CHART_COLORS.GENDER_FEMALE],
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight: '80%',
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: 1,
      colors: ['#fff'],
    },

    grid: {
      xaxis: {
        lines: {
          show: false,
        },
      },
    },
    yaxis: {
      min: -100,
      max: 100,
      title: {
        // text: 'Age',
      },
    },
    tooltip: {
      shared: false,
      x: {
        formatter(val) {
          return val;
        },
      },
      y: {
        formatter(val) {
          return `${Math.abs(val)}%`;
        },
      },
    },
    xaxis: {
      categories: ['60대 이상', '50대 후반', '50대 초반', '40대 후반', '40대 초반', '30대 후반', '30대 초반', '20대 후반', '20대 초반', '10대'],
      title: {
        text: '비율',
      },
      labels: {
        formatter(val) {
          return `${Math.abs(Math.round(val))}%`;
        },
      },
    },
  },
};

export const radarData = {

  series: [{
    name: 'GS SHOP',
    data: [80, 50, 30, 40, 100, 20],
  }, {
    name: '브랜드',
    data: [20, 30, 40, 80, 20, 80],
  }],
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'radar',
      dropShadow: {
        enabled: true,
        blur: 1,
        left: 1,
        top: 1,
      },
    },
    title: {
      // text: 'Radar Chart - Multi Series',
    },
    stroke: {
      width: 2,
    },
    fill: {
      opacity: 0.1,
    },
    markers: {
      size: 0,
    },
    xaxis: {
      categories: ['', '', 'VVIP', 'VIP'],
    },
  },
};

export const barData2 = {

  series: [{
    data: [21, 22, 10, 28, 16, 21, 13, 30],
  }],
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
    },
    // colors,
    plotOptions: {
      bar: {
        columnWidth: '45%',
        distributed: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: false,
    },
    xaxis: {
      categories: [
        '서울',
        '인천',
        '경기',
        '대전',
        '부산',
        '대구',
        '충북',
        '전주',
      ],
      labels: {
        style: {
          // colors,
          fontSize: '12px',
        },
      },
    },
  },

};

export const treemapData = {

  series: [
    {
      data: [
        {
          x: '서울',
          y: 218,
        },
        {
          x: '인천',
          y: 149,
        },
        {
          x: '경기',
          y: 184,
        },
        {
          x: '대전',
          y: 55,
        },
        {
          x: '대구',
          y: 84,
        },
        {
          x: '전주',
          y: 31,
        },
        {
          x: '충북',
          y: 70,
        },
        {
          x: '춘천',
          y: 30,
        },
        {
          x: '강릉',
          y: 44,
        },
        {
          x: '부산',
          y: 68,
        },

      ],
    },
  ],
  options: {
    legend: {
      show: false,
    },
    chart: {
      toolbar: {
        show: false,
      },
      type: 'treemap',
    },
    colors: [
      '#3B93A5',
      '#F7B844',
      '#ADD8C7',
      '#EC3C65',
      '#CDD7B6',
      '#C1F666',
      '#D43F97',
      '#1E5D8C',
      '#421243',
      '#7F94B0',
      '#EF6537',
      '#C0ADDB',
    ],
    plotOptions: {
      treemap: {
        distributed: true,
        enableShades: false,
      },
    },
  },

};

export const barData3 = {
  series: [{
    name: '~ 3회',
    data: [44, 55, 41, 67, 22, 43, 21, 49],
  }, {
    name: '4 ~ 6회',
    data: [13, 23, 20, 8, 13, 27, 33, 12],
  }, {
    name: '6회 ~',
    data: [11, 17, 15, 15, 21, 14, 15, 13],
  }],
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
      stacked: true,
      stackType: '100%',
    },
    responsive: [{
      breakpoint: 480,
      options: {
        legend: {
          position: 'bottom',
          offsetX: -10,
          offsetY: 0,
        },
      },
    }],
    xaxis: {
      categories: ['', '', '', '', '', '', '', ''],
    },
    fill: {
      opacity: 1,
    },
  },

};

export const barData3_2 = {
  series: [{
    name: '~ 3만',
    data: [44, 55, 41, 67, 22, 43, 21, 49],
  }, {
    name: '4 ~ 6만',
    data: [13, 23, 20, 8, 13, 27, 33, 12],
  }, {
    name: '6만 ~',
    data: [11, 17, 15, 15, 21, 14, 15, 13],
  }],
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
      stacked: true,
      stackType: '100%',
    },
    responsive: [{
      breakpoint: 480,
      options: {
        legend: {
          position: 'bottom',
          offsetX: -10,
          offsetY: 0,
        },
      },
    }],
    xaxis: {
      categories: ['', '', '', '', '', '', '', ''],
    },
    fill: {
      opacity: 1,
    },
  },

};

export const barData4 = {

  series: [{
    data: [2000, 1500, 800, 1000, 540, 900, 690, 1100, 1200, 1380],
  }],
  options: {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      categories: ['GS25', 'GS THE FRESH', '랄라블라', 'GS Fresh Mall', '파르나스호텔', '우리동네 딜리버리', 'GS파크24',
        '유어스', '브레디크', '심플리쿡'],
    },
  },

};

export const tagData = ['GS25', 'GS THE FRESH', '랄라블라', 'GS Fresh Mall', '파르나스호텔', '우리동네 딜리버리', 'GS파크24',
  '유어스', '브레디크', '심플리쿡'];
