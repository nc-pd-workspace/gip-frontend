import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),

  statistics: asyncApiState.initial([]),
  lineAndColumn: asyncApiState.initial([]),
  barNegative: asyncApiState.initial([]),
  radarMultiple: asyncApiState.initial([]),
  columnDistributed: asyncApiState.initial([]),
  treemapDistributed: asyncApiState.initial([]),
  stackedColumnCount: asyncApiState.initial([]),
  stackedColumnAmount: asyncApiState.initial([]),
  barCategoryL: asyncApiState.initial([]),
  barCategoryM: asyncApiState.initial([]),
  brandGraph: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'customer/allActivityCustomer',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));
      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },

    // 전체 활동고객 분석 검색
    getAllActivityCustomerStatistics: (state, { payload }) => {
      state.statistics = asyncApiState.request();
    },
    getAllActivityCustomerStatisticsSuccess: (state, { payload }) => {
      const data = {
        new_cust_count: payload.data.newActiveCustCount,
        new_cust_count_compared_year: payload.data.newActiveCustCountComparedYear,
        active_cust_count: payload.data.totalActiveCustCount,
        active_cust_count_compared_year: payload.data.totalActiveCustCountComparedYear,

      };
      const result = { ...payload, data: data || {} };
      state.statistics = asyncApiState.success(result);
    },
    getAllActivityCustomerStatisticsFailure: (state, { payload }) => {
      state.statistics = asyncApiState.error(payload);
    },
    // 메인 그래프
    getallActivityCustomerLineAndColumn: (state, { payload }) => {
      state.lineAndColumn = asyncApiState.request();
    },
    getallActivityCustomerLineAndColumnSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.lineAndColumn = asyncApiState.success(result);
    },
    getallActivityCustomerLineAndColumnFailure: (state, { payload }) => {
      state.lineAndColumn = asyncApiState.error(payload);
    },
    // 성별/연령별 그래프
    getallActivityCustomerBarNegative: (state, { payload }) => {
      state.barNegative = asyncApiState.request();
    },
    getallActivityCustomerBarNegativeSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barNegative = asyncApiState.success(result);
    },
    getallActivityCustomerBarNegativeFailure: (state, { payload }) => {
      state.barNegative = asyncApiState.error(payload);
    },
    // 멤버십 등급 그래프
    getallActivityCustomerRadarMultiple: (state, { payload }) => {
      state.radarMultiple = asyncApiState.request();
    },
    getallActivityCustomerRadarMultipleSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.radarMultiple = asyncApiState.success(result);
    },
    getallActivityCustomerRadarMultipleFailure: (state, { payload }) => {
      state.radarMultiple = asyncApiState.error(payload);
    },
    // 지역(전국) 그래프
    getallActivityCustomerColumnDistributed: (state, { payload }) => {
      state.columnDistributed = asyncApiState.request();
    },
    getallActivityCustomerColumnDistributedSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.columnDistributed = asyncApiState.success(result);
    },
    getallActivityCustomerColumnDistributedFailure: (state, { payload }) => {
      state.columnDistributed = asyncApiState.error(payload);
    },
    // 지역(수도권) 그래프
    getallActivityCustomerTreemapDistributed: (state, { payload }) => {
      state.treemapDistributed = asyncApiState.request();
    },
    getallActivityCustomerTreemapDistributedSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.treemapDistributed = asyncApiState.success(result);
    },
    getallActivityCustomerTreemapDistributedFailure: (state, { payload }) => {
      state.treemapDistributed = asyncApiState.error(payload);
    },
    // 연간 구매 형태 > 연간 구매 횟수 그래프 API
    getallActivityCustomerStackedColumnCount: (state, { payload }) => {
      state.stackedColumnCount = asyncApiState.request();
    },
    getallActivityCustomerStackedColumnCountSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.stackedColumnCount = asyncApiState.success(result);
    },
    getallActivityCustomerStackedColumnCountFailure: (state, { payload }) => {
      state.stackedColumnCount = asyncApiState.error(payload);
    },
    // 연간 구매 형태 > 연간 구매 금액 그래프 API
    getallActivityCustomerStackedColumnAmount: (state, { payload }) => {
      state.stackedColumnAmount = asyncApiState.request();
    },
    getallActivityCustomerStackedColumnAmountSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.stackedColumnAmount = asyncApiState.success(result);
    },
    getallActivityCustomerStackedColumnAmountFailure: (state, { payload }) => {
      state.stackedColumnAmount = asyncApiState.error(payload);
    },
    // 카테고리/브랜드 구매 선호도 > 카테고리(대) 그래프 API
    getallActivityCustomerBarCategoryL: (state, { payload }) => {
      state.barCategoryL = asyncApiState.request();
    },
    getallActivityCustomerBarCategoryLSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barCategoryL = asyncApiState.success(result);
    },
    getallActivityCustomerBarCategoryLFailure: (state, { payload }) => {
      state.barCategoryL = asyncApiState.error(payload);
    },
    // 카테고리/브랜드 구매 선호도 > 카테고리(중) 그래프 API
    getallActivityCustomerBarCategoryM: (state, { payload }) => {
      state.barCategoryM = asyncApiState.request();
    },
    getallActivityCustomerBarCategoryMSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barCategoryM = asyncApiState.success(result);
    },
    getallActivityCustomerBarCategoryMFailure: (state, { payload }) => {
      state.barCategoryM = asyncApiState.error(payload);
    },
    // 브랜드 그래프
    getallActivityCustomerBrandGraph: (state, { payload }) => {
      state.brandGraph = asyncApiState.request();
    },
    getallActivityCustomerBrandGraphSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.brandGraph = asyncApiState.success(result);
    },
    getallActivityCustomerBrandGraphFailure: (state, { payload }) => {
      state.brandGraph = asyncApiState.error(payload);
    },
  },
});

export const { updateState, getCategoryOptions, getBrandOptions, getAllActivityCustomerStatistics, getallActivityCustomerLineAndColumn, getallActivityCustomerBarNegative, getallActivityCustomerRadarMultiple, getallActivityCustomerColumnDistributed, getallActivityCustomerTreemapDistributed, getallActivityCustomerStackedColumnCount, getallActivityCustomerStackedColumnAmount, getallActivityCustomerBarCategoryL, getallActivityCustomerBarCategoryM, getallActivityCustomerBrandGraph } = actions;

export default reducer;
