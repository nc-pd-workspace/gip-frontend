import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getCategoryOptions, getBrandOptions, getAllActivityCustomerStatistics, getallActivityCustomerLineAndColumn, getallActivityCustomerBarNegative, getallActivityCustomerRadarMultiple, getallActivityCustomerColumnDistributed, getallActivityCustomerTreemapDistributed, getallActivityCustomerStackedColumnCount, getallActivityCustomerStackedColumnAmount, getallActivityCustomerBarCategoryL, getallActivityCustomerBarCategoryM, getallActivityCustomerBrandGraph } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

const allActivityCustomerStatisticsSaga = createPromiseSaga(getAllActivityCustomerStatistics, API.AllActivityCustomer.allActivityCustomerStatistics);
const allActivityCustomerLineAndColumnSaga = createPromiseSaga(getallActivityCustomerLineAndColumn, API.AllActivityCustomer.allActivityCustomerLineAndColumn);
const allActivityCustomerBarNegativeSaga = createPromiseSaga(getallActivityCustomerBarNegative, API.AllActivityCustomer.allActivityCustomerBarNegative);
const allActivityCustomerRadarMultipleSaga = createPromiseSaga(getallActivityCustomerRadarMultiple, API.AllActivityCustomer.allActivityCustomerRadarMultiple);
const allActivityCustomerColumnDistributedSaga = createPromiseSaga(getallActivityCustomerColumnDistributed, API.AllActivityCustomer.allActivityCustomerColumnDistributed);
const allActivityCustomerTreemapDistributedSaga = createPromiseSaga(getallActivityCustomerTreemapDistributed, API.AllActivityCustomer.allActivityCustomerTreemapDistributed);
const allActivityCustomerStackedColumnCountSaga = createPromiseSaga(getallActivityCustomerStackedColumnCount, API.AllActivityCustomer.allActivityCustomerStackedColumnCount);
const allActivityCustomerStackedColumnAmountSaga = createPromiseSaga(getallActivityCustomerStackedColumnAmount, API.AllActivityCustomer.allActivityCustomerStackedColumnAmount);
const allActivityCustomerBarCategoryLSaga = createPromiseSaga(getallActivityCustomerBarCategoryL, API.AllActivityCustomer.allActivityCustomerBarCategoryL);
const allActivityCustomerBarCategoryMSaga = createPromiseSaga(getallActivityCustomerBarCategoryM, API.AllActivityCustomer.allActivityCustomerBarCategoryM);
const allActivityCustomerBrandGraphSaga = createPromiseSaga(getallActivityCustomerBrandGraph, API.AllActivityCustomer.allActivityCustomerBrandGraph);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);

  yield takeLatest(getAllActivityCustomerStatistics, allActivityCustomerStatisticsSaga);
  yield takeLatest(getallActivityCustomerLineAndColumn, allActivityCustomerLineAndColumnSaga);
  yield takeLatest(getallActivityCustomerBarNegative, allActivityCustomerBarNegativeSaga);
  yield takeLatest(getallActivityCustomerRadarMultiple, allActivityCustomerRadarMultipleSaga);
  yield takeLatest(getallActivityCustomerColumnDistributed, allActivityCustomerColumnDistributedSaga);
  yield takeLatest(getallActivityCustomerTreemapDistributed, allActivityCustomerTreemapDistributedSaga);
  yield takeLatest(getallActivityCustomerStackedColumnCount, allActivityCustomerStackedColumnCountSaga);
  yield takeLatest(getallActivityCustomerStackedColumnAmount, allActivityCustomerStackedColumnAmountSaga);
  yield takeLatest(getallActivityCustomerBarCategoryL, allActivityCustomerBarCategoryLSaga);
  yield takeLatest(getallActivityCustomerBarCategoryM, allActivityCustomerBarCategoryMSaga);
  yield takeLatest(getallActivityCustomerBrandGraph, allActivityCustomerBrandGraphSaga);
}

/**
 * slice.js에서 생성된 type를 가지고
 * (type[string], api)가  정의되어 있는 상태에서
 * saga에 지켜보고 있다가 type에 맞는 생성함수를 찾아서 실행
 * //

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
