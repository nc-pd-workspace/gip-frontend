import React, { useState, useEffect, useRef, useCallback } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';
import { Radio } from 'antd';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';

import Search from '../../../../components/search';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import MultipleRow from '../../../../components/search/MultipleRow';
import Paper from '../../../../components/paper';
import TabSelectChart from '../components/TabSelectChart';
import {
  getCategoryOptions,
  getBrandOptions,
  getAllActivityCustomerStatistics,
  getallActivityCustomerLineAndColumn,
  getallActivityCustomerBarNegative,
  getallActivityCustomerRadarMultiple,
  getallActivityCustomerColumnDistributed,
  getallActivityCustomerTreemapDistributed,
  getallActivityCustomerStackedColumnCount,
  getallActivityCustomerStackedColumnAmount,
  getallActivityCustomerBarCategoryL,
  getallActivityCustomerBarCategoryM,
  getallActivityCustomerBrandGraph,
} from '../redux/slice';
import { AlertTabHeadTitle } from '../constants';
import { alertMessage } from '../../../../components/message';

import TabCharts from '../components/TabCharts';

import { getCustomerSelects } from '../../../../redux/commonReducer';
import CategorySelects from '../../../../components/search/CategorySelects';
import { ButtonExcel } from '../../../../components/button';
import { excelDownload } from '../../../../utils/utils';
// 전체 활동고객 분석 Container
function AllActivityCustomerContainer({ query }) {
  const searchRef = useRef();
  const [brand, setBrand] = useState(null);

  const [radioType, setRadioType] = useState('avg');
  const [customerType, setCustomerType] = useState('total');
  const [queryActiveKey, setQueryActiveKey] = useState('');
  const [search, setSearch] = useState({});

  const [detailOnOff, setDetailOnOff] = useState('on');

  const [categoryMDisabled, setCategoryMDisabled] = useState(true);
  const [customerTabVisible, setCustomerTabVisible] = useState('');
  const [buttonExcelDisabled, setButtonExcelDisabled] = useState(false);
  const [searchParams, setSearchParams] = useState({});

  const { categoryOption, brandOption, statisticsData, lineAndColumn, barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph } = useSelector((state) => ({
    categoryOption: state.customer.allActivityCustomer.categoryOption,
    brandOption: state.customer.allActivityCustomer.brandOption,

    statisticsData: state.customer.allActivityCustomer.statistics,
    lineAndColumn: state.customer.allActivityCustomer.lineAndColumn,
    barNegative: state.customer.allActivityCustomer.barNegative,
    radarMultiple: state.customer.allActivityCustomer.radarMultiple,
    columnDistributed: state.customer.allActivityCustomer.columnDistributed,
    treemapDistributed: state.customer.allActivityCustomer.treemapDistributed,
    stackedColumnCount: state.customer.allActivityCustomer.stackedColumnCount,
    stackedColumnAmount: state.customer.allActivityCustomer.stackedColumnAmount,
    barCategoryL: state.customer.allActivityCustomer.barCategoryL,
    barCategoryM: state.customer.allActivityCustomer.barCategoryM,
    brandGraph: state.customer.allActivityCustomer.brandGraph,

  }));

  const { selectPtnIdx, customerSelects, userInfo } = useSelector((state) => state.common);

  const dispatch = useDispatch();

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    dispatch(getBrandOptions({ params }));
    dispatch(getCustomerSelects({ params }));
  }, []);

  useEffect(() => {
    if (query && Object.keys(query).length) {
      setSearch({ ...query });
      searchRef.current.setValue({ ...query });
      // 카드ui 전체활동고객, 하단 탭: 카테고리.브랜드 주문 선호도
      setCustomerType(query.cardType);
      setQueryActiveKey(query.activeKey);
    }
  }, [query]);

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  useEffect(() => {
    if (search?.month) {
      const params = {
        ptnIdx: selectPtnIdx,
        searchMonth: search?.month ? search.month.replace('.', '') : '',
        categoryLCode: search?.catData?.[0] ? search.catData[0] : '',
        categoryMCode: search?.catData?.[1] ? search.catData[1] : '',
        brandCode: search.brand ? search.brand.toString() : '',
        genderCode: search.gender ? search.gender : '',
        custGradeCode: search.membership ? search.membership : '',
        sidoCode: search.location ? search.location : '',
        age5UnitCode: search.age ? search.age.toString() : '',
        dataType: radioType,
        cardType: customerType,
      };

      // 카테고리(중)탭 유무
      // if (search?.catData?.[1]) {
      //   setCategoryMDisabled(true);
      // } else {
      //   setCategoryMDisabled(false);
      // }
      // 고객 특성 탭 유무
      let searchAge = false;
      if (search?.age === undefined) {
        searchAge = true;
      } else if (search?.age.length === customerSelects.data.ageUnit.length) {
        searchAge = true;
      }
      if (search?.gender === undefined && search?.membership === undefined && search?.location === undefined && searchAge) {
        setCustomerTabVisible(true);
      } else {
        setCustomerTabVisible(false);
      }

      dispatch(getAllActivityCustomerStatistics({ params }));
      dispatch(getallActivityCustomerLineAndColumn({ params }));
      // sub chart
      dispatch(getallActivityCustomerBarNegative({ params }));
      dispatch(getallActivityCustomerRadarMultiple({ params }));
      dispatch(getallActivityCustomerColumnDistributed({ params }));
      dispatch(getallActivityCustomerTreemapDistributed({ params }));
      dispatch(getallActivityCustomerStackedColumnCount({ params }));
      dispatch(getallActivityCustomerStackedColumnAmount({ params }));
      dispatch(getallActivityCustomerBarCategoryL({ params }));
      dispatch(getallActivityCustomerBarCategoryM({ params }));
      dispatch(getallActivityCustomerBrandGraph({ params }));
      setSearchParams(params);
    }
  }, [search, radioType, customerType]);

  useEffect(() => {
    searchRef.current.clickSearch();
  }, []);

  const RadioOnchange = ({ target }) => {
    setRadioType(target.value);
  };
  const onChangeSelectTab = useCallback((key) => {
    if (isLoadingSubChart()) {
      alertMessage(`${AlertTabHeadTitle[customerType]} 데이터 조회 중입니다. 잠시만 기다려 주세요.`);
      return;
    }
    setCustomerType(key);
  }, [customerType, barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph]);

  const isLoadingSubChart = () => {
    if (isCustomerTabVisible()) {
      const arr = [barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount,
        stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
      return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
    }
    const arr = [stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
    return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
  };
  const isCustomerTabVisible = () => {
    if (search.genderCode || search.custGradeCode || search.sidoCode || search.age5UnitCode) return false;
    return true;
  };

  const excelButtonOnClick = useCallback(() => {
    setButtonExcelDisabled(true);
    excelDownload('/api/statistics/customer/totalActivity/excel', userInfo.accessToken, '전체 활동 고객 분석', searchParams, 'month')
      .then((result) => {
        if (result === 'success') {
          setButtonExcelDisabled(false);
        } else {
          setButtonExcelDisabled(false);
        }
      });
  }, [searchParams]);

  return (
    <Container>
      <PageHeader
        title="전체 활동고객 분석"
        subTitle="내 브랜드의 전체 활동고객과 특성을 확인할 수 있습니다."
      />
      <Search
        setSearch={setSearch}
        detailOnOff={detailOnOff}
        setDetailOnOff={setDetailOnOff}
        ref={searchRef}
        onReset={onResetSearch}
      >
        <SingleInputItem
          type="DatePicker"
          column="month"
          title="조회기간"
          width="50%"
          picker="month"
          placeholder="전체"
        />
        <SingleInputItem
          type="Select"
          column="brand"
          title="브랜드"
          width="50%"
          loading={brandOption.status === 'pending'}
          options={brandOption.data}
          onChange={onChangeBrand}
          placeholder="전체"
        />
        <CategorySelects
          column="catData"
          title="카테고리"
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />
        <MultipleRow isDetail detailOnOff={detailOnOff}>
          <SingleInputItem
            type="Select"
            column="gender"
            title="성별"
            options={customerSelects.data.gender}
            placeholder="전체"
          />
          <SingleInputItem
            type="Select"
            column="membership"
            title="고객 등급"
            options={customerSelects.data.custGrade}
            placeholder="전체"
          />
          <SingleInputItem
            type="Select"
            column="location"
            title="지역"
            options={customerSelects.data.sido}
            placeholder="전국"
          />
        </MultipleRow>
        <SingleInputItem
          type="MultiSelect"
          column="age"
          title="연령대"
          width="50%"
          options={customerSelects.data.ageUnit}
          isDetail
          detailOnOff={detailOnOff}
        />
      </Search>
      <SearchResult>
        <Title>
          <p>활동고객 분석 결과</p>
          {statisticsData.status === 'success' && statisticsData.data
          && (
            <div className="searchResultToolBox">
              <span className="caption-text">데이터 기준</span>
              <div className="right-item set-section-toggle">
                <Radio.Group
                  defaultValue={radioType}
                  buttonStyle="solid"
                  onChange={RadioOnchange}
                >
                  <Radio.Button value="avg">평균</Radio.Button>
                  <Radio.Button value="sum">합계</Radio.Button>
                </Radio.Group>
              </div>
              <ButtonExcel disabled={buttonExcelDisabled} onClick={excelButtonOnClick} />
            </div>
          ) }
        </Title>
        <div className="loadingDiv">
          <TabCharts
            setSelectedTab={onChangeSelectTab}
            customerType={customerType}
            setCustomerType={setCustomerType}
            statisticsData={statisticsData}
            lineAndColumn={lineAndColumn}
          />
        </div>
        <div className="loadingDiv">
          <TabSelectChart
            barNegative={barNegative}
            radarMultiple={radarMultiple}
            columnDistributed={columnDistributed}
            treemapDistributed={treemapDistributed}
            stackedColumnCount={stackedColumnCount}
            stackedColumnAmount={stackedColumnAmount}
            barCategoryL={barCategoryL}
            barCategoryM={barCategoryM}
            brandGraph={brandGraph}
            categoryMDisabled={categoryMDisabled}
            customerTabVisible={customerTabVisible}
            queryActiveKey={queryActiveKey}
          />
        </div>
        {/* <Descripition
          text={[
            '조회월 기준에 해당하는 고객 분석 데이터를 제공합니다.',
            '* 표시된 데이터는 실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
          ]}
          padding="0"
        /> */}
      </SearchResult>
    </Container>
  );
}

const Container = styled(PageLayout)`
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
`;
const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px 20px;

`;
const Title = styled.div`
  padding: 20px 0 24px;
  color: var(--color-gray-900);
  font-weight: 700;
  font-size: 20px;
`;

export default AllActivityCustomerContainer;
