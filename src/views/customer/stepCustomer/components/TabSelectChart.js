import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import ReactApexChart from 'react-apexcharts';
import TagCloud from 'react-tag-cloud';
import moment from 'moment';

import Table from '../../../../components/table';

import { funnelTableColumns } from '../constants';
import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import {
  TAGCLOUD_VARIATION,
  BLUE_VARIATION,
  GREEN_VARIATION,
  CHART_COLORS,
  TAGCLOUD_VARIATION_BLUE,
} from '../../../../styles/chartColors';
import { ChartContainer } from '../../../../styles/ChartContainer';
import {
  optionsBarCategory,
  optionsBarNegative,
  optionsColumnDistributed,
  optionsRadarMultiple,
  optionsStackedColumn,
  optionsTreemapDistributed,
  optionsDonutGraph,
} from '../../../../utils/chartOptions';

function TabSelectChart({
  barNegative,
  radarMultiple,
  columnDistributed,
  treemapDistributed,
  stackedColumnCount,
  stackedColumnAmount,
  barCategoryL,
  barCategoryM,
  brandGraph,
  donutGraph,
  customerTabVisible,
  queryActiveKey,
}) {
  // 브랜드 tagcloud
  const [brandGraphData, setBrandGraph] = useState('');
  useEffect(() => {
    if (brandGraph?.data?.length > 0) {
      setBrandGraph(brandGraph?.data);
    } else {
      setBrandGraph('');
    }
  }, [brandGraph]);

  const { TabPane } = Tabs;
  // const [descripText, setDescripText] = useState(['* 표시된 데이터는 실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다. ']);
  const [activeKey, setActiveKey] = useState('');

  function tapClickCallback(key) {
    setActiveKey(key);
    // let text;
    // switch (key) {
    // case '1':
    //   text = '';
    //   break;
    // case '2':
    //   text = '';
    //   break;
    // case '3':
    //   text = ['설정한 조회기간 기준으로 최근 1년간의 고객 분석 데이터를 제공합니다.'];
    //   break;
    // default:
    //   text = ['* 표시된 데이터는 실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.'];
    // }
    // setDescripText(text);
  }

  // 성별/연령별
  const [barNegativeData, setBarNegativeData] = useState('');
  useEffect(() => {
    if (queryActiveKey) {
      setActiveKey(queryActiveKey);
    } else {
      setActiveKey('1');
    }

    if (barNegative?.data?.categories?.length > 0) {
      const arr = [...barNegative.data.series[0].data];
      const maxValue = arr.reduce((previousItem, currentItem, index, array) => {
        let max = 0;
        barNegative.data.series.forEach((item) => {
          if (item?.data?.length >= index) {
            max += item.data[index];
          }
        });
        if (previousItem < max) {
          return max;
        }
        return previousItem;
      }, 0);

      const maxCeil = Math.ceil(maxValue / 10) * 10;
      const maxData = maxCeil + 10;

      const changeArr = barNegative.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const barNegativeChartData = {
        series: changeArr,
        options: {
          ...optionsBarNegative,
          xaxis: {
            categories: barNegative.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              show: true,
              formatter(val, index) {
                if (index === 0) return 0;
                return `${val}%`;
              },
            },
          },
        },
      };
      setBarNegativeData(barNegativeChartData);
    } else {
      setBarNegativeData('');
    }
  }, [barNegative]);
  // 고객 등급
  const [radarMultipleData, setRadarMultipleData] = useState('');
  useEffect(() => {
    if (radarMultiple?.data?.categories?.length > 0) {
      const changeArr = radarMultiple.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const radarMultipleChartData = {
        series: changeArr,
        options: {
          ...optionsRadarMultiple,
          xaxis: {
            categories: radarMultiple.data.categories,
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              style: {
                colors: 'var(--color-gray-700)',
              },
              formatter(val, index) {
                if (index === 5) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
        },
      };
      setRadarMultipleData(radarMultipleChartData);
    } else {
      setRadarMultipleData('');
    }
  }, [radarMultiple]);
  // 지역(전국)
  const [columnDistributedData, setColumnDistributedData] = useState('');
  useEffect(() => {
    if (columnDistributed?.data?.categories?.length) {
      const columnDistributedChartData = {
        series: columnDistributed.data.series.map(({ data }) => ({
          name: '',
          data,
        })),
        options: {
          ...optionsColumnDistributed,
          xaxis: {
            categories: columnDistributed.data.categories,
            labels: {
              hideOverlappingLabels: false,
              style: {
                fontSize: '11px',
                cssClass: 'apexcharts-xaxis-label-overlapping',
              },
              offsetY: -5,
            },
          },
          yaxis: {
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
        },
      };
      setColumnDistributedData(columnDistributedChartData);
    } else {
      setColumnDistributedData('');
    }
  }, [columnDistributed]);
  // 지역(수도권)
  const [treemapDistributedData, setTreemapDistributedData] = useState('');
  useEffect(() => {
    if (treemapDistributed?.data?.data) {
      const treemapDistributedChartData = {
        series: [{ data: treemapDistributed.data.data }],
        options: {
          ...optionsTreemapDistributed,
        },
      };
      setTreemapDistributedData(treemapDistributedChartData);
    } else {
      setTreemapDistributedData('');
    }
  }, [treemapDistributed]);
  // 연간 구매횟수
  const [stackedColumnCountData, setStackedColumnCountData] = useState('');
  useEffect(() => {
    if (stackedColumnCount?.data?.categories?.length > 0) {
      const changeArr = stackedColumnCount.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const stackedColumnCountChartData = {
        series: changeArr,
        options: {
          ...optionsStackedColumn,
          colors: BLUE_VARIATION,
          xaxis: {
            categories: stackedColumnCount.data.categories,
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            labels: {
              formatter(val, index) {
                return `${Math.abs(Math.round(val))}${Math.abs(Math.round(val)) === 0 ? '' : '%'}`;
              },
            },
          },
          tooltip: {
            ...optionsStackedColumn.tooltip,
            x: {
              formatter(val, index) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M.')}`;
              },
            },
          },
          dataLabels: {
            style: {
              colors: [
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
              ],
            },
            formatter(val) {
              return `${Math.abs(Math.round(val))}`;
            },
          },
        },
      };
      setStackedColumnCountData(stackedColumnCountChartData);
    } else {
      setStackedColumnCountData('');
    }
  }, [stackedColumnCount]);
  // 연간 구매 금액
  const [stackedColumnAmountData, setStackedColumnAmountData] = useState('');
  useEffect(() => {
    if (stackedColumnAmount?.data?.categories?.length > 0) {
      const changeArr = stackedColumnAmount.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const stackedColumnAmountChartData = {
        series: changeArr,
        options: {
          ...optionsStackedColumn,
          colors: GREEN_VARIATION,
          xaxis: {
            categories: stackedColumnAmount.data.categories,
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            labels: {
              formatter(val, index) {
                return `${Math.abs(Math.round(val))}${Math.abs(Math.round(val)) === 0 ? '' : '%'}`;
              },
            },
          },
          dataLabels: {
            style: {
              colors: [
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_BLACK,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
                CHART_COLORS.TEXT_WHITE,
              ],
            },
            formatter(val) {
              return `${Math.abs(Math.round(val))}`;
            },
          },
          tooltip: {
            ...optionsStackedColumn.tooltip,
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M.')}`;
              },
            },
          },
        },
      };
      setStackedColumnAmountData(stackedColumnAmountChartData);
    } else {
      setStackedColumnAmountData('');
    }
  }, [stackedColumnAmount]);
  // 카테고리(대)
  const [barCategoryLData, setBarCategoryLData] = useState('');

  useEffect(() => {
    if (barCategoryL?.data?.categories?.length > 0) {
      const maxMap = barCategoryL.data.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxData = getChartYAxisMax(maxValue);

      const barCategoryLChartData = {
        series: barCategoryL.data.series.map(({ data }) => ({
          name: '',
          data,
        })),
        options: {
          ...optionsBarCategory,
          xaxis: {
            categories: barCategoryL.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              formatter(val) {
                return getChartValuePeople(val);
              },
            },
          },
        },
      };
      setBarCategoryLData(barCategoryLChartData);
    } else {
      setBarCategoryLData('');
    }
  }, [barCategoryL]);
  // 카테고리(중)
  const [barCategoryMData, setBarCategoryMData] = useState('');
  useEffect(() => {
    if (barCategoryM?.data?.categories?.length > 0) {
      const maxMap = barCategoryM.data.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxData = getChartYAxisMax(maxValue);
      const barCategoryMChartData = {
        series: barCategoryM.data.series.map(({ data }) => ({
          name: '',
          data,
        })),
        options: {
          ...optionsBarCategory,
          xaxis: {
            min: 0,
            max: maxData,
            tickAmount: 5,
            categories: barCategoryM.data.categories,
            labels: {
              formatter(val) {
                return getChartValuePeople(val);
              },
            },
          },
        },
      };
      setBarCategoryMData(barCategoryMChartData);
    } else {
      setBarCategoryMData('');
    }
  }, [barCategoryM]);
  // 유입경로
  const [donutGraphData, setDonutGraphData] = useState('');
  const [donutTableData, setDonutTableData] = useState('');
  useEffect(() => {
    if (donutGraph?.data?.labels?.length) {
      const donutGraphChartData = {
        series: donutGraph.data.series,
        options: {
          ...optionsDonutGraph,
          labels: donutGraph.data.labels,
          tooltip: {
            y: {
              formatter(val) {
                return `${val.toLocaleString()}명`;
              },
            },
          },
        },
      };

      const donutGraphTableData = donutGraph.data.data.map((v, idx) => ({
        key: idx,
        funnel: v.funnel,
        // numberCustomers: getChartValuePeople(v.numberCustomers),
        numberCustomers: `${v.numberCustomers.toLocaleString()}명`,
        proportion: `${v.proportion}%`,
        salesRate: `${v.salesRate}%`,
      }));
      let sumNumberCustomers = 0;
      let sumProportion = 0;
      let sumSalesRate = 0;
      donutGraph.data.data.forEach((v) => {
        sumNumberCustomers += v.numberCustomers;
        sumProportion += v.proportion;
        sumSalesRate += v.salesRate;
      });
      const sumData = {
        key: 'total',
        funnel: '합계',
        numberCustomers: `${sumNumberCustomers.toLocaleString()}명`,
        proportion: `${(sumProportion <= 100.01 && sumProportion >= 99.99) ? Math.floor(sumProportion.toFixed(1)) : sumProportion.toFixed(2)}%`,
        salesRate: `${(sumSalesRate <= 100.01 && sumSalesRate >= 99.99) ? Math.floor(sumSalesRate.toFixed(1)) : sumSalesRate.toFixed(2)}%`,
      };
      donutGraphTableData.push(sumData);
      setDonutGraphData(donutGraphChartData);
      setDonutTableData(donutGraphTableData);
    } else {
      setDonutGraphData('');
      setDonutTableData('');
    }
  }, [donutGraph]);

  return (
    <Container>
      <Tabs
        activeKey={activeKey}
        onChange={(v) => {
          tapClickCallback(v);
        }}
        className="subTabs"
      >
        <TabPane tab="카테고리/브랜드 주문 선호도" key="1">
          <div className="chart-name-wrap">
            <p className="chart-name">
              내 브랜드의 고객이 GS SHOP에서 선호하는 카테고리와 브랜드를
              확인해보세요.
            </p>
          </div>
          <SubContainer>
            <Tabs defaultActiveKey="01" className="categoryTabs">
              <TabPane tab="카테고리(대)" key="1">
                <ChartContainerBox>
                  <div className="chart-area loadingDiv heightAuto">
                    <LoadingComponent
                      isLoading={barCategoryL?.status === 'pending'}
                      background="#f7f8fa"
                    />
                    {barCategoryLData ? (
                      <ReactApexChart
                        options={barCategoryLData.options}
                        series={barCategoryLData.series}
                        type="bar"
                        height="400"
                      />
                    ) : (
                      <EmptyGraph />
                    )}
                  </div>
                </ChartContainerBox>
              </TabPane>
              <TabPane tab="카테고리(중)" key="2">
                <ChartContainerBox>
                  <div className="chart-area loadingDiv heightAuto">
                    <LoadingComponent
                      isLoading={barCategoryM?.status === 'pending'}
                      background="#f7f8fa"
                    />
                    {barCategoryMData ? (
                      <ReactApexChart
                        options={barCategoryMData.options}
                        series={barCategoryMData.series}
                        type="bar"
                        height="400"
                      />
                    ) : (
                      <EmptyGraph />
                    )}
                  </div>
                </ChartContainerBox>
              </TabPane>
              <TabPane tab="브랜드" key="3">
                <ChartContainerBox>
                  <div className="tag-area">
                    <div className="app-outer">
                      <div className="app-inner">
                        <LoadingComponent
                          isLoading={brandGraph?.status === 'pending'}
                          background="#f7f8fa"
                        />
                        {brandGraph?.status === 'success' && brandGraphData ? (

                          <TagCloud
                            className="tag-cloud"
                            style={{
                              fontSize: 20,
                              padding: 8,
                              fontFamily: 'Pretendard',
                              color: () => TAGCLOUD_VARIATION[
                                Math.floor(
                                  Math.random() * TAGCLOUD_VARIATION.length,
                                )
                              ],
                            }}
                          >
                            {brandGraphData
                              && brandGraphData.map((val, idx) => (
                                <div key={`tag${idx}`} style={{ color: TAGCLOUD_VARIATION_BLUE[idx] }}>{val}</div>
                              ))}
                          </TagCloud>
                        ) : (
                          <EmptyGraph />
                        )}
                      </div>
                    </div>
                  </div>
                </ChartContainerBox>
              </TabPane>
            </Tabs>
          </SubContainer>
        </TabPane>
        {customerTabVisible && (
          <TabPane tab="고객 특성" key="2">
            <div className="chart-name-wrap">
              <p className="chart-name">
                내 브랜드 고객의 특성을 확인해 보세요.
              </p>
            </div>
            <ChartContainerBox>
              <div className="chart-area loadingDiv apexcharts-legend-first-none apexcharts-tooltip-first-none">
                <p className="chart-title">성별/연령별</p>
                <LoadingComponent
                  isLoading={barNegative?.status === 'pending'}
                  background="#f7f8fa"
                />
                {barNegativeData ? (
                  <ReactApexChart
                    options={barNegativeData.options}
                    series={barNegativeData.series}
                    type="bar"
                    height="294"
                  />
                ) : (
                  <EmptyGraph />
                )}
              </div>
              <div className="chart-area loadingDiv padding20 apexcharts-legend-first-none ">
                <p className="chart-title">고객 등급</p>
                <LoadingComponent
                  isLoading={radarMultiple?.status === 'pending'}
                  background="#f7f8fa"
                />
                {radarMultipleData ? (
                  <ReactApexChart
                    options={radarMultipleData.options}
                    series={radarMultipleData.series}
                    type="radar"
                    height="314"
                  />
                ) : (
                  <EmptyGraph />
                )}
              </div>
            </ChartContainerBox>
            <ChartContainerBox>
              <div className="chart-area loadingDiv">
                <p className="chart-title">지역(전국)</p>
                <LoadingComponent
                  isLoading={columnDistributed?.status === 'pending'}
                  background="#f7f8fa"
                />
                {columnDistributedData ? (
                  <ReactApexChart
                    options={columnDistributedData.options}
                    series={columnDistributedData.series}
                    type="bar"
                    height="294"
                  />
                ) : (
                  <EmptyGraph />
                )}
              </div>
              <div className="chart-area loadingDiv padding20">
                <p className="chart-title">지역(수도권)</p>
                <LoadingComponent
                  isLoading={treemapDistributed?.status === 'pending'}
                  background="#f7f8fa"
                />
                {treemapDistributedData ? (
                  <ReactApexChart
                    options={treemapDistributedData.options}
                    series={treemapDistributedData.series}
                    type="treemap"
                    height="294"
                  />
                ) : (
                  <EmptyGraph />
                )}
              </div>
            </ChartContainerBox>
          </TabPane>
        )}
        <TabPane tab="연간 주문 형태" key="3">
          <div className="chart-name-wrap">
            <p className="chart-name">
              GS SHOP에서 내 브랜드 고객이 소비하는 연간 주문형태를
              확인해보세요.
            </p>
          </div>
          <ChartContainerBox>
            <div className="chart-area loadingDiv heightAuto apexcharts-legend-first-none apexcharts-tooltip-first-none">
              <p className="chart-title">연간 주문횟수</p>
              <LoadingComponent
                isLoading={stackedColumnCount?.status === 'pending'}
                background="#f7f8fa"
              />
              {stackedColumnCountData ? (
                <ReactApexChart
                  options={stackedColumnCountData.options}
                  series={stackedColumnCountData.series}
                  type="bar"
                  height="400"
                />
              ) : (
                <EmptyGraph />
              )}
            </div>
            <div className="chart-area loadingDiv heightAuto apexcharts-legend-first-none  apexcharts-tooltip-first-none">
              <p className="chart-title">연간 주문금액</p>
              <LoadingComponent
                isLoading={stackedColumnAmount?.status === 'pending'}
                background="#f7f8fa"
              />
              {stackedColumnAmountData ? (
                <ReactApexChart
                  options={stackedColumnAmountData.options}
                  series={stackedColumnAmountData.series}
                  type="bar"
                  height="400"
                />
              ) : (
                <EmptyGraph />
              )}
            </div>
          </ChartContainerBox>
        </TabPane>
        <TabPane tab="유입경로" key="4">
          <div className="chart-name-wrap">
            <p className="chart-name">
              내 브랜드로 유입된 고객수와 전환율을 확인해보세요.
            </p>
          </div>
          <SubContainer>
            <LoadingComponent isLoading={donutGraph?.status === 'pending'} />
            {donutGraphData ? (
              <FunnelContainer>
                <FunnelChart>
                  <ReactApexChart
                    options={donutGraphData.options}
                    series={donutGraphData.series}
                    type="donut"
                    height="364"
                  />
                </FunnelChart>
                <FunnelTable>
                  <Table
                    dataSource={donutTableData}
                    columns={funnelTableColumns}
                  />
                </FunnelTable>
              </FunnelContainer>
            ) : (
              <EmptyGraph />
            )}
          </SubContainer>
        </TabPane>
      </Tabs>
      {/* {descripText
      && (
        <Descripition
          text={descripText}
          color="#8F959D"
        />
      )} */}
    </Container>
  );
}

const SubContainer = styled.div`
  border-radius: 4px;
  background-color: #f7f8fa;
  margin-top: 10px;
  .ant-tabs-nav {
    align-items: center;
    &.ant-tabs-nav-wrap {
      flex: 0;
      width: auto;
    }
  }

  .app-outer {
    align-items: center; 
    bottom: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    left: 0;
    padding: 20px 0;
    position: absolute;
    right: 0;
    top: 0;
  }

  .app-inner {
    display: flex;
    flex-direction: column;
    height: 100%;
    max-width: 1000px;
    width: 100%;
  }

  .tag-cloud {
    flex: 1;
  }

  .tag-cloud > div {
    transition: 1s;
  }
`;
const ChartContainerBox = styled(ChartContainer)`
`;

const Container = styled.div`
  margin-bottom: 10px;
  .subTabs > .ant-tabs-nav {
    margin-top: 10px;
    margin-right: 40px;
    margin-left: 40px;
    margin-bottom: 30px;
    .ant-tabs-nav-wrap {
      flex: 1;
      .ant-tabs-nav-list {
        border-radius: 4px;
        flex:1;
        background-color: #f7f8fa;
      }
      .ant-tabs-tab-btn {
        padding: 0 12px;
      }
    }
    .ant-tabs-tab{
      display: flex !important;
      padding: 0px;
      height: 40px;
      line-height: 40px;
      margin: 0;
      justify-content: center;
      flex: 1;
      font-size: 13px;
    }
    .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
      border-radius: 4px;
      background-color: var(--color-steelGray-800);
      color: var(--color-white);
    }
  }
  .ant-tabs-nav:before {
      content: none !important;
  }
  .ant-tabs-ink-bar{
      display: none;
  }
  .ant-tabs-tab-btn{
      width: 100%;
      height: 100%;
      text-align: center;
  }
  .ant-tabs{
    margin-bottom: 20px;
  }
  .chart-name{
    padding-bottom: 10px;
  }
  .categoryTabs {
    padding-top:20px;
    .ant-tabs-nav-operations {
      display: none;
    }
    > .ant-tabs-nav{
      justify-content: center;
      margin-bottom: 0;
      .ant-tabs-nav-wrap {
        justify-content: center;
      }
      .ant-tabs-tab {
        padding: 0;
      }
      .ant-tabs-tab-btn {
        background-color: var(--color-white);
        border-radius: 0;
        height:32px;
        line-height: 32px;
        width: 88px;
        font-size: 13px;
        border:var(--border-default);
        border-left: 0 none;
      }
      .ant-tabs-tab:first-child .ant-tabs-tab-btn {
        border-radius:4px 0 0 4px;
        border-left:var(--border-default);
      }
      .ant-tabs-tab:nth-last-child(2) .ant-tabs-tab-btn {
        border-radius:0 4px 4px 0;;
      }
      .ant-tabs-tab+.ant-tabs-tab {
        margin: 0;
      }
      .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
        border: 0 none;
        background-color: var(--color-steelGray-800);
        color: var(--color-white);
      }
    }
  }
`;

const FunnelContainer = styled.div`
  display: flex;
  padding: 20px;
`;
const FunnelChart = styled.div`
  flex: 1 1 50%;
  align-items: center;
  display: flex;
  height: 364px;
  > div {
    flex: 1 1 100%;
  }
`;
const FunnelTable = styled.div`
  flex:1 1 50%;
  .ant-table table > tbody > tr:last-child > td {
    font-weight: 600;
  }
`;
export default TabSelectChart;
