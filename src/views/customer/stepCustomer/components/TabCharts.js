import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import moment from 'moment';

import ReactApexChart from 'react-apexcharts';

import { Tabs, TabPane } from '../../../../components/tabs';

import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';
import ToolTip from '../../../../components/toolTip';
import { TabHeadTitle } from '../constants';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import { CHART_COLORS } from '../../../../styles/chartColors';
import PeopleView from '../../../../components/PeopleView';

// const TabHeadTitle = ['전체 활동고객수', '전체 방문고객수', '전체 관심고객수', '전체 주문고객수', '전체 충성고객수'];
const TooltipText = [
  ( // 전체 활동고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에서 활동(방문, 관심, 주문, 충성)한 고객수 입니다.
    </>
  ),
  ( // 전체 방문고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에 방문한 이력이 있는 고객 데이터 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 전체 활동고객수 -전월  전체 활동고객수)/전월 전체 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 -전년도  전체 활동고객수)/전년도 전체 활동고객수 * 100%
    </>
  ),
  ( // 전체 관심고객수
    <>
      조회기간 동안 최근 6개월간  장바구니 담기, 찜하기 등 관심 행동 이력이 있는 고객 데이터 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 전체 활동고객수 -전월  전체 활동고객수)/전월 전체 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 -전년도  전체 활동고객수)/전년도 전체 활동고객수 * 100%
    </>
  ),
  ( // 전체 주문고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품을  1회 주문 완료한 이력이 있는 고객수 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 전체 활동고객수 -전월  전체 활동고객수)/전월 전체 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 -전년도  전체 활동고객수)/전년도 전체 활동고객수 * 100%
    </>
  ),
  ( // 전체 충성 고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품을 2회 이상 주문완료한 고객수 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 전체 활동고객수 -전월  전체 활동고객수)/전월 전체 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 -전년도  전체 활동고객수)/전년도 전체 활동고객수 * 100%
    </>
  ),
];

function TabCharts({ setSelectedTab, customerType, statisticsData, lineAndColumn, chartNmTitle, dateType }) {
  const tabHead = (idx, total, prevMonth = '', prevYear = '') => (
    <>
      <Title>
        {TabHeadTitle[idx]}
        <ToolTip text={TooltipText[idx]} position={(idx >= 2) ? 'right' : false} />
      </Title>
      <Total>
        {total ? total.toLocaleString('ko-KR') : 0}
        <div className="unit">명</div>
      </Total>
      <ChangeRate>
        <div className="item">
          {(idx !== 0)
          && (
            <DataWrap>
              {/* <li>
                <DataTitle>전일</DataTitle>
                <DataNumber number={prevDay}>
                  {prevDay}
                  명
                </DataNumber>
              </li> */}
              <li>
                <DataTitle>전월</DataTitle>
                <PeopleView data={prevMonth} />

                {/* <DataNumber number={prevMonth}>
                  {(prevMonth > 0 ? `+${prevMonth}` : prevMonth) || 0 }
                  명
                </DataNumber> */}
              </li>
              <li>
                <DataTitle>전년</DataTitle>
                <PeopleView data={prevYear} />

                {/* <DataNumber number={prevYear}>
                  {(prevYear > 0 ? `+${prevYear}` : prevYear) || 0 }
                  명
                </DataNumber> */}
              </li>
            </DataWrap>
          ) }
        </div>
      </ChangeRate>
    </>
  );
  const [lineAndColumnData, setLineAndColumnData] = useState('');
  useEffect(() => {
    if (lineAndColumn?.data?.labels?.length > 0) {
      const changeArr = lineAndColumn.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const lineAndColumnChartData = {
        series: changeArr,
        options: {
          chart: {
            type: 'bar',
            height: 350,
            toolbar: {
              show: false,
            },
            zoom: {
              enabled: false,
            },
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '100%',
              barHeight: '100%',
              endingShape: 'rounded',
            },
          },
          dataLabels: {
            enabled: false,
          },
          colors: [CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.CUSTOMER_INTERESTED, CHART_COLORS.CUSTOMER_PURCHASE, CHART_COLORS.CUSTOMER_LOYALTY],
          stroke: {
            show: true,
            width: 2,
            colors: ['transparent'],
          },
          xaxis: {
            categories: lineAndColumn.data.labels,
            tooltip: {
              enabled: false,
            },
            axisTicks: {
              show: false,
            },
            labels: {
              show: true,
              hideOverlappingLabels: false,
              style: {
                colors: 'var(--color-gray-400)',
                fontSize: '11px',
                fontWeight: 400,
                cssClass: 'apexcharts-xaxis-label',
              },
              offsetX: 2,
              offsetY: 0,
              formatter(val) {
                if (dateType === 'month') {
                  return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
                }
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
          fill: {
            opacity: 1,
          },
          tooltip: {
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            x: {
              formatter(val) {
                if (dateType === 'month') {
                  return `${moment(val, 'YYYYMMDD').format('YYYY. M.')}`;
                }
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return val && `${(val).toLocaleString()}명`;
              },
            },
          },
        },
      };
      setLineAndColumnData(lineAndColumnChartData);
    } else {
      setLineAndColumnData('');
    }
  }, [lineAndColumn]);

  return (
    <Container>
      <div className="loadingContainer">
        <LoadingComponent isLoading={statisticsData.data === null || statisticsData.data === ''} />

        { statisticsData.data
        && (
          <Tabs activeKey={customerType} onChange={(v) => { setSelectedTab(v); }}>
            <TabPane
              key="total"
              tab={tabHead(0, statisticsData.data.totalActiveCustCount)}
            />
            <TabPane
              key="visit"
              tab={
                tabHead(
                  1,
                  statisticsData.data.totalVisitCustCount,
                  statisticsData.data.totalVisitCustCountComparedMonth,
                  statisticsData.data.totalVisitCustCountComparedYear,
                )
              }
            />
            <TabPane
              key="inst"
              tab={
                tabHead(
                  2,
                  statisticsData.data.totalInterestCustCount,
                  statisticsData.data.totalInterestCustCountComparedMonth,
                  statisticsData.data.totalInterestCustCountComparedYear,
                )
              }
            />
            <TabPane
              key="order"
              tab={
                tabHead(
                  3,
                  statisticsData.data.totalOrderCustCount,
                  statisticsData.data.totalOrderCustCountComparedMonth,
                  statisticsData.data.totalOrderCustCountComparedYear,
                )
              }
            />
            <TabPane
              key="roy"
              tab={
                tabHead(
                  4,
                  statisticsData.data.totalRoyaltyCustCount,
                  statisticsData.data.totalRoyaltyCustCountComparedMonth,
                  statisticsData.data.totalRoyaltyCustCountComparedYear,
                )
              }
            />
          </Tabs>
        ) }
        { statisticsData.data
      && (
        <>
          <div className="chart-name-wrap">
            <p className="chart-name">{chartNmTitle}</p>
          </div>
          <div className="loadingDiv apexcharts-legend-first-none">
            <LoadingComponent isLoading={lineAndColumn.status === 'pending' && !lineAndColumnData} />
            {lineAndColumnData
              ? <ReactApexChart options={lineAndColumnData.options} series={lineAndColumnData.series} type={lineAndColumnData.options.chart.type} height={350} />
              : <EmptyGraph />}
          </div>
        </>
      )}
      </div>
    </Container>
  );
}

const Container = styled.div`
  .loadingContainer{
    height: 600px;
    position: relative;
  }
  .ant-tabs-nav:before {
    content: none !important;
  }
  .ant-tabs-ink-bar{
    display: none;
  }
  .ant-tabs-nav-list{
    flex:1;
  }
  .ant-tabs-tab-btn{
    width: 100%;
  }
  .ant-tabs{
    margin-bottom: 5px;
  }
  .ant-tabs-tab{
    display: flex !important;
    padding:0px;
    line-height: 40px; 
    margin: 0px;
    justify-content: center;
    flex: 1;
    margin-left: 10px;
    background-color: #fff;
  }
  .ant-tabs-tab:first-child{
    margin-left: 0px !important;
  }
  .ant-tabs-tab .ant-tabs-tab-btn{
    border-radius: 8px;
    border: var(--border-default);
    color: #333;
    transition: none;
    padding: 16px;
    height: 152px;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
    border-radius: 8px;
    border: 2px solid var(--color-blue-500);
    color: #333;
    padding: 15px;
    text-shadow: none;
  }
  .ant-tabs-tab:hover {
    color: #333;
  }
`;

const Title = styled.h3`
  margin-bottom: 6px;
  color: var(--color-gray-700);
  font-size: 13px;
  font-weight: 700;
  line-height: 18px;
  display: flex;
  align-items: center;
  position: relative;
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  color: var(--color-gray-900);
  font-size: 20px;
  font-weight: 700;
  line-height: 30px;
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 24px;
    margin-left: 2px;
    vertical-align: bottom;
  }
`;

const ChangeRate = styled.div`
  display: flex;
  align-items: center;
  margin-top: 2px;
  .item {
    display: flex;
    align-items: center;
    font-size: 12px;
    font-weight: 400;
  }
  .text{
    color: var(--color-gray-700);
  }
  .text3{
    color: var(--color-gray-700);
    line-height: 22px;
  }
  .text3 b { color:black;padding-left:5px;}
`;

const DataWrap = styled.ul`
  padding: 0;
  li {
    line-height: 18px;
  }
`;
const DataTitle = styled.span`
  color: var(--color-gray-700);
  padding-right: 4px;
  display: inline-block;
`;
const DataNumber = styled.span`
  display: inline-block;
  color:${(props) => {
    if (props.number > 0) {
      return 'var(--color-blue-500)';
    }
    if (props.number < 0) {
      return 'var(--color-red-500)';
    }
    return 'var(--color-gray-700)';
  }}
`;
export default TabCharts;
