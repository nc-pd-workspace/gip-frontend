export const TabHeadTitle = ['전체 활동고객수', '전체 방문고객수', '전체 관심고객수', '전체 주문고객수', '전체 충성고객수'];
export const AlertTabHeadTitle = { total: '전체 활동고객수', visit: '전체 방문고객수', inst: '전체 관심고객수', order: '전체 주문고객수', roy: '전체 충성고객수' };

export const brandOptions = [
  { label: '전체', value: 'all' },
  { label: '나이키', value: 'nike' },
  { label: '아디다스', value: 'adidas' },
  { label: '언더아머', value: 'underarmor' },
];

export const genderOptions = [
  { label: '전체', value: 'all' },
  { label: '남자', value: 'male' },
  { label: '여자', value: 'female' },
];

export const membershipOptions = [
  { label: '전체', value: 'all' },
  { label: 'vvip', value: 'diamond' },
  { label: 'vip', value: 'platinum' },
  { label: 'gold', value: 'gold' },
  { label: '멤버십 외', value: 'others' },
];

export const locationOptions = [
  { label: '전체', value: 'all' },
  { label: '서울', value: 'seoul' },
  { label: '경기', value: 'gyeongGi' },
  { label: '인천,울산', value: 'incheonUlsan' },
  { label: '부산', value: 'busan' },
];

export const ageOptions = [
  { label: '전체', value: 'all' },
  { label: '~10세', value: '0010' },
  { label: '11~20세', value: '1120' },
  { label: '21~30세', value: '2130' },
  { label: '31~35세', value: '3135' },
  { label: '36~40세', value: '3640' },
  { label: '41~50세', value: '4150' },
  { label: '51~60세', value: '5160' },
  { label: '60~', value: '60' },
];
export const defaultCategoryOptions = [
  [
    { label: '뷰티', value: 'beauty' },
    { label: '생활/건강', value: 'life' },
    { label: '신발', value: 'shoes' },
  ],
  [
    { label: '아모레퍼시픽', value: 'beauty01', parent: 'beauty' },
    { label: '더페이스샵', value: 'beauty02', parent: 'beauty' },
    { label: '참존', value: 'beauty03', parent: 'beauty' },
    { label: '리빙', value: 'life01', parent: 'life' },
    { label: '가구', value: 'life02', parent: 'life' },
    { label: '수납', value: 'life03', parent: 'life' },
    { label: '나이키', value: 'shoes01', parent: 'shoes' },
    { label: '아디다스', value: 'shoes02', parent: 'shoes' },
    { label: '컨버스', value: 'shoes03', parent: 'shoes' },
  ],
  [
    { label: '화장품', value: 'beauty0101', parent: 'beauty01' },
    { label: '미백', value: 'beauty0201', parent: 'beauty02' },
    { label: '보습', value: 'beauty0301', parent: 'beauty03' },
    { label: '냄비', value: 'life0101', parent: 'life01' },
    { label: '장롱', value: 'life0201', parent: 'life02' },
    { label: '책상', value: 'life0202', parent: 'life02' },
    { label: '서랍', value: 'life0301', parent: 'life03' },
    { label: '조던', value: 'shoes0101', parent: 'shoes01' },
    { label: '운동화', value: 'shoes0102', parent: 'shoes01' },
    { label: 'Special Edition', value: 'shoes0103', parent: 'shoes01' },

  ],
];

export const funnelTableData = [
  {
    key: '1',
    funnel: '직접방문',
    numberCustomers: '1000',
    proportion: '00%',
    salesRate: '00%',
  },
  {
    key: '2',
    funnel: '제휴/가격비교',
    numberCustomers: '1000',
    proportion: '00%',
    salesRate: '00%',
  },
  {
    key: '3',
    funnel: '광고',
    numberCustomers: '1000',
    proportion: '00%',
    salesRate: '00%',
  },
  {
    key: '4',
    funnel: '푸시',
    numberCustomers: '1000',
    proportion: '00%',
    salesRate: '00%',
  },
  {
    key: '5',
    funnel: '합계',
    numberCustomers: '1000',
    proportion: '00%',
    salesRate: '00%',
  },
];

export const funnelTableColumns = [
  {
    title: '유입경로',
    dataIndex: 'funnel',
    key: 'funnel',
  },
  {
    title: '고객수',
    dataIndex: 'numberCustomers',
    key: 'numberCustomers',
    align: 'left',
  },
  {
    title: '비중',
    dataIndex: 'proportion',
    key: 'proportion',
    align: 'center',
  },
  {
    title: '매출 전환율',
    dataIndex: 'salesRate',
    key: 'salesRate',
    align: 'right',
  },
];
