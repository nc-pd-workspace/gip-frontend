import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
  statistics: asyncApiState.initial([]),
  lineAndColumn: asyncApiState.initial([]),
  barCategoryL: asyncApiState.initial([]),
  barCategoryM: asyncApiState.initial([]),
  brandGraph: asyncApiState.initial([]),
  barNegative: asyncApiState.initial([]),
  radarMultiple: asyncApiState.initial([]),
  columnDistributed: asyncApiState.initial([]),
  treemapDistributed: asyncApiState.initial([]),
  stackedColumnAmount: asyncApiState.initial([]),
  stackedColumnCount: asyncApiState.initial([]),
  donutGraph: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'activity/stepCustomer',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], [], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], [], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));
      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },

    getStepCustomerStatistics: (state, { payload }) => {
      state.statistics = asyncApiState.request();
    },
    getStepCustomerStatisticsSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.statistics = asyncApiState.success(result);
    },
    getStepCustomerStatisticsFailure: (state, { payload }) => {
      state.statistics = asyncApiState.error(payload);
    },
    //  전체 활동고객 분석 메인 그래프 API
    getStepCustomerLineAndColumn: (state, { payload }) => {
      state.lineAndColumn = asyncApiState.request();
    },
    getStepCustomerLineAndColumnSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.lineAndColumn = asyncApiState.success(result);
    },
    getStepCustomerLineAndColumnFailure: (state, { payload }) => {
      state.lineAndColumn = asyncApiState.error(payload);
    },
    // 카테고리/브랜드 구매 선호도 > 카테고리(대) 그래프 API
    getStepCustomerBarCategoryL: (state, { payload }) => {
      state.barCategoryL = asyncApiState.request();
    },
    getStepCustomerBarCategoryLSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barCategoryL = asyncApiState.success(result);
    },
    getStepCustomerBarCategoryLFailure: (state, { payload }) => {
      state.barCategoryL = asyncApiState.error(payload);
    },
    // 카테고리/브랜드 구매 선호도 > 카테고리(중) 그래프 API
    getStepCustomerBarCategoryM: (state, { payload }) => {
      state.barCategoryM = asyncApiState.request();
    },
    getStepCustomerBarCategoryMSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barCategoryM = asyncApiState.success(result);
    },
    getStepCustomerBarCategoryMFailure: (state, { payload }) => {
      state.barCategoryM = asyncApiState.error(payload);
    },
    // 카테고리/브랜드 구매 선호도 > 브랜드 그래프 API
    getStepCustomerBrandGraph: (state, { payload }) => {
      state.brandGraph = asyncApiState.request();
    },
    getStepCustomerBrandGraphSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.brandGraph = asyncApiState.success(result);
    },
    getStepCustomerBrandGraphFailure: (state, { payload }) => {
      state.brandGraph = asyncApiState.error(payload);
    },
    //  고객 특성 > 성별/연령별 그래프 API
    getStepCustomerBarNegative: (state, { payload }) => {
      state.barNegative = asyncApiState.request();
    },
    getStepCustomerBarNegativeSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.barNegative = asyncApiState.success(result);
    },
    getStepCustomerBarNegativeFailure: (state, { payload }) => {
      state.barNegative = asyncApiState.error(payload);
    },
    //  고객 특성 > 멤버십 등급 그래프 API
    getStepCustomerRadarMultiple: (state, { payload }) => {
      state.radarMultiple = asyncApiState.request();
    },
    getStepCustomerRadarMultipleSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.radarMultiple = asyncApiState.success(result);
    },
    getStepCustomerRadarMultipleFailure: (state, { payload }) => {
      state.radarMultiple = asyncApiState.error(payload);
    },
    //  고객 특성 > 지역(전국) 그래프 API
    getStepCustomerColumnDistributed: (state, { payload }) => {
      state.columnDistributed = asyncApiState.request();
    },
    getStepCustomerColumnDistributedSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.columnDistributed = asyncApiState.success(result);
    },
    getStepCustomerColumnDistributedFailure: (state, { payload }) => {
      state.columnDistributed = asyncApiState.error(payload);
    },
    //  고객 특성 > 지역(수도권) 그래프 API
    getStepCustomerTreemapDistributed: (state, { payload }) => {
      state.treemapDistributed = asyncApiState.request();
    },
    getStepCustomerTreemapDistributedSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.treemapDistributed = asyncApiState.success(result);
    },
    getStepCustomerTreemapDistributedFailure: (state, { payload }) => {
      state.treemapDistributed = asyncApiState.error(payload);
    },
    //  연간 구매 형태 > 연간 구매 금액 그래프 API
    getStepCustomerStackedColumnAmount: (state, { payload }) => {
      state.stackedColumnAmount = asyncApiState.request();
    },
    getStepCustomerStackedColumnAmountSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.stackedColumnAmount = asyncApiState.success(result);
    },
    getStepCustomerStackedColumnAmountFailure: (state, { payload }) => {
      state.stackedColumnAmount = asyncApiState.error(payload);
    },
    //  연간 구매 형태 > 연간 구매 횟수 그래프 API
    getStepCustomerStackedColumnCount: (state, { payload }) => {
      state.stackedColumnCount = asyncApiState.request();
    },
    getStepCustomerStackedColumnCountSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.stackedColumnCount = asyncApiState.success(result);
    },
    getStepCustomerStackedColumnCountFailure: (state, { payload }) => {
      state.stackedColumnCount = asyncApiState.error(payload);
    },
    //  유입경로 그래프 API
    getStepCustomerDonutGraph: (state, { payload }) => {
      state.donutGraph = asyncApiState.request();
    },
    getStepCustomerDonutGraphSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.donutGraph = asyncApiState.success(result);
    },
    getStepCustomerDonutGraphFailure: (state, { payload }) => {
      state.donutGraph = asyncApiState.error(payload);
    },
  },
});

export const { updateState, getCategoryOptions, getBrandOptions, getStepCustomerStatistics, getStepCustomerLineAndColumn, getStepCustomerBarCategoryL, getStepCustomerBarCategoryM, getStepCustomerBrandGraph, getStepCustomerBarNegative, getStepCustomerRadarMultiple, getStepCustomerColumnDistributed, getStepCustomerTreemapDistributed, getStepCustomerStackedColumnAmount, getStepCustomerStackedColumnCount, getStepCustomerDonutGraph } = actions;

export default reducer;
