import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import {
  getCategoryOptions,
  getBrandOptions,
  getStepCustomerStatistics,
  getStepCustomerLineAndColumn,
  getStepCustomerBarCategoryL,
  getStepCustomerBarCategoryM,
  getStepCustomerBrandGraph,
  getStepCustomerBarNegative,
  getStepCustomerRadarMultiple,
  getStepCustomerColumnDistributed,
  getStepCustomerTreemapDistributed,
  getStepCustomerStackedColumnAmount,
  getStepCustomerStackedColumnCount,
  getStepCustomerDonutGraph,
} from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);

const StepCustomerStatisticsSaga = createPromiseSaga(getStepCustomerStatistics, API.StepCustomer.stepCustomerStatistics);
const StepCustomerLineAndColumnSaga = createPromiseSaga(getStepCustomerLineAndColumn, API.StepCustomer.stepCustomerLineAndColumn);
const StepCustomerBarCategoryLSaga = createPromiseSaga(getStepCustomerBarCategoryL, API.StepCustomer.stepCustomerBarCategoryL);
const StepCustomerBarCategoryMSaga = createPromiseSaga(getStepCustomerBarCategoryM, API.StepCustomer.stepCustomerBarCategoryM);
const StepCustomerBrandGraphSaga = createPromiseSaga(getStepCustomerBrandGraph, API.StepCustomer.stepCustomerBrandGraph);
const StepCustomerBarNegativeSaga = createPromiseSaga(getStepCustomerBarNegative, API.StepCustomer.stepCustomerBarNegative);
const StepCustomerRadarMultipleSaga = createPromiseSaga(getStepCustomerRadarMultiple, API.StepCustomer.stepCustomerRadarMultiple);
const StepCustomerColumnDistributedSaga = createPromiseSaga(getStepCustomerColumnDistributed, API.StepCustomer.stepCustomerColumnDistributed);
const StepCustomerTreemapDistributedSaga = createPromiseSaga(getStepCustomerTreemapDistributed, API.StepCustomer.stepCustomerTreemapDistributed);
const StepCustomerStackedColumnAmountSaga = createPromiseSaga(getStepCustomerStackedColumnAmount, API.StepCustomer.stepCustomerStackedColumnAmount);
const StepCustomerStackedColumnCountSaga = createPromiseSaga(getStepCustomerStackedColumnCount, API.StepCustomer.stepCustomerStackedColumnCount);
const StepCustomerDonutGraphSaga = createPromiseSaga(getStepCustomerDonutGraph, API.StepCustomer.stepCustomerDonutGraph);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);

  yield takeLatest(getStepCustomerStatistics, StepCustomerStatisticsSaga);
  yield takeLatest(getStepCustomerLineAndColumn, StepCustomerLineAndColumnSaga);
  yield takeLatest(getStepCustomerBarCategoryL, StepCustomerBarCategoryLSaga);
  yield takeLatest(getStepCustomerBarCategoryM, StepCustomerBarCategoryMSaga);
  yield takeLatest(getStepCustomerBrandGraph, StepCustomerBrandGraphSaga);
  yield takeLatest(getStepCustomerBarNegative, StepCustomerBarNegativeSaga);
  yield takeLatest(getStepCustomerRadarMultiple, StepCustomerRadarMultipleSaga);
  yield takeLatest(getStepCustomerColumnDistributed, StepCustomerColumnDistributedSaga);
  yield takeLatest(getStepCustomerTreemapDistributed, StepCustomerTreemapDistributedSaga);
  yield takeLatest(getStepCustomerStackedColumnAmount, StepCustomerStackedColumnAmountSaga);
  yield takeLatest(getStepCustomerStackedColumnCount, StepCustomerStackedColumnCountSaga);
  yield takeLatest(getStepCustomerDonutGraph, StepCustomerDonutGraphSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
