import { useCallback, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { Radio } from 'antd';

import { useDispatch, useSelector } from 'react-redux';

import CategorySelects from '../../../../components/search/CategorySelects';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import MultipleRow from '../../../../components/search/MultipleRow';
import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';
import TabCharts from '../components/TabCharts';
import TabSelectChart from '../components/TabSelectChart';
import {
  getCategoryOptions,
  getBrandOptions,
  getStepCustomerStatistics,
  getStepCustomerLineAndColumn,
  getStepCustomerBarNegative,
  getStepCustomerRadarMultiple,
  getStepCustomerColumnDistributed,
  getStepCustomerTreemapDistributed,
  getStepCustomerStackedColumnCount,
  getStepCustomerStackedColumnAmount,
  getStepCustomerBarCategoryL,
  getStepCustomerBarCategoryM,
  getStepCustomerBrandGraph,
  getStepCustomerDonutGraph,
} from '../redux/slice';

import { getCustomerSelects } from '../../../../redux/commonReducer';
import { AlertTabHeadTitle } from '../constants';
import { alertMessage } from '../../../../components/message';
import { excelDownload } from '../../../../utils/utils';
import { ButtonExcel } from '../../../../components/button';

function StepCustomerContainer({ query }) {
  const searchRef = useRef();
  const [brand, setBrand] = useState(null);

  const [chartNmTitle, setChartNmTitle] = useState('내 브랜드 전체 활동고객 특성의 변화를 확인해보세요.');

  const [radioType, setRadioType] = useState('avg');
  const [customerType, setCustomerType] = useState('total');
  const [queryActiveKey, setQueryActiveKey] = useState('');
  const [dateType, setDateType] = useState('');
  const [search, setSearch] = useState({});
  const [detailOnOff, setDetailOnOff] = useState('on');
  const {
    brandOption,
    categoryOption,
    statisticsData,
    lineAndColumn,
    barNegative,
    radarMultiple,
    columnDistributed,
    treemapDistributed,
    stackedColumnCount,
    stackedColumnAmount,
    barCategoryL,
    barCategoryM,
    brandGraph,
    donutGraph,
  } = useSelector((state) => ({
    categoryOption: state.activity.stepCustomer.categoryOption,
    brandOption: state.activity.stepCustomer.brandOption,
    statisticsData: state.activity.stepCustomer.statistics,
    lineAndColumn: state.activity.stepCustomer.lineAndColumn,
    barNegative: state.activity.stepCustomer.barNegative,
    radarMultiple: state.activity.stepCustomer.radarMultiple,
    columnDistributed: state.activity.stepCustomer.columnDistributed,
    treemapDistributed: state.activity.stepCustomer.treemapDistributed,
    stackedColumnCount: state.activity.stepCustomer.stackedColumnCount,
    stackedColumnAmount: state.activity.stepCustomer.stackedColumnAmount,
    barCategoryL: state.activity.stepCustomer.barCategoryL,
    barCategoryM: state.activity.stepCustomer.barCategoryM,
    brandGraph: state.activity.stepCustomer.brandGraph,
    donutGraph: state.activity.stepCustomer.donutGraph,
  }));

  const { selectPtnIdx, customerSelects, userInfo } = useSelector((state) => state.common);
  const [customerTabVisible, setCustomerTabVisible] = useState('');
  const [buttonExcelDisabled, setButtonExcelDisabled] = useState(false);
  const [searchParams, setSearchParams] = useState({});

  const dispatch = useDispatch();

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    dispatch(getBrandOptions({ params }));
    dispatch(getCustomerSelects({ params }));
    searchRef.current.clickSearch();
  }, []);

  useEffect(() => {
    if (query && Object.keys(query).length) {
      setSearch({ ...query });
      searchRef.current.setValue({ ...query });
      // 카드ui 전체활동고객, 하단 탭: 카테고리.브랜드 주문 선호도
      setCustomerType(query.cardType);
      setQueryActiveKey(query.activeKey);
    }
  }, [query]);

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  useEffect(() => {
    if (search.searchEndDate) {
      const params = {
        ptnIdx: selectPtnIdx,
        brandCode: search.brand ? search.brand.toString() : '',
        categoryLCode: search?.catData?.[0] ? search.catData[0] : '',
        categoryMCode: search?.catData?.[1] ? search.catData[1] : '',
        categorySCode: search?.catData?.[2] ? search.catData[2] : '',
        genderCode: search.gender ? search.gender : '',
        custGradeCode: search.membership ? search.membership : '',
        sidoCode: search.location ? search.location : '',
        age5UnitCode: search.age ? search.age.toString() : '',
        dataType: radioType,
        cardType: customerType,
      };
      if (search.searchStartDate) {
        if (search.searchStartDate.length > 7) params.searchDateType = 'date';
        else params.searchDateType = 'month';
        setDateType(params.searchDateType);
        params.searchStartDate = search.searchStartDate;
      }
      if (search.searchEndDate) params.searchEndDate = search.searchEndDate;

      // 고객 특성 탭 유무
      let searchAge = false;
      if (search?.age === undefined) {
        searchAge = true;
      } else if (search?.age.length === customerSelects.data.ageUnit.length) {
        searchAge = true;
      }
      if (search?.gender === undefined && search?.membership === undefined && search?.location === undefined && searchAge) {
        setCustomerTabVisible(true);
      } else {
        setCustomerTabVisible(false);
      }

      dispatch(getStepCustomerStatistics({ params }));
      dispatch(getStepCustomerLineAndColumn({ params }));
      // sub chart
      dispatch(getStepCustomerBarCategoryL({ params }));
      dispatch(getStepCustomerBarCategoryM({ params }));
      dispatch(getStepCustomerBrandGraph({ params }));
      dispatch(getStepCustomerBarNegative({ params }));
      dispatch(getStepCustomerRadarMultiple({ params }));
      dispatch(getStepCustomerColumnDistributed({ params }));
      dispatch(getStepCustomerTreemapDistributed({ params }));
      dispatch(getStepCustomerStackedColumnAmount({ params }));
      dispatch(getStepCustomerStackedColumnCount({ params }));
      dispatch(getStepCustomerDonutGraph({ params }));
      setSearchParams(params);
    }
  }, [search, radioType, customerType]);

  const RadioOnchange = ({ target }) => {
    setRadioType(target.value);
  };
  const onChangeSelectTab = useCallback((key) => {
    if (isLoadingSubChart()) {
      alertMessage(`${AlertTabHeadTitle[customerType]} 데이터 조회 중입니다. 잠시만 기다려 주세요.`);
      return;
    }
    setCustomerType(key);
    let tempTitle = '';
    switch (key) {
    case 'total':
      tempTitle = '내 브랜드 전체 활동고객 특성의 변화를 확인해보세요.';
      break;
    case 'visit':
      tempTitle = '내 브랜드 전체 방문고객수의 변화를 확인해보세요.';
      break;
    case 'inst':
      tempTitle = '내 브랜드 전체 관심고객수의 변화를 확인해보세요.';
      break;
    case 'order':
      tempTitle = '내 브랜드 전체 주문고객수의 변화를 확인해보세요.';
      break;
    case 'roy':
      tempTitle = '내 브랜드 전체 충성고객수의 변화를 확인해보세요.';
      break;
    default:
      tempTitle = '';
      break;
    }
    setChartNmTitle(tempTitle);
  }, [customerType, barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph]);

  const isLoadingSubChart = () => {
    if (isCustomerTabVisible()) {
      const arr = [barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount,
        stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
      return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
    }
    const arr = [stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
    return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
  };
  const isCustomerTabVisible = () => {
    if (search.genderCode || search.custGradeCode || search.sidoCode || search.age5UnitCode) return false;
    return true;
  };

  const excelButtonOnClick = useCallback(() => {
    setButtonExcelDisabled(true);
    excelDownload('/api/statistics/customer/stageActivity/excel', userInfo.accessToken, '단계별 활동 고객 분석', searchParams, searchParams.searchDateType)
      .then((result) => {
        if (result === 'success') {
          setButtonExcelDisabled(false);
        } else {
          setButtonExcelDisabled(false);
        }
      });
  }, [searchParams]);

  return (
    <Container>
      <PageHeader
        title="단계별 활동고객 분석"
        subTitle="브랜드의 활동고객 단계와 특성을 확인할 수 있습니다."
      />

      <Search
        search={search}
        setSearch={setSearch}
        detailOnOff={detailOnOff}
        setDetailOnOff={setDetailOnOff}
        ref={searchRef}
        onReset={onResetSearch}
      >
        <SingleRangePicker
          column={['searchStartDate', 'searchEndDate']}
          search={search}
          setSearch={setSearch}
          title="조회기간"
          disabledToday
          maxSelectDate={30}
          maxSelectMonth={12}
          dropdownClassName="stepCustomerPickerPopup"
        />
        {/* <SearchCover> */}
        <SingleInputItem
          type="Select"
          column="brand"
          title="브랜드"
          width="50%"
          loading={brandOption.status === 'pending'}
          options={brandOption.data}
          onChange={onChangeBrand}
          placeholder="전체"
        />
        <CategorySelects
          column="catData"
          title="카테고리"
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />
        {/* </SearchCover>
        <SearchLine className={detailOnOff} /> */}
        <MultipleRow isDetail detailOnOff={detailOnOff}>
          <SingleInputItem
            placeholder="전체"
            type="Select"
            column="membership"
            title="고객등급"
            options={customerSelects.data.custGrade}
          />
          <SingleInputItem
            placeholder="전체"
            type="Select"
            column="gender"
            title="성별"
            options={customerSelects.data.gender}
          />
          <SingleInputItem
            placeholder="전국"
            type="Select"
            column="location"
            title="지역"
            options={customerSelects.data.sido}
          />
        </MultipleRow>
        {/* <SearchCover> */}
        <SingleInputItem
          placeholder="전체"
          type="MultiSelect"
          column="age"
          title="연령대"
          width="50%"
          options={customerSelects.data.ageUnit}
          isDetail
          detailOnOff={detailOnOff}
        />
        {/* </SearchCover> */}
      </Search>

      <SearchResult>
        <Title>
          <p>고객특성 분석 결과</p>
          {statisticsData.status === 'success' && lineAndColumn?.data?.labels?.length > 0
          && (
            <div className="searchResultToolBox">
              <span className="caption-text">데이터 기준</span>
              <div className="right-item set-section-toggle">
                <Radio.Group
                  defaultValue={radioType}
                  buttonStyle="solid"
                  onChange={RadioOnchange}
                >
                  <Radio.Button value="avg">평균</Radio.Button>
                  <Radio.Button value="sum">합계</Radio.Button>
                </Radio.Group>
              </div>
              <ButtonExcel disabled={buttonExcelDisabled} onClick={excelButtonOnClick} />
            </div>
          )}
        </Title>
        <div className="loadingDiv">
          <TabCharts
            setSelectedTab={onChangeSelectTab}
            statisticsData={statisticsData}
            customerType={customerType}
            lineAndColumn={lineAndColumn}
            chartNmTitle={chartNmTitle}
            dateType={dateType}
          />
        </div>
        <div className="loadingDiv">
          <TabSelectChart
            barNegative={barNegative}
            radarMultiple={radarMultiple}
            columnDistributed={columnDistributed}
            treemapDistributed={treemapDistributed}
            stackedColumnCount={stackedColumnCount}
            stackedColumnAmount={stackedColumnAmount}
            barCategoryL={barCategoryL}
            barCategoryM={barCategoryM}
            brandGraph={brandGraph}
            donutGraph={donutGraph}
            customerTabVisible={customerTabVisible}
            queryActiveKey={queryActiveKey}
          />
        </div>
      </SearchResult>
    </Container>
  );
}

const Container = styled(PageLayout)``;
const SearchResult = styled(Paper)`
    margin-top: 50px;
    border: 1px solid #E3E4E7;
    padding: 0px 20px;
`;
const Title = styled.div`
    display:flex;
    font-weight: 700;
  p{
    padding: 20px 0px;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .total{
    position: absolute;
    left: 40px;
    bottom: 20px;
    font-size: 12px;
    color: var(--color-blue-500);
  }

`;

export default StepCustomerContainer;
