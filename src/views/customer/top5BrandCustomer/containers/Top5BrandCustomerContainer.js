import { useEffect, useState, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Radio } from 'antd';

import Table from '../../../../components/table';
import Search from '../../../../components/search';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import Description from '../../../../components/paper/descripition';
import SelectHeader from '../components/SelectHeader';
import Images from '../../../../Images';
import SectionHeader from '../../../../components/header/SectionHeader';
import FirstResultChart from '../components/ResultChart';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';

import { columns,
} from '../constants';
import Paper from '../../../../components/paper';
import { getCategoryOptions, getBrandOptions, getTop5BrandCustomerList, getTop5BrandCustomerLineData } from '../redux/slice';
import LoadingComponent from '../../../../components/loading';
import CategorySelects from '../../../../components/search/CategorySelects';

import EmptyList from '../../../../components/emptyList';
import { ButtonExcel } from '../../../../components/button';
import { excelDownload } from '../../../../utils/utils';

function Top5BrandCustomerContainer() {
  const initialParams = {
    ptnIdx: '',
    searchMonth: '',
    brandCode: '',
    categoryLCode: '',
    categoryMCode: '',
    dataType: '',
  };

  const dispatch = useDispatch();
  const searchRef = useRef();
  const [brand, setBrand] = useState(null);

  const currentSelectedInfoRef = useRef();
  const [radioType, setRadioType] = useState('avg');
  const [search, setSearch] = useState({});
  const [searchParams, setSearchParams] = useState(initialParams);
  const [currentSelectedRow, setCurrentSelectedRow] = useState('');
  const [buttonExcelDisabled, setButtonExcelDisabled] = useState(false);

  const { categoryOption, brandOption, firstRow, gsShopRow, listData, lineData } = useSelector((state) => ({
    categoryOption: state.activity.top5BrandCustomer.categoryOption,
    brandOption: state.activity.top5BrandCustomer.brandOption,
    listData: state.activity.top5BrandCustomer.list,
    lineData: state.activity.top5BrandCustomer.lineData,
    gsShopRow: state.activity.top5BrandCustomer.gsShopRow,
    firstRow: state.activity.top5BrandCustomer.firstRow,

  }));

  useEffect(() => {
    setCurrentSelectedRow(firstRow);
  }, [firstRow]);

  const { selectPtnIdx, userInfo } = useSelector((state) => state.common);

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    dispatch(getBrandOptions({ params }));
    searchRef.current.clickSearch();
  }, []);

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  const onChangeBrand = (value) => {
    setBrand(value);
  };
  useEffect(() => {
    if (search?.month) {
      const params = {
        ptnIdx: selectPtnIdx,
        searchMonth: search?.month ? search.month.replace('.', '') : '',
        brandCode: search.brand ? search.brand.toString() : '',
        categoryLCode: search?.categoryData?.[0] ? search.categoryData[0] : '',
        categoryMCode: search?.categoryData?.[1] ? search.categoryData[1] : '',
        dataType: radioType,
      };

      setSearchParams(params);
      dispatch(getTop5BrandCustomerList({ params }));
    }
  }, [search, radioType]);

  useEffect(() => {
    if (currentSelectedRow && searchParams.ptnIdx && listData?.data.length > 1) {
      const params = {
        ptnIdx: searchParams.ptnIdx ? searchParams.ptnIdx : '',
        searchMonth: searchParams.searchMonth ? searchParams.searchMonth : '',
        brandCode: currentSelectedRow.brandCd,
        categoryLCode: searchParams.categoryLCode ? searchParams.categoryLCode : '',
        categoryMCode: searchParams.categoryMCode ? searchParams.categoryMCode : '',
        dataType: searchParams.dataType ? searchParams.dataType : '',
      };

      dispatch(getTop5BrandCustomerLineData({ params }));
    }
  }, [currentSelectedRow]);

  const onClickRow = useCallback(async (getData) => {
    await setCurrentSelectedRow(getData);
    currentSelectedInfoRef.current.scrollIntoView({ behavior: 'smooth' });
  });

  const RadioOnchange = ({ target }) => {
    setRadioType(target.value);
  };
  const conditionHighlight = (row) => {
    if (currentSelectedRow && row.brandCd === currentSelectedRow.brandCd) {
      return 'currentSelectedRow';
    }
    return '';
  };
  const excelButtonOnClick = useCallback(() => {
    setButtonExcelDisabled(true);
    excelDownload('/api/statistics/customer/rankBrand/excel', userInfo.accessToken, 'TOP5 브랜드 활동 고객 분석', searchParams, 'month')
      .then((result) => {
        if (result === 'success') {
          setButtonExcelDisabled(false);
        } else {
          setButtonExcelDisabled(false);
        }
      });
  }, [searchParams]);

  return (
    <Container>
      <PageHeader
        title="TOP5 브랜드 활동고객 분석"
        subTitle="GS SHOP의 TOP 5 브랜드와 내 브랜드의 활동고객과 성장율을 비교할 수 있습니다."
      />
      <Search search={search} setSearch={setSearch} ref={searchRef} onReset={onResetSearch}>
        <SingleInputItem
          type="DatePicker"
          column="month"
          title="조회 월"
          width="50%"
          picker="month"
          placeholder="전체"
        />
        <SingleInputItem type="Select" column="brand" title="브랜드" width="50%" loading={brandOption.status === 'pending'} onChange={onChangeBrand} options={brandOption.data} placeholder="전체" />
        <CategorySelects column="categoryData" title="카테고리" depth={categoryOption.data.length} options={categoryOption.data} loading={categoryOption.status === 'pending'} placeholder="카테고리 선택" />
      </Search>

      <SearchResult>
        <Title>
          <p>브랜드 고객활동 조회</p>
          {listData?.status === 'success' && listData?.data.length > 0
          && (
            <div className="searchResultToolBox">
              <span className="caption-text">데이터 기준</span>
              <div className="right-item set-section-toggle">
                <Radio.Group defaultValue={radioType} buttonStyle="solid" onChange={RadioOnchange}>
                  <Radio.Button value="avg">평균</Radio.Button>
                  <Radio.Button value="sum">합계</Radio.Button>
                </Radio.Group>
              </div>
              <ButtonExcel disabled={buttonExcelDisabled} onClick={excelButtonOnClick} />
            </div>
          )}
        </Title>
        <div className="loadingDiv">
          <LoadingComponent isLoading={listData?.status === 'pending'} />
          {listData?.status === 'success' && listData?.data.length > 0
            ? (
              <Table
                className="tableClassName"
                columns={columns}
                dataSource={listData.data}
                fixedHeader="gsShop"
                onClickRow={onClickRow}
                conditionalHighlight={conditionHighlight}
                scrollY={239}

              />
            ) : <EmptyList warningSubTitle="조회 기준을 다시 설정해보세요." /> }
        </div>
      </SearchResult>
      <Description text={[
        '* 표시된 데이터는 실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
      ]}
      />
      {listData?.status === 'success' && listData?.data.length > 1
      && (
        <PageSection ref={currentSelectedInfoRef} style={{ marginTop: '51px' }} border>
          {listData?.status === 'success' && currentSelectedRow
      && (
        <SectionHeader title={`TOP5 브랜드와 ${currentSelectedRow.brandNm}의 고객활동 비교`} />
      ) }

          <div className="loadingDiv">
            { listData?.status === 'success' && currentSelectedRow
      && (
        <SelectHeader currentSelectedRow={currentSelectedRow} gsShopRow={gsShopRow} />
      ) }
            <div className="loadingDiv">
              <LoadingComponent isLoading={listData?.status === 'pending' || lineData?.status === 'pending'} />
              <FirstResultChart chartData={lineData} />
            </div>
          </div>
        </PageSection>
      )}

    </Container>
  );
}

const PageSection = styled.div`
  margin-top: 50px;
  background-color: #FFF;
  border-radius: 8px;
  border: 1px solid #e3e4e7;
`;

const Container = styled(PageLayout)``;
const SearchResult = styled(Paper)`
  margin-top: 40px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;
  span.brand {
    padding-left: 20px;
  }
  .gsShop{
    display:inline-block;
    background-image: url(${Images.gsShopLogo});
    background-position: left 1px;
    background-repeat: no-repeat;
    background-size: 16px 16px;
    line-height:20px;
  }
  table {
    width: 100%;
    table-layout: fixed !important;
  }
  table  {
    tr th:nth-of-type(1),
    tr td:nth-of-type(1) {
      width: auto !important;
    }
    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      width: calc(33% - 130px) !important;
      padding-right: 40px;
    }
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      width: 170px !important;
      padding-right: 40px;

    }
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      width: 170px !important;
      padding-right: 40px;
    }
    tr th:nth-of-type(5),
    tr td:nth-of-type(5) {
      width: 130px !important;
    }
  }
  .fixedTable {
    table {
      tr th:nth-of-type(5),
      tr td:nth-of-type(5) {
        width: 136px !important;
        padding-right: 16px !important;
      }
    }
  }
  
  @media screen and (min-width: 1400px){
    table  {
    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      padding-right: 70px;

    }
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      padding-right: 70px;
      width: 200px !important;
    }
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      padding-right: 70px;
      width: 200px !important;
    } 
   }
  }
 
`;
const Title = styled.div`
  display:flex;
  font-weight: 700;
  p {
    padding: 20px 0px;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
`;
export default Top5BrandCustomerContainer;
