import styled from 'styled-components';

import PercentView from '../../../../components/percentView';

const TooltipText = [
  ( // 전체 활동고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에서 활동(방문, 관심, 주문, 충성)한 고객수 입니다.
    </>
  ),
  ( // 전체 방문고객수
    <>
      최근 1년간 내 브랜드 상품에 대한 주문 금액입니다.
      <br />
      <br />
      툴팁 추가 내용
    </>
  ),
];

function SelectHeader({ currentSelectedRow, gsShopRow }) {
  return (
    <Container>
      <ContentWrap>
        <ContentRow>
          <Title>
            GS SHOP TOP5 브랜드
          </Title>
          <BrandName>
            전체 활동고객수
            {/* <ToolTip text={TooltipText[0]} /> */}
          </BrandName>
          <ContentNumber>
            {gsShopRow.actCustCnt.toLocaleString('ko-KR')}
            <span>명</span>
          </ContentNumber>
          <ComparedList>
            {/* <li>
              전주
              <ArrowContainer ArrowData="-3" />
            </li> */}
            <li>
              전월
              <PercentView data={gsShopRow.preMonthActCustCnt} />
            </li>
            <li>
              전년
              <PercentView data={gsShopRow.preYearActCustCnt} />
            </li>
          </ComparedList>
          <LastYearOrder>
            {/* <ToolTip text={TooltipText[1]}> */}
            <p>* 최근 1년 주문액</p>
            {/* <img className="circle" src={Images.exclamation_circle} alt="exclamation_circle" width={16} height={16} /> */}
            <p className="value">
              {gsShopRow.oneYearOrdAmt.toLocaleString('ko-KR')}
              원
            </p>
            {/* </ToolTip> */}
          </LastYearOrder>
        </ContentRow>
        <ContentRow>
          <Title>
            {currentSelectedRow.brandNm}
          </Title>
          <BrandName>
            전체 활동고객수
            {/* <ToolTip text={TooltipText[0]} position="right" /> */}
          </BrandName>
          <ContentNumber>
            {currentSelectedRow.actCustCnt.toLocaleString('ko-KR')}
            <span>명</span>
          </ContentNumber>
          <ComparedList>
            {/* <li>
              전주
              <ArrowContainer ArrowData="-3" />
            </li> */}
            <li>
              전월
              <PercentView data={currentSelectedRow.preMonthActCustCnt} />
            </li>
            <li>
              전년
              <PercentView data={currentSelectedRow.preYearActCustCnt} />
            </li>
          </ComparedList>
          <LastYearOrder>
            {/* <ToolTip text={TooltipText[1]}> */}
            <p>* 최근 1년 주문액</p>
            {/* <img className="circle" src={Images.exclamation_circle} alt="exclamation_circle" width={16} height={16} /> */}
            <p className="value">
              {currentSelectedRow.oneYearOrdAmt.toLocaleString('ko-KR')}
              원
            </p>
            {/* </ToolTip> */}
          </LastYearOrder>
        </ContentRow>
      </ContentWrap>
    </Container>
  );
}

const Container = styled.div` 
  padding:0 20px;
`;

const ContentWrap = styled.ul`
  display: flex;
  border-top: var(--border-default);
  border-bottom: var(--border-default);
  padding: 16px 0;
  display:flex;
`;

const ContentRow = styled.li`
  flex: 0 0 50%;
  position: relative;
  padding-left: 20px;
  &:first-child:before {
    display: none;
  }
  &:before {
    position: absolute;
    top: 16px;
    bottom: 16px;
    left: 0;
    width: 1px;
    background-color: var(--color-gray-100);
    content: "";
  }
`;

const Title = styled.h3`
  margin-bottom: 8px;
  color: var(--color-gray-900);
  font-size: 13px;
  font-weight: 700;
  line-height: 20px;
  /* vertical-align: center; */
  display: flex;
  align-items:center;
  img{
    margin-left: 5px;
  }
`;

const BrandName = styled.p`
  position: relative;
  margin-bottom: 2px;
  font-weight: 400;
  font-size: 12px;
  line-height: 18px;
  letter-spacing: -0.5px;
  color: var(--color-gray-500);
  vertical-align: center;
  button {
    vertical-align: middle;
    margin-left: 4px;
    margin-top: -2px;
  }
`;
const ContentNumber = styled.p`
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  color: var(--color-gray-900);
  span {
    margin-left: 2px;
    font-weight: normal;
    font-size: 11px;
    letter-spacing: -0.5px;
    color: var(--color-gray-900);
  }
`;
const ComparedList = styled.ul`
  display: flex;
  font-size: 12px;
  color: var(--color-gray-700);
  font-weight: 400;
  li {
    margin-right: 20px;
    flex: 0 0 auto;
    &:last-child {
      margin-right: 0;
    }
    display: flex;
  }
`;
const LastYearOrder = styled.div`
  padding:4px 0;
  display:flex;
  font-size:12px;
  align-items: center;
  > img {
    flex:0 0 auto;
  }
  p {
    flex:0 0 auto;
    color: var(--color-gray-700);
  }
  p.value {
    color: var(--color-gray-900);
    letter-spacing: -0.5px;
    padding-left: 4px;
  }

`;

export default SelectHeader;
