import React from 'react';
import styled from 'styled-components';

function ResultChartBox({ children, title }) {
  return (
    <Container>
      <Inner className="apexcharts-legend-first-none">
        <h4>{title}</h4>
        {children}
      </Inner>
    </Container>
  );
}

const Container = styled.div`
  padding: 0 20px;
  .apexcharts-legend {
    justify-content:center!important;
  }
`;

const Inner = styled.div`
  padding: 20px 0;
  /* border-top:1px solid var(--color-gray-200); */
  h4 {
    font-size: 14px;
    line-height: 21px;
    color: var(--color-black);
    font-weight: 400;
    margin-bottom: 10px;
  }
`;

export default ResultChartBox;
