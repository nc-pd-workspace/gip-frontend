import { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';

import EmptyGraph from '../../../../components/emptyGraph';

import ResultChartBox from './ResultChartBox';
import { CHART_COLORS } from '../../../../styles/chartColors';
import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import { legendDefault } from '../../../../utils/chartOptions';

function FirstResultChart({ chartData }) {
  const [lineData, setLineData] = useState('');
  useEffect(() => {
    if (chartData?.data?.series) {
      const changeArr = chartData.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const lineChartData = {
        series: changeArr,
        options: {
          chart: {
            zoom: {
              enabled: false,
            },
            height: 350,
            type: 'line',
            dropShadow: {
              enabled: false,
            },
            toolbar: {
              show: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.top5brandChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
              },
            },
          },
          colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: 'straight',
            width: 3,
          },
          grid: {
            borderColor: '#E3E4E7',
            row: {
              colors: ['transparent'],
            },
          },
          markers: {
            size: 3,
            colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.COMPARE_BRAND],
            strokeWidth: 1,
          },
          xaxis: {
            categories: chartData.data.labels,
            axisTicks: {
              show: false,
            },
            tooltip: {
              enabled: false,
            },
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            show: true,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tooltip: {
              enabled: false,
            },
            tickAmount: 5,
          },
          tooltip: {
            shared: true,
            followCursor: false,
            intersect: false,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = chartData.data.labels[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return val && `${(val).toLocaleString()}명`;
              },
            },
          },
          legend: {
            ...legendDefault,
          },
        },
      };
      setLineData(lineChartData);
    } else {
      setLineData('');
    }
  }, [chartData]);
  return (
    <ResultChartBox title="GS SHOP TOP5 브랜드와 내 브랜드의 전체 활동고객수 변화를 비교해보세요.">
      {lineData
        ? (<ReactApexChart options={lineData.options} series={lineData.series} type="line" height={350} className="top5brandChart" />) : <EmptyGraph />}
    </ResultChartBox>
  );
}

export default FirstResultChart;
