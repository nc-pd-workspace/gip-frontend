import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
  list: asyncApiState.initial([]),
  lineData: asyncApiState.initial([]),
  gsShopRow: '',
  firstRow: '',

};

export const { actions, reducer } = createSlice({
  name: 'activity/competitorsActivity',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));
      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },

    getTop5BrandCustomerList: (state, { payload }) => {
      state.list = asyncApiState.request();
    },
    getTop5BrandCustomerListSuccess: (state, { payload }) => {
      const data = payload.data.map((v, idx) => {
        if (idx === 0) {
          v.key = 'gsShop';
          state.gsShopRow = v;
        } else {
          v.key = v.brandCd;
          if (idx === 1)state.firstRow = v;
        }
        return v;
      });
      const result = { ...payload, data: data || {} };
      state.list = asyncApiState.success(result);
    },
    getTop5BrandCustomerListFailure: (state, { payload }) => {
      state.list = asyncApiState.error(payload);
    },
    getTop5BrandCustomerLineData: (state, { payload }) => {
      state.lineData = asyncApiState.request();
    },
    getTop5BrandCustomerLineDataSuccess: (state, { payload }) => {
      const result = { ...payload || {} };

      state.lineData = asyncApiState.success(result);
    },
    getTop5BrandCustomerLineDataFailure: (state, { payload }) => {
      state.lineData = asyncApiState.error(payload);
    },

  },
});

export const { updateState, getBrandOptions, getCategoryOptions, getTop5BrandCustomerList, getTop5BrandCustomerLineData } = actions;

export default reducer;
