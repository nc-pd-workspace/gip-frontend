import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

import { createPromiseSaga } from '../../../../redux/lib';

import { getCategoryOptions, getBrandOptions, getTop5BrandCustomerList, getTop5BrandCustomerLineData } from './slice';
import API from '../../../../api';

const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

const top5BrandCustomerListSaga = createPromiseSaga(getTop5BrandCustomerList, API.Top5BrandCustomer.top5BrandCustomerList);
const top5BrandCustomerLineDataSaga = createPromiseSaga(getTop5BrandCustomerLineData, API.Top5BrandCustomer.top5BrandCustomerLineData);

/* dispatch type 구독 설정, 종류에 따라 watch함수 분기해도 좋음 */
function* watchCommon() {
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);

  yield takeLatest(getTop5BrandCustomerList, top5BrandCustomerListSaga);
  yield takeLatest(getTop5BrandCustomerLineData, top5BrandCustomerLineDataSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
