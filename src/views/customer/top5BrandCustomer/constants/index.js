import ToolTip from '../../../../components/toolTip';

export const brandOptions = [
  { label: '나이키', value: 'nike' },
  { label: '아디다스', value: 'adidas' },
  { label: '언더아머', value: 'underarmor' },
];

export const defaultCategoryOptions = [
  [
    { label: '아디다스', value: 'adidas' },
    { label: '나이키', value: 'nike' },
    { label: '언더아머', value: 'underarmor' },
  ],
  [
    { label: '운동화-01', value: 'adidas01', parent: 'adidas' },
    { label: '운동화-02', value: 'adidas02', parent: 'adidas' },
    { label: '운동화-03', value: 'adidas03', parent: 'adidas' },
    { label: '신발-01', value: 'nike01', parent: 'nike' },
    { label: '신발-02', value: 'nike02', parent: 'nike' },
    { label: '신발-03', value: 'nike03', parent: 'nike' },
    { label: '가방-01', value: 'underarmor01', parent: 'underarmor' },
    { label: '가방-02', value: 'underarmor02', parent: 'underarmor' },
    { label: '가방-03', value: 'underarmor03', parent: 'underarmor' },
  ],
];

export const columns = [
  {
    title: '',
    dataIndex: 'brandNm',
    key: 'brandNm',
    render: (text, row) => <span className={`brand ${row.key}`}>{text}</span>,
  },
  {
    title: (
      <>
        전체 활동고객수
        <ToolTip position="left" text={(<>조회기간 동안 최근 6개월간 내 브랜드 상품에서 활동(방문, 관심, 주문, 충성)한 고객수 입니다.</>)} />
      </>
    ),
    dataIndex: 'actCustCnt',
    key: 'actCustCnt',
    type: 'number',
  },
  // {
  //   title: '전주 대비율',
  //   dataIndex: 'prevWeekPercent',
  //   key: 'prevWeekPercent',
  //   type: 'percent',
  // },
  {
    title: (
      <>
        전월 대비율
        <ToolTip
          position="center"
          text={(
            <>
              · 내 브랜드의 전월대비 성장률 입니다.
              <br />
              <br />
              · 전월대비 성장률은 아래와 같이 계산됩니다.
              <br />
              · 전월대비 성장률=
              {'{'}
              (
              이번달 전체 활동고객수 -지난달 전체 활동고객수)/지난달 전체 활동고객수
              {'}'}
              {' '}
              * 100%
              <br />
              &nbsp;
              · 예) 이번달 기준일 2021.11.7
              <br />
              &nbsp;
              · 전월대비 성장률=
              {' '}
              {'{'}
              (11.1~ 7 합계 - 11.1~7 합계) / 10.1~7 합계
              {' '}
              {'}'}
              {' '}
              * 100%
            </>
          )}
        />
      </>
    ),
    dataIndex: 'preMonthActCustCnt',
    key: 'preMonthActCustCnt',
    type: 'percent',
  },
  {
    title: (
      <>
        전년 대비율
        <ToolTip
          position="right"
          text={(
            <>
              · 내 브랜드의 전년대비 성장률 입니다.
              <br />
              <br />
              · 전년대비 성장률은 아래와 같이 계산됩니다.
              <br />
              · 전년대비 성장률=
              {'{'}
              (
              올해년도 전체 활동고객수 -전년도 전체 활동고객수)/전년도 전체 활동고객수
              {'}'}
              {' '}
              * 100%
              <br />
              &nbsp;
              · 예) 올해 기준일 2022. 1. 7.
              <br />
              &nbsp;
              · 전년대비 성장률=
              {' '}
              {'{'}
              (2022.1.1~ 7 합계 - 2021.1.1~7 합계) / 2021.1.1~7 합계
              {' '}
              {'}'}
              {' '}
              * 100%
            </>
          )}
        />
      </>
    ),
    dataIndex: 'preYearActCustCnt',
    key: 'preYearActCustCnt',
    type: 'percent',
  },
  // {
  //   title: (
  //     <>
  //       경쟁사 대비율
  //       <ToolTip
  //         position="right"
  //         text={(
  //           <>
  //             · 내 브랜드와 경쟁사 브랜드의 대비율 입니다.
  //             <br />
  //             · 내 브랜드의 경쟁사 대비율은 아래와 같이 계산됩니다.
  //             <br />
  //             · 경쟁사 대비율=(내 브랜드 전체 활동고개수  - GS SHOP TOP5 브랜드의 전체 활동고객수) /GS SHOP  TOP5  브랜드의 전체 활동고객수
  //           </>
  //         )}
  //       />
  //     </>
  //   ),
  //   dataIndex: 'competitorContrasRatio',
  //   key: 'competitorContrasRatio',
  //   type: 'percentHyphen',
  // },
  {
    // title: '* 최근 1년 주문액',
    title: (
      <>
        * 최근 1년 주문액
      </>
    ),
    dataIndex: 'oneYearOrdAmt',
    key: 'oneYearOrdAmt',
    type: 'number',
  },
];
