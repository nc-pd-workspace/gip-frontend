export const brandOptions = [
  { label: '나이키', value: 'nike' },
  { label: '아디다스', value: 'adidas' },
  { label: '언더아머', value: 'underarmor' },
];

export const defaultCategoryOptions = [
  [
    { label: '뷰티', value: 'beauty' },
    { label: '생활/건강', value: 'life' },
    { label: '신발', value: 'shoes' },
  ],
  [
    { label: '아모레퍼시픽', value: 'beauty01', parent: 'beauty' },
    { label: '더페이스샵', value: 'beauty02', parent: 'beauty' },
    { label: '참존', value: 'beauty03', parent: 'beauty' },
    { label: '리빙', value: 'life01', parent: 'life' },
    { label: '가구', value: 'life02', parent: 'life' },
    { label: '수납', value: 'life03', parent: 'life' },
    { label: '나이키', value: 'shoes01', parent: 'shoes' },
    { label: '아디다스', value: 'shoes02', parent: 'shoes' },
    { label: '컨버스', value: 'shoes03', parent: 'shoes' },
  ],
  [
    { label: '화장품', value: 'beauty0101', parent: 'beauty01' },
    { label: '미백', value: 'beauty0201', parent: 'beauty02' },
    { label: '보습', value: 'beauty0301', parent: 'beauty03' },
    { label: '냄비', value: 'life0101', parent: 'life01' },
    { label: '장롱', value: 'life0201', parent: 'life02' },
    { label: '책상', value: 'life0202', parent: 'life02' },
    { label: '서랍', value: 'life0301', parent: 'life03' },
    { label: '조던', value: 'shoes0101', parent: 'shoes01' },
    { label: '운동화', value: 'shoes0102', parent: 'shoes01' },
    { label: 'Special Edition', value: 'shoes0103', parent: 'shoes01' },

  ],
];
// export const columns = [
//   {
//     title: '',
//     dataIndex: 'brandNm',
//     key: 'brandNm',
//     render: (text, row) => <span className={`brand ${row.key}`}>{text}</span>,
//   },
//   {
//     title: '신규 방문 고객수',
//     dataIndex: 'totalVisitCust',
//     key: 'totalVisitCust',
//     type: 'number',
//   },
//   {
//     title: '신규 관심 고객수',
//     dataIndex: 'totalInterestCust',
//     key: 'totalInterestCust',
//     type: 'number',
//   },
//   {
//     title: '신규 구매 고객수',
//     dataIndex: 'totalOrderCust',
//     key: 'totalOrderCust',
//     type: 'number',
//   },
//   {
//     title: '신규 충성 고객수',
//     dataIndex: 'totalRoyaltyCust',
//     key: 'totalRoyaltyCust',
//     type: 'number',
//   },
//   {
//     title: '신규 활동고객수',
//     dataIndex: 'totalNewCust',
//     key: 'totalNewCust',
//     type: 'number',
//   },

// ];

export const columns = [
  {
    title: '',
    dataIndex: 'brandNm',
    key: 'brandNm',
    render: (text, row) => <span className={`brand ${row.key}`}>{text}</span>,
  },
  {
    title: '신규 활동고객수',
    dataIndex: 'totalNewCust',
    key: 'totalNewCust',
    type: 'numberTooltip',
  },
  {
    title: '신규 방문고객수',
    dataIndex: 'totalVisitCust',
    key: 'totalVisitCust',
    type: 'numberTooltip',
  },
  {
    title: '신규 관심고객수',
    dataIndex: 'totalInterestCust',
    key: 'totalInterestCust',
    type: 'numberTooltip',
  },
  {
    title: '신규 주문고객수',
    dataIndex: 'totalOrderCust',
    key: 'totalOrderCust',
    type: 'numberTooltip',
  },
  {
    title: '신규 충성고객수',
    dataIndex: 'totalRoyaltyCust',
    key: 'totalRoyaltyCust',
    type: 'numberTooltip',
  },

];

export const exampleTableData = {
  tableKey: 'exampleTable',
  totalRow: 1000,
  pageCurrent: 1,
  pageTotal: 10,
  excelDownload: true,
  excelFileLink: 'excel.xlsx',
  rows: [
    {
      key: 'total',
      brand_nm: '브랜드 전체',
      new_cust: 111000000,
      total_new_cust: 111000000,
      total_interest_cust: 111000000,
      total_order_cust: 111000000,
      total_loyalty_cust: 111000000,
      total_active_cust: 111000000,
    },
    {
      key: 'gsShop',
      brand_nm: 'GS SHOP',
      new_cust: 11000000,
      total_new_cust: 11000000,
      total_interest_cust: 11000000,
      total_order_cust: 11000000,
      total_loyalty_cust: 11000000,
      total_active_cust: 11000000,
    },
    {
      key: 'gsShop1',
      brand_nm: 'GS SHOP1',
      new_cust: 11000000,
      total_new_cust: 11000000,
      total_interest_cust: 11000000,
      total_order_cust: 11000000,
      total_loyalty_cust: 11000000,
      total_active_cust: 11000000,
    }, {
      key: 'gsShop2',
      brand_nm: 'GS SHOP2',
      new_cust: 11000000,
      total_new_cust: 11000000,
      total_interest_cust: 11000000,
      total_order_cust: 11000000,
      total_loyalty_cust: 11000000,
      total_active_cust: 11000000,
    }, {
      key: 'gsShop3',
      brand_nm: 'GS SHOP3',
      new_cust: 11000000,
      total_new_cust: 11000000,
      total_interest_cust: 11000000,
      total_order_cust: 11000000,
      total_loyalty_cust: 11000000,
      total_active_cust: 11000000,
    },
  ],
};

export const areaData = {
  series: [{
    name: 'series1',
    data: [31, 40, 28, 51, 42, 109, 100],
  }, {
    name: 'series2',
    data: [11, 32, 45, 32, 34, 52, 41],
  }],
  options: {
    chart: {
      height: 350,
      type: 'area',
      toolbar: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth',
    },
    xaxis: {
      type: 'datetime',
      categories: ['2018-09-19T00:00:00.000Z', '2018-09-19T01:30:00.000Z', '2018-09-19T02:30:00.000Z', '2018-09-19T03:30:00.000Z', '2018-09-19T04:30:00.000Z', '2018-09-19T05:30:00.000Z', '2018-09-19T06:30:00.000Z'],
    },
    tooltip: {
      enabled: true,
      enabledOnSeries: undefined,
      shared: true,
      followCursor: false,
      intersect: false,
      inverseOrder: false,
      custom: undefined,
      fillSeriesColor: false,
      theme: false,
      style: {
        fontSize: '14px',
        fontFamily: undefined,
      },
      onDatasetHover: {
        highlightDataSeries: false,
      },
      x: {
        show: false,
        format: 'dd MMM',
        formatter: undefined,
      },
      y: {
        show: true,
        // eslint-disable-next-line arrow-body-style
        formatter: (value, { series, seriesIndex, dataPointIndex, w }) => {
          return `${value}명`;
        },
        title: {
          formatter: (seriesName) => (`${seriesName}ads`),
        },
      },
      z: {
        formatter: undefined,
        title: 'Size: ',
      },
      marker: {
        show: true,
      },
      items: {
        display: 'flex',
      },
      fixed: {
        enabled: false,
        position: 'topRight',
        offsetX: 0,
        offsetY: 0,
      },
    },
  },

};

export const barData = {
  series: [{
    name: 'Marine Sprite',
    data: [44, 55, 41, 37, 22, 43, 21],
  }, {
    name: 'Striking Calf',
    data: [53, 32, 33, 52, 13, 43, 32],
  }, {
    name: 'Tank Picture',
    data: [12, 17, 11, 9, 15, 11, 20],
  }, {
    name: 'Bucket Slope',
    data: [9, 7, 5, 8, 6, 9, 4],
  }, {
    name: 'Reborn Kid',
    data: [25, 12, 19, 32, 25, 24, 10],
  }],
  options: {
    chart: {
      type: 'bar',
      height: 350,
      stacked: true,
    },
    plotOptions: {
      bar: {
        horizontal: true,
      },
    },
    stroke: {
      width: 1,
      colors: ['#fff'],
    },
    xaxis: {
      categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014],
      labels: {
        formatter(val) {
          return `${val}K`;
        },
      },
    },
    yaxis: {
      title: {
        text: undefined,
      },
    },
    tooltip: {
      y: {
        formatter(val) {
          return `${val}K`;
        },
      },
    },
    fill: {
      opacity: 1,
    },
    legend: {
      position: 'top',
      horizontalAlign: 'left',
      offsetX: 40,
    },
  } };
