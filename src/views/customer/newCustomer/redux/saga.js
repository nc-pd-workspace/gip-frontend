import {
  all, fork,
  takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getCategoryOptions, getBrandOptions, getNewCustomerList, getNewCustomerChartSpline, getNewCustomerChartStackedBar } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

const newCustomerListSaga = createPromiseSaga(getNewCustomerList, API.NewCustomer.newCustomerList);
const newCustomerChartSplineSaga = createPromiseSaga(getNewCustomerChartSpline, API.NewCustomer.newCustomerChartSpline);
const newCustomerChartStackedBarSaga = createPromiseSaga(getNewCustomerChartStackedBar, API.NewCustomer.newCustomerChartStackedBar);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);

  yield takeLatest(getNewCustomerList, newCustomerListSaga);
  yield takeLatest(getNewCustomerChartSpline, newCustomerChartSplineSaga);
  yield takeLatest(getNewCustomerChartStackedBar, newCustomerChartStackedBarSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
