import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
  firstRow: '',
  list: asyncApiState.initial([]),
  chartSpline: asyncApiState.initial([]),
  chartStackedBar: asyncApiState.initial([]),

};

export const { actions, reducer } = createSlice({
  name: 'customer/newCustomer',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], [], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], [], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));
      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },

    getNewCustomerList: (state, { payload }) => {
      state.list = asyncApiState.request();
    },
    getNewCustomerListSuccess: (state, { payload }) => {
      const data = payload.data.map((v, idx) => {
        if (idx === 0) {
          v.key = 'total';
          state.firstRow = v;
        } else {
          v.key = v.brandCode;
          // if (idx === 1)state.firstRow = v;
        }
        return v;
      });
      const result = { ...payload, data: data || {} };

      state.list = asyncApiState.success(result);
    },
    getNewCustomerListFailure: (state, { payload }) => {
      state.list = asyncApiState.error(payload);
    },

    getNewCustomerChartSpline: (state, { payload }) => {
      state.chartSpline = asyncApiState.request();
    },
    getNewCustomerChartSplineSuccess: (state, { payload }) => {
      const result = { ...payload || {} };

      state.chartSpline = asyncApiState.success(result);
    },
    getNewCustomerChartSplineFailure: (state, { payload }) => {
      state.chartSpline = asyncApiState.error(payload);
    },

    getNewCustomerChartStackedBar: (state, { payload }) => {
      state.chartStackedBar = asyncApiState.request();
    },
    getNewCustomerChartStackedBarSuccess: (state, { payload }) => {
      const result = { ...payload || {} };

      state.chartStackedBar = asyncApiState.success(result);
    },
    getNewCustomerChartStackedBarFailure: (state, { payload }) => {
      state.chartStackedBar = asyncApiState.error(payload);
    },

  },
});

export const { updateState, getBrandOptions, getCategoryOptions, getNewCustomerList, getNewCustomerChartSpline, getNewCustomerChartStackedBar } = actions;

export default reducer;
