import React from 'react';
import ReactApexChart from 'react-apexcharts';
import styled from 'styled-components';

import EmptyGraph from '../../../../components/emptyGraph';

import LoadingComponent from '../../../../components/loading';

function ReusltCharts({ listData, chartSpline, chartSplineData, chartStackedBarDisabled, chartStackedBar, chartStackedBarData }) {
  return (
    <>
      <DetailItem className="detailItem apexcharts-legend-first-none">
        <div className="loadingDiv">
          <LoadingComponent isLoading={chartSpline?.status === 'pending' || listData?.status === 'pending'} />
          {chartSplineData
            ? (
              <>
                <DetailDesc>내 브랜드의 신규고객 특성별 고객 수의 변화를 비교해보세요.</DetailDesc>
                <ReactApexChart options={chartSplineData.options} series={chartSplineData.series} type="area" height={350} />
              </>
            ) : <EmptyGraph />}
        </div>
      </DetailItem>
      { chartStackedBarDisabled
          && (
            <DetailItem className="detailItem">
              <div className="loadingDiv">
                <LoadingComponent isLoading={chartStackedBar?.status === 'pending' || listData?.status === 'pending'} />
                {chartStackedBarData
                  ? (
                    <>
                      <DetailDesc>각 카테고리별 신규고객 특성의 확인해보세요.</DetailDesc>
                      <ReactApexChart options={chartStackedBarData.options} series={chartStackedBarData.series} type="bar" height={350} />
                    </>
                  ) : <EmptyGraph />}
              </div>
            </DetailItem>
          ) }
    </>
  );
}
const DetailItem = styled.div``;

const DetailDesc = styled.div`
  font-weight: 400;
  font-size: 14px;
  padding-top: 24px;
  padding-bottom: 10px;
`;
export default ReusltCharts;
