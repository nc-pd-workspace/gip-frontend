import styled from 'styled-components';
import React from 'react';

import FixStatisticsItem from './FixStatisticsItem';

function Statistics({ data, title, tooltip }) {
  return (
    <Container>
      <Contents>
        {data && data.map((val, idx) => (
          <FixStatisticsItem
            key={idx}
            idx={idx}
            title={title[idx]}
            tooltip={tooltip[idx]}
            amount={val.amount}
            changeRateByYear={val.changeRateByYear}
          />
        ))}

      </Contents>
    </Container>
  );
}

const Container = styled.div``;

const Contents = styled.div`
  padding: 20px 10px;
  display: flex;
  border-top: 1px solid #e3e4e7;
  border-bottom: 1px solid #e3e4e7;
`;

export default Statistics;
