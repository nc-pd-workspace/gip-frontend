import { useEffect, useState, useCallback, useRef } from 'react';
import styled from 'styled-components';
import { Radio } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import { CHART_COLORS } from '../../../../styles/chartColors';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import Paper from '../../../../components/paper';
import FixStatistics from '../components/FixStatistics';
import PageHeader from '../../../../components/header/PageHeader';
import Description from '../../../../components/paper/descripition';

import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Table from '../../../../components/table';

import { getCategoryOptions, getBrandOptions, getNewCustomerList, getNewCustomerChartSpline, getNewCustomerChartStackedBar } from '../redux/slice';
import { columns } from '../constants';
import LoadingComponent from '../../../../components/loading';
import EmptyList from '../../../../components/emptyList';
import { eventYaxisIndex, excelDownload, getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';
import CategorySelects from '../../../../components/search/CategorySelects';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import { legendDefault } from '../../../../utils/chartOptions';
import ReusltCharts from '../components/ResultCharts';
import { ButtonExcel } from '../../../../components/button';

function NewCustomerContainer() {
  const searchRef = useRef();
  const currentSelectedInfoRef = useRef();
  const [brand, setBrand] = useState(null);
  const [dateType, setDateType] = useState('');
  const [radioType, setRadioType] = useState('avg');
  const { selectPtnIdx, userInfo } = useSelector((state) => state.common);
  const { categoryOption, brandOption, firstRow, listData, chartSpline, chartStackedBar } = useSelector((state) => ({
    categoryOption: state.customer.newCustomer.categoryOption,
    brandOption: state.customer.newCustomer.brandOption,
    firstRow: state.customer.newCustomer.firstRow,
    listData: state.customer.newCustomer.list,
    chartSpline: state.customer.newCustomer.chartSpline,
    chartStackedBar: state.customer.newCustomer.chartStackedBar,
  }));

  const initialParams = {
    ptnIdx: '',
    searchDateType: '',
    searchStartDate: '',
    searchEndDate: '',
    brandCode: '',
    categoryLCode: '',
    categoryMCode: '',
    categorySCode: '',
    dataType: '',
  };

  const dispatch = useDispatch();

  const [search, setSearch] = useState({});
  const [searchParams, setSearchParams] = useState(initialParams);
  const [currentSelectedRow, setCurrentSelectedRow] = useState('');
  const [currentSelectedNmRow, setCurrentSelectedNmRow] = useState('');
  const [statisticsData, setStatisticsData] = useState();
  const [chartStackedBarDisabled, setChartStackedBarDisabled] = useState(true);
  const [buttonExcelDisabled, setButtonExcelDisabled] = useState(false);

  const FixTitle = ['신규 활동고객수', '신규 방문고객수', '신규 관심고객수', '신규 주문고객수', '신규 충성고객수'];
  const TooltipText = [
    ( // 신규 활동고객수
      <>
        조회기간 동안 최근 6개월간 내 브랜드 상품을 새롭게 방문한 고객이
        <br />
        활동(신규방문, 신규관심, 신규주문, 신규충성)한 고객수 입니다.
      </>
    ),
    ( // 신규 방문 고객수
      <>
        조회기간 동안 최근 6개월간내 브랜드 상품에 새로 방문한 이력이 있는 고객 데이터 입니다.
      </>
    ),
    ( // 신규 관심 고객수
      <>
        내 브랜드 상품에 새로 진입한 고객 중에서 관심 행동을 보인 고객수 입니다.
        <br />
        조회기간 동안 최근 6개월간  장바구니 담기, 찜하기 등 관심 행동 이력이 있는 고객 데이터 입니다.
      </>
    ),
    ( // 신규 주문고객수
      <>
        조회기간 동안 최근 6개월간 내 브랜드 상품에 새로 진입한
        <br />
        고객중에서 1회 주문 완료한 이력이 있는 고객수 입니다.
      </>
    ),
    ( // 신규 충성 고객수
      <>
        조회기간 동안 최근 6개월간 내 브랜드 상품에 새로 진입한
        <br />
        고객중에서 2회 이상 주문완료한 고객수 입니다.
      </>
    ),
  ];

  const RadioOnchange = ({ target }) => {
    setRadioType(target.value);
  };

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    dispatch(getBrandOptions({ params }));
    searchRef.current.clickSearch();
  }, []);

  const fetchCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const onResetSearch = () => {
    setBrand(null);
  };

  useEffect(() => {
    fetchCategory();
  }, [brand]);

  const onChangeBrand = (value) => {
    setBrand(value);
  };
  useEffect(() => {
    const tempArr = [];
    for (let i = 0; i < 5; i += 1) {
      let temNm = 0;
      switch (i) {
      case 0:
        temNm = firstRow.totalNewCust;
        break;
      case 1:
        temNm = firstRow.totalVisitCust;
        break;
      case 2:
        temNm = firstRow.totalInterestCust;
        break;
      case 3:
        temNm = firstRow.totalOrderCust;
        break;
      default:
        temNm = firstRow.totalRoyaltyCust;
        break;
      }
      tempArr.push({ amount: {
        value: temNm,
        unit: '명',
      } });
    }
    setStatisticsData(tempArr);
    setCurrentSelectedRow(firstRow.brandCode);
    setCurrentSelectedNmRow(firstRow.brandNm);
  }, [firstRow]);

  useEffect(() => {
    if (search.searchStartDate) {
      const params = {
        ptnIdx: selectPtnIdx,
        brandCode: search.brand ? search.brand.toString() : '',
        categoryLCode: search?.categoryData?.[0] ? search.categoryData[0] : '',
        categoryMCode: search?.categoryData?.[1] ? search.categoryData[1] : '',
        categorySCode: search?.categoryData?.[2] ? search.categoryData[2] : '',
        dataType: radioType,
      };

      if (search?.categoryData?.[2] === '' || search?.categoryData?.[2] === undefined || search?.categoryData?.[2] === null) {
        setChartStackedBarDisabled(true);
      } else {
        setChartStackedBarDisabled(false);
      }
      if (search.searchStartDate) {
        if (search.searchStartDate.length > 7) params.searchDateType = 'date';
        else params.searchDateType = 'month';
        setDateType(params.searchDateType);

        params.searchStartDate = search.searchStartDate;
      }
      if (search.searchEndDate) params.searchEndDate = search.searchEndDate;
      setSearchParams(params);
      dispatch(getNewCustomerList({ params }));
    }
  }, [search, radioType]);

  useEffect(() => {
    if (statisticsData?.length > 0 && searchParams.ptnIdx) {
      const params = {
        ptnIdx: searchParams.ptnIdx ? searchParams.ptnIdx : '',
        searchDateType: searchParams.searchDateType ? searchParams.searchDateType : '',
        searchStartDate: searchParams.searchStartDate ? searchParams.searchStartDate : '',
        searchEndDate: searchParams.searchEndDate ? searchParams.searchEndDate : '',
        brandCode: currentSelectedRow,
        categoryLCode: searchParams.categoryLCode ? searchParams.categoryLCode : '',
        categoryMCode: searchParams.categoryMCode ? searchParams.categoryMCode : '',
        categorySCode: searchParams.categorySCode ? searchParams.categorySCode : '',
        dataType: searchParams.dataType ? searchParams.dataType : '',
      };

      dispatch(getNewCustomerChartSpline({ params }));
      dispatch(getNewCustomerChartStackedBar({ params }));
    }
  }, [statisticsData]);

  const onClickRow = useCallback(async (getData) => {
    const tempArr = [];
    for (let i = 0; i < 5; i += 1) {
      let temNm = 0;
      switch (i) {
      case 0:
        temNm = getData.totalNewCust;
        break;
      case 1:
        temNm = getData.totalVisitCust;
        break;
      case 2:
        temNm = getData.totalInterestCust;
        break;
      case 3:
        temNm = getData.totalOrderCust;
        break;
      default:
        temNm = getData.totalRoyaltyCust;
        break;
      }
      tempArr.push({ amount: {
        value: temNm,
        unit: '명',
      } });
    }
    setStatisticsData(tempArr);
    setCurrentSelectedNmRow(getData.brandNm);
    await setCurrentSelectedRow(getData.brandCode);
    // brandNm brandCode
    currentSelectedInfoRef.current.scrollIntoView({ behavior: 'smooth' });
  });

  const conditionHighlight = (row) => {
    if (currentSelectedRow && row.brandCode === currentSelectedRow) {
      return 'currentSelectedRow';
    }
    return '';
  };

  const [chartSplineData, setChartSplineData] = useState('');

  useEffect(() => {
    if (chartSpline?.data?.categories) {
      const changeArr = chartSpline.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      const chartSplineChartData = {
        // series: chartSpline.data.series.map(({ data, name }) => ({ data, name })),
        series: changeArr,
        options: {
          chart: {
            zoom: {
              enabled: false,
            },
            height: 350,
            type: 'area',
            toolbar: {
              show: false,
            },
            events: eventYaxisIndex,
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            width: 2,
            curve: 'straight',
          },
          colors: [CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.VARIATION_6, CHART_COLORS.CUSTOMER_PURCHASE, CHART_COLORS.CUSTOMER_LOYALTY],
          xaxis: {
            type: 'string',
            categories: chartSpline.data.categories,
            labels: {
              formatter(val) {
                if (dateType === 'month') {
                  return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
                }
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
            tooltip: {
              enabled: false,
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
          legend: {
            ...legendDefault,
          },
          markers: {
            size: 3,
            strokeWidth: 1,
          },
          tooltip: {
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = chartSpline.data.categories[dataPointIndex];
                if (dateType === 'month') {
                  return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
                }
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return val && `${(val).toLocaleString()}명`;
              },
            },
          },
        },
      };
      setChartSplineData(chartSplineChartData);
    } else {
      setChartSplineData('');
    }
  }, [chartSpline]);
  const [chartStackedBarData, setChartStackedBarData] = useState('');

  useEffect(() => {
    if (chartStackedBar?.data?.categories) {
      const maxMap = chartStackedBar.data.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxData = getChartYAxisMax(maxValue);

      const chartStackedBarChartData = {
        series: chartStackedBar.data.series.map(({ data, name }) => ({ data, name })),
        options: {
          chart: {
            type: 'bar',
            height: 350,
            stacked: true,
            toolbar: {
              show: false,
            },
          },
          plotOptions: {
            bar: {
              horizontal: true,
              columnWidth: '50%',
            },
          },
          colors: [CHART_COLORS.CUSTOMER_VISIT, CHART_COLORS.VARIATION_6, CHART_COLORS.CUSTOMER_PURCHASE, CHART_COLORS.CUSTOMER_LOYALTY],
          stroke: {
            width: 0,
            colors: ['#fff'],
          },
          xaxis: {
            categories: chartStackedBar.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              formatter(val) {
                return getChartValuePeople(val);
              },
            },
          },
          fill: {
            opacity: 1,
          },
          dataLabels: {
            enabled: true,
            style: {
              fontSize: '12px',
            },
            formatter(val) {
              return `${(val).toLocaleString()}`;
            },
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          legend: {
            ...legendDefault,
          },
        } };
      setChartStackedBarData(chartStackedBarChartData);
    } else {
      setChartStackedBarData('');
    }
  }, [chartStackedBar]);

  // 엑셀다운로드
  const excelButtonOnClick = useCallback(() => {
    setButtonExcelDisabled(true);
    excelDownload('/api/statistics/customer/newActivity/excel', userInfo.accessToken, '신규 활동 고객 분석', searchParams, searchParams.searchDateType)
      .then((result) => {
        if (result === 'success') {
          setButtonExcelDisabled(false);
        } else {
          setButtonExcelDisabled(false);
        }
      });
  }, [searchParams]);

  return (
    <Container>
      <PageHeader
        title="신규 활동고객 분석"
        subTitle="내 브랜드에서 새롭게 활동을 시작한 고객을 확인할 수 있습니다."
      />
      <Search search={search} setSearch={setSearch} ref={searchRef} onReset={onResetSearch}>
        <SingleRangePicker
          column={['searchStartDate', 'searchEndDate']}
          title="조회기간"
          disabledToday
          disabled2020
          maxSelectDate={30}
          maxSelectMonth={12}
          dropdownClassName="newCustomerPickerPopup"
        />
        <SingleInputItem type="Select" column="brand" title="브랜드" width="50%" loading={brandOption.status === 'pending'} onChange={onChangeBrand} options={brandOption.data} placeholder="전체" />
        <CategorySelects column="categoryData" title="카테고리" depth={categoryOption.data.length} options={categoryOption.data} loading={categoryOption.status === 'pending'} placeholder="카테고리 선택" />

      </Search>
      <SearchResult>
        <Title>
          <p>브랜드 신규고객 조회</p>
          {listData?.status === 'success' && listData?.data.length > 0
          && (
            <div className="searchResultToolBox">
              <span className="caption-text">데이터 기준</span>
              <div className="right-item set-section-toggle">
                <Radio.Group value={radioType} buttonStyle="solid" onChange={RadioOnchange}>
                  <Radio.Button value="avg">평균</Radio.Button>
                  <Radio.Button value="sum">합계</Radio.Button>
                </Radio.Group>
              </div>
              <ButtonExcel disabled={buttonExcelDisabled} onClick={excelButtonOnClick} />
            </div>
          )}
        </Title>
        <div className="loadingDiv">
          <LoadingComponent isLoading={listData?.status === 'pending'} />
          {listData?.status === 'success' && listData?.data.length > 0
            ? (
              <Table
                columns={columns}
                dataSource={listData.data}
                onClickRow={onClickRow}
                conditionalHighlight={conditionHighlight}
                scrollY={248}
              />
            ) : <EmptyList warningSubTitle="조회 기준을 다시 설정해보세요." /> }
        </div>
      </SearchResult>
      <Description text={['브랜드명을 클릭 하시면 , 하단에  GS SHOP과 브랜드의 전년대비 성장률을 비교한 그래프가 제공됩니다.']} />

      {listData?.status === 'success' && listData?.data.length > 0
      && (
        <PageSection ref={currentSelectedInfoRef}>
          {
            listData?.status === 'success' && (
              <PaperTitle>
                <h3>
                  {currentSelectedNmRow}
                  의 신규 활동고객 분석 결과
                </h3>
              </PaperTitle>
            )
          }
          <SectionDetail>
            {
              (listData?.status === 'success' && statisticsData?.length > 0) && (
                <FixStatistics data={statisticsData} title={FixTitle} tooltip={TooltipText} />
              )
            }
            <ReusltCharts listData={listData} chartSpline={chartSpline} chartSplineData={chartSplineData} chartStackedBarDisabled={chartStackedBarDisabled} chartStackedBar={chartStackedBar} chartStackedBarData={chartStackedBarData} />
          </SectionDetail>
        </PageSection>
      ) }
    </Container>
  );
}

const Container = styled(PageLayout)`

`;
const SearchResult = styled(Paper)`
  margin-top: 40px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;
  table  {
    col:nth-of-type(1),
    tr th:nth-of-type(1),
    tr td:nth-of-type(1) {
      width: 25% !important;
    }
    col:nth-of-type(2),
    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      width: 15% !important;
    }
    col:nth-of-type(3),
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      width: 15% !important;
    }
    col:nth-of-type(4),
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      width: 15% !important;
    }
    col:nth-of-type(5),
    tr th:nth-of-type(5),
    tr td:nth-of-type(5) {
      width: 15% !important;
    }
    col:nth-of-type(6),
    tr th:nth-of-type(6),
    tr td:nth-of-type(6) {
      width: 15% !important;
    }
  }
`;

const Title = styled.div`
  display:flex;
  font-weight: 700;
  p{
    padding: 20px 0px;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .right{
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
  }
  .caption-text{
    color: var(--color-gray-500);
    margin-right: 5px;
    font-weight: 400;
    font-size: 12px;
  }
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
`;
const PageSection = styled.div`
    margin-top: 50px;
    background-color: #FFF;
    border-radius: 8px;
    padding: 20px 20px 0;
    border: 1px solid #e3e4e7;

`;
const SectionDetail = styled.div`
  .detailItem:last-child{
    border-top: 1px solid #e3e4e7;
  }
`;

const PaperTitle = styled.div`
  display: flex;
  h3 {
    flex:1 1 auto;
    padding-bottom: 18px;
    font-size: 20px;
    line-height: 30px;
    font-weight: 700;
  }
  div {
    text-align:right;
    flex:1 1 auto;
  }
`;

export default NewCustomerContainer;
