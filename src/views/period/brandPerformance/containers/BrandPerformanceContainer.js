import { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { useDispatch, useSelector } from 'react-redux';

import { Radio, List, Tabs } from 'antd';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import CategorySelects from '../../../../components/search/CategorySelects';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';
import TabButton from '../components/TabButton';
import { getActivityStageTrans, getAllAciveCustomer, getBrandOptions, getCategoryOptions, getCustomerNumber, getLastYearOrder, getStatistics, getTotalActivityStage, getTotalOrder } from '../redux/slice';
import ActiveCustomer from '../components/activeCustomer';
import CustomerNumber from '../components/customerNumber';
import TotalOrder from '../components/totalOrder';

import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';

const { TabPane } = Tabs;
const TooltipText = [
  ( // 활동고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에서
      <br />
      활동(방문, 관심, 주문, 충성)한 고객수 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 전체 활동고객수 - 전월 전체 활동고객수)/전월 전체 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 전체 활동고객수 - 전년도 전체 활동고객수)/전년도 전체 활동고객수 * 100%
    </>
  ),
  ( // 신규 고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품을 새롭게 방문한 고객이
      <br />
      활동(신규방문, 신규관심, 신규주문, 신규충성)한 고객수 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 신규 활동고객수 - 전월 신규 활동고객수)/전월 신규 활동고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 신규  활동고객수 - 전년도 신규 활동고객수)/전년도 신규 활동고객수 * 100%
    </>
  ),
  ( // 주문고객수
    <>
      조회기간 동안 내 브랜드 상품을 주문한 고객수(합계/평균) 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 주문고객수 - 전월 주문고객수)/전월 주문고객수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 주문고객수 - 전년도 주문고객수)/전년도 주문고객수 * 100%
    </>
  ),
  ( // 주문 금액
    <>
      조회기간 동안 내 브랜드에서 주문완료한 주문 금액(합계/평균)입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 주문금액 - 전월 주문금액)/전월 주문금액 * 100%
      <br />
      - 전년대비 성장률=(올해년도 주문금액 - 전년도 주문금액)/전년도 주문금액 * 100%
    </>
  ),
  ( // 주문수
    <>
      조회기간 동안 내 브랜드 상품을 주문한 건 수(합계/평균) 입니다.
      <br />
      <br />
      전월/전년 대비 성장률은 아래와 같이 계산됩니다.
      <br />
      - 전월대비 성장률=(이번달 주문수 - 전월 주문수)/전월 주문수 * 100%
      <br />
      - 전년대비 성장률=(올해년도 주문수 - 전년도 주문수)/전년도 주문수 * 100%
    </>
  ),
];

function BrandPerformanceContainer() {
  // const { userInfo } = useSelector((state) => state.common);
  const { selectPtnIdx } = useSelector((state) => state.common);
  const { categoryOption, brandOption } = useSelector((state) => ({
    categoryOption: state.marketing.customerPerformance.categoryOption,
    brandOption: state.marketing.customerPerformance.brandOption,
  }));

  const { statistics } = useSelector((state) => ({
    statistics: state.marketing.customerPerformance.statistics,
  }));

  const [dataType, setDataType] = useState('avg');
  const [tabIdx, setTabIdx] = useState('1');
  const [search, setSearch] = useState({});
  const [detailOnOff, setDetailOnOff] = useState('on');
  const [brand, setBrand] = useState(null);

  const dispatch = useDispatch();
  const searchRef = useRef();

  useEffect(() => {
    dispatch(getBrandOptions({ params: {
      ptnIdx: selectPtnIdx,
    } }));
    searchRef.current.clickSearch();
  }, []);

  useEffect(() => {
    getCategory();
  }, [brand]);

  useEffect(() => {
    if (Object.keys(search).length === 0) return;
    const params = {
      ptnIdx: selectPtnIdx,
      searchStartDate: search.termStartAt ? search.termStartAt : '',
      searchEndDate: search.termEndAt ? search.termEndAt : '',
      brandCode: search.brand ? search.brand : '',
      dataType,
    };

    if (search.catData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.catData;
      if (categorySCode) params.categorySCode = categorySCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categoryLCode) params.categoryLCode = categoryLCode;
    }

    // 브랜드 성과 분석
    dispatch(getStatistics({ params }));
    // 전체활동고객 그래프
    dispatch(getAllAciveCustomer({ params }));
    // 전년 동기간 주문금액 그래프
    dispatch(getLastYearOrder({ params }));
    // 활동 단계별 고객수
    dispatch(getTotalActivityStage({ params }));
    // 활동 단계별 전환 고객수
    dispatch(getActivityStageTrans({ params }));
    // 신규 활동고객
    dispatch(getCustomerNumber({ params }));
    dispatch(getTotalOrder({ params }));
  }, [search, selectPtnIdx, dataType]);

  const getCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const handleDataTypeChange = (value) => {
    setDataType(value.target.value);
  };

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  const onReset = () => {
    getCategory();
  };

  return (
    <Container>
      <PageHeader
        title="브랜드 성과 분석"
        subTitle="행사/마케팅/프로모션 기간의 매출과 고객수 변화를 확인할 수 있습니다."
      />

      <Search detailOnOff={false} setSearch={setSearch} ref={searchRef} setDetailOnOff={setDetailOnOff} onReset={onReset}>
        <SingleRangePicker
          column={['termStartAt', 'termEndAt']}
          title="조회기간"
          dropdownClassName="brandPerformanceRangePickerPopup"
          showDateType={false}
          search={search}
          setSearch={setSearch}
          disabledToday
          maxSelectDate={90}
          disabled2020
        />
        <SingleInputItem
          type="Select"
          column="brand"
          title="브랜드"
          width="50%"
          loading={brandOption.status === 'pending'}
          options={brandOption.data}
          onChange={onChangeBrand}
          placeholder="전체"
        />
        <CategorySelects
          column="catData"
          title="카테고리"
          detailOnOff={detailOnOff}
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />

      </Search>

      <SearchResult>
        <Title>
          <p>
            브랜드 성과 분석 결과
          </p>
          {
            statistics.status === 'success' && (
              <div className="searchResultToolBox">
                <span className="caption-text">데이터 기준</span>
                <div className="right-item set-section-toggle">
                  <Radio.Group value={dataType} buttonStyle="solid" onChange={handleDataTypeChange}>
                    <Radio.Button value="avg">평균</Radio.Button>
                    <Radio.Button value="sum">합계</Radio.Button>
                  </Radio.Group>
                </div>
                {/* <ButtonExcel /> */}
              </div>
            )
          }
        </Title>
        <Contents>
          <LoadingComponent isLoading={statistics.status === 'pending'} />
          {
            statistics.status === 'success' ? (
              <List
                className="brandPerformanceList"
                grid={{ gutter: 10, column: 5 }}
                dataSource={statistics.data ? statistics.data : []}
                renderItem={({
                  title, amount, changeRateByMonth, changeRateByMonthPercent, changeRateByYear, changeRateByYearPercent,
                }, index) => (
                  <List.Item>
                    <TabButton
                      title={title}
                      amount={amount}
                      changeRateByMonth={changeRateByMonth}
                      changeRateByMonthPercent={changeRateByMonthPercent}
                      changeRateByYear={changeRateByYear}
                      changeRateByYearPercent={changeRateByYearPercent}
                      TooltipText={TooltipText[index]}
                      index={index}
                    />
                  </List.Item>
                )}
              />
            ) : <EmptyGraph />
          }
          {/* {
            statistics.status === 'pending' && (
              <div style={{ height: '100px' }} />
            )
          } */}
          <LoadingComponent isLoading={statistics.status === 'pending'} />
        </Contents>
        <Tabs defaultActiveKey={tabIdx}>
          <TabPane tab="전체 활동고객" key="1">
            <span className="chart-span">내 브랜드 고객활동의 특성과 효과를 확인해보세요.</span>
            <ActiveCustomer />
          </TabPane>
          <TabPane tab="신규 활동고객" key="2">
            <span className="chart-span">내 브랜드 신규 활동고객과 주문고객수의 추이를 확인해보세요.</span>
            <CustomerNumber />
          </TabPane>
          <TabPane tab="주문고객" key="3">
            <span className="chart-span">내 브랜드의 주문 실적을 확인해보세요.</span>
            <TotalOrder />
          </TabPane>
        </Tabs>
      </SearchResult>
    </Container>
  );
}

const Container = styled(PageLayout)`
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }

  .ant-tabs-nav {
    margin-right: 20px;
    margin-left: 20px;
  }
  .ant-tabs-nav:before {
    content: none !important;
  }
  .ant-tabs-ink-bar{
    display: none;
  }
  .ant-tabs-nav-list{
    flex: 1;
    background-color: #f7f8fa;
    border-radius: 4px;
  }
  .ant-tabs-tab-btn{
    width: 100%;
    height: 100%;
    text-align: center;
  }
  .ant-tabs-tab{
    display: flex !important;
    padding:0px;
    height: 40px;
    line-height: 40px; 
    margin: 0px;
    justify-content: center;
    flex: 1;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
    border-radius: 4px;
    background-color: var(--color-steelGray-800);
    color: #fff;
  }
  .ant-list-grid .ant-col>.ant-list-item {
    margin-bottom: 20px;
  }
  .brandPerformanceList {
    .ant-row > div {
      width: auto !important;
      max-width: none !important;
      flex:1 1 auto !important;
    }
  }
`;

const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px 20px;
`;

const Title = styled.div`
    padding: 20px 0px 24px;
    color: var(--color-gray-900);
    font-weight: 700;
    font-size: 20px;
    .right{
      display: flex;
      align-items: center;
      justify-content: flex-end;
      flex: 1;
      }
  
      .caption-text {
      color: var(--color-gray-500);
      margin-right: 5px;
      font-weight: 400;
      font-size: 12px;
    }
`;

const Contents = styled.div`
  position: relative;
  padding: 20px 0 0;
  border-top: var(--border-default);
  border-bottom: var(--border-default);
  margin-bottom: 24px;
  min-height: 180px;
`;

export default BrandPerformanceContainer;
