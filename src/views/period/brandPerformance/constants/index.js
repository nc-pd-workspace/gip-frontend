export const brandOptions = [
  { label: '나이키', value: 'nike' },
  { label: '아디다스', value: 'adidas' },
  { label: '언더아머', value: 'underarmor' },
];

export const defaultCategoryOptions = [
  [
    { label: '아디다스', value: 'adidas' },
    { label: '나이키', value: 'nike' },
    { label: '언더아머', value: 'underarmor' },
  ],
  [
    { label: '운동화-01', value: 'adidas01', parent: 'adidas' },
    { label: '운동화-02', value: 'adidas02', parent: 'adidas' },
    { label: '운동화-03', value: 'adidas03', parent: 'adidas' },
    { label: '신발-01', value: 'nike01', parent: 'nike' },
    { label: '신발-02', value: 'nike02', parent: 'nike' },
    { label: '신발-03', value: 'nike03', parent: 'nike' },
    { label: '가방-01', value: 'underarmor01', parent: 'underarmor' },
    { label: '가방-02', value: 'underarmor02', parent: 'underarmor' },
    { label: '가방-03', value: 'underarmor03', parent: 'underarmor' },
  ],
];
