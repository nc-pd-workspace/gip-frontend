import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';
import moment from 'moment';

import { CHART_COLORS } from '../../../../../styles/chartColors';
import LoadingComponent from '../../../../../components/loading';
import EmptyGraph from '../../../../../components/emptyGraph';
import { getChartValueMoney, getChartValuePeople, getChartValueUnit, getChartYAxisMax } from '../../../../../utils/utils';
import { legendDefault } from '../../../../../utils/chartOptions';

function TotalOrder(props) {
  const { totalOrder } = useSelector((state) => ({
    totalOrder: state.marketing.customerPerformance.totalOrder,
  }));

  const [totalOrderChart, setTotalOrderChart] = useState();

  useEffect(() => {
    if (totalOrder.status === 'success' && totalOrder.data) {
      const changeArr = [...totalOrder.data.series];
      changeArr.unshift({ data: changeArr[0].data.map(() => (null)), name: '', type: 'line' });

      setTotalOrderChart({
        series: changeArr,
        options: {
          colors: [
            CHART_COLORS.VARIATION_19,
            CHART_COLORS.CUSTOMER_ORDER,
            CHART_COLORS.VARIATION_23,
            CHART_COLORS.VARIATION_19,
          ],
          chart: {
            toolbar: {
              show: false,
            },
            zoom: {
              enabled: false,
            },
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            width: [0, 0, 0, 2],
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = totalOrder.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: [
              {
                formatter(val) {
                  return null;
                },
              },
              {
                formatter(val) {
                  if (val === 0) {
                    return '0명';
                  }
                  if (val) {
                    return `${(val).toLocaleString()}명`;
                  }
                  return null;
                },
              },
              {
                formatter(val) {
                  if (val === 0) {
                    return '0건';
                  }
                  if (val) {
                    return `${(val).toLocaleString()}건`;
                  }
                  return null;
                },
              },
              {
                formatter(val) {
                  if (val === 0) {
                    return '0원';
                  }
                  if (val) {
                    return `${(val).toLocaleString()}원`;
                  }
                  return null;
                },
              },
            ],
          },
          legend: {
            ...legendDefault,
          },
          xaxis: {
            categories: totalOrder.data.categories,
            axisTicks: { show: false },
            tooltip: {
              enabled: false,
            },
            labels: {
              show: true,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: [
            {
              labels: {
                show: false,
              },
            },
            {
              max: (max) => getChartYAxisMax(max),
              min: 0,
              labels: {
                style: {
                  cssClass: 'yaxis-label-CUSTOMER_ORDER',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValuePeople(val);
                },
              },
              tickAmount: 5,
              axisBorder: {
                show: true,
                color: CHART_COLORS.CUSTOMER_ORDER,
              },
              title: {
                text: '주문고객수(명)',
                style: {
                  color: CHART_COLORS.CUSTOMER_ORDER,
                },
              },
            },
            {
              opposite: true,
              max: (max) => getChartYAxisMax(max),
              min: 0,
              labels: {
                style: {
                  cssClass: 'yaxis-label-VARIATION_23',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValueUnit(val);
                },
              },
              tickAmount: 5,
              axisBorder: {
                show: true,
                color: CHART_COLORS.VARIATION_23,
                offsetX: -6,
              },
              title: {
                text: '주문수(건)',
                style: {
                  color: CHART_COLORS.VARIATION_23,
                },
                offsetX: -2,
              },
            },
            {
              opposite: true,
              max: (max) => getChartYAxisMax(max),
              min: 0,
              labels: {
                style: {
                  cssClass: 'yaxis-label-VARIATION_19',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValueMoney(val);
                },
              },
              tickAmount: 5,
              axisBorder: {
                show: true,
                color: CHART_COLORS.VARIATION_19,
                offsetX: -6,
              },
              title: {
                text: '주문 금액(원)',
                style: {
                  color: CHART_COLORS.VARIATION_19,
                },
                offsetX: -2,
              },
            },
          ],
        },
      });
    }
  }, [totalOrder]);

  return (
    <Container>
      <ChartContainer>
        <div className="chart-area apexcharts-legend-first-none">
          <LoadingComponent isLoading={totalOrder.status === 'pending'} />
          {
            totalOrderChart ? (
              <ReactApexChart options={totalOrderChart.options} series={totalOrderChart.series} type="line" height="350" />
            ) : (
              <div style={{ justifyContent: 'center' }}>
                <EmptyGraph />
              </div>
            )
          }
        </div>
      </ChartContainer>
    </Container>
  );
}

const Container = styled.div`

`;

const ChartContainer = styled.div`
  display: flex;
  margin-top: 10px;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 

  .chart-area{
    position: relative;
  /* height: 252px; */
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
    border-radius: 4px;
    padding: 20px;
  & > p {
    font-size: 14px;
    font-weight: 700;
  }
  .count {
    font-size: 20px;
    font-weight: 700;
  }
  .percent {
    line-height: 31px
  }
  & > div {
    display: flex;
  }
}

.tag-area{
  height: 400px;
  width: 100%;
  position: relative;
}
`;

export default TotalOrder;
