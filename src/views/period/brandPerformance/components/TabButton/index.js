/* eslint-disable no-nested-ternary */
import React from 'react';
import styled from 'styled-components';
import isNil from 'lodash-es/isNil';

import ToolTip from '../../../../../components/toolTip';
import { SvgNone } from '../../../../../Images';
import { COLORS } from '../../../../../styles/Colors';

function addSign(data) {
  if (isNil(data)) {
    return <div><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></div>;
  }
  return data.toLocaleString('ko-KR');
}

function PlusMinus({ data, unit }) {
  // eslint-disable-next-line no-nested-ternary
  const className = (data > 0) ? 'plus' : (data < 0) ? 'minus' : '';
  const status = (data > 0) ? '+' : '';

  return (
    <PlusMinusBox className={className}>
      (
      {status}
      {addSign(data)}
      {unit}
      )
    </PlusMinusBox>
  );
}

function TabButton({
  title, amount, changeRateByMonth, changeRateByMonthPercent, changeRateByYear, changeRateByYearPercent, TooltipText, index,
}) {
  const setPercentToPlus = (data) => {
    if (isNil(data)) {
      return '';
    }
    return Math.abs(data);
  };

  return (
    <Container>
      <Title>
        {title}
        <ToolTip text={TooltipText} position={(index >= 3) ? 'right' : false} />
      </Title>
      <Total>
        {(amount.value).toLocaleString('ko-KR')}
        <div className="unit">{amount.unit}</div>
      </Total>

      <ChangeRate>
        <div className="item">
          <div className="text">
            전월대비
          </div>
          <div className="inner-item">
            {
              isNil(changeRateByMonthPercent) ? (
                <>
                  <div style={{ display: 'flex' }}>
                    <div><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></div>
                    {setPercentToPlus(changeRateByMonthPercent)}
                  </div>
                </>
              )
                : (
                  <>
                    <div style={{ display: changeRateByMonthPercent === 0 ? 'flex' : '' }} className={changeRateByMonthPercent >= 0 ? (changeRateByMonthPercent === 0 ? 'zero' : 'arrow up') : 'arrow down'}>
                      {
                        changeRateByMonthPercent === 0 ? <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
                          : ''
                      }
                      {setPercentToPlus(changeRateByMonthPercent)}
                      %
                    </div>
                  </>
                )
            }
            {
              isNil(changeRateByMonthPercent) ? '' : (
                <>
                  <PlusMinus data={changeRateByMonth} unit={amount.unit} />
                </>
              )
            }
          </div>
        </div>
        <div className="item">
          <div className="text">
            전년대비
          </div>
          <div className="inner-item">
            {
              isNil(changeRateByYearPercent) ? (
                <>
                  <div style={{ display: 'flex' }}>
                    <div><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></div>
                    {setPercentToPlus(changeRateByYearPercent)}
                  </div>
                </>
              )
                : (
                  <>
                    <div style={{ display: changeRateByYearPercent === 0 ? 'flex' : '' }} className={changeRateByYearPercent >= 0 ? (changeRateByYearPercent === 0 ? 'zero' : 'arrow up') : 'arrow down'}>
                      {
                        changeRateByYearPercent === 0 ? <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
                          : ''
                      }
                      {setPercentToPlus(changeRateByYearPercent)}
                      %
                    </div>
                  </>
                )
            }
            {
              isNil(changeRateByYearPercent) ? '' : (
                <>
                  <PlusMinus data={changeRateByYear} unit={amount.unit} />
                </>
              )
            }
          </div>
        </div>
      </ChangeRate>
    </Container>
  );
}

const Container = styled.div`
  padding-left: 20px;
  position: relative;
  &::before {
    content: '';
    position:absolute;
    top: 0;
    bottom: 0;
    left: 0;
    height: 115px;
    width: 1px;
    background: var(--color-gray-200);
    margin: auto;
    .ant-row > div:first-child & {
      display:none;
    }
  }
`;

const Title = styled.div`
  position: relative;
  color:var(--color-gray-900);
  font-size: 13px;
  line-height: 18px;
  font-weight: 700;
  margin-bottom: 6px;
  .circle{
    padding-left: 2px;
  }
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  color: var(--color-gray-900);
  font-size: 24px;
  font-weight: 700;
  line-height: 36px;
  letter-spacing: -.75px;
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 18px;
    padding-bottom: 7px;
    margin-left: 2px;
  }
`;

const ChangeRate = styled.div`
//   display: flex;

  flex-direction: column;

  align-items: center;
  margin-top: 2px;
  .item {
    // display: flex;
    flex-direction: column;
    // align-items: center;
    font-size: 12px;
    font-weight: 400;
  }
  .text{
    color: var(--color-gray-700);
  }
  .arrow{
    margin-left: 4px;
    &.down{
      color: #e05a42;
    }
    &.down::before {
      content: "";
      display: inline-block;
      width: 0;
      height: 0;
      margin-right: 4px;
      border-radius: 2px;
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-bottom: 4px solid #e05a42;
      vertical-align: middle;
      transform: rotate(180deg);
    }
    &.up{
      color: var(--color-blue-500);
    }
    &.up::before {
      content: "";
      display: inline-block;
      width: 0;
      height: 0;
      margin-right: 4px;
      border-radius: 2px;
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-bottom: 4px solid var(--color-blue-500);
      vertical-align: middle;
    }
    &.zero::before {
      margin-left: 4px;
    }
  }
  .inner-item {
    display: flex;
  }
`;

const PlusMinusBox = styled.span`
  margin-left: 4px;
  &.equal{
    color: var(--color-gray-500);
  }
  &.minus{
    color: var(--color-red-500);
  }
  &.plus{
    color: var(--color-blue-500);
  }
`;

export default TabButton;
