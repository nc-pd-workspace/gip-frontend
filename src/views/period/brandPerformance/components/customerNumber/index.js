import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import { useSelector } from 'react-redux';

import ReactApexChart from 'react-apexcharts';

import { CHART_COLORS } from '../../../../../styles/chartColors';
import LoadingComponent from '../../../../../components/loading';
import EmptyGraph from '../../../../../components/emptyGraph';
import { getChartValuePeople, getChartYAxisMax } from '../../../../../utils/utils';
import { legendDefault } from '../../../../../utils/chartOptions';

function CustomerNumber() {
  const { customerNumber } = useSelector((state) => ({
    customerNumber: state.marketing.customerPerformance.customerNumber,
  }));

  const [customerNumberChart, setCustomerNumberChart] = useState();

  useEffect(() => {
    if (customerNumber.status === 'success' && customerNumber.data) {
      const changeArr = [...customerNumber.data.series];
      changeArr.unshift({ data: changeArr[0].data.map(() => (null)), name: '', type: 'line' });

      setCustomerNumberChart({
        series: changeArr,
        colors: [
          CHART_COLORS.CUSTOMER_NEW, CHART_COLORS.CUSTOMER_NEW, CHART_COLORS.CUSTOMER_ORDER,
        ],
        options: {
          chart: {
            height: 350,
            type: 'line',
            zoom: {
              enabled: false,
            },
            dropShadow: {
              enabled: false,
            },
            dataLabels: {
              enabled: false,
            },
            toolbar: {
              show: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.customerNumberChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
              },
            },
          },
          stroke: {
            curve: 'straight',
            width: 3,
          },
          colors: [CHART_COLORS.CUSTOMER_NEW, CHART_COLORS.CUSTOMER_NEW, CHART_COLORS.CUSTOMER_ORDER],
          // dataLabels: {
          //   enabled: false,
          //   formatter(val) {
          //     return getChartValuePeople(val);
          //   },
          // },
          // grid: {
          //   borderColor: '#E3E4E7',
          //   row: {
          //     colors: ['transparent'],
          //   },
          // },
          xaxis: {
            categories: customerNumber.data.categories,
            axisTicks: { show: false },
            tooltip: {
              enabled: false,
            },
            labels: {
              show: true,
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tooltip: {
              enabled: false,
            },
            tickAmount: 5,
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = customerNumber.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: {
              formatter(val) {
                return val && `${(val).toLocaleString()}명`;
              },
            },
            // y: [
            //   {
            //     formatter(val) {
            //       if (val === 0) {
            //         return '0명';
            //       }
            //       if (val) {
            //         return `${(val).toLocaleString()}명`;
            //       }
            //       return '';
            //     },
            //   },
            //   {
            //     formatter(val) {
            //       if (val === 0) {
            //         return '0명';
            //       }
            //       if (val) {
            //         return `${(val).toLocaleString()}명`;
            //       }
            //       return '';
            //     },
            //   },
            // ],
          },
          legend: {
            ...legendDefault,
          },
        },
      });
    }
  }, [customerNumber]);

  return (
    <Container>
      <ChartContainer>
        <div className="chart-area apexcharts-legend-first-none ">
          <LoadingComponent isLoading={customerNumber.status === 'pending'} />
          {
            customerNumberChart ? (
              <ReactApexChart options={customerNumberChart.options} series={customerNumberChart.series} type="line" height="350" className="customerNumberChart" />
            )
              : (
                <div style={{ justifyContent: 'center' }}>
                  <EmptyGraph />
                </div>
              )
          }
        </div>
      </ChartContainer>
    </Container>
  );
}

const Container = styled.div`

`;

const ChartContainer = styled.div`
  display: flex;
  margin-top: 10px;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 
  .chart-area{
    position: relative;
    /* height: 252px; */
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
    border-radius: 4px;
    padding: 20px;
  & > p {
    font-size: 14px;
    font-weight: 700;
  }
  .count {
    font-size: 20px;
    font-weight: 700;
  }
  .percent {
    line-height: 31px
  }
  & > div {
    display: flex;
  }
}

.tag-area{
  height: 400px;
  width: 100%;
  position: relative;
}
`;

export default CustomerNumber;
