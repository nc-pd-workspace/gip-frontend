/* eslint-disable react/no-unstable-nested-components */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

import { useSelector } from 'react-redux';

import { isNil } from 'lodash-es';

import LoadingComponent from '../../../../../components/loading';
import { CHART_COLORS } from '../../../../../styles/chartColors';
import EmptyGraph from '../../../../../components/emptyGraph';
import { SvgArrowDown, SvgArrowUp, SvgNone } from '../../../../../Images';
import { getChartValueMoney, getChartValuePeople, getChartYAxisMax } from '../../../../../utils/utils';
import { COLORS } from '../../../../../styles/Colors';

function ActiveCustomer() {
  const { statistics, allAciveCustomer, lastYearOrder, totalActivityStage, activityStageTrans } = useSelector((state) => ({
    statistics: state.marketing.customerPerformance.statistics,
    allAciveCustomer: state.marketing.customerPerformance.allAciveCustomer,
    lastYearOrder: state.marketing.customerPerformance.lastYearOrder,
    totalActivityStage: state.marketing.customerPerformance.totalActivityStage,
    activityStageTrans: state.marketing.customerPerformance.activityStageTrans,
  }));

  const [allAciveCustomerChart, setAllAciveCustomerChart] = useState();
  const [lastYearOrderChart, setLastYearOrderChart] = useState();
  const [totalActivityStageChart, setTotalActivityStageChart] = useState();
  const [activityStageTransChart, setActivityStageTransChart] = useState();
  const [lastYearOrderCount, setLastYearOrderCount] = useState();

  useEffect(() => {
    if (allAciveCustomer.status === 'success' && allAciveCustomer.data) {
      setAllAciveCustomerChart({
        series: [{
          data: allAciveCustomer.data.series[0].data,
          // data: [0, null, 1],
          name: '',
        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          colors: [CHART_COLORS.CUSTOMER_ACTIVITY],
          plotOptions: {
            bar: {
              columnWidth: '45%',
              distributed: true,
            },
          },
          tooltip: {
            x: {
              formatter(val) {
                return val;
              },
            },
            y: {
              formatter(val) {
                if (isNil(val)) {
                  return '-';
                }
                return `${(val).toLocaleString()}명`;
              },
            },
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: true,
          },
          dataLabels: {
            enabled: false,
          },
          legend: {
            show: false,
          },
          xaxis: {
            categories: allAciveCustomer.data.categories,
            labels: {
              style: {
                fontSize: '12px',
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
          },
        },
      });
    }
  }, [allAciveCustomer]);

  useEffect(() => {
    if (lastYearOrder.status === 'success' && lastYearOrder.data) {
      setLastYearOrderCount(lastYearOrder.data.series[0].data[0]);
      setLastYearOrderChart({

        series: [{
          data: lastYearOrder.data.series[0].data,
          // data: [0, 0, 0],
          name: '',

        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          colors: [CHART_COLORS.CUSTOMER_ACTIVITY],
          plotOptions: {
            bar: {
              columnWidth: '45%',
              distributed: true,
              dataLabels: {
              },
            },
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: true,
            x: {
              formatter(val) {
                return val;
              },
            },
            y: {
              formatter(val) {
                if (isNil(val)) {
                  return '-';
                }
                return `${(val).toLocaleString()}원`;
              },
            },
          },
          dataLabels: {
            enabled: false,
          },
          legend: {
            show: false,
          },
          xaxis: {
            categories: lastYearOrder.data.categories,
            labels: {
              style: {
                fontSize: '12px',
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValueMoney(val);
              },
            },
          },
        },
      });
    }
  }, [lastYearOrder]);

  useEffect(() => {
    if (totalActivityStage.status === 'success' && totalActivityStage.data) {
      setTotalActivityStageChart({
        series: [{
          data: totalActivityStage.data.series[0].data,
          name: '',

        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          colors: [CHART_COLORS.CUSTOMER_VISIT],
          plotOptions: {
            bar: {
              columnWidth: '45%',
              distributed: true,
              dataLabels: {
                position: 'top', // top, center, bottom
              },
            },
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: true,
            x: {
              formatter(val) {
                return val;
              },
            },
            y: {
              formatter(val) {
                return `${val.toLocaleString('ko-KR')}%`;
              },
            },
          },
          dataLabels: {
            enabled: false,
            formatter(val) {
              return `${val}%`;
            },
            offsetY: -20,
            style: {
              fontSize: '12px',
              colors: ['#304758'],
            },
          },
          legend: {
            show: false,
          },
          xaxis: {
            categories: totalActivityStage.data.categories,
            labels: {
              style: {
              // colors,
                fontSize: '12px',
              },
            },
          },
          yaxis: {
            axisBorder: {
              show: false,
            },
            axisTicks: {
              show: false,
            },
            tickAmount: 5,
            min: 0,
            max: 100,
            decimalsInFloat: Number,
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return `${Math.abs(Math.round(val))}%`;
              },
            },
          },
        },
      });
    }
  }, [totalActivityStage]);

  useEffect(() => {
    if (activityStageTrans.status === 'success' && activityStageTrans.data) {
      setActivityStageTransChart({
        series: [
          {
            name: activityStageTrans.data.series[2].name,
            type: 'bar',
            data: activityStageTrans.data.series[2].data,
          },
          {
            name: activityStageTrans.data.series[1].name,
            type: 'bar',
            data: activityStageTrans.data.series[1].data,
          },
          {
            name: activityStageTrans.data.series[0].name,
            type: 'bar',
            data: activityStageTrans.data.series[0].data,
          },
        ],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          colors: [CHART_COLORS.COMPARE_GS_SHOP, CHART_COLORS.CUSTOMER_INTERESTED, CHART_COLORS.CUSTOMER_VISIT],
          dataLabels: {
            enabled: false,
          },
          legend: {
            show: false,
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            y: [
              {
                formatter(val) {
                  return `${(val).toLocaleString()}명`;
                },
              },
              {
                formatter(val) {
                  return `${(val).toLocaleString()}명`;
                },
              },
              {
                formatter(val) {
                  return `${(val).toLocaleString()}명`;
                },
              },
            ],
          },
          xaxis: {
            categories: activityStageTrans.data.categories,
            labels: {
              style: {
              // colors,
                fontSize: '12px',
              },
            },
          },
          yaxis: {
            axisBorder: {
              show: false,
            },
            axisTicks: {
              show: false,
            },
            tickAmount: 5,
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
          },
        },
      });
    }
  }, [activityStageTrans]);

  function YearArrow({ yaearArrowData }) {
    if (yaearArrowData === 0) {
      return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
        </div>
      );
    }
    if (isNil(yaearArrowData)) {
      return '';
    }
    return (
      <>
        {yaearArrowData > 0
          ? <SvgArrowUp style={{ marginTop: '7px' }} width="16" height="16" fill="#0091FF" />
          : <SvgArrowDown style={{ marginTop: '7px' }} width="16" height="16" fill="#FF6C63" /> }
      </>
    );
  }

  function yearPercent(CustomerData) {
    if (CustomerData && CustomerData.data) {
      if (isNil(CustomerData.data.totalActiveCustComparedYear)) {
        return (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
          </div>
        );
      }
      return CustomerData.data.totalActiveCustComparedYear;
    }
    return '';
  }

  function nullCheck(data) {
    if (isNil(data)) {
      return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
        </div>
      );
    }
    return data;
  }

  return (
    <Container>
      <ChartContainer>
        <div className="chart-area loadingDiv">
          <p className="chart-title">
            전체 활동고객수
          </p>
          <LoadingComponent isLoading={allAciveCustomer.status === 'pending'} />
          {
            allAciveCustomer.status === 'success' && (
              <div>
                <p className="chart-title count">
                  {nullCheck(allAciveCustomer && allAciveCustomer.data ? allAciveCustomer.data.series[0].data[2].toLocaleString('ko-KR') : '')}
                </p>
                <p className="chart-title percent">
                  명 (전년대비
                  <YearArrow yaearArrowData={allAciveCustomer && allAciveCustomer.data ? allAciveCustomer.data.totalActiveCustComparedYear : ''} />
                  {
                    isNil(allAciveCustomer.data.totalActiveCustComparedYear)
                      ? (
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
                        </div>
                      )
                      : (
                        <YearArrowP yaearArrowData={allAciveCustomer && allAciveCustomer.data ? allAciveCustomer.data.totalActiveCustComparedYear : ''}>
                          {yearPercent(allAciveCustomer)}
                          %
                        </YearArrowP>
                      )
                  }
                  )
                </p>
              </div>
            )
          }
          {
            allAciveCustomerChart
              ? (
                <ReactApexChart
                  options={allAciveCustomerChart.options}
                  series={allAciveCustomerChart.series}
                  type="bar"
                  height="200"
                />
              )
              : (
                <div style={{ justifyContent: 'center' }}>
                  <EmptyGraph />
                </div>
              )
          }
        </div>
        <div className="chart-area loadingDiv">
          <p className="chart-title">
            주문 금액
          </p>
          <LoadingComponent isLoading={lastYearOrder.status === 'pending'} />
          {
            lastYearOrder.status === 'success' && (
              <div>
                <p className="chart-title count">
                  {nullCheck(lastYearOrder && lastYearOrder.data ? lastYearOrder.data.series[0].data[2].toLocaleString('ko-KR') : '')}
                </p>
                <p className="chart-title percent">
                  원 (전년대비
                  <YearArrow yaearArrowData={lastYearOrder.data ? lastYearOrder.data.totalOrderAmountComparedYear : ''} />
                  {
                    isNil(lastYearOrder.data.totalOrderAmountComparedYear)
                      ? (
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
                        </div>
                      )
                      : (
                        <YearArrowP yaearArrowData={lastYearOrder && lastYearOrder.data ? lastYearOrder.data.totalOrderAmountComparedYear : ''}>
                          {lastYearOrder.data ? lastYearOrder.data.totalOrderAmountComparedYear : ''}
                          %
                        </YearArrowP>
                      )
                  }
                  )
                </p>
              </div>
            )
          }

          {
            lastYearOrderChart ? (
              <ReactApexChart
                options={lastYearOrderChart.options}
                series={lastYearOrderChart.series}
                type="bar"
                height="200"
              />
            ) : (
              <div style={{ justifyContent: 'center' }}>
                <EmptyGraph />
              </div>
            )
          }
        </div>
      </ChartContainer>
      <ChartContainer>
        <div className="chart-area loadingDiv">
          <p className="chart-title">
            전체 활동 단계별 고객 비중
          </p>
          <LoadingComponent isLoading={totalActivityStage.status === 'pending'} />

          {
            totalActivityStageChart ? (
              <ReactApexChart
                options={totalActivityStageChart.options}
                series={totalActivityStageChart.series}
                type="bar"
                height="200"
              />
            ) : (
              <div style={{ justifyContent: 'center' }}>
                <EmptyGraph />
              </div>
            )
          }
        </div>
        <div className="chart-area loadingDiv">
          <p className="chart-title">
            전체 활동 단계별 고객 수
          </p>
          <LoadingComponent isLoading={activityStageTrans.status === 'pending'} />

          {
            activityStageTransChart ? (
              <ReactApexChart
                options={activityStageTransChart.options}
                series={activityStageTransChart.series}
                type="bar"
                height="200"
              />
            )
              : (
                <div style={{ justifyContent: 'center' }}>
                  <EmptyGraph />
                </div>
              )
          }
        </div>
      </ChartContainer>
      {/* <Descripition
        text={['전월/전년 기준은 조회기간 대비 이전 동일 기간으로 분석한 결과입니다.']}
        color="#8F959D"
        padding="0"
      /> */}
    </Container>
  );
}

const Container = styled.div`

`;

const ChartContainer = styled.div`
  display: flex;
  margin-top: 10px;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 

  .chart-area{
    position: relative;
    /* height: 252px; */
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
    border-radius: 4px;
    padding: 20px;
    & > p {
    font-size: 14px;
    font-weight: 700;
  }
  .count {
    font-size: 20px;
    font-weight: 700;
  }
  .percent {
    line-height: 31px;
    display: flex;
  }
  & > div {
    display: flex;
  }
}

.tag-area{
  height: 400px;
  width: 100%;
  position: relative;
}

// .chart-title percent {
//   display: flex;
// }
`;

const YearArrowP = styled.span`
  color:${(props) => {
    if (props.yaearArrowData > 0) {
      return 'var(--color-blue-500)';
    } if (props.yaearArrowData === 0) {
      return 'var(--color-gray-900)';
    }
    return 'var(--color-red-500)';
  }}
`;

export default ActiveCustomer;
