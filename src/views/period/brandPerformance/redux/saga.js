import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getStatistics,
  getAllAciveCustomer,
  getLastYearOrder,
  getTotalActivityStage,
  getActivityStageTrans,
  getCustomerNumber,
  getTotalOrder,
  getCategoryKeyword,
  getCategoryOptions,
  getBrandOptions,
} from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** api call type과 매칭시킬 api 요청 메소드 */
const { getCustomerStatistics } = API.CustomerPerformance;

/** createPromiseSaga로 api공통 로직 적용 */
const getCustomerStatisticsSaga = createPromiseSaga(getStatistics, getCustomerStatistics);
const getAllAciveCustomerSaga = createPromiseSaga(getAllAciveCustomer, API.CustomerPerformance.getAllAciveCustomer);
const getLastYearOrderSaga = createPromiseSaga(getLastYearOrder, API.CustomerPerformance.getLastYearOrder);
const getTotalActivityStageSaga = createPromiseSaga(getTotalActivityStage, API.CustomerPerformance.getTotalActivityStage);
const getActivityStageTransSaga = createPromiseSaga(getActivityStageTrans, API.CustomerPerformance.getActivityStageTrans);
const getCustomerNumberSaga = createPromiseSaga(getCustomerNumber, API.CustomerPerformance.getCustomerNumber);
const getTotalOrderSaga = createPromiseSaga(getTotalOrder, API.CustomerPerformance.getTotalOrder);
const getCategoryKeywordSaga = createPromiseSaga(getCategoryKeyword, API.Common.getCategoryKeyword);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getStatistics, getCustomerStatisticsSaga);
  yield takeLatest(getAllAciveCustomer, getAllAciveCustomerSaga);
  yield takeLatest(getLastYearOrder, getLastYearOrderSaga);
  yield takeLatest(getTotalActivityStage, getTotalActivityStageSaga);
  yield takeLatest(getActivityStageTrans, getActivityStageTransSaga);
  yield takeLatest(getCustomerNumber, getCustomerNumberSaga);
  yield takeLatest(getTotalOrder, getTotalOrderSaga);
  yield takeLatest(getCategoryKeyword, getCategoryKeywordSaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
