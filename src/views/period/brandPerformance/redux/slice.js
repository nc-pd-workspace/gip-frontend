import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  statistics: asyncApiState.initial([]),
  allAciveCustomer: asyncApiState.initial([]),
  lastYearOrder: asyncApiState.initial([]),
  totalActivityStage: asyncApiState.initial([]),
  activityStageTrans: asyncApiState.initial([]),
  customerNumber: asyncApiState.initial([]),
  totalOrder: asyncApiState.initial([]),
  category: asyncApiState.initial([[], []]),
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'marketing/customerPerformance',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getStatistics: (state, { payload }) => {
      state.statistics = asyncApiState.request();
    },
    getStatisticsSuccess: (state, { payload }) => {
      const result = { ...payload };
      const test = [
        {
          title: '전체 활동고객수',
          amount: {
            value: result.data.totalActiveCust,
            unit: '명',
          },
          changeRateByMonth: result.data.totalActiveCustComparedMonthCount,
          changeRateByMonthPercent: result.data.totalActiveCustComparedMonth,
          changeRateByYear: result.data.totalActiveCustComparedYearCount,
          changeRateByYearPercent: result.data.totalActiveCustComparedYear,
        },
        {
          title: '신규 활동고객수',
          amount: {
            value: result.data.totalNewCust,
            unit: '명',
          },
          changeRateByMonth: result.data.totalNewCustComparedMonthCount,
          changeRateByMonthPercent: result.data.totalNewCustComparedMonth,
          changeRateByYear: result.data.totalNewCustComparedYearCount,
          changeRateByYearPercent: result.data.totalNewCustComparedYear,
        },
        {
          title: '주문고객수',
          amount: {
            value: result.data.totalOrderCust,
            unit: '명',
          },
          changeRateByMonth: result.data.totalOrderCustComparedMonthCount,
          changeRateByMonthPercent: result.data.totalOrderCustComparedMonth,
          changeRateByYear: result.data.totalOrderCustComparedYearCount,
          changeRateByYearPercent: result.data.totalOrderCustComparedYear,
        },
        {
          title: '주문금액',
          amount: {
            value: result.data.totalOrderAmount,
            unit: '원',
          },
          changeRateByMonth: result.data.totalOrderAmountComparedMonthAmount,
          changeRateByMonthPercent: result.data.totalOrderAmountComparedMonth,
          changeRateByYear: result.data.totalOrderAmountComparedYearAmount,
          changeRateByYearPercent: result.data.totalOrderAmountComparedYear,
        },
        {
          title: '주문수',
          amount: {
            value: result.data.totalOrderCount,
            unit: '개',
          },
          changeRateByMonth: result.data.totalOrderCountComparedMonthCount,
          changeRateByMonthPercent: result.data.totalOrderCountComparedMonth,
          changeRateByYear: result.data.totalOrderCountComparedYearCount,
          changeRateByYearPercent: result.data.totalOrderCountComparedYear,
        },
      ];
      result.data = [...test];
      state.statistics = asyncApiState.success(result);
    },
    getStatisticsFailure: (state, { payload }) => {
      state.statistics = asyncApiState.error(payload);
    },
    getAllAciveCustomer: (state, { payload }) => {
      const result = { ...payload || {} };
      state.allAciveCustomer = asyncApiState.request(result);
    },
    getAllAciveCustomerSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.allAciveCustomer = asyncApiState.success(result);
    },
    getAllAciveCustomerFailure: (state, { payload }) => {
      state.allAciveCustomer = asyncApiState.error(payload);
    },
    getLastYearOrder: (state, { payload }) => {
      const result = { ...payload || {} };
      state.lastYearOrder = asyncApiState.request(result);
    },
    getLastYearOrderSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.lastYearOrder = asyncApiState.success(result);
    },
    getLastYearOrderFailure: (state, { payload }) => {
      state.lastYearOrder = asyncApiState.error(payload);
    },
    getTotalActivityStage: (state, { payload }) => {
      const result = { ...payload || {} };
      state.totalActivityStage = asyncApiState.request(result);
    },
    getTotalActivityStageSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.totalActivityStage = asyncApiState.success(result);
    },
    getTotalActivityStageFailure: (state, { payload }) => {
      state.totalActivityStage = asyncApiState.error(payload);
    },
    getActivityStageTrans: (state, { payload }) => {
      const result = { ...payload || {} };
      state.activityStageTrans = asyncApiState.request(result);
    },
    getActivityStageTransSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.activityStageTrans = asyncApiState.success(result);
    },
    getActivityStageTransFailure: (state, { payload }) => {
      state.activityStageTrans = asyncApiState.error(payload);
    },
    getCustomerNumber: (state, { payload }) => {
      const result = { ...payload || {} };
      state.customerNumber = asyncApiState.request(result);
    },
    getCustomerNumberSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.customerNumber = asyncApiState.success(result);
    },
    getCustomerNumberFailure: (state, { payload }) => {
      state.customerNumber = asyncApiState.error(payload);
    },
    getTotalOrder: (state, { payload }) => {
      const result = { ...payload || {} };
      state.totalOrder = asyncApiState.request(result);
    },
    getTotalOrderSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.totalOrder = asyncApiState.success(result);
    },
    getTotalOrderFailure: (state, { payload }) => {
      state.totalOrder = asyncApiState.error(payload);
    },
    getCategoryKeyword: (state, { payload }) => {
      state.category = asyncApiState.request([[], [], []]);
    },
    getCategoryKeywordSuccess: (state, { payload }) => {
      const result = payload;
      const arr = [];

      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.category = asyncApiState.success({ data: arr });
    },
    getCategoryKeywordFailure: (state, { payload }) => {
      state.category = asyncApiState.error(payload);
    },
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));

      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },
  },
});

export const {
  updateState,
  getStatistics,
  getAllAciveCustomer,
  getLastYearOrder,
  getTotalActivityStage,
  getActivityStageTrans,
  getCustomerNumber,
  getTotalOrder,
  getCategoryKeyword,
  getCategoryOptions,
  getBrandOptions,
} = actions;

export default reducer;
