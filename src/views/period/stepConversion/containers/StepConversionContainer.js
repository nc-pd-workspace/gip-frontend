/* eslint-disable no-prototype-builtins */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/jsx-no-comment-textnodes */
import { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { Radio, Tabs } from 'antd';

import isNil from 'lodash-es/isNil';

import {
  getAnnualPurchaseCount,
  getAnnualPurchasePay,
  getBrandOptions,
  getCategoryOptions,
  getCustomerCharacteColumnDistributed,
  getCustomerCharacteMembership,
  getCustomerCharacteStackedBar,
  getCustomerCharacteTreemapDistributed,
  getPreferencePurchaseBrand,
  getPreferencePurchaseLargeCate,
  getPreferencePurchaseMiddleCate,
  getStepConversion,
} from '../redux/slice';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import MultipleRow from '../../../../components/search/MultipleRow';
import CategorySelects from '../../../../components/search/CategorySelects';
import Paper from '../../../../components/paper';
import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';

import CustomerCharacte from '../components/customerCharacter';
import PreferencePurchase from '../components/preferencePurchase';
import AnnualPurchaseType from '../components/annualPurchaseType';

import Images, { SvgNone } from '../../../../Images';
import { getCustomerSelects } from '../../../../redux/commonReducer';
import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';
import PercentView from '../../../../components/percentView';

import { COLORS } from '../../../../styles/Colors';

const { TabPane } = Tabs;
function StepConversionContainer() {
  const { selectPtnIdx, customerSelects } = useSelector((state) => state.common);
  const searchRef = useRef();

  const [search, setSearch] = useState({});
  const [dataType, setDataType] = useState('avg');
  const [stageType, setStageType] = useState('');
  const [detailOnOff, setDetailOnOff] = useState('on');
  const [customerCharacterTab, setCustomerCharacterTab] = useState(true);

  const [categoryMiddleTab, setCategoryMiddleTab] = useState(true);

  const [brand, setBrand] = useState(null);

  const { stepConversion, categoryOption, brandOption } = useSelector((state) => ({
    stepConversion: state.marketing.stepConversion.stepConversionData,
    categoryOption: state.marketing.stepConversion.categoryOption,
    brandOption: state.marketing.stepConversion.brandOption,
  }));
  const dispatch = useDispatch();

  useEffect(() => {
    // 검색 조건
    dispatch(getBrandOptions({ params: {
      ptnIdx: selectPtnIdx,
    } }));
    dispatch(getCustomerSelects({ params: {
      ptnIdx: selectPtnIdx,
    } }));
  }, []);

  useEffect(() => {
    getCategory();
  }, [brand]);

  useEffect(() => {
    // test
    if (search.categoryData && ((search.categoryData[0] === '' || search.categoryData[0] === null) || (search.categoryData[1] === '' || search.categoryData[1] === null))) {
      setCategoryMiddleTab(true);
    } else {
      setCategoryMiddleTab(true);
    }

    if ((!search.age5UnitCode || search.age5UnitCode.length === 0 || search.age5UnitCode.length === 7) && !search.custGradeCode && !search.sidoCode && !search.genderCode) {
      setCustomerCharacterTab(true);
    } else {
      setCustomerCharacterTab(false);
    }
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (Object.keys(search).length === 0) return;

    if (search.searchStartDate) {
      params.searchStartDate = search.searchStartDate.replace('.', '');
    }
    if (search.searchEndDate) {
      params.searchEndDate = search.searchEndDate.replace('.', '');
    }
    if (search.compareStartDate) {
      params.compareStartDate = search.compareStartDate.replace('.', '');
    }
    if (search.compareEndDate) {
      params.compareEndDate = search.compareEndDate.replace('.', '');
    }
    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categoryLCode) params.categoryLCode = categoryLCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categorySCode) params.categorySCode = categorySCode;
    }
    if (search.brandCd) params.brandCode = search.brandCd ? search.brandCd.toString() : '';
    if (search.genderCode) params.genderCode = search.genderCode;
    if (search.custGradeCode) params.custGradeCode = search.custGradeCode;
    if (search.sidoCode) params.sidoCode = search.sidoCode;
    if (search.age5UnitCode) params.age5UnitCode = search.age5UnitCode ? search.age5UnitCode.toString() : '';
    params.dataType = dataType;
    dispatch(getStepConversion({ params }));

    const chartParams = {
      ptnIdx: selectPtnIdx,
    };
    if (Object.keys(search).length === 0) return;
    if (search.searchStartDate) {
      chartParams.searchStartDate = search.searchStartDate.replace('.', '');
    }
    if (search.searchEndDate) {
      chartParams.searchEndDate = search.searchEndDate.replace('.', '');
    }
    if (search.compareStartDate) {
      chartParams.compareStartDate = search.compareStartDate.replace('.', '');
    }
    if (search.compareEndDate) {
      chartParams.compareEndDate = search.compareEndDate.replace('.', '');
    }
    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categoryLCode) chartParams.categoryLCode = categoryLCode;
      if (categoryMCode) chartParams.categoryMCode = categoryMCode;
      if (categorySCode) chartParams.categorySCode = categorySCode;
    }
    if (search.brandCd) chartParams.brandCode = search.brandCd ? search.brandCd.toString() : '';
    if (search.genderCode) chartParams.genderCode = search.genderCode;
    if (search.custGradeCode) chartParams.custGradeCode = search.custGradeCode;
    if (search.sidoCode) chartParams.sidoCode = search.sidoCode;
    if (search.age5UnitCode) chartParams.age5UnitCode = (search.age5UnitCode ? search.age5UnitCode.toString() : '');

    chartParams.stageType = '';
    // 카테고리(대)
    dispatch(getPreferencePurchaseLargeCate({ params: chartParams }));

    // if (search.categoryData && search.categoryData[0] !== '') {
    // 카테고리(중)
    dispatch(getPreferencePurchaseMiddleCate({ params: chartParams }));
    // }

    // 카테고리/브랜드 주문 선호도 - 브랜드
    dispatch(getPreferencePurchaseBrand({ params: chartParams }));

    if (search.age5UnitCode === undefined && search.custGradeCode === undefined && search.sidoCode === undefined && search.genderCode === undefined) {
      // 고객특성 - 성별/연령별 등급
      dispatch(getCustomerCharacteStackedBar({ params: chartParams }));

      // 고객특성 - 멤버쉽 등급
      dispatch(getCustomerCharacteMembership({ params: chartParams }));

      // 고객특성 - 전국 그래프
      dispatch(getCustomerCharacteColumnDistributed({ params: chartParams }));

      // 고객특성 - 수도권 그래프
      dispatch(getCustomerCharacteTreemapDistributed({ params: chartParams }));
    }

    // 연간 주문 횟수
    dispatch(getAnnualPurchaseCount({ params: chartParams }));

    // 연간 주문 금액
    dispatch(getAnnualPurchasePay({ params: chartParams }));
  }, [search, selectPtnIdx, dataType]);

  useEffect(() => {
    searchRef.current.clickSearch();
  }, []);

  const handleTableCardClick = (data, name) => {
    if (data === 0) {
      return;
    }
    setStageType(name);

    const chartParams = {
      ptnIdx: selectPtnIdx,
    };
    if (Object.keys(search).length === 0) return;
    if (search.searchStartDate) {
      chartParams.searchStartDate = search.searchStartDate.replace('.', '');
    }
    if (search.searchEndDate) {
      chartParams.searchEndDate = search.searchEndDate.replace('.', '');
    }
    if (search.compareStartDate) {
      chartParams.compareStartDate = search.compareStartDate.replace('.', '');
    }
    if (search.compareEndDate) {
      chartParams.compareEndDate = search.compareEndDate.replace('.', '');
    }
    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categoryLCode) chartParams.categoryLCode = categoryLCode;
      if (categoryMCode) chartParams.categoryMCode = categoryMCode;
      if (categorySCode) chartParams.categorySCode = categorySCode;
    }
    if (search.brandCd) chartParams.brandCode = search.brandCd ? search.brandCd.toString() : '';
    if (search.genderCode) chartParams.genderCode = search.genderCode;
    if (search.custGradeCode) chartParams.custGradeCode = search.custGradeCode;
    if (search.sidoCode) chartParams.sidoCode = search.sidoCode;
    if (search.age5UnitCode) chartParams.age5UnitCode = (search.age5UnitCode ? search.age5UnitCode.toString() : '');

    chartParams.stageType = name;
    // 카테고리(대)
    dispatch(getPreferencePurchaseLargeCate({ params: chartParams }));

    // if (search.categoryData && search.categoryData[0] !== '') {
    // 카테고리(중)
    dispatch(getPreferencePurchaseMiddleCate({ params: chartParams }));
    // }

    // 카테고리/브랜드 주문 선호도 - 브랜드
    dispatch(getPreferencePurchaseBrand({ params: chartParams }));

    if (search.age5UnitCode === undefined && search.custGradeCode === undefined && search.sidoCode === undefined && search.genderCode === undefined) {
      // 고객특성 - 성별/연령별 등급
      dispatch(getCustomerCharacteStackedBar({ params: chartParams }));

      // 고객특성 - 멤버쉽 등급
      dispatch(getCustomerCharacteMembership({ params: chartParams }));

      // 고객특성 - 전국 그래프
      dispatch(getCustomerCharacteColumnDistributed({ params: chartParams }));

      // 고객특성 - 수도권 그래프
      dispatch(getCustomerCharacteTreemapDistributed({ params: chartParams }));
    }

    // 연간 주문 횟수
    dispatch(getAnnualPurchaseCount({ params: chartParams }));

    // 연간 주문 금액
    dispatch(getAnnualPurchasePay({ params: chartParams }));
  };

  const handleDataTypeChange = (value) => {
    setDataType(value.target.value);
  };

  const possibleClick = useCallback((value) => {
    if (value !== 0) {
      return 'on';
    }
    return '';
  }, []);

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  function nameType(type) {
    if (type === 'TS' || type === 'NS' || type === 'VS' || type === 'IS' || type === 'PS' || type === 'LS') {
      return '이탈';
    }
    if (type === 'TV' || type === 'NV' || type === 'VV' || type === 'IV' || type === 'PV' || type === 'LV') {
      return '방문';
    }
    if (type === 'TI' || type === 'NI' || type === 'VI' || type === 'II' || type === 'PI' || type === 'LI') {
      return '관심';
    }
    if (type === 'TP' || type === 'NP' || type === 'VP' || type === 'IP' || type === 'PP' || type === 'LP') {
      return '주문';
    }
    if (type === 'TL' || type === 'NL' || type === 'VL' || type === 'IL' || type === 'PL' || type === 'LL') {
      return '충성';
    }
    return '전체 활동';
  }

  function subTitleType(type) {
    if (type === 'TS' || type === 'NS' || type === 'VS' || type === 'IS' || type === 'PS' || type === 'LS') {
      return '내 브랜드에서 이탈한 고객의 특성을 확인해보세요.';
    }
    if (type === 'TV' || type === 'NV' || type === 'VV' || type === 'IV' || type === 'PV' || type === 'LV') {
      return '내 브랜드에 방문한 고객의 특성을 확인해보세요.';
    }
    if (type === 'TI' || type === 'NI' || type === 'VI' || type === 'II' || type === 'PI' || type === 'LI') {
      return '내 브랜드 관심고객의 특성을 확인해보세요.';
    }
    if (type === 'TP' || type === 'NP' || type === 'VP' || type === 'IP' || type === 'PP' || type === 'LP') {
      return '내 브랜드 주문고객의 특성을 확인해보세요.';
    }
    if (type === 'TL' || type === 'NL' || type === 'VL' || type === 'IL' || type === 'PL' || type === 'LL') {
      return '내 브랜드 충성고객의 특성을 확인해보세요.';
    }
    return '';
  }

  const onReset = () => {
    getCategory();
  };

  const getCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const nullCheck = (data) => (isNil(data)
    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : data);

  return (
    <Container>
      <PageHeader
        title="단계별 고객 전환 분석"
        subTitle="브랜드 활동고객에 대한 단계별 전환 분석을 통해 행사/마케팅/프로모션 전후의 성과를 비교 분석할 수 있습니다."
      />
      <Search setSearch={setSearch} detailOnOff={detailOnOff} setDetailOnOff={setDetailOnOff} onReset={onReset} ref={searchRef}>
        <MultipleRow>
          <SingleRangePicker
            search={search}
            column={['searchStartDate', 'searchEndDate']}
            title="조회기간"
            showDateType={false}
            disabledToday
            disabled2020
            maxSelectDate={90}
            dropdownClassName="stepConversionPickerPopup01"
          />
          <SingleRangePicker
            search={search}
            column={['compareStartDate', 'compareEndDate']}
            title="비교기간"
            showDateType={false}
            disabledToday
            setDefaultDate={false}
            comparisonDefaultDate
            maxSelectDate={90}
            dropdownClassName="stepConversionPickerPopup02"
          />
        </MultipleRow>
        <SingleInputItem
          type="Select"
          column="brandCd"
          title="브랜드"
          width="50%"
          loading={brandOption.status === 'pending'}
          options={brandOption.data}
          onChange={onChangeBrand}
          placeholder="전체"
        />
        <CategorySelects
          column="categoryData"
          title="카테고리"
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />
        <MultipleRow isDetail detailOnOff={detailOnOff}>
          <SingleInputItem
            type="Select"
            column="genderCode"
            title="성별"
            options={customerSelects.data.gender}
            placeholder="전체"
          />
          <SingleInputItem
            type="Select"
            column="custGradeCode"
            title="고객등급"
            options={customerSelects.data.custGrade}
            placeholder="전체"
          />
          <SingleInputItem
            type="Select"
            column="sidoCode"
            title="지역"
            options={customerSelects.data.sido}
            placeholder="전체"
          />
        </MultipleRow>
        <SingleInputItem
          type="MultiSelect"
          column="age5UnitCode"
          title="연령대"
          width="50%"
          options={customerSelects.data.ageUnit}
          isDetail
          detailOnOff={detailOnOff}
        />
      </Search>

      <SearchResult>
        <Title className="border-bottom">
          <p>
            단계별 활동고객 조회
          </p>
          {
            stepConversion.status === 'success' && (
              <div className="searchResultToolBox">
                <span className="caption-text">데이터 기준</span>
                <div className="right-item set-section-toggle">
                  <Radio.Group value={dataType} buttonStyle="solid" onChange={handleDataTypeChange}>
                    <Radio.Button value="avg">평균</Radio.Button>
                    <Radio.Button value="sum">합계</Radio.Button>
                  </Radio.Group>
                </div>
                {/* <ButtonExcel /> */}
              </div>
            )
          }
        </Title>

        <Contents className="G-MK-02-01" style={{ minHeight: '300px' }}>
          <LoadingComponent isLoading={stepConversion.status === 'pending'} />
          {
            stepConversion.data !== undefined && stepConversion.status === 'success' ? (
              <div className="ui-box">
                <div className="data-info">
                  <div className="data-table-wrap">
                    <div className="ui-table seperate-type block">
                      <div style={{ flex: 1 }}>
                        <table>
                          <colgroup>
                            <col width="100%" />
                          </colgroup>
                          <thead>
                            <tr>
                              <th>비교기간</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div className="date-check block">
                                  <span className="date">{stepConversion.data.compareTerm}</span>
                                  <div className="profit-num margin-top">
                                    <span>
                                      {
                                        isNil(stepConversion.data.compareTermCust)
                                          ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span>
                                          : (stepConversion.data.compareTermCust).toLocaleString('ko-KR')
                                      }
                                      명
                                    </span>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span className="txt">미활동</span>
                                <span className="num">
                                  {isNil(stepConversion.data.compareNewCust)
                                    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : (stepConversion.data.compareNewCust).toLocaleString('ko-KR')}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span className="txt">방문</span>
                                <span className="num">
                                  {isNil(stepConversion.data.compareVisitCust)
                                    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : (stepConversion.data.compareVisitCust).toLocaleString('ko-KR')}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span className="txt">관심</span>
                                <span className="num">
                                  {isNil(stepConversion.data.compareInterestCust)
                                    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : (stepConversion.data.compareInterestCust).toLocaleString('ko-KR')}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span className="txt">주문</span>
                                <span className="num">
                                  {isNil(stepConversion.data.compareOrderCust)
                                    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : (stepConversion.data.compareOrderCust).toLocaleString('ko-KR')}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span className="txt">충성</span>
                                <span className="num">
                                  {isNil(stepConversion.data.compareLoyaltyCust)
                                    ? <span style={{ verticalAlign: 'middle' }}><SvgNone width="16" height="16" fill={COLORS.GRAY[900]} /></span> : (stepConversion.data.compareLoyaltyCust).toLocaleString('ko-KR')}
                                </span>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div className="icon-area">
                        <img src={Images.arrow_triple} alt="arrow" width={16} height={16} />
                      </div>
                    </div>
                    <div className="ui-table seperate-type table">
                      <table>
                        <colgroup>
                          <col width="20%" />
                          <col width="20%" />
                          <col width="20%" />
                          <col width="20%" />
                          <col width="20%" />
                        </colgroup>
                        <thead>
                          <tr>
                            <th colSpan="5">조회기간</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colSpan="5" style={{ height: '48px' }}>
                              <div className="date-check">
                                <span className="date">
                                  {stepConversion.data.searchTerm}
                                </span>
                                <div className="profit-num">
                                  {
                                    (stepConversion.data.searchTermCust === 0) || isNil(stepConversion.data.searchTermCust)
                                      ? (
                                        <>
                                          <div className="item">
                                            <div>
                                              {nullCheck(stepConversion.data.searchTermCust)}
                                              명
                                            </div>
                                            <div style={{ lineHeight: '13px' }}>
                                              <PercentView data={stepConversion.data.searchTermCustConversionRate} />
                                            </div>
                                          </div>
                                        </>
                                      )
                                      : (
                                        <>
                                          <div className="item">
                                            <div style={{ textDecorationLine: 'underline', cursor: 'pointer' }} onClick={() => handleTableCardClick(100, '')}>
                                              {(stepConversion.data.searchTermCust).toLocaleString('ko-KR')}
                                              명
                                            </div>
                                            <div style={{ lineHeight: '13px' }}>
                                              <PercentView data={stepConversion.data.searchTermCustConversionRate} />
                                            </div>
                                          </div>
                                        </>
                                      )
                                  }

                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td className={`segment bad ${stepConversion.data.totalLeave !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalLeave, 'TS')}>
                              <span className="txt">이탈</span>
                              <span className="num">
                                {nullCheck((stepConversion.data.totalLeave).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.totalVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalVisit, 'TV')}>
                              <span className="txt">방문</span>
                              <span className="num">
                                {nullCheck((stepConversion.data.totalVisit).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.totalInterest !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalInterest, 'TI')}>
                              <span className="txt">관심</span>
                              <span className="num">
                                {nullCheck((stepConversion.data.totalInterest).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.totalOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalOrder, 'TP')}>
                              <span className="txt">주문</span>
                              <span className="num">
                                {nullCheck((stepConversion.data.totalOrder).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.totalLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalLoyalty, 'TL')}>
                              <span className="txt">충성</span>
                              <span className="num">
                                {nullCheck((stepConversion.data.totalLoyalty).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                          </tr>
                          <tr>
                            <td className={`segment average ${stepConversion.data.newLeave !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.newLeave, 'NS')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.newLeave).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.newVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.newVisit, 'NV')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.newVisit).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.newInterest !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.newInterest, 'NI')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.newInterest).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.newOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.newOrder, 'NP')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.newOrder).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.newLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.newLoyalty, 'NL')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.newLoyalty).toLocaleString('ko-KR'))}
                              </span>
                            </td>
                          </tr>
                          {/* 방문 */}
                          <tr>
                            <td className={`segment bad ${possibleClick(stepConversion.data.visitLeave)}`} onClick={() => handleTableCardClick(stepConversion.data.visitVisit, 'VS')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.visitLeave)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.visitLeaveConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.visitLeaveConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment average ${stepConversion.data.visitVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.visitVisit, 'VV')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.visitVisit)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.visitVisitConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.visitVisitConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.visitInterest !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.visitInterest, 'VI')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.visitInterest)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.visitInterestConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.visitInterestConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.visitOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.visitOrder, 'VP')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.visitOrder).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.visitOrderConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.visitOrderConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.visitLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.visitLoyalty, 'VL')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.visitLoyalty).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.visitLoyaltyConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.visitLoyaltyConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                          </tr>
                          {/* 관심 */}
                          <tr>
                            <td className={`segment bad ${stepConversion.data.interestLeave !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalLeave, 'IS')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.interestLeave)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.interestLeaveConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.interestLeaveConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.interestVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.interestVisit, 'IV')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.interestVisit)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.interestVisitConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.interestVisitConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td
                              className={`segment average ${stepConversion.data.interestInterest !== 0 ? 'on' : ''}`}
                              onClick={() => handleTableCardClick(stepConversion.data.interestInterest, 'II')}
                            >
                              <span className="num">
                                {nullCheck((stepConversion.data.interestInterest)).toLocaleString('ko-KR')}
                                {
                                  stepConversion.data.interestInterestConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.interestInterestConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.interestOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.interestOrder, 'IP')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.interestOrder).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.interestOrderConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.interestOrderConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.interestLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.interestLoyalty, 'IL')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.interestLoyalty).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.interestLoyaltyConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.interestLoyaltyConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                          </tr>
                          {/* 주문 */}
                          <tr>
                            <td className={`segment bad ${stepConversion.data.orderLeave !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.orderLeave, 'PS')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.orderLeave).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.orderLeaveConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.orderLeaveConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.orderVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.orderVisit, 'PV')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.orderVisit).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.orderVisitConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.orderVisitConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.orderInterest !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.totalLeave, 'PI')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.orderInterest).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.orderInterestConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.orderInterestConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment average ${stepConversion.data.orderOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.orderOrder, 'PP')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.orderOrder).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.orderOrderConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.orderOrderConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment good ${stepConversion.data.orderLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.orderLoyalty, 'PL')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.orderLoyalty).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.orderLoyaltyConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.orderLoyaltyConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                          </tr>
                          {/* 충성 */}
                          <tr>
                            <td className={`segment bad ${stepConversion.data.loyaltyLeave !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.loyaltyLeave, 'LS')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.loyaltyLeave).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.loyaltyLeaveConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.loyaltyLeaveConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.loyaltyVisit !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.loyaltyVisit, 'LV')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.loyaltyVisit).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.loyaltyVisitConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.loyaltyVisitConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.loyaltyInterest !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.loyaltyInterest, 'LI')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.loyaltyInterest).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.loyaltyInterest !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.loyaltyInterestConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment bad ${stepConversion.data.loyaltyOrder !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.loyaltyOrder, 'LP')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.loyaltyOrder).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.loyaltyOrderConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.loyaltyOrderConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                            <td className={`segment average ${stepConversion.data.loyaltyLoyalty !== 0 ? 'on' : ''}`} onClick={() => handleTableCardClick(stepConversion.data.loyaltyLoyalty, 'LL')}>
                              <span className="num">
                                {nullCheck((stepConversion.data.loyaltyLoyalty).toLocaleString('ko-KR'))}
                                {
                                  stepConversion.data.loyaltyLoyaltyConversionRate !== 0 && (
                                    <span className="sub-num">
                                      (
                                      {nullCheck(stepConversion.data.loyaltyLoyaltyConversionRate)}
                                      %
                                      )
                                    </span>
                                  )
                                }
                              </span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="ui-side-note">
                    <ul className="ui-side-note-wrap">
                      <li className="note square bad">
                        <Square className="red" />
                        비교기간 대비 조회기간 동안 이전 동일기간 대비
                        {' '}
                        <span> 이탈 및 관심/주문/충성도가 낮아진 고객</span>
                      </li>
                      <li className="note square average">
                        <Square className="yellow" />
                        비교기간 대비 조회기간 동안 이전 동일기간 대비
                        {' '}
                        <span> 이동에 큰 변화가 없는 고객</span>
                      </li>
                      <li className="note square good">
                        <Square className="blue" />
                        비교기간 대비 조회기간 동안 이전 동일기간 대비
                        {' '}
                        <span> 긍정적인 이동을 한 고객</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            ) : <EmptyGraph />
          }
        </Contents>

      </SearchResult>

      {
        stepConversion.data !== undefined && stepConversion.status === 'success' && stepConversion.data.searchTermCust !== 0 && (
          <SearchResult>
            <Title>
              <p>
                {nameType(stageType)}
                고객 특성 분석 결과
              </p>
            </Title>
            <Tabs defaultActiveKey="1" className="subTabs">
              <TabPane tab="카테고리/브랜드 주문 선호도" key="1">
                <span className="chart-span">내 브랜드의 고객이 GS SHOP에서 선호하는 카테고리와 브랜드를 확인해보세요.</span>
                <PreferencePurchase categoryMiddleTab={categoryMiddleTab} />
                {/* <Descripition text={
              [
                '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
              ]
            }
            /> */}
              </TabPane>
              {
                customerCharacterTab && (
                  <TabPane tab="고객 특성" key="2">
                    <span className="chart-span">{subTitleType(stageType)}</span>
                    <CustomerCharacte />
                    {/* <Descripition text={
                  [
                    '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
                  ]
                }
                /> */}
                  </TabPane>
                )
              }
              <TabPane tab="연간 주문 형태" key="3">
                <span className="chart-span">GS SHOP에서 내 브랜드 고객이 소비하는 연간 주문형태를 확인해보세요.</span>
                <AnnualPurchaseType />
                {/* <Descripition text={
              [
                '* 표시된 데이터는  실제 수치가 아닌 GIP만의 방식으로 계산된 지수화 데이터 입니다.',
              ]
            }
            /> */}
              </TabPane>
            </Tabs>
          </SearchResult>
        )
      }

    </Container>
  );
}

const Container = styled(PageLayout)`
  .subTabs > .ant-tabs-nav {
    margin-right: 40px;
    margin-left: 40px;
    margin-bottom: 20px;
    .ant-tabs-nav-wrap {
      flex: 1;
      .ant-tabs-nav-list {
        border-radius: 4px;
        flex: 1;
        background-color: var(--color-gray-50);
        display: flex;
      }
      .ant-tabs-tab-btn {
        padding: 0 12px;
      }
    }
    .ant-tabs-tab{
      display: flex !important;
      padding: 0px;
      height: 40px;
      line-height: 40px;
      margin: 0;
      justify-content: center;
      flex: 1;
      font-size: 13px;
    }
    .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
      border-radius: 4px;
      background-color: var(--color-steelGray-800);
      color: var(--color-white);
    }
  }
  .ant-tabs-nav:before {
      content: none !important;
  }
  .ant-tabs-ink-bar{
      display: none;
  }
  .ant-tabs-tab-btn{
      width: 100%;
      height: 100%;
      text-align: center;
  }
  .ant-tabs{
    margin-top: 10px;
    margin-bottom: 10px;
  }

.G-MK-02-01 .data-info .ui-side-note-wrap .note.good span {
  color: rgba(59, 123, 246, 0.7);
  margin-left: 3px;
  font-weight: 400;
}
  .G-MK-02-01 .btn-category-area {
    margin-top: -5.5px;
  }

  .G-MK-02-01 .ui-box .title-wrap {
    margin-bottom: 24px;
  }

  .G-MK-02-01 .ui-box .ui-table {
    margin: 0 20px 20px;
  }

.G-MK-02-01 .data-info .ui-side-note-wrap .note.average span {
  color: #ffde80;
  margin-left: 3px;
  font-weight: 400;
}
  .G-MK-02-01 .ui-box .ui-table .seperate-type {
    margin: 0 20px 20px;
  }

  .ui-table.seperate-type.block{
    display: flex;
    flex: 2;
  }

  .ui-table.seperate-type.table{
    flex: 5;
  }

.G-MK-02-01 .data-info .ui-side-note-wrap .note.bad span {
  color: rgba(224, 90, 66, 0.7);
  margin-left: 3px;
  font-weight: 400;
}
  .G-MK-02-01 .data-info .ui-side-note-wrap {
    li {
      font-size: 12px;
      display:flex;
      align-items: center;
      color: var(--color-gray-500);
      line-height: 18px;
      em {
        font-style: normal;
        line-height: 18px;
        margin-left: 3px;
        font-weight: 400;
      }
      &.good em {
        color: #00B681;
      }
      &.average em {
        color: #FCAF69;
      }
      &.bad em {
        color: #E05A42;
      }
    }
  }

  .G-MK-02-01 .data-table-wrap {
    display: flex;
    flex-wrap: wrap;
    position: relative;
    margin-left: -6px;
    margin-right: -6px;
  }

  .G-MK-02-01 .data-table-wrap .ui-table {
    z-index: 0;
    margin: 0;
    width: calc(100% - 360px);
  }

  .G-MK-02-01 .data-table-wrap .ui-table:first-child {
    // width: 280px;
    // width: calc(100% - 690px);
    // margin-right: 80px;
  }

  .G-MK-02-01 .data-table-wrap .icon-area {
    flex-basis: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .G-MK-02-01 .data-table-wrap .date-check {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .G-MK-02-01 .data-table-wrap .date-check .date {
    position: relative;
    font-size: 13px;
  }

  .G-MK-02-01 .data-table-wrap .profit-num {
    font-weight: 400;
    font-size: 13px;
    .up {
      color: var(--color-blue-500);
      font-style: normal;
    }
    .up::before{
      margin-left: 10px;
      content: "";
      display: inline-block;
      width: 0;
      height: 0;
      margin-right: 4px;
      border-radius: 2px;
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-bottom: 4px solid var(--color-blue-500);
      vertical-align: middle;
    }
    .down::before{
      margin-left: 10px;
      content: "";
      display: inline-block;
      width: 0;
      height: 0;
      margin-right: 4px;
      border-radius: 2px;
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-bottom: 4px solid #e05a42;
      vertical-align: middle;
      transform: rotate(180deg);
    }
    .item {
      display: flex;
    align-items: center;
    }
  }

  .G-MK-02-01 .data-table-wrap .profit-num.margin-top {
    font-weight: 400;
    font-size: 13px;
    margin-top: 3px;
  }

  .G-MK-02-01 .data-table-wrap .date-check .date::after {
    content: "";
    display: inline-block;
    width: 1px;
    height: 8px;
    margin-left: 10px;
    margin-right: 10px;
    background-color: #e3e4e7;
  }

  .G-MK-02-01 .data-table-wrap .date-check.block {
    flex-direction: column;
    padding-top: 28px;
    padding-bottom: 28px;
  }

  .G-MK-02-01 .data-table-wrap .date-check.block .date::after {
    content: none;
  }

  .G-MK-02-01 .data-table-wrap .txt {
    color: #333;
    font-weight: 600;
    font-size: 13px;
  }

  .G-MK-02-01 .data-table-wrap .num {
    color: #333;
    font-size: 13px;
    margin-left: 2px;
  }

  .G-MK-02-01 .data-table-wrap .sub-num {
    margin-left: 2px;
  }


  .G-MK-02-01 .data-table-wrap .segment {
    border-color: transparent;
  }

  .G-MK-02-01 .data-table-wrap .segment.on .num {
    position: relative;
  }

  .G-MK-02-01 .data-table-wrap .segment.on .num::after {
    content: "";
    display: inline-block;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 1px;
    background-color: #333;
  }

  .G-MK-02-01 .data-table-wrap .segment.good {
    background-color: rgba(59, 123, 246, 0.1);
  }

  .G-MK-02-01 .data-table-wrap .segment.good.on {
    border-color: rgba(59, 123, 246, 0.5);
  }

  .G-MK-02-01 .data-table-wrap .segment.good.on:hover {
    cursor: pointer;
  }

  .G-MK-02-01 .data-table-wrap .segment.average {
    background-color: rgba(255, 222, 128, 0.2);
  }

  .G-MK-02-01 .data-table-wrap .segment.average.on {
    border-color: #ffde80;
  }

  .G-MK-02-01 .data-table-wrap .segment.average.on:hover {
    cursor: pointer;
  }

  .G-MK-02-01 .data-table-wrap .segment.bad {
    background-color: rgba(224, 90, 66, 0.1);
  }

  .G-MK-02-01 .data-table-wrap .segment.bad.on {
    border-color: rgba(224, 90, 66, 0.5);
  }

  .G-MK-02-01 .data-table-wrap .segment.bad.on:hover {
    cursor: pointer;
  }

  .G-MK-02-01 .tab-info .chart-desc {
    margin-bottom: 10px;
  }

  .G-MK-02-01 .ui-tab {
    padding: 20px 40px;
  }

  .G-MK-02-01 .ui-side-note {
    margin-top: 10px;
  }

  .ui-table {
    max-height: 550px;
    overflow: auto;
  }

  .ui-table table{
    width: 100%;
    table-layout: fixed;
    & > tbody {
      display: table-row-group;
      vertical-align: middle !important;
      border-color: inherit;
    }
  }

  .ui-table table thead{
    z-index: 1;
    position: sticky;
    top: 0;
  }

  .ui-table table th {
    height: 16px;
    border-top: 1px solid rgba(227, 228, 231, 0.5);
    border-bottom: 1px solid rgba(227, 228, 231, 0.5);
    color: var(--color-gray-700);
    font-size: 12px;
    font-weight: 400;
  }

  .ui-table table tbody tr {
    position: relative;
  }

  .ui-table.seperate-type table thead th {
    border-top: 0;
    border-bottom: 0;
    background-color: #fff;
    text-align: center;
  }

  .ui-table table td {
    height: 50px;
  }

  .ui-table.seperate-type table tbody td {
    position: relative;
    height: 44px;
    border-radius: 4px;
    border: 1px solid #e3e4e7;
    text-align: center;
  }

  .ui-table table th, .ui-table table td {
    position: relative;
    width: 20%;
    padding: 0 10px;
    text-align: left;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;

    vertical-align: middle;
  }

  .ui-table.seperate-type table {
    border-collapse: separate;
    border-spacing: 6px;
  }

  .border-bottom {
    border-bottom: 1px solid var(--border-cell);
  }
  .chart-span {
    font-size: 14px;
    font-weight: 400;
    color: var(--color-black);
  }
`;

const Square = styled.div`
  height: 8px;
  width: 8px;
  border-radius: 2px;
  margin-right: 6px;
  margin-top: -1px;
  &.red {
    background: #F9DED9;
  }
  &.yellow {
    background: #FFE8A7;
  }
  &.blue {
    background: #C5D8FD;
  }
`;
const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px 10px;
`;

const Title = styled.div`
  padding: 20px 0;
  color: var(--color-gray-900);
  font-weight: 700;
  font-size: 20px;
  .right{
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
    }

    .caption-text{
    color: var(--color-gray-500);
    margin-right: 5px;
    font-weight: 400;
    font-size: 12px;
  }
`;

const Contents = styled.div`
position: relative;

  padding: 0 0 20px;
  .ant-tabs-nav {
    width: 50%;
    margin: 20px auto !important;
  }
`;

export default StepConversionContainer;
