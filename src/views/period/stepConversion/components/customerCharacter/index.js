import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';

import { useSelector } from 'react-redux';

import LoadingComponent from '../../../../../components/loading';
import EmptyGraph from '../../../../../components/emptyGraph';
import { CHART_VARIATION } from '../../../../../styles/chartColors';
import { getChartValuePeople, getChartYAxisMax } from '../../../../../utils/utils';
import { optionsBarNegative, optionsRadarMultiple } from '../../../../../utils/chartOptions';

function CustomerCharacte() {
  const { customerCharacteMembership, customerCharacteColumnDistributed, customerCharacteTreemapDistributed, customerCharacteStackedBar } = useSelector((state) => ({
    customerCharacteMembership: state.marketing.stepConversion.customerCharacteMembership,
    customerCharacteColumnDistributed: state.marketing.stepConversion.customerCharacteColumnDistributed,
    customerCharacteTreemapDistributed: state.marketing.stepConversion.customerCharacteTreemapDistributed,
    customerCharacteStackedBar: state.marketing.stepConversion.customerCharacteStackedBar,
  }));

  const [customerCharacteMembershipChart, setCustomerCharacteMembershipChart] = useState();
  const [customerCharacteColumnDistributedChart, setCustomerCharacteColumnDistributedChart] = useState();
  const [customerCharacteTreemapDistributedChart, setCustomerCharacteTreemapDistributedChart] = useState();
  const [customerCharacteStackedBarChart, setCustomerCharacteStackedBarChart] = useState();

  useEffect(() => {
    if (customerCharacteStackedBar.status === 'success') {
      const arr = [...customerCharacteStackedBar.data.series[0].data];
      const maxValue = arr.reduce((previousItem, currentItem, index, array) => {
        let max = 0;
        customerCharacteStackedBar.data.series.forEach((item) => {
          if (item?.data?.length >= index) {
            max += item.data[index];
          }
        });
        if (previousItem < max) {
          return max;
        }
        return previousItem;
      }, 0);
      const maxCeil = Math.ceil(maxValue / 10) * 10;
      const maxData = maxCeil + 10;

      const changeArr = customerCharacteStackedBar.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      // 성별/연령별
      setCustomerCharacteStackedBarChart({
        series: changeArr,
        options: {
          ...optionsBarNegative,
          xaxis: {
            categories: customerCharacteStackedBar.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              show: true,
              formatter(val, index) {
                if (index === 0) return 0;
                return `${Math.round(val)}${Math.round(val) === 0 ? '' : '%'}`;
              },
            },
          },
          // yaxis: {
          //   title: {
          //     text: undefined,
          //   },
          // },
          // tooltip: {
          //   enabled: true,
          //   shared: true,
          //   followCursor: false,
          //   intersect: false,
          //   inverseOrder: false,
          //   y: {
          //     formatter(val) {
          //       return `${val}%`;
          //       // return `${Math.round(val)}%`;
          //     },
          //   },
          // },
        },
      });
    }
  }, [customerCharacteStackedBar]);

  useEffect(() => {
    if (customerCharacteMembership.status === 'success') {
      const changeArr = customerCharacteMembership.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });
      // 고객 등급
      setCustomerCharacteMembershipChart({
        series: changeArr,
        options: {
          ...optionsRadarMultiple,
          xaxis: {
            categories: customerCharacteMembership.data.categories,
          },
          yaxis: {
            min: 0,
            max: (max) => getChartYAxisMax(max),
            labels: {
              style: {
                colors: 'var(--color-gray-700)',
              },
              formatter(val, index) {
                if (index === 5) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
        },
      });
    }
  }, [customerCharacteMembership]);

  useEffect(() => {
    if (customerCharacteColumnDistributed.status === 'success') {
      setCustomerCharacteColumnDistributedChart({
        // 지역(전국)
        series: [{
          data: customerCharacteColumnDistributed.data.series[0].data,
          name: '',
        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          zoom: {
            enabled: false,
          },
          // colors,
          plotOptions: {
            bar: {
              columnWidth: '45%',
              distributed: true,
            },
          },
          dataLabels: {
            enabled: false,
          },
          legend: {
            show: false,
          },
          tooltip: {
            enabled: true,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: true,
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          colors: CHART_VARIATION,
          xaxis: {
            categories: customerCharacteColumnDistributed.data.categories,
            labels: {
              rotate: 0,
              hideOverlappingLabels: false,
              style: {
                fontSize: '11px',
                cssClass: 'apexcharts-xaxis-label-overlapping',
              },
              offsetY: -5,
            },
          },
          yaxis: {
            max: (max) => getChartYAxisMax(max),
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
            tickAmount: 5,
          },
        },
      });
    }
  }, [customerCharacteColumnDistributed]);

  useEffect(() => {
    if (customerCharacteTreemapDistributed.status === 'success') {
      // 지역(수도권)
      setCustomerCharacteTreemapDistributedChart({
        series: [{ data: customerCharacteTreemapDistributed.data.series[0].data.filter((element) => element.y > 0) }],
        options: {
          legend: {
            show: false,
          },
          chart: {
            toolbar: {
              show: false,
            },
            type: 'treemap',
          },
          tooltip: {
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          dataLabels: {
            offsetY: -2,
            style: {
              fontSize: '12px',
            },
          },
          stroke: {
            show: true,
            curve: 'smooth',
            lineCap: 'butt',
            colors: ['#FFF'],
            width: 1,
            dashArray: 0,
          },
          colors: CHART_VARIATION,
          plotOptions: {
            treemap: {
              distributed: true,
              enableShades: false,
            },
          },
        },
      });
    }
  }, [customerCharacteTreemapDistributed]);
  return (
    <Container>
      <ChartContainer>
        <div className="chart-area apexcharts-legend-first-none apexcharts-tooltip-first-none">
          <p className="chart-title">
            성별/연령별
          </p>
          <LoadingComponent isLoading={customerCharacteStackedBar.status === 'pending'} />
          {
            customerCharacteStackedBarChart ? (
              <ReactApexChart options={customerCharacteStackedBarChart.options} series={customerCharacteStackedBarChart.series} type="bar" height="294" />
            ) : <EmptyGraph />
          }
        </div>
        <div className="chart-area padding20 apexcharts-legend-first-none">
          <p className="chart-title">
            고객 등급
          </p>
          <LoadingComponent isLoading={customerCharacteMembership.status === 'pending'} />

          {
            customerCharacteMembershipChart ? (
              <ReactApexChart options={customerCharacteMembershipChart.options} series={customerCharacteMembershipChart.series} type="radar" height="314" />
            ) : <EmptyGraph />
          }
        </div>
      </ChartContainer>
      <ChartContainer>
        <div className="chart-area">
          <p className="chart-title">
            지역(전국)
          </p>
          <LoadingComponent isLoading={customerCharacteColumnDistributed.status === 'pending'} />

          {
            customerCharacteColumnDistributedChart ? (
              <ReactApexChart options={customerCharacteColumnDistributedChart.options} series={customerCharacteColumnDistributedChart.series} type="bar" height="294" />
            ) : <EmptyGraph />
          }
        </div>
        <div className="chart-area padding20">
          <p className="chart-title">
            지역(수도권)
          </p>
          <LoadingComponent isLoading={customerCharacteTreemapDistributed.status === 'pending'} />

          {
            customerCharacteTreemapDistributedChart && (customerCharacteTreemapDistributedChart && customerCharacteTreemapDistributedChart.series[0].data.length !== 0) ? (
              <ReactApexChart options={customerCharacteTreemapDistributedChart.options} series={customerCharacteTreemapDistributedChart.series} type="treemap" height="294" />
            ) : <EmptyGraph />
          }
        </div>
      </ChartContainer>
    </Container>
  );
}

const Container = styled.div`

`;

const ChartContainer = styled.div`
  display: flex;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 
  .chart-area{
    /* height: 252px; */
    position: relative;
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
    border-radius: 4px;
    padding: 20px;
    .chart-title{
      font-weight: 700;
    }
    & > div {
      min-height: 215px !important;
    }
  }
  .tag-area{
    height: 400px;
    width: 100%;
    position: relative;
}
`;

export default CustomerCharacte;
