import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';
import moment from 'moment';

import { useSelector } from 'react-redux';

import { ChartContainer } from '../../../../../styles/ChartContainer';
import LoadingComponent from '../../../../../components/loading';
import { BLUE_VARIATION, GREEN_VARIATION } from '../../../../../styles/chartColors';
import { optionsStackedColumn } from '../../../../../utils/chartOptions';

function AnnualPurchaseType() {
  const { annualPurchaseCount, annualPurchasePay } = useSelector((state) => ({
    annualPurchaseCount: state.marketing.stepConversion.annualPurchaseCount,
    annualPurchasePay: state.marketing.stepConversion.annualPurchasePay,
  }));

  const [annualPurchaseCountChart, setAnnualPurchaseCountChart] = useState();
  const [annualPurchasePayChart, setAnnualPurchasePayChart] = useState();

  useEffect(() => {
    if (annualPurchaseCount.status === 'success') {
      const changeArr = annualPurchaseCount.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });

      setAnnualPurchaseCountChart({
        series: changeArr,
        options: {
          ...optionsStackedColumn,
          colors: BLUE_VARIATION,
          xaxis: {
            categories: annualPurchaseCount.data.categories,
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            labels: {
              formatter(val) {
                return `${Math.abs(Math.round(val))}${Math.abs(Math.round(val)) === 0 ? '' : '%'}`;
              },
            },
          },
          tooltip: {
            ...optionsStackedColumn.tooltip,
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M.')}`;
              },
            },
          },
        },
      });
    }
  }, [annualPurchaseCount]);

  useEffect(() => {
    if (annualPurchasePay.status === 'success') {
      const changeArr = annualPurchasePay.data.series.map(({ data, name }) => ({ data, name }));
      const emptyArr = changeArr[0].data.map((v) => (null));
      changeArr.unshift({ data: emptyArr, name: '' });
      setAnnualPurchasePayChart({
        series: changeArr,
        options: {
          ...optionsStackedColumn,
          colors: GREEN_VARIATION,
          xaxis: {
            categories: annualPurchasePay.data.categories,
            labels: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M.')}`;
              },
            },
          },
          yaxis: {
            tickAmount: 5,
            min: 0,
            labels: {
              formatter(val) {
                return `${Math.abs(Math.round(val))}${Math.abs(Math.round(val)) === 0 ? '' : '%'}`;
              },
            },
          },
          tooltip: {
            ...optionsStackedColumn.tooltip,
            x: {
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YYYY. M.')}`;
              },
            },
          },
        },
      });
    }
  }, [annualPurchasePay]);

  return (
    <Container>
      <ChartContainer>
        <div className="chart-area heightAuto apexcharts-legend-first-none apexcharts-tooltip-first-none">
          <p className="chart-title">
            연간 주문횟수
          </p>
          <LoadingComponent isLoading={annualPurchaseCount?.status === 'pending'} />
          {
            annualPurchaseCountChart && (
              <ReactApexChart options={annualPurchaseCountChart.options} series={annualPurchaseCountChart.series} type="bar" height="400" />
            )
          }
        </div>
        <div className="chart-area heightAuto apexcharts-legend-first-none apexcharts-tooltip-first-none">
          <p className="chart-title">
            연간 주문금액
          </p>
          <LoadingComponent isLoading={annualPurchasePay?.status === 'pending'} />
          {
            annualPurchasePayChart && (
              <ReactApexChart options={annualPurchasePayChart.options} series={annualPurchasePayChart.series} type="bar" height="400" />

            )
          }
        </div>
      </ChartContainer>
    </Container>
  );
}

const Container = styled.div`

`;

export default AnnualPurchaseType;
