import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';
import { Tabs } from 'antd';
import TagCloud from 'react-tag-cloud';

import { useSelector } from 'react-redux';

import LoadingComponent from '../../../../../components/loading';
import { ChartContainer } from '../../../../../styles/ChartContainer';
import EmptyGraph from '../../../../../components/emptyGraph';
import { getChartValuePeople, getChartYAxisMax } from '../../../../../utils/utils';
import { TAGCLOUD_VARIATION, TAGCLOUD_VARIATION_BLUE } from '../../../../../styles/chartColors';

const { TabPane } = Tabs;
function PreferencePurchase(props) {
  const { categoryMiddleTab } = props;
  const { preferencePurchaseLargeCate, preferencePurchaseMiddleCate, preferencePurchaseBrand } = useSelector((state) => ({
    preferencePurchaseLargeCate: state.marketing.stepConversion.preferencePurchaseLargeCate,
    preferencePurchaseMiddleCate: state.marketing.stepConversion.preferencePurchaseMiddleCate,
    preferencePurchaseBrand: state.marketing.stepConversion.preferencePurchaseBrand,
  }));

  const [preferencePurchaseLargeCateChart, setPreferencePurchaseLargeCateChart] = useState();
  const [preferencePurchaseMiddleCateChart, setPreferencePurchaseMiddleCateChart] = useState();
  const [preferencePurchaseBrandChart, setPreferencePurchaseBrandChart] = useState();

  useEffect(() => {
    if (preferencePurchaseLargeCate.status === 'success') {
      const maxMap = preferencePurchaseLargeCate.data.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxData = getChartYAxisMax(maxValue);

      setPreferencePurchaseLargeCateChart({
        series: [{
          data: preferencePurchaseLargeCate.data.series[0].data,
          name: '',
        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          plotOptions: {
            bar: {
              borderRadius: 2,
              horizontal: true,
              barHeight: '50%',
            },
          },
          dataLabels: {
            enabled: false,
          },
          tooltip: {
            followCursor: true,
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          xaxis: {
            categories: preferencePurchaseLargeCate.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
          },
          // legend: {
          //   ...legendDefault,
          //   horizontalAlign: 'center',
          //   customLegendItems: ['전체 활동고객수'],
          // },
        },
      });
    }
  }, [preferencePurchaseLargeCate]);

  useEffect(() => {
    if (preferencePurchaseMiddleCate.status === 'success') {
      const maxMap = preferencePurchaseMiddleCate.data.series.map(({ data }) => data);
      const maxValue = Math.max(...maxMap.flat());
      const maxData = getChartYAxisMax(maxValue);

      setPreferencePurchaseMiddleCateChart({
        series: [{
          data: preferencePurchaseMiddleCate.data.series[0].data,
          name: '',
        }],
        options: {
          chart: {
            toolbar: {
              show: false,
            },
            type: 'bar',
          },
          plotOptions: {
            bar: {
              borderRadius: 2,
              horizontal: true,
              barHeight: '50%',
            },
          },
          dataLabels: {
            enabled: false,
          },
          tooltip: {
            followCursor: true,
            y: {
              formatter(val) {
                return `${(val).toLocaleString()}명`;
              },
            },
          },
          xaxis: {
            categories: preferencePurchaseMiddleCate.data.categories,
            min: 0,
            max: maxData,
            tickAmount: 5,
            labels: {
              formatter(val, index) {
                if (index === 0) {
                  return '0';
                }
                return getChartValuePeople(val);
              },
            },
          },
          // legend: {
          //   ...legendDefault,
          //   horizontalAlign: 'center',
          //   customLegendItems: ['전체 활동고객수'],
          // },
        },
      });
    }
  }, [preferencePurchaseMiddleCate]);

  useEffect(() => {
    if (preferencePurchaseBrand.status === 'success') {
      setPreferencePurchaseBrandChart(preferencePurchaseBrand.data);
    }
  }, [preferencePurchaseBrand]);

  return (
    <Container>
      {/* <div className="chart-name-wrap">
        <p className="chart-name">총 주문금액을 GS SHOP과 비교해보세요.</p>
      </div> */}
      <SubContainer>

        <Tabs defaultActiveKey="1" className="categoryTabs">
          <TabPane tab="카테고리(대)" key="1">
            <ChartContainer>
              <LoadingComponent isLoading={preferencePurchaseLargeCate.status === 'pending'} />
              <div className="chart-area heightAuto">
                {
                  preferencePurchaseLargeCateChart ? (
                    <ReactApexChart options={preferencePurchaseLargeCateChart.options} series={preferencePurchaseLargeCateChart.series} type="bar" height="400" />
                  ) : <EmptyGraph />
                }
              </div>
            </ChartContainer>
          </TabPane>
          {
            categoryMiddleTab && (
              <TabPane tab="카테고리(중)" key="2">
                <ChartContainer>
                  <LoadingComponent isLoading={preferencePurchaseMiddleCate.status === 'pending'} />
                  <div className="chart-area heightAuto">
                    {
                      preferencePurchaseMiddleCateChart ? (
                        <ReactApexChart options={preferencePurchaseMiddleCateChart.options} series={preferencePurchaseMiddleCateChart.series} type="bar" height="400" />
                      ) : <EmptyGraph />
                    }
                  </div>
                </ChartContainer>
              </TabPane>
            )
          }
          <TabPane tab="브랜드" key="3">
            <ChartContainer>
              <LoadingComponent isLoading={preferencePurchaseBrand.status === 'pending'} />
              <div className="tag-area">
                <div className="app-outer">
                  <div className="app-inner">
                    <TagCloud
                      className="tag-cloud"
                      style={{
                        fontFamily: 'Pretendard',
                        fontSize: 20,
                        padding: 10,
                        color: () => (TAGCLOUD_VARIATION[Math.floor(Math.random() * TAGCLOUD_VARIATION.length)]),
                      }}
                    >
                      {preferencePurchaseBrandChart
                          && preferencePurchaseBrandChart.map((val, idx) => (<div key={idx} style={{ color: TAGCLOUD_VARIATION_BLUE[idx] }}>{val}</div>))}
                    </TagCloud>
                  </div>
                </div>
              </div>
            </ChartContainer>
          </TabPane>
        </Tabs>
      </SubContainer>

    </Container>
  );
}

const Container = styled.div`

`;

const SubContainer = styled.div`
  border-radius: 4px;
  background-color: #f7f8fa;
  .app-outer {
    align-items: center; 
    bottom: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    left: 0;
    padding: 20px 0;
    position: absolute;
    right: 0;
    top: 0;
  }

  .app-inner {
    display: flex;
    flex-direction: column;
    height: 100%;
    max-width: 1000px;
    width: 100%;
  }

  .tag-cloud {
    flex: 1;
  }

  .tag-cloud > div {
    transition: 1s;
  }
  .categoryTabs {
    padding-top:20px;
    .ant-tabs-nav-operations {
      display: none;
    }
    > .ant-tabs-nav{
      justify-content: center;
      margin-bottom: 0;
      .ant-tabs-nav-wrap {
        justify-content: center;
      }
      .ant-tabs-tab {
        padding: 0;
      }
      .ant-tabs-tab-btn {
        background-color: var(--color-white);
        border-radius: 0;
        height:32px;
        line-height: 32px;
        width: 88px;
        font-size: 13px;
        border:var(--border-default);
        border-left: 0 none;
      }
      .ant-tabs-tab:first-child .ant-tabs-tab-btn {
        border-radius:4px 0 0 4px;
        border-left:var(--border-default);
      }
      .ant-tabs-tab:nth-last-child(2) .ant-tabs-tab-btn {
        border-radius:0 4px 4px 0;;
      }
      .ant-tabs-tab+.ant-tabs-tab {
        margin: 0;
      }
      .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
        border: 0 none;
        background-color: var(--color-steelGray-800);
        color: var(--color-white);
      }
    }
  }
`;

export default PreferencePurchase;
