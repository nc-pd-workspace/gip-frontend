import {
  all, fork,
  takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import {
  getStepConversion,
  getPreferencePurchaseLargeCate,
  getPreferencePurchaseMiddleCate,
  getPreferencePurchaseBrand,
  getCustomerCharacteMembership,
  getCustomerCharacteColumnDistributed,
  getCustomerCharacteTreemapDistributed,
  getCustomerCharacteStackedBar,
  getCategoryOptions,
  getBrandOptions,
  getAnnualPurchaseCount,
  getAnnualPurchasePay,
} from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const stepConversionSaga = createPromiseSaga(getStepConversion, API.StepConversion.getStepConversion);
const preferencePurchaseLargeCataSaga = createPromiseSaga(getPreferencePurchaseLargeCate, API.StepConversion.getPreferencePurchaseLargeCate);
const preferencePurchaseMiddleCataSaga = createPromiseSaga(getPreferencePurchaseMiddleCate, API.StepConversion.getPreferencePurchaseMiddleCate);
const preferencePurchaseBrandSaga = createPromiseSaga(getPreferencePurchaseBrand, API.StepConversion.getPreferencePurchaseBrand);
const customerCharacteMembershipSaga = createPromiseSaga(getCustomerCharacteMembership, API.StepConversion.getCustomerCharacteMembership);
const getCustomerCharacteColumnDistributedSaga = createPromiseSaga(getCustomerCharacteColumnDistributed, API.StepConversion.getCustomerCharacteColumnDistributed);
const getCustomerCharacteTreemapDistributedSaga = createPromiseSaga(getCustomerCharacteTreemapDistributed, API.StepConversion.getCustomerCharacteTreemapDistributed);
const getCustomerCharacteStackedBarSaga = createPromiseSaga(getCustomerCharacteStackedBar, API.StepConversion.getCustomerCharacteStackedBar);
const getAnnualPurchaseCountSaga = createPromiseSaga(getAnnualPurchaseCount, API.StepConversion.getAnnualPurchaseCount);
const getAnnualPurchasePaySaga = createPromiseSaga(getAnnualPurchasePay, API.StepConversion.getAnnualPurchasePay);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const getBrandOptionsSaga = createPromiseSaga(getBrandOptions, API.Common.getBrandOptions);

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getStepConversion, stepConversionSaga);
  yield takeLatest(getPreferencePurchaseLargeCate, preferencePurchaseLargeCataSaga);
  yield takeLatest(getPreferencePurchaseMiddleCate, preferencePurchaseMiddleCataSaga);
  yield takeLatest(getPreferencePurchaseBrand, preferencePurchaseBrandSaga);
  yield takeLatest(getCustomerCharacteMembership, customerCharacteMembershipSaga);
  yield takeLatest(getCustomerCharacteColumnDistributed, getCustomerCharacteColumnDistributedSaga);
  yield takeLatest(getCustomerCharacteTreemapDistributed, getCustomerCharacteTreemapDistributedSaga);
  yield takeLatest(getCustomerCharacteStackedBar, getCustomerCharacteStackedBarSaga);
  yield takeLatest(getAnnualPurchaseCount, getAnnualPurchaseCountSaga);
  yield takeLatest(getAnnualPurchasePay, getAnnualPurchasePaySaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getBrandOptions, getBrandOptionsSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
