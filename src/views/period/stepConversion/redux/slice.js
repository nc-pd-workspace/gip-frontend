import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  stepConversionData: asyncApiState.initial([]),
  preferencePurchaseLargeCate: asyncApiState.initial([]),
  preferencePurchaseMiddleCate: asyncApiState.initial([]),
  preferencePurchaseBrand: asyncApiState.initial([]),
  customerCharacteMembership: asyncApiState.initial([]),
  customerCharacteColumnDistributed: asyncApiState.initial([]),
  customerCharacteTreemapDistributed: asyncApiState.initial([]),
  customerCharacteStackedBar: asyncApiState.initial([]),
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
  annualPurchaseCount: asyncApiState.initial([]),
  annualPurchasePay: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'marketing/stepConversion',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getStepConversion: (state) => {
      state.stepConversionData = asyncApiState.request();
    },
    getStepConversionSuccess: (state, { payload }) => {
      const result = payload;
      state.stepConversionData = asyncApiState.success(result);
    },
    getStepConversionFailure: (state, { payload }) => {
      state.stepConversionData = asyncApiState.error(payload);
    },
    getPreferencePurchaseLargeCate: (state) => {
      state.preferencePurchaseLargeCate = asyncApiState.request();
    },
    getPreferencePurchaseLargeCateSuccess: (state, { payload }) => {
      const result = payload;
      state.preferencePurchaseLargeCate = asyncApiState.success(result);
    },
    getPreferencePurchaseLargeCateFailure: (state, { payload }) => {
      state.preferencePurchaseLargeCate = asyncApiState.error(payload);
    },
    getPreferencePurchaseMiddleCate: (state) => {
      state.preferencePurchaseMiddleCate = asyncApiState.request();
    },
    getPreferencePurchaseMiddleCateSuccess: (state, { payload }) => {
      const result = payload;
      state.preferencePurchaseMiddleCate = asyncApiState.success(result);
    },
    getPreferencePurchaseMiddleCateFailure: (state, { payload }) => {
      state.preferencePurchaseMiddleCate = asyncApiState.error(payload);
    },
    getPreferencePurchaseBrand: (state) => {
      state.preferencePurchaseBrand = asyncApiState.request();
    },
    getPreferencePurchaseBrandSuccess: (state, { payload }) => {
      const result = payload;
      state.preferencePurchaseBrand = asyncApiState.success(result);
    },
    getPreferencePurchaseBrandFailure: (state, { payload }) => {
      state.preferencePurchaseBrand = asyncApiState.error(payload);
    },
    getCustomerCharacteMembership: (state) => {
      state.customerCharacteMembership = asyncApiState.request();
    },
    getCustomerCharacteMembershipSuccess: (state, { payload }) => {
      const result = payload;
      state.customerCharacteMembership = asyncApiState.success(result);
    },
    getCustomerCharacteMembershipFailure: (state, { payload }) => {
      state.customerCharacteMembership = asyncApiState.error(payload);
    },
    getCustomerCharacteColumnDistributed: (state) => {
      state.customerCharacteColumnDistributed = asyncApiState.request();
    },
    getCustomerCharacteColumnDistributedSuccess: (state, { payload }) => {
      const result = payload;
      state.customerCharacteColumnDistributed = asyncApiState.success(result);
    },
    getCustomerCharacteColumnDistributedFailure: (state, { payload }) => {
      state.customerCharacteColumnDistributed = asyncApiState.error(payload);
    },
    getCustomerCharacteTreemapDistributed: (state) => {
      state.customerCharacteTreemapDistributed = asyncApiState.request();
    },
    getCustomerCharacteTreemapDistributedSuccess: (state, { payload }) => {
      const result = payload;
      state.customerCharacteTreemapDistributed = asyncApiState.success(result);
    },
    getCustomerCharacteTreemapDistributedFailure: (state, { payload }) => {
      state.customerCharacteTreemapDistributed = asyncApiState.error(payload);
    },
    getCustomerCharacteStackedBar: (state) => {
      state.customerCharacteStackedBar = asyncApiState.request();
    },
    getCustomerCharacteStackedBarSuccess: (state, { payload }) => {
      const result = payload;
      state.customerCharacteStackedBar = asyncApiState.success(result);
    },
    getCustomerCharacteStackedBarFailure: (state, { payload }) => {
      state.customerCharacteStackedBar = asyncApiState.error(payload);
    },
    getCategoryOptions: (state) => {
      state.categoryOption = asyncApiState.request([[], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state) => {
      state.categoryOption = asyncApiState.error([[], []]);
    },
    getBrandOptions: (state) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));

      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state) => {
      state.brandOption = asyncApiState.error([]);
    },
    getAnnualPurchaseCount: (state) => {
      state.annualPurchaseCount = asyncApiState.request();
    },
    getAnnualPurchaseCountSuccess: (state, { payload }) => {
      const result = payload;
      state.annualPurchaseCount = asyncApiState.success(result);
    },
    getAnnualPurchaseCountFailure: (state, { payload }) => {
      state.annualPurchaseCount = asyncApiState.error(payload);
    },
    getAnnualPurchasePay: (state) => {
      state.annualPurchasePay = asyncApiState.request();
    },
    getAnnualPurchasePaySuccess: (state, { payload }) => {
      const result = payload;
      state.annualPurchasePay = asyncApiState.success(result);
    },
    getAnnualPurchasePayFailure: (state, { payload }) => {
      state.annualPurchasePay = asyncApiState.error(payload);
    },
  },
});

export const {
  updateState,
  getStepConversion,
  getPreferencePurchaseLargeCate,
  getPreferencePurchaseMiddleCate,
  getPreferencePurchaseBrand,
  getCustomerCharacteMembership,
  getCustomerCharacteColumnDistributed,
  getCustomerCharacteTreemapDistributed,
  getCustomerCharacteStackedBar,
  getAnnualPurchaseCount,
  getAnnualPurchasePay,
  getCategoryOptions,
  getBrandOptions,
} = actions;

export default reducer;
