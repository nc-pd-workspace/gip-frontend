import { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import TagCloud from 'react-tag-cloud';

import { TAGCLOUD_FONT_SIZE, TAGCLOUD_VARIATION, TAGCLOUD_VARIATION_BLUE } from '../../../../styles/chartColors';

import { SvgArrowDropdown } from '../../../../Images';

import Search from '../../../../components/search';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import CategorySelects from '../../../../components/search/CategorySelects';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';

import { getBrandOptions, getCategoryOptions, getInflow } from '../redux/slice';

import LoadingComponent from '../../../../components/loading';
import EmptyGraph from '../../../../components/emptyGraph';

function InflowKeywordContainer() {
  const dispatch = useDispatch();

  const { selectPtnIdx } = useSelector((state) => state.common);

  const { inflow, categoryOption, brandOption } = useSelector((state) => ({
    inflow: state.marketing.inflowKeyword.inflow,
    categoryOption: state.marketing.inflowKeyword.categoryOption,
    brandOption: state.marketing.inflowKeyword.brandOption,
  }));
  const searchRef = useRef();
  const [search, setSearch] = useState({});
  const [sort, setSort] = useState('srchCnt'); // dataType avg, sum
  const [brand, setBrand] = useState(null);

  useEffect(() => {
    dispatch(getBrandOptions({ params: {
      ptnIdx: selectPtnIdx,
    } }));
  }, []);

  useEffect(() => {
    searchRef.current.clickSearch();
  }, []);

  useEffect(() => {
    getCategory();
  }, [brand]);

  useEffect(() => {
    const params = {
      ptnIdx: selectPtnIdx,
    };

    if (Object.keys(search).length === 0) return;
    if (search.month) {
      params.searchMonth = search.month.replace('.', '');
    }

    if (search.categoryData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.categoryData;
      if (categorySCode) params.categorySCode = categorySCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categoryLCode) params.categoryLCode = categoryLCode;
    }
    if (search.brandCd) params.brandCd = search.brandCd ? search.brandCd.toString() : '';
    // params.dataType = dataType;
    params.sortOrder = sort;

    dispatch(getInflow({ params }));
  }, [search, selectPtnIdx, sort]);

  // const handleDataTypeChange = (value) => {
  //   setDataType(value.target.value);
  // };

  const handleChangeSort = (value) => {
    setSort(value);
  };

  const sortOptions = [
    { label: '검색수순', value: 'srchCnt' },
    { label: '진입률순', value: 'entryRate' },
    { label: '매출전환율순', value: 'salesConversionRate' },
    { label: '검색어순', value: 'searchWord' },
  ];

  const onChangeBrand = (value) => {
    setBrand(value);
  };

  const onReset = () => {
    getCategory();
  };

  const getCategory = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (brand) {
      params.brandCd = brand;
    }
    dispatch(getCategoryOptions({ params }));
  };

  const limitConut = (val, idx) => {
    if (idx < 50) {
      return (<div key={idx} style={{ fontSize: TAGCLOUD_FONT_SIZE[idx], color: TAGCLOUD_VARIATION_BLUE[idx] }}>{val.searchWord}</div>);
    }
    return (<div />);
  };

  return (
    <Container>
      <PageHeader
        title="유입 검색어 분석"
        subTitle="GS SHOP에서의 내 브랜드로의 유입 검색어, 진입률, 매출 전환율을 확인할 수 있습니다."
      />
      <Search setSearch={setSearch} ref={searchRef} onReset={onReset}>
        <SingleInputItem
          type="DatePicker"
          column="month"
          title="조회 월"
          width="50%"
          picker="month"
        />
        <SingleInputItem
          type="Select"
          column="brandCd"
          title="브랜드"
          width="50%"
          loading={brandOption.status === 'pending'}
          options={brandOption.data}
          onChange={onChangeBrand}
          placeholder="전체"
        />
        <CategorySelects
          column="categoryData"
          title="카테고리"
          // required
          depth={categoryOption.data.length}
          options={categoryOption.data}
          loading={categoryOption.status === 'pending'}
          placeholder="카테고리 선택"
        />
      </Search>

      <SearchResult>
        <Title>
          <p>
            조회 결과
          </p>
          <div className="searchResultToolBox">
            {/* <span className="caption-text">데이터 기준</span>
            <div className="right-item set-section-toggle">
              <Radio.Group defaultValue="avg" buttonStyle="solid" onChange={handleDataTypeChange}>
                <Radio.Button value="avg">평균</Radio.Button>
                <Radio.Button value="sum">합계</Radio.Button>
              </Radio.Group>
            </div> */}
            <div className="right-item set-select">
              <Select
                onChange={handleChangeSort}
                style={{ width: 120 }}
                options={sortOptions}
                value={sort}
                suffixIcon={<SvgArrowDropdown />}
              />
            </div>
            {/* <ButtonExcel /> */}
          </div>
        </Title>
        <LoadingComponent isLoading={inflow.status === 'pending'} />
        <div>
          {
            inflow.status === 'success' && inflow.data ? (
              <Contents>
                <ChartWrap>
                  <ChartContainer>
                    <div className="tag-area">
                      <div className="app-outer">
                        <div className="app-inner">
                          {
                            inflow.status === 'success' && inflow.data ? (
                              <TagCloud
                                className="tag-cloud"
                                style={{
                                  fontSize: 13,
                                  padding: 4,
                                  fontFamily: 'Pretendard',
                                  color: () => (TAGCLOUD_VARIATION[Math.floor(Math.random() * TAGCLOUD_VARIATION.length)]),
                                }}
                              >
                                {inflow.status === 'success'
                          && inflow.data.map((val, idx) => limitConut(val, idx))}
                              </TagCloud>
                            ) : <EmptyGraph />
                          }

                        </div>
                      </div>
                    </div>
                  </ChartContainer>
                </ChartWrap>
                <TableWrap>
                  <table className="ui-table">
                    <colgroup>
                      <col width="50px" />
                      <col width="40%" />
                      <col width="20%" />
                      <col width="20%" />
                      <col width="20%" />
                    </colgroup>
                    <thead className="ui-thead">
                      <tr>
                        <th>번호</th>
                        <th>검색어</th>
                        <th>검색수</th>
                        <th>진입율</th>
                        <th>매출 전환율</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        inflow.data && inflow.status === 'success' ? (
                          inflow.data.map((data, index) => (
                            <tr key={index}>
                              <td>{data.seqNo}</td>
                              <td>{data.searchWord}</td>
                              <td>{data.srchCnt.toLocaleString('ko-KR')}</td>
                              <td>
                                {data.entryRate}
                                %
                              </td>
                              <td>
                                {data.salesConversionRate}
                                %
                              </td>
                            </tr>
                          ))
                        ) : <EmptyGraph />
                      }
                    </tbody>
                  </table>
                </TableWrap>
              </Contents>
            ) : <EmptyGraph />
          }
        </div>
      </SearchResult>
    </Container>
  );
}

const SearchResult = styled(Paper)`
  margin-top: 40px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;

  .set-select {
    margin-left:6px;
    .ant-select-single:not(.ant-select-customize-input) .ant-select-selector{
      height:28px;
    }
    .ant-select-single .ant-select-selector .ant-select-selection-item, .ant-select-single .ant-select-selector .ant-select-selection-placeholder{
      line-height:26px;
      font-size:12px;
    }
  }

  .descText{
    padding:24px 0;
    color: var(--color-black);
    font-size:14px;
  }
  .set-select {
    margin-left:6px;
    .ant-select-single:not(.ant-select-customize-input) .ant-select-selector{
      height:28px;
    }
    .ant-select-single .ant-select-selector .ant-select-selection-item, .ant-select-single .ant-select-selector .ant-select-selection-placeholder{
      line-height:26px;
      font-size:12px;
    }
  }
  .ant-select-arrow {
    width:16px;
    height:16px;
    margin-top:-10px;
  }
`;

const Title = styled.div`
  padding-top: 19px;
  color: var(--color-gray-900);
  font-weight: 700;
  font-size: 20px;
  height:60px;
  .right {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
  }
  .caption-text {
    color: var(--color-gray-500);
    margin-right: 5px;
    font-weight: 400;
    font-size: 12px;
  }
`;

const Contents = styled.div`
  position: relative;
  padding: 20px 0;
  display: flex;
  height: auto;
`;

const ChartWrap = styled.div`
  max-height: 500px;
  overflow: auto;
  flex: 1;
  padding-right: 20px;
  .app-outer {
    align-items: center; 
    bottom: 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
  }
  .app-inner {
    display: flex;
    flex-direction: column;
    height: 100%;
    max-width: 1000px;
    width: 100%;
  }
  .tag-cloud {
    flex: 1;
    width: 100%;
    height: 100%;
    -moz-border-radius: 100%;
    -webkit-border-radius: 100%;
    border-radius: 100%;
  }
  .tag-cloud > div {
    transition: 1s;
  }
`;
const TableWrap = styled.div`
  flex: 0 0 402px;
  max-height: 534px;
  overflow: auto;
  border-top: 1px solid rgba(227, 228, 231, 0.5);
  &::-webkit-scrollbar{ 
    width: 6px; 
    height: 6px;
  }
  &::-webkit-scrollbar-thumb{
    background-color: rgba(55,55,55,0.5); 
  }
  &::-webkit-scrollbar-track{ 
    background-color: var(--background-default);
  }
  .ui-table {
    table-layout: fixed;
    width: 100%;
  }
  .ui-thead {
    position: sticky;
    top: 0;
    z-index: 1;
    background-color: #fff;
  }

  & > table > thead > tr > th {
    height: 32px;
    border-bottom: 1px solid rgba(227, 228, 231, 0.5);
    color: #666;
    font-size: 12px;
    font-weight: 400;
    vertical-align: middle;
    &:nth-child(1) {
      text-align: left;
      padding-left: 10px;
    }
    &:nth-child(2) {
      text-align: left;
    }
    &:nth-child(3) {
      text-align: right;
    }
    &:nth-child(4) {
      text-align: right;
      padding-right: 10px;
    }
  }

  & > table > tbody > tr {
    border-bottom: 1px solid rgba(227, 228, 231, 0.5);
    font-size: 13px;
  }

  & > table > tbody> tr > td {
    position: relative;
    width: 20%;
    padding: 0 0px;
    text-align: left;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    height: 50px;
    vertical-align: middle;
    &:first-child {
      text-align: left;
      padding-left: 10px;
    }
    &:nth-child(3) {
      text-align: right;
    }
    &:nth-child(4) {
      text-align: right;
    }
    &:nth-child(5) {
      text-align: right;
      padding-right: 10px;
    }
  }
`;

const Container = styled(PageLayout)`
  .ui-box {
    padding: 20px;
    border-radius: 8px;
    border: 1px solid #e3e4e7;
    background-color: #fff;
  }
`;

const ChartContainer = styled.div`
  display: flex;
  margin-bottom: 10px;
  .chart-area:first-child{margin-right:5px;} 
  .chart-area:last-child{margin-left:5px;} 
  .chart-title{
    padding: 20px 0px 0px 20px;
  }
  .chart-area{
    flex-direction: column;
    background: #F7F8FA;
    display: flex;
    flex: 1;
  }
  .tag-area{
    height: 400px;
    width: 100%;
    position: relative;
  }
`;

export default InflowKeywordContainer;
