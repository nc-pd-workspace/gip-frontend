import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  inflow: asyncApiState.initial([]),
  categoryOption: asyncApiState.initial([]),
  brandOption: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'marketing/inflowKeyword',
  initialState,
  reducers: {
    updateState: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    getInflow: (state, { payload }) => {
      state.inflow = asyncApiState.request();
    },
    getInflowSuccess: (state, { payload }) => {
      const result = { ...payload };
      state.inflow = asyncApiState.success(result);
    },
    getInflowFailure: (state, { payload }) => {
      state.inflow = asyncApiState.error(payload);
    },
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], []]);
    },
    getBrandOptions: (state, { payload }) => {
      state.brandOption = asyncApiState.request([]);
    },
    getBrandOptionsSuccess: (state, { payload }) => {
      const result = payload;
      const arr = result.data.map((v) => ({ label: v.brandNm, value: v.brandCd }));

      arr.unshift({ label: '전체', value: '' });
      state.brandOption = asyncApiState.success({ data: arr });
    },
    getBrandOptionsFailure: (state, { payload }) => {
      state.brandOption = asyncApiState.error([]);
    },
  },
});

export const { updateState, getInflow, getCategoryOptions, getBrandOptions } = actions;

export default reducer;
