import { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Radio } from 'antd';

import { useDispatch, useSelector } from 'react-redux';

import Search from '../../../../components/search';
import SingleRangePicker from '../../../../components/search/SingleRangePicker';
import SingleInputItem from '../../../../components/search/SingleInputItem';
import MultipleRow from '../../../../components/search/MultipleRow';
import ProductSearch from '../components/ProductSearch';

import PageHeader from '../../../../components/header/PageHeader';
import { PageLayout } from '../../../shared/layout/Layout.Styled';
import Paper from '../../../../components/paper';
import TabSelectChart from '../../../customer/allActivityCustomer/components/TabSelectChart';
import TabCharts from '../components/TabCharts';
import Table from '../../../../components/table';
import { resetStore, getProductCustomerList, getProductCustomerStatistics, getProductCustomerSubChart } from '../redux/slice';
import { getCustomerSelects } from '../../../../redux/commonReducer';

import { talbecolumns, TabHeadTitle } from '../constants';
import TotalRow from '../../../../components/paper/totalRow';
import LoadingComponent from '../../../../components/loading';
import EmptyList from '../../../../components/emptyList';
import { alertMessage } from '../../../../components/message';

// 상품별 고객 분석
function ProductCustomerContainer() {
  const dispatch = useDispatch();
  const { selectPtnIdx } = useSelector((state) => state.common);
  const { customerSelects } = useSelector((state) => state.common);
  const {
    productList,
    statistics,
    newUser,
    total,
    interest,
    order,
    barNegative,
    radarMultiple,
    columnDistributed,
    treemapDistributed,
    stackedColumnCount,
    stackedColumnAmount,
    barCategoryL,
    barCategoryM,
    brandGraph,
  } = useSelector((state) => state.customer.productCustomer);

  const [search, setSearch] = useState({});
  const [detailOnOff, setDetailOnOff] = useState('on');
  const [currentSelectRow, setCurrentSelectRow] = useState(null);
  const [dataType, setDataType] = useState('avg');
  const [selectedTab, setSelectedTab] = useState(0);

  function RadioOnchange(v) {
    setDataType(v.target.value);
  }

  const onClickRow = (currRow) => {
    setCurrentSelectRow(currRow);
  };

  const conditionHighlight = (row) => {
    if (currentSelectRow && row.seqNo === currentSelectRow.seqNo) {
      return 'currentSelectedRow';
    }
    return '';
  };

  const makeDefaultSearchParam = () => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (search.searchStartDate) {
      if (search.searchStartDate.length > 7) params.searchDateType = 'date';
      else params.searchDateType = 'month';

      params.searchStartDate = search.searchStartDate;
    }
    if (search.searchEndDate) params.searchEndDate = search.searchEndDate;

    if (search.productCode) {
      params.productCode = search.productCode;
    }
    if (search.genderCode) params.genderCode = search.genderCode;
    if (search.custGradeCode) params.custGradeCode = search.custGradeCode;
    if (search.sidoCode) params.sidoCode = search.sidoCode;
    if (search.age5UnitCode) {
      params.age5UnitCode = search.age5UnitCode.join(',');
    }
    return params;
  };

  const fetchStatistics = () => {
    const defaultParams = makeDefaultSearchParam();
    defaultParams.prdCd = currentSelectRow.prdCd;

    const params = {
      statistics: { ...defaultParams, dataType },
      newUser: { ...defaultParams, cardType: 'new' },
      total: { ...defaultParams, cardType: 'total' },
      interest: { ...defaultParams, cardType: 'interest' },
      order: { ...defaultParams, cardType: 'order' },
    };
    dispatch(getProductCustomerStatistics({ params }));
  };

  const onChangeSelectTab = useCallback((tab) => {
    if (isLoadingSubChart()) {
      alertMessage(`${TabHeadTitle[selectedTab]} 데이터 조회 중입니다. 잠시만 기다려 주세요.`);
      return;
    }
    setSelectedTab(tab);
  }, [selectedTab, barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph]);

  const isLoadingSubChart = () => {
    if (isCustomerTabVisible()) {
      const arr = [barNegative, radarMultiple, columnDistributed, treemapDistributed, stackedColumnCount,
        stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
      return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
    }
    const arr = [stackedColumnCount, stackedColumnAmount, barCategoryL, barCategoryM, brandGraph].map((v) => v.status);
    return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
  };

  const isLoadingStaticsChart = () => {
    const arr = [statistics, newUser, total, interest, order].map((v) => v.status);

    return arr.filter((v) => v === 'initial' || v === 'pending').length > 0;
  };

  const isCustomerTabVisible = () => {
    if (search.genderCode || search.custGradeCode || search.sidoCode || (search.age5UnitCode && search.age5UnitCode.length > 0)) return false;
    return true;
  };

  useEffect(() => {
    if (search.productCode) {
      const params = makeDefaultSearchParam();
      dispatch(getProductCustomerList({ params }));
    }
  }, [search]);

  useEffect(() => {
    dispatch(getCustomerSelects());
    return () => {
      dispatch(resetStore());
    };
  }, []);

  useEffect(() => {
    if (productList.status === 'success') {
      if (productList.data.length > 0) {
        setCurrentSelectRow(productList.data[0]);
      }
    }
  }, [productList]);

  useEffect(() => {
    if (customerSelects.status === 'error') {
      alertMessage('select 데이터를 불러오지 못했습니다.');
    }
  }, [customerSelects]);

  useEffect(() => {
    if (currentSelectRow) fetchStatistics();
  }, [currentSelectRow, dataType]);

  useEffect(() => {
    if (!isLoadingStaticsChart()) {
      if (currentSelectRow) {
        const customerTabVisible = isCustomerTabVisible();
        const defaultParams = makeDefaultSearchParam();
        const tabtype = ['new', 'total', 'interest', 'order'];
        delete defaultParams.productCode;
        defaultParams.prdCd = currentSelectRow.prdCd;
        defaultParams.cardType = tabtype[selectedTab];

        const params = {
          barNegative: customerTabVisible ? defaultParams : null,
          radarMultiple: customerTabVisible ? defaultParams : null,
          columnDistributed: customerTabVisible ? defaultParams : null,
          treemapDistributed: customerTabVisible ? defaultParams : null,
          stackedColumnCount: defaultParams,
          stackedColumnAmount: defaultParams,
          barCategoryL: defaultParams,
          barCategoryM: defaultParams,
          brandGraph: defaultParams,
        };
        dispatch(getProductCustomerSubChart({ params }));
      }
    }
  }, [selectedTab, statistics, newUser, total, interest, order]);

  return (
    <Container>
      <PageHeader
        title="상품별 고객 분석"
        subTitle="브랜드 상품별 활동고객과 특성을 확인할 수 있습니다."
      />
      <Search setSearch={setSearch} detailOnOff={detailOnOff} setDetailOnOff={setDetailOnOff}>
        <SingleRangePicker
          column={['searchStartDate', 'searchEndDate']}
          title="조회기간"
          disabledToday
          disabled2020
          maxSelectDate={90}
          maxSelectMonth={12}
          dropdownClassName="productCustomerPickerPopup"
        />

        <ProductSearch column="productCode" title="상품코드" />
        <MultipleRow isDetail detailOnOff={detailOnOff}>
          <SingleInputItem type="Select" column="genderCode" title="성별" options={customerSelects.data.gender} placeholder="전체" />
          <SingleInputItem type="Select" column="custGradeCode" title="고객등급" options={customerSelects.data.custGrade} placeholder="전체" />
          <SingleInputItem type="Select" column="sidoCode" title="지역" options={customerSelects.data.sido} placeholder="전체" />
        </MultipleRow>
        <SingleInputItem type="MultiSelect" column="age5UnitCode" title="연령대" width="50%" options={customerSelects.data.ageUnit} isDetail detailOnOff={detailOnOff} placeholder="전체" />
      </Search>
      {
        productList.status === 'success' && productList.data.length > 0 ? (
          <>
            <TableResult>
              <Title>
                <p>상품정보 조회</p>
                <TotalRow row={productList.data.length} />
                <div className="searchResultToolBox">
                  {/* <ButtonExcel /> */}
                </div>
              </Title>
              <Table
                dataSource={productList.data ? productList.data : []}
                columns={talbecolumns}
                conditionalHighlight={conditionHighlight}
                onClickRow={onClickRow}
                rowKey={(record) => record.seqNo}
                scrollY={239}
              />
            </TableResult>
            <p className="bottom_text">
              · 브랜드명을 클릭 하시면, 하단에 GS SHOP과 브랜드의 전년대비 성장률을 비교한 그래프가 제공됩니다.
            </p>
            {
              currentSelectRow && (
                <>
                  <SearchResult>
                    <Title>
                      <p>
                        <ProductTitle>{currentSelectRow.prdNm.length > 16 ? `${currentSelectRow.prdNm.substr(0, 10)}...` : currentSelectRow.prdNm}</ProductTitle>
                        상품의 고객활동 분석
                      </p>
                      {
                        !isLoadingStaticsChart() && (
                          <div className="searchResultToolBox">
                            <span className="caption-text">데이터 기준</span>
                            <div className="right-item set-section-toggle">
                              <Radio.Group value={dataType} buttonStyle="solid" onChange={(v) => { RadioOnchange(v); }}>
                                <Radio.Button value="avg">평균</Radio.Button>
                                <Radio.Button value="sum">합계</Radio.Button>
                              </Radio.Group>
                            </div>
                          </div>
                        )
                      }
                    </Title>
                    {
                      !isLoadingStaticsChart() ? (
                        <>
                          <TabCharts selectedTab={selectedTab} setSelectedTab={onChangeSelectTab} />
                          <TabSelectChart
                            barNegative={barNegative}
                            radarMultiple={radarMultiple}
                            columnDistributed={columnDistributed}
                            treemapDistributed={treemapDistributed}
                            stackedColumnCount={stackedColumnCount}
                            stackedColumnAmount={stackedColumnAmount}
                            barCategoryL={barCategoryL}
                            barCategoryM={barCategoryM}
                            brandGraph={brandGraph}
                            categoryMDisabled
                            customerTabVisible={isCustomerTabVisible()}
                          />
                        </>
                      ) : (
                        <LoadingWrap>
                          <LoadingComponent isLoading />
                        </LoadingWrap>
                      )
                    }
                  </SearchResult>
                  {
                    !isLoadingStaticsChart() && (
                      <p className="bottom_text">
                        · 조회월에 활동한 고객 데이터를 제공합니다. (데이터 기준 : 활동고객수)
                      </p>
                    )
                  }
                </>
              )
            }
          </>
        ) : (
          <>
            {
              productList.status === 'pending' ? (
                <TableResult>
                  <Title>
                    <p>상품정보 조회</p>
                  </Title>
                  <LoadingWrap>
                    <LoadingComponent isLoading />
                  </LoadingWrap>
                </TableResult>
              ) : (
                <>
                  <SearchResult>
                    <EmptyList warningTitle={productList.status === 'initial' ? '상품정보 조회를 위해 상품코드를 입력해 주세요.' : null} />
                  </SearchResult>
                </>
              )
            }
          </>
        )
      }
    </Container>
  );
}

const Container = styled(PageLayout)`
.caption-text{
  color: var(--color-gray-500);
  margin-right: 5px;
    font-weight: 400;
    font-size: 12px;
}
  .bottom_text{
    color: var(--color-gray-500);
    font-size: 12px; 
    margin-top:10px;
  }
  
`;
const TableResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;
  table  {
    col:nth-of-type(1),
    tr th:nth-of-type(1),
    tr td:nth-of-type(1) {
      width: 50px !important;
      padding-left: 10px;
    }
    col:nth-of-type(2),
    tr th:nth-of-type(2),
    tr td:nth-of-type(2) {
      width: calc(25% - 50px) !important;
      white-space: nowrap
    }
    col:nth-of-type(3),
    tr th:nth-of-type(3),
    tr td:nth-of-type(3) {
      width: 50% !important;
    }
    col:nth-of-type(4),
    tr th:nth-of-type(4),
    tr td:nth-of-type(4) {
      width: calc(25% - 6px) !important;
    }
    col:nth-of-type(5),
    tr th:nth-of-type(5) {
      width: 6px !important;
    }
  }
`;

const SearchResult = styled(Paper)`
  margin-top: 50px;
  border: 1px solid #E3E4E7;
  padding: 0px 20px;
`;
const Title = styled.div`
  display:flex;
  font-weight: 700;
  p{
    padding: 20px 0px;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .right{
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
  }
  .total{
    position: absolute;
    left: 135px;
    top: 30px;
    font-size: 12px;
    color: var(--color-blue-500);
  }

`;

const ProductTitle = styled.span`
  overflow: hidden;
  display: inline-block;
  vertical-align: bottom;
`;

const LoadingWrap = styled.div`
  position: relative;
  height: 330px;
`;

export default ProductCustomerContainer;
