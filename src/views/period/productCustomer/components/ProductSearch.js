import {
  useRef, useState, useImperativeHandle, forwardRef, useEffect, useCallback, useLayoutEffect,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styled from 'styled-components';
import cn from 'classnames';

import { Button, Input, Modal } from 'antd';

import TypeText from '../../../../components/search/TypeText';
import ModalSearch from '../../../../components/search/ModalSearch';
import CategorySelects from '../../../../components/search/CategorySelects';
import Paper from '../../../../components/paper';
// import Table from '../../../../components/table';
import PagingTable from '../../../../components/pagingTable';

import Images from '../../../../Images';
import { getCategoryOptions, getProductCustomerModalList, updateStore } from '../redux/slice';
import { getProductValidationTransform } from '../../../../utils/utils';
import { asyncApiState } from '../../../../redux/constants';
import { confirmMessage } from '../../../../components/message';

function ProductSearch({
  column, className, title, isDetail, detailOnOff,
}, ref) {
  const MIN_TEXTAREA_HEIGHT = 22;
  const dispatch = useDispatch();

  const { selectPtnIdx } = useSelector((state) => state.common);
  const { categoryOption, modalList } = useSelector((state) => state.customer.productCustomer);

  const initialState = {
    text: '',
    isModalVisible: false,
    // table
    data: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: false,
    },
  };

  const columns = [
    {
      title: '상품 코드',
      dataIndex: 'prdCd',
      type: 'ellipsis',
    },
    {
      title: '상품명',
      dataIndex: 'prdNm',
      type: 'ellipsis',
    },
    {
      title: '카테고리',
      dataIndex: 'sectNm',
      type: 'ellipsis',
    },
  ];

  const wordTypeOptions = [
    { label: '상품코드', value: 'prdCd' },
    { label: '상품명', value: 'prdNm' },
  ];
  const tableRef = useRef(null);
  const textAreaRef = useRef(null);

  const [search, setSearch] = useState({});
  const [state, setState] = useState(initialState);
  const [isFocus, setIsFocus] = useState(false);
  const [error, setError] = useState(null);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const onCheckedRow = useCallback((selectedCheckRow) => {
    setSelectedRowKeys(selectedCheckRow);
  });

  const onCheckedDelete = useCallback((rowKey) => {
    setSelectedRowKeys(selectedRowKeys.filter((data) => data !== rowKey));
  });

  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const onClickModalOpen = () => {
    updateState({ isModalVisible: true });
  };

  const onChangeText = (e) => {
    let { value } = e.target;
    value = getProductValidationTransform(value);
    setError(null);
    const arr = value.split(',');
    if (arr.length > 50) {
      setError('최대 50개까지 조회 가능합니다.');
      arr.splice(50);
    }
    updateState({ text: arr.join(',') });
  };

  const onFocusOutTextArea = () => {
    setIsFocus(false);
    setError(null);
    const arr = state.text.split(',').filter((v) => v !== '');

    const uniqueArr = arr.filter((element, index) => arr.indexOf(element) === index);

    if (uniqueArr.length > 50) {
      setError('최대 50개까지 조회 가능합니다.');
      uniqueArr.splice(50);
    }
    updateState({ text: uniqueArr.join(',') });
  };
  const handleOk = () => {
    // updateState({ text: '989928712, 827819374, 722284224', isModalVisible: false });
    setError(null);
    setSelectedRowKeys([]);
    updateState({
      text: selectedRowKeys.join(','),
      isModalVisible: false,
      data: [],
      pagination: {
        current: 1,
        pageSize: 10,
        total: 0,
        showSizeChanger: false,
      } });
  };

  const handleCancel = () => {
    confirmMessage('취소하시겠습니까?', () => {
      updateState({
        isModalVisible: false,
        data: [],
        pagination: {
          current: 1,
          pageSize: 10,
          total: 0,
          showSizeChanger: false,
        },
      });
      setSelectedRowKeys([]);
      dispatch(updateStore({ modalList: asyncApiState.initial({}) }));
    }, '예', '아니오');
  };

  const handleTableChange = (pagination, filters, sorter) => {
    // console.log('tableChange', pagination);
    loadList(pagination);
    updateState({ ...state, pagination: { ...state.pagination, ...pagination } });
  };

  const onValidationTransform = (select, text) => {
    if (select === 'prdCd') return getProductValidationTransform(text);
    return text;
  };

  const loadList = (pagination) => {
    const params = {
      ptnIdx: selectPtnIdx,
    };
    if (search.catData) {
      const [categoryLCode = null, categoryMCode = null, categorySCode = null] = search.catData;
      if (categoryLCode) params.categoryLCode = categoryLCode;
      if (categoryMCode) params.categoryMCode = categoryMCode;
      if (categorySCode) params.categorySCode = categorySCode;
    }
    if (search.searchType) {
      params.searchType = search.searchType;
      params.searchValue = search.searchValue;
    }
    params.page = pagination.current - 1;
    params.size = pagination.pageSize;

    // 나중에 category L 데이터 필수로 지정여부 검토
    dispatch(getProductCustomerModalList({ params }));
  };

  const renderError = () => {
    if (error) {
      return (
        <ErrorWrap role="alert" className="ant-form-item-explain-error">
          {error}
        </ErrorWrap>
      );
    }
    return (<></>);
  };

  useImperativeHandle(ref, () => ({
    getResultData: () => {
      if (!state.text || state.text === '') setError('상품 코드를 입력해주세요.');
      if (state.text) {
        return { [column]: state.text };
      }
      return {};
    },
    setReset: () => {
      setError(null);
      updateState(initialState);
    },
  }));

  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState);
    }
  }, [detailOnOff]);

  useEffect(() => {
    if (Object.keys(search).length > 0) {
      loadList(state.pagination);
    }
  }, [search]);

  useEffect(() => {
    dispatch(getCategoryOptions({ params: { ptnIdx: selectPtnIdx } }));
  }, []);

  // table
  useEffect(() => {
    if (modalList.status === 'success') {
      setState({
        ...state,
        data: modalList.data && modalList.data.items ? modalList.data.items : [],
        pagination: {
          ...state.pagination,
          total: modalList.data && modalList.data.totalItem
            ? modalList.data.totalItem : 0,
        } });
    }
  }, [modalList]);

  useLayoutEffect(() => {
    textAreaRef.current.style.height = 'inherit';
    // Set height
    textAreaRef.current.style.height = `${Math.max(
      textAreaRef.current.scrollHeight,
      MIN_TEXTAREA_HEIGHT,
    )}px`;
  }, [state.text]);

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
      <div className="title required">{title}</div>
      <div className="content">
        <ContentWrap>
          <FormWrap>
            <CustomTextArea
              placeholder={isFocus ? '' : 'GS SHOP 내 상품코드를 입력해주세요. (ex. 98990065, 98990065 ...)'}
              onFocus={() => setIsFocus(true)}
              onBlur={() => onFocusOutTextArea()}
              onChange={onChangeText}
              rows={1}
              ref={textAreaRef}
              value={state.text}
            />
            <ModalButton onClick={onClickModalOpen}>
              <img src={Images.iconSearch} alt="상품찾기" />
              상품찾기
            </ModalButton>
            {
              state.isModalVisible && (
                <ModalContent
                  title={title}
                  visible
                  wrapClassName="rxtione"
                  onOk={handleOk}
                  onCancel={handleCancel}
                  // width={800}
                  width={800}
                  footer={[<Button key="back" onClick={handleCancel}>취소</Button>,
                    <Button
                      key="submit"
                      type="primary"
                      className={selectedRowKeys.length === 0 ? 'disabled' : ''}
                      disabled={selectedRowKeys.length === 0}
                      onClick={handleOk}
                    >
                      선택완료
                    </Button>]}
                >
                  <ModalSearch setSearch={setSearch} className="test">
                    <CategorySelects
                      column="catData"
                      title="카테고리"
                      loading={categoryOption.status === 'pending'}
                      depth={categoryOption.data.length}
                      options={categoryOption.data}
                      popupContainer=".test"
                      placeholder="카테고리 선택"
                    />
                    <TypeText
                      column="searchValue"
                      selectColumn="searchType"
                      title="검색어"
                      options={wordTypeOptions}
                      width="98.5%"
                      validationTransform={onValidationTransform}
                      defaultSelectValue="prdCd"
                      validation={(text) => (text.length === 1 ? '검색어는 최소 2자 이상 입력해 주세요.' : null)}
                    />
                  </ModalSearch>
                  <SearchResult>
                    <Title>
                      <p>조회 결과</p>
                      <span className="total">
                        { (modalList.data.totalItem > 0) ? (<>{`총 ${modalList.data.totalItem}개`}</>) : '' }
                      </span>
                    </Title>
                    {modalList.status === 'initial' || (modalList.status === 'success' && state.data.length === 0)
                      ? (
                        <EmptyWrap>
                          <img className="" src={Images.modal_empty} alt="excel_log" width={150} height={150} />
                          <p>
                            { modalList.status === 'initial' ? '상품 조회를 위해 카테고리 선택 또는 검색어를 입력해주세요.' : (
                              <>
                                조회가능한 상품이 없습니다
                                <br />
                                카테고리 설정 또는 입력하신 검색어를 다시 확인해보세요.
                              </>
                            )}
                          </p>
                        </EmptyWrap>
                      ) : (
                        <>
                          <CustomPagingTable
                            ref={tableRef}
                            columns={columns}
                            data={state.data}
                            pagination={state.pagination}
                            loading={modalList.status === 'pending'}
                            rowKey={(record) => record.prdCd}
                            maxSelectRowCount={50}
                            // showRowIndex
                            onChange={handleTableChange}
                            onCheckedRow={onCheckedRow}
                            rowSelected={selectedRowKeys}
                            // onRowClick={handleRowClick}
                          />
                          {
                            selectedRowKeys.length > 0 && (
                              <Paper>
                                <ListSelectData>
                                  {
                                    selectedRowKeys.map((data) => (
                                      <li key={data}>
                                        <button onClick={() => onCheckedDelete(data)}>
                                          {data}
                                          <img className="" src={Images.x_circle} alt="excel_log" width={14} height={14} />
                                        </button>
                                      </li>
                                    ))
                                  }
                                </ListSelectData>
                                <SelectData>
                                  <span>선택된 항목</span>
                                  <span className="selectNumber">
                                    {selectedRowKeys.length}
                                    /
                                    50
                                    개
                                  </span>
                                </SelectData>
                              </Paper>
                            )
                          }
                        </>
                      ) }
                  </SearchResult>

                </ModalContent>
              )
            }

          </FormWrap>
          {renderError()}

        </ContentWrap>
      </div>
    </Container>
  );
}

const SelectData = styled.div`
  text-align: right;
  font-size: 13px;
  margin-top: 6px;
  color: var(--color-gray-500);
  font-weight: 400;
  .selectNumber{
    color: var(--color-blue-500);
    margin-left: 2px;
  }
`;
const EmptyWrap = styled.div`
    text-align: center;
    margin: 50px;
  p{
    padding-top: 10px;
    font-size: 14px;
    color: var(--color-gray-700);
  }

`;
const ListSelectData = styled.ul`
  margin-top: 20px;
  padding: 10px;
  text-align: center;
  display: flow-root;
  background: #F7F8FA;
  border: 1px solid #E3E4E7;
  border-radius: 4px;
  li {
    /* border:1px solid #ccc; */
    border-radius: 3px;
    margin: 5px;
    float: left;
  }
  li > button{
    display: flex;
    align-items: center;
    background:var(--color-white) !important;
    border: 1px solid #E3E4E7 !important;
  }
  li > button > img {
    margin-left: 4px;
  }
`;

const Title = styled.div`
  display:flex;
  font-weight: 700;
  position: relative;
  p{
    padding: 20px 0px 8px 0px;
    color: var(--color-gray-900);
    font-size: 20px;
  }
  .total{
    position: absolute;
    left: 80px;
    bottom: 12px;
    font-size: 12px;
    color: var(--color-blue-500);
  }
`;
const SearchResult = styled(Paper)`
    /* margin-top: 40px; */
    /* border: 1px solid #E3E4E7; */
    padding: 0px 20px;
    border-radius: 0px;

`;
const ModalContent = styled(Modal)`
  position: relative;
  .ant-modal-body{
      background-color: #F7F8FA;
    padding: 0px;
    }
    .ant-modal-footer{
      padding: 40px 0;
      border: 0px;
      text-align: center !important;

      .disabled {
        color: rgba(0,0,0,.25);
        border-color: #d9d9d9;
        background: #f5f5f5;
      }
      .disabled:hover {
        color: rgba(0,0,0,.25);
        border-color: #d9d9d9;
        background: #f5f5f5;
      }
    }
    .ant-btn{
      width: 120px;
      height: 40px;
    }
    .ant-btn-primary{
      width: 250px;
      height: 40px;
    }
`;
const Container = styled.div`
  display: flex;
  padding: 7px 0;
  &.off {
    display: none;
  }

  .title {
    position: relative;
    &.required:after {
      content: "";
      position: absolute;
      top: 10px;
      display: inline-block;
      margin-left: 3px;
      background: var(--color-danger);
      border-radius: 50%;
      width: 5px;
      height: 5px;
      margin-top: 0;
    }
  }

  .content {
    display: flex;
    flex-grow: 1;
    height: auto !important;
  }
`;
const ContentWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
const DescriptionWrap = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5px;

  > p {
    color: var(--color-gray-700);
    font-size: 13px;
    line-height: 18px;
    vertical-align: Top;
    letter-spacing: -0.5px;
  }
`;

const FormWrap = styled.div`
  display: flex;

`;

const SingleInput = styled(Input)`
  flex-grow: 1;

  &[disabled] {
    background: #fff;
  }
`;
const ModalButton = styled(Button)`
  flex:0 0 89px;
  width: 89px;
  height: 34px;
  margin-left: 6px;
  padding:0 0;
  img {
    vertical-align: middle;
    display:inline-block;
    margin-right: 4px;
  }
  span {
    color: var(--color-gray-700);
    font-size: 13px;
  }
`;

const CustomPagingTable = styled(PagingTable)`
  .ant-table-container::before,
  .ant-table-container::after {
    display: none !important;
  }
  table {
    width: 100% !important;
    tr {
      th:nth-of-type(1), td:nth-of-type(1) {
        width: 34px !important;
        text-align: left !important;
      }
      th:nth-of-type(2), td:nth-of-type(2) {
        width: 110px !important;
      }
      th:nth-of-type(3), td:nth-of-type(3) {
        width: auto !important;
      }
      th:nth-of-type(4), td:nth-of-type(4) {
        width: 210px !important;
      }
      th,
      td {
        padding-right: 0 !important;
      }
    }
  }
`;

const CustomTextArea = styled.textarea`
  width: 100%;
  height: 22px;
  padding: 5px 11px;
  resize: none;
  font-size: 13px;
  line-height: 22px;
  overflow-y: hidden;
  border: var(--border-default);
  border-radius: 4px;
  min-height: 34px;
`;

const ErrorWrap = styled.div`
  margin-top:4px;
  width: 100%;
  height: auto;
  min-height: 18px;
  opacity: 1;
  color: #ff4d4f;
  font-size: 12px;
  line-height: 18px;

  span {
    padding-left: 1px;
    img {
      width: 14px;
    }
    svg {
      margin-right: 2px;
    }
  }
`;

export default forwardRef(ProductSearch);
