import styled from 'styled-components';
import { useEffect, useState } from 'react';
import moment from 'moment';
import isNil from 'lodash-es/isNil';

import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

import { Tabs, TabPane } from '../../../../components/tabs';

import { TabHeadTitle, TooltipText, toplineData1, toplineData2 } from '../constants';
import ToolTip from '../../../../components/toolTip';
import { getChartValueMoney, getChartValuePeople, getChartValueUnit, getChartYAxisMax } from '../../../../utils/utils';
import { CHART_COLORS, CHART_VARIATION } from '../../../../styles/chartColors';
import { legendDefault } from '../../../../utils/chartOptions';

function TabCharts({ selectedTab, setSelectedTab }) {
  const { statistics, newUser, total, interest, order } = useSelector((state) => state.customer.productCustomer);
  const [localStatistics, setLocalStatistics] = useState({});
  const [localNewUser, setLocalNewUser] = useState(toplineData1);
  const [localTotalUser, setLocalTotalUser] = useState(toplineData1);
  const [localInterest, setLocalInterest] = useState(toplineData1);
  const [localOrder, setLocalOrder] = useState(toplineData2);

  const formatterDate = (val) => {
    if (!val) return val;
    return `${val.length === 10 ? moment(val, 'YYYY.MM.DD').format('YY. M. D.') : moment(val, 'YYYY.MM').format('YY. M.')}`;
  };

  const tabHead = (idx, totalCount, count = '', amount = '') => (
    <>
      <Title>
        {TabHeadTitle[idx]}
        <ToolTip text={TooltipText[idx]} position={(idx >= 2) ? 'right' : false} />
      </Title>
      <Total>
        {totalCount.toLocaleString('ko-KR')}
        <div className="unit">명</div>
      </Total>
      <ChangeRate>
        <div className="item">
          {(idx === 3)
          && (
            <>
              <div className="text3">
                <div>
                  총 주문수
                  <span>
                    {' '}
                    <span className="count">{count.toLocaleString('ko-KR')}</span>
                    개

                  </span>
                </div>
                <div>
                  총 주문금액
                  <span>
                    {' '}
                    <span className="count">{amount.toLocaleString('ko-KR')}</span>
                    원
                  </span>
                </div>
              </div>
            </>
          ) }
        </div>
      </ChangeRate>
    </>
  );

  const chartOptions = {
    chart: {
      toolbar: {
        show: false,
      },
      type: 'bar',
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: 2,
    },
    legend: {
      show: false,
    },
    colors: CHART_VARIATION,
    tooltip: {
      y: {
        formatter(val) {
          return `${(val).toLocaleString()}명`;
        },
      },
    },
    markers: {
      size: 3,
      strokeWidth: 1,
    },
    yaxis: {
      max: (max) => getChartYAxisMax(max),
      labels: {
        formatter(val, index) {
          if (index === 0) {
            return '0';
          }
          return getChartValuePeople(val);
        },
      },
      tickAmount: 5,
    },
  };
  const chartOptionsXaxis = {
    labels: {
      rotate: 0,
      hideOverlappingLabels: false,
      style: {
        fontSize: '11px',
        cssClass: 'apexcharts-xaxis-label-overlapping',
      },
      formatter(val) {
        return formatterDate(val);
      },
    },
    tooltip: {
      enabled: false,
    },
  };
  useEffect(() => {
    if (statistics.status === 'success') {
      setLocalStatistics(statistics.data);
    }
  }, [statistics]);

  useEffect(() => {
    if (newUser.status === 'success') {
      setLocalNewUser({
        ...localNewUser,
        series: newUser.data.series,
        options: {
          ...newUser.data.options,
          ...chartOptions,
          xaxis: {
            categories: newUser.data.categories,
            ...chartOptionsXaxis,
          },
          tooltip: {
            ...chartOptions.tooltip,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = newUser.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
          },
        },
      });
    }
  }, [newUser]);

  useEffect(() => {
    if (total.status === 'success') {
      setLocalTotalUser({
        ...localTotalUser,
        series: total.data.series,
        options: {
          ...total.data.options,
          ...chartOptions,
          xaxis: {
            categories: total.data.categories,
            ...chartOptionsXaxis,
          },
          tooltip: {
            ...chartOptions.tooltip,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = total.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
          },
        },
      });
    }
  }, [total]);

  useEffect(() => {
    if (interest.status === 'success') {
      setLocalInterest({
        ...localInterest,
        series: interest.data.series,
        options: {
          ...interest.data.options,
          ...chartOptions,
          xaxis: {
            categories: interest.data.categories,
            ...chartOptionsXaxis,
          },
          tooltip: {
            ...chartOptions.tooltip,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = interest.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
          },
        },
      });
    }
  }, [interest]);

  useEffect(() => {
    if (order.status === 'success') {
      setLocalOrder({
        ...localOrder,
        series: [
          {
            name: order.data.series[0].name,
            type: 'column',
            data: order.data.series[0].data,
          },
          {
            name: order.data.series[1].name,
            type: 'column',
            data: order.data.series[1].data,
          },
          {
            name: order.data.series[2].name,
            type: 'line',
            data: order.data.series[2].data,
          },
        ],
        options: {
          chart: {
            height: 350,
            type: 'line',
            dropShadow: {
              enabled: false,
            },
            toolbar: {
              show: false,
            },
            events: {
              mounted: () => {
                const apexchartsSvg = document.querySelector('.top5brandChart').querySelector('.apexcharts-svg');
                const apexchartsGraphical = apexchartsSvg.querySelector('.apexcharts-graphical');
                apexchartsSvg.appendChild(apexchartsGraphical);
              },
            },
          },
          colors: [
            CHART_COLORS.CUSTOMER_ORDER,
            CHART_COLORS.VARIATION_19,
            CHART_COLORS.VARIATION_21,
          ],
          stroke: {
            curve: 'straight',
            width: 3,
          },
          grid: {
            borderColor: '#E3E4E7',
            row: {
              colors: ['transparent'],
            },
          },
          markers: {
            size: 3,
            strokeWidth: 1,
          },
          xaxis: {
            categories: order.data.categories,
            axisTicks: {
              show: false,
            },
            tooltip: {
              enabled: false,
            },
            labels: {
              style: {
                fontSize: '10px',
              },
              formatter(val) {
                return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
              },
            },
          },
          yaxis: [
            {
              title: {
                text: order.data.series[0].name,
                offsetX: 6,
                style: {
                  color: CHART_COLORS.CUSTOMER_ORDER,
                },
              },
              axisBorder: {
                show: true,
                color: CHART_COLORS.CUSTOMER_ORDER,
                offsetX: -6,
              },
              tickAmount: 5,
              labels: {
                offsetX: 6,
                style: {
                  cssClass: 'yaxis-label-CUSTOMER_ORDER',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValuePeople(val);
                },
              },
              max: (max) => getChartYAxisMax(max),
            },
            {
              title: {
                text: order.data.series[1].name,
                offsetX: 6,
                style: {
                  color: CHART_COLORS.VARIATION_19,
                },
              },
              axisBorder: {
                show: true,
                color: CHART_COLORS.VARIATION_19,
                offsetX: -6,
              },
              tickAmount: 5,
              labels: {
                offsetX: 6,
                style: {
                  cssClass: 'yaxis-label-VARIATION_19',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValueUnit(val);
                },
              },
              max: (max) => getChartYAxisMax(max),
            },
            {
              opposite: true,
              title: {
                text: order.data.series[2].name,
                offsetX: 6,
                style: {
                  color: CHART_COLORS.VARIATION_21,
                },
              },
              axisBorder: {
                show: true,
                color: CHART_COLORS.VARIATION_21,
                offsetX: -6,
              },
              tickAmount: 5,
              labels: {
                offsetX: 6,
                style: {
                  cssClass: 'yaxis-label-VARIATION_21',
                },
                formatter(val, index) {
                  if (index === 0) {
                    return '0';
                  }
                  return getChartValueMoney(val);
                },
              },
              max: (max) => getChartYAxisMax(max),
            },
          ],
          tooltip: {
            shared: true,
            followCursor: false,
            intersect: false,
            x: {
              formatter(_, { __, ___, dataPointIndex }) {
                const val = order.data.categories[dataPointIndex];
                return `${moment(val, 'YYYYMMDD').format('YYYY. M. D.')}`;
              },
            },
            y: [
              {
                formatter(val) {
                  return `${!isNil(val) && (val).toLocaleString()}${val !== 0 ? '' : '명'}`;
                },
              },
              {
                formatter(val) {
                  return `${!isNil(val) && (val).toLocaleString()}${val !== 0 ? '' : '건'}`;
                },
              },
              {
                formatter(val) {
                  return `${!isNil(val) && (val).toLocaleString()}${val !== 0 ? '' : '원'}`;
                },
              },
            ],
          },
          dataLabels: {
            enabled: false,
          },
          legend: {
            ...legendDefault,
          },
        },
      });
    }
  }, [order]);

  useEffect(() => {

  }, [selectedTab]);

  return (
    <Container>
      {Object.keys(localStatistics).length > 0
      && (
        <Tabs activeKey={selectedTab} onChange={(v) => { setSelectedTab(v); }}>
          <TabPane tab={tabHead(0, localStatistics.newCustCnt)} key={0}>
            <SubTitle>신규 활동고객수의 변화를 확인해보세요.</SubTitle>
            <ReactApexChart options={localNewUser.options} series={localNewUser.series} type="line" height={350} />
          </TabPane>
          <TabPane tab={tabHead(1, localStatistics.totalCustCnt)} key={1}>
            <SubTitle>전체 활동고객수의 주문전환 추이를 확인해보세요.</SubTitle>
            <ReactApexChart options={localTotalUser.options} series={localTotalUser.series} type="line" height={350} />
          </TabPane>
          <TabPane tab={tabHead(2, localStatistics.interestCustCnt)} key={2}>
            <SubTitle>전체 관심고객수와 주문전환 추이를 확인해보세요.</SubTitle>
            <ReactApexChart options={localInterest.options} series={localInterest.series} type="line" height={350} />
          </TabPane>
          <TabPane tab={tabHead(3, localStatistics.orderCustCnt, localStatistics.totalOrderCount, localStatistics.totalOrderAmount)} key={3}>
            <SubTitle>내 브랜드의 주문 실적을 확인해보세요.</SubTitle>
            <ReactApexChart options={localOrder.options} series={localOrder.series} type="line" height={350} />
          </TabPane>
        </Tabs>
      ) }
    </Container>
  );
}

const Container = styled.div`
  .ant-tabs-nav:before {
    content: none !important;
  }
  .ant-tabs-ink-bar{
    display: none;
  }
  .ant-tabs-nav-list{
    flex:1;
  }
  .ant-tabs-tab-btn{
    width: 100%;
  }
  .ant-tabs{
    margin-bottom: 20px;
  }
  .ant-tabs-tab{
    display: flex !important;
    padding:0px;
    line-height: 40px; 
    margin: 0px;
    justify-content: center;
    flex: 1;
    margin-left: 10px;
    background-color: #fff;
  }
  .ant-tabs-tab:first-child{
    margin-left: 0px !important;
  }
  .ant-tabs-tab .ant-tabs-tab-btn{
    border-radius: 8px;
    border: var(--border-default);
    color: #333;
    transition: none;
    padding: 16px;
    height: 130px;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn{
    border-radius: 8px;
    border: 2px solid var(--color-blue-500);
    color: #333;
    padding: 15px;
    text-shadow: none;
  }
  .ant-tabs-tab:hover {
    color: #333;
  }
`;

const Title = styled.h3`
  margin-bottom: 6px;
  color: var(--color-gray-700);
  font-size: 13px;
  font-weight: 700;
  line-height: 18px;
  display: flex;
  align-items: center;
  position: relative;
`;

const Total = styled.div`
  display: flex;
  align-items: flex-end;
  color: var(--color-gray-900);
  font-size: 24px;
  font-weight: 700;
  line-height: 36px;
  letter-spacing: -0.75px;
  .unit {
    font-size: 12px;
    font-weight: 400;
    line-height: 18px;
    padding-bottom: 7px;
    margin-left: 2px;
  }
`;

const ChangeRate = styled.div`
  .item {
    font-size: 12px;
    font-weight: 400;
    .text {
      padding-top: 4px;
      line-height: 18px;
    }
    .percent {
      align-items: center;
      line-height: 20px;
      display: flex;
    }
  }
  .text{
    color: var(--color-gray-700);
  }

  .text3{
    color: var(--color-gray-700);
    line-height: 22px;
    span {
      color: var(--color-gray-900);
      font-weight: 400;
      span.count {
        letter-spacing: -0.75px;
        padding-right: 1px;
        display: inline-block;
      }
    }
  }
  
`;

const SubTitle = styled.span`
  margin-top: 4px;
  margin-bottom: 10px;
`;
export default TabCharts;
