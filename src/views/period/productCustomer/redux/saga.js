import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

/** 공통 사가 함수 임포트 */
import { createPromiseSaga, createPromiseMultiApiSaga } from '../../../../redux/lib';

/** slice에서 api call type 선언을 가져와서 정의한다. */
import { getCategoryOptions, getProductCustomerList, getProductCustomerStatistics, getProductCustomerModalList, getProductCustomerSubChart } from './slice';

/** api call type과 매칭시킬 api 요청 메소드 */
import API from '../../../../api';

/** createPromiseSaga로 api공통 로직 적용 */
const productCustomerListSaga = createPromiseSaga(getProductCustomerList, API.ProductCustomer.productCustomerList);
const productCustomerModalListSaga = createPromiseSaga(getProductCustomerModalList, API.Common.productCustomerModalList);
const getCategoryOptionsSaga = createPromiseSaga(getCategoryOptions, API.Common.getCategoryOptions);
const productCustomerStatisticsSaga = createPromiseMultiApiSaga(getProductCustomerStatistics, {
  statistics: API.ProductCustomer.productCustomerStatistics,
  newUser: API.ProductCustomer.productCustomerLineChart,
  total: API.ProductCustomer.productCustomerLineChart,
  interest: API.ProductCustomer.productCustomerLineChart,
  order: API.ProductCustomer.productCustomerMultipleYAxis,
});

const productCustomerSubChartSaga = createPromiseMultiApiSaga(getProductCustomerSubChart, {
  barNegative: API.ProductCustomer.productCustomerBarNegative,
  radarMultiple: API.ProductCustomer.productCustomerRadarMultiple,
  columnDistributed: API.ProductCustomer.productCustomerColumnDistributed,
  treemapDistributed: API.ProductCustomer.productCustomerTreemapDistributed,
  stackedColumnCount: API.ProductCustomer.productCustomerStackedColumnCount,
  stackedColumnAmount: API.ProductCustomer.productCustomerStackedColumnAmount,
  barCategoryL: API.ProductCustomer.productCustomerBarCategoryL,
  barCategoryM: API.ProductCustomer.productCustomerBarCategoryM,
  brandGraph: API.ProductCustomer.productCustomerBrandGraph,
});

/* api call type과 createPromiseSaga로 생성한 함수를 매칭한다. */
function* watchCommon() {
  yield takeLatest(getProductCustomerList, productCustomerListSaga);
  yield takeLatest(getProductCustomerStatistics, productCustomerStatisticsSaga);
  yield takeLatest(getProductCustomerModalList, productCustomerModalListSaga);
  yield takeLatest(getCategoryOptions, getCategoryOptionsSaga);
  yield takeLatest(getProductCustomerSubChart, productCustomerSubChartSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
