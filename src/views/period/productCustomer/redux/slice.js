import { createSlice } from '@reduxjs/toolkit';

import { asyncApiState } from '../../../../redux/constants';

const initialState = {
  productList: asyncApiState.initial([]),
  modalList: asyncApiState.initial({}),
  categoryOption: asyncApiState.initial([[], [], []]),

  statistics: asyncApiState.initial({}),
  newUser: asyncApiState.initial({}),
  total: asyncApiState.initial({}),
  interest: asyncApiState.initial({}),
  order: asyncApiState.initial({}),

  barNegative: asyncApiState.initial({}),
  radarMultiple: asyncApiState.initial({}),
  columnDistributed: asyncApiState.initial({}),
  treemapDistributed: asyncApiState.initial({}),
  stackedColumnCount: asyncApiState.initial({}),
  stackedColumnAmount: asyncApiState.initial({}),
  barCategoryL: asyncApiState.initial({}),
  barCategoryM: asyncApiState.initial({}),
  brandGraph: asyncApiState.initial([]),
};

export const { actions, reducer } = createSlice({
  name: 'customer/productCustomer',
  initialState,
  reducers: {
    resetStore: (state, { payload }) => ({
      ...initialState,
    }),
    updateStore: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    /** 상품 정보 리스트 */
    getProductCustomerList: (state, { payload }) => {
      state.productList = asyncApiState.request([]);
    },
    getProductCustomerListSuccess: (state, { payload }) => {
      const result = payload;

      state.productList = asyncApiState.success(result);
    },
    getProductCustomerListFailure: (state, { payload }) => {
      state.productList = asyncApiState.error(payload);
    },

    getProductCustomerStatistics: (state, { payload }) => {
      state.statistics = asyncApiState.request({});
      state.newUser = asyncApiState.request({});
      state.total = asyncApiState.request({});
      state.interest = asyncApiState.request({});
      state.order = asyncApiState.request({});
    },
    getProductCustomerStatisticsSuccess: (state, { payload }) => {
      const { key, data } = payload;
      if (key) {
        state[key] = asyncApiState.success(data);
      }
    },
    getProductCustomerStatisticsFailure: (state, { payload }) => {
      const { key, data } = payload;
      if (key) {
        state[key] = asyncApiState.error(data);
      }
    },

    getProductCustomerSubChart: (state, { payload }) => {
      state.barNegative = asyncApiState.request({});
      state.radarMultiple = asyncApiState.request({});
      state.columnDistributed = asyncApiState.request({});
      state.treemapDistributed = asyncApiState.request({});
      state.stackedColumnCount = asyncApiState.request({});
      state.stackedColumnAmount = asyncApiState.request({});
      state.barCategoryL = asyncApiState.request({});
      state.barCategoryM = asyncApiState.request({});
      state.brandGraph = asyncApiState.request([]);
    },
    getProductCustomerSubChartSuccess: (state, { payload }) => {
      const { key, data } = payload;
      if (key) {
        if (key === 'treemapDistributed') {
          state[key] = asyncApiState.success({ data: data.data.series[0] });
        } else state[key] = asyncApiState.success(data);
      }
    },
    getProductCustomerSubChartFailure: (state, { payload }) => {
      const { key, data } = payload;
      if (key) {
        state[key] = asyncApiState.error(data);
      }
    },

    getProductCustomerModalList: (state, { payload }) => {
      state.modalList = asyncApiState.request({});
    },
    getProductCustomerModalListSuccess: (state, { payload }) => {
      const result = payload;
      // if need customize
      // result.data.apiArray.map((v) => {
      //   v.date = moment(v.date).format('YYYY-MM-DD');
      // });
      state.modalList = asyncApiState.success(result);
    },
    getProductCustomerModalListFailure: (state, { payload }) => {
      state.modalList = asyncApiState.error(payload);
    },
    getCategoryOptions: (state, { payload }) => {
      state.categoryOption = asyncApiState.request([[], [], []]);
    },
    getCategoryOptionsSuccess: (state, { payload }) => {
      const result = payload.data;
      const arr = [];
      if (result.categoryL) {
        const changeArr = result.categoryL.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
        }));

        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryM) {
        const changeArr = result.categoryM.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }
      if (result.categoryS) {
        const changeArr = result.categoryS.map((v) => ({
          label: `${v.categoryName}`,
          value: v.categoryCode,
          parent: v.categoryParentsCode,
        }));
        changeArr.unshift({ label: '전체', value: '' });
        arr.push(changeArr);
      }

      state.categoryOption = asyncApiState.success({ data: arr });
    },
    getCategoryOptionsFailure: (state, { payload }) => {
      state.categoryOption = asyncApiState.error([[], [], []]);
    },
  },
});

export const {
  resetStore,
  updateStore,
  getProductCustomerList,
  getProductCustomerSubChart,
  getProductCustomerStatistics,
  getProductCustomerModalList,
  getCategoryOptions,
} = actions;

export default reducer;
