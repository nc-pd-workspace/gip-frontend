import moment from 'moment';

import { getChartValuePeople, getChartYAxisMax } from '../../../../utils/utils';

export const TabHeadTitle = ['신규 활동고객수', '전체 활동고객수', '전체 관심고객수', '주문고객수'];
export const TooltipText = [
  ( // 신규 활동고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품을 새롭게 방문한 고객이
      <br />
      활동(신규방문, 신규관심, 신규주문, 신규충성)한 고객수 입니다.
    </>
  ),
  ( // 전체 활동고객수
    <>
      조회기간 동안 최근 6개월간 내 브랜드 상품에서 활동(방문, 관심, 주문, 충성)한 고객수 입니다.
    </>
  ),
  ( // 전체 관심고객수
    <>
      조회기간 동안 최근 6개월간  장바구니 담기, 찜하기 등 관심 행동 이력이 있는 고객 데이터 입니다.
    </>
  ),
  ( // 주문고객수
    <>
      조회기간 동안 내 브랜드 상품을 주문한 고객수(합계/평균) 입니다.
      <br />
      <br />
      - 주문금액: 조회기간 동안 내 브랜드에서 주문완료한 주문 금액(합계/평균)입니다.
      <br />
      - 주문수: 조회기간 동안 내 브랜드 상품을 주문한 건 수(합계/평균) 입니다.
    </>
  ),
];

export const talbecolumns = [
  {
    title: '번호',
    dataIndex: 'seqNo',
    key: 'seqNo',
    width: 50,
  },
  {
    title: '상품코드',
    dataIndex: 'prdCd',
    key: 'prdCd',
    width: 'calc(16.5% - 50px)',
  },
  {
    title: '상품명',
    dataIndex: 'prdNm',
    key: 'prdNm',
    width: '53%',
  },
  {
    title: '카테고리',
    dataIndex: 'categoryNm',
    key: 'categoryNm',
    width: '30.5%',
  },
];

export const toplineData1 = {
  series: [],
  options: {
    chart: {
      height: 350,
      type: 'line',
      toolbar: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'straight',
    },
    grid: {
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5,
      },
    },
    xaxis: {
      type: 'category',
      categories: [],
      tickPlacement: 'between',
      axisTicks: {
        show: false,
      },
      tooltip: {
        enabled: false,
      },
      labels: {
        show: false,
        rotate: 0,
        hideOverlappingLabels: false,
        minHeight: 32,
        maxHeight: 32,
        style: {
          colors: 'var(--color-gray-400)',
          fontSize: '11px',
          fontWeight: 400,
          cssClass: 'apexcharts-xaxis-label',
        },
        offsetX: 2,
        offsetY: 0,
        formatter(val) {
          return `${moment(val, 'YYYYMMDD').format('YY. M. D.')}`;
        },
      },
    },
    yaxis: {
      max: (max) => getChartYAxisMax(max),
      labels: {
        formatter(val, index) {
          if (index === 0) {
            return '0';
          }
          return getChartValuePeople(val);
        },
      },
      tickAmount: 5,
    },
  },
};

export const toplineData2 = {
  series: [],
  options: {
    chart: {
      height: 350,
      type: 'line',
      stacked: false,
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [1, 1, 4],
    },
    title: {
      align: 'left',
      offsetX: 110,
    },
    xaxis: {
      categories: [],
    },
    yaxis: [
      {
        max: (max) => getChartYAxisMax(max),
        tickAmount: 5,
        axisTicks: {
          show: true,
        },
        axisBorder: {
          show: true,
          color: '#008FFB',
        },
        labels: {
          style: {
            colors: '#008FFB',
          },
        },
        title: {
          style: {
            color: '#008FFB',
          },
        },
        tooltip: {
          enabled: true,
        },
      },
      {
        max: (max) => getChartYAxisMax(max),
        tickAmount: 5,
        seriesName: 'Income',
        opposite: true,
        axisTicks: {
          show: true,
        },
        axisBorder: {
          show: true,
          color: '#00E396',
        },
        labels: {
          style: {
            colors: '#00E396',
          },
        },
        title: {
          style: {
            color: '#00E396',
          },
        },
      },
      {
        max: (max) => getChartYAxisMax(max),
        tickAmount: 5,
        seriesName: 'Revenue',
        opposite: true,
        axisTicks: {
          show: true,
        },
        axisBorder: {
          show: true,
          color: '#FEB019',
        },
        labels: {
          style: {
            colors: '#FEB019',
          },
        },
        title: {
          // text: 'Revenue (thousand crores)',
          style: {
            color: '#FEB019',
          },
        },
      },
    ],
    tooltip: {
      fixed: {
        enabled: true,
        position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
        offsetY: 30,
        offsetX: 60,
      },
    },
    legend: {
      horizontalAlign: 'left',
      offsetX: 40,
    },
  },

};
