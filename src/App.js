import { useEffect, useState } from 'react';
import styled from 'styled-components';

import { datadogRum } from '@datadog/browser-rum';
import { useSelector, useDispatch } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';

import { GlobalStyle } from './styles/GlobalStyle';
import AppLayout from './views/shared/layout/AppLayout';
import NotFound from './views/shared/result/NotFound';
import { logout, setUserInfo } from './redux/commonReducer';

import Login from './views/account/login/containers/LoginContainer';
import IdSearchContainer from './views/account/login/containers/IdSearchContainer';
import FindPasswordContainer from './views/account/login/containers/FindPasswordContainer';
import PrivacyPolicy from './views/privacyPolicy/PrivacyPolicy';

import Message from './components/message';
import About from './views/about/about';

const sessionInfo = window.sessionStorage.getItem('GIPADMIN_USER');
const siteValue = window.location.href.includes('gip.gsretail.com');
if (process.env.NODE_ENV === 'production' && siteValue) {
  datadogRum.init({
    applicationId: '6fe97fc5-51b0-4841-806a-c365b406fe2b',
    clientToken: 'pub84aa151d21e1511d28363d255adcff11',
    site: 'datadoghq.com',
    service: 'gip',
    sampleRate: 100,
    trackInteractions: true,
    defaultPrivacyLevel: 'mask-user-input',
  });
  datadogRum.startSessionReplayRecording();
}

function App() {
  const dispatch = useDispatch();
  const { userInfo, messageList } = useSelector((state) => state.common);
  const [isSessionToken, setIsSessionToken] = useState(sessionInfo && JSON.parse(sessionInfo).accessToken ? true : null);

  useEffect(() => {
    if (Object.keys(userInfo).length === 0) {
      setIsSessionToken(null);
    }
  }, [userInfo]);

  useEffect(() => {
    const sessionCheckUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');

    if (sessionCheckUserInfo !== null) {
      // console.log('#############  set user info ##################', JSON.parse(sessionCheckUserInfo));
      const tempData = JSON.parse(sessionCheckUserInfo);
      dispatch(setUserInfo(tempData));
    } else {
      window.localStorage.setItem('GIPADMIN_REQUEST_LOGIN_INFO', 'true');
      window.localStorage.removeItem('GIPADMIN_REQUEST_LOGIN_INFO');
    }

    window.addEventListener('storage', (event) => {
      if (event.key === 'GIPADMIN_LOGOUT') {
        const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
        if (sessionUserInfo !== null) {
          dispatch(logout());
          window.location.reload();
        }
      } else if (event.key === 'GIPADMIN_REQUEST_LOGIN_INFO') {
        const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
        if (sessionUserInfo !== null) {
          window.localStorage.setItem('GIPADMIN_RESPONSE_LOGIN_INFO', sessionUserInfo);
          window.localStorage.removeItem('GIPADMIN_RESPONSE_LOGIN_INFO');
        }
      } else if (event.key === 'GIPADMIN_RESPONSE_LOGIN_INFO') {
        // 받았을 때 토큰 정보가 없다면 로그인 정보가 없는 것
        const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
        if (sessionUserInfo === null && event.oldValue !== null) {
          const info = JSON.parse(event.oldValue);
          window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(info));
          dispatch(setUserInfo(info));
          window.location.reload();
        }
      } else if (event.key === 'GIPADMIN_UPDATETOKEN') {
        // reissue로 재발급 받은 토큰 정보를 store 와 세션정보에 업데이트
        const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
        if (sessionUserInfo && event.oldValue) {
          const tempUserInfo = JSON.parse(sessionUserInfo);
          const tokenInfo = JSON.parse(event.oldValue);
          const { accessToken, refreshToken, expiredDate, refreshTokenExpiredDate } = tokenInfo;
          const newUserInfo = { ...tempUserInfo, accessToken, refreshToken, expiredDate, refreshTokenExpiredDate };
          dispatch(setUserInfo(newUserInfo));
          window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(newUserInfo));
        }
      }
      // } else if (event.key === 'GIPADMIN_TOKEN_EXPIRED') {
      //   // 받았을 때 토큰 정보가 없다면 로그인 정보가 없는 것
      //   const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');
      //   if (sessionUserInfo !== null) {
      //     dispatch(logout('로그인 시간이 만료되어 자동으로 로그아웃 되었습니다.\n로그인 후 사용해 주세요.'));
      //   }
      // }
    });
  }, []);

  return (
    <Container>
      <GlobalStyle />
      <Switch>
        <Route path="/login" exact name="Login"><Login /></Route>
        <Route path="/first-login" exact name="FirstLogin"><FindPasswordContainer /></Route>
        <Route path="/forgot-id" exact name="IdSearch"><IdSearchContainer /></Route>
        <Route path="/find-password" exact name="FindPassword"><FindPasswordContainer /></Route>
        <Route path="/privacyPolicy" name="privacyPolicy"><PrivacyPolicy /></Route>
        <Route path="/about" exact name="about"><About /></Route>
        <Route path="/result/notFound" name="notFound"><NotFound /></Route>
        {((userInfo && userInfo.accessToken) || isSessionToken !== null) ? (
          <>
            <Route exact path="/" component={AppLayout} />
            <Route path="/:id" component={AppLayout} />
          </>
        ) : (
          <Redirect to="/login" />
        )}

        <Route path="*" component={NotFound} />
      </Switch>
      {
        messageList.map((v, index) => (
          <Message id={v.id} key={index} type={v.type} message={v.message} onOk={v.onOk} okText={v.okText} cancelText={v.cancelText} onCancel={v.onCancel} />
        ))
      }
    </Container>
  );
}

const Container = styled.div`
  // min-width: 1024px;  
`;

export default App;
