import { Children, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Card } from 'antd';

import cn from 'classnames';

function TabButtonList({ children, gutter, onChange, defaultValue, conPadding = '0 20' }) {
  const [selectedIdx, setSelectedIdx] = useState(-1);

  const onClickCard = (idx) => {
    setSelectedIdx(idx);
    onChange(idx);
  };

  useEffect(() => {
    setSelectedIdx(defaultValue);
    onChange(defaultValue);
  }, [defaultValue]);

  return (
    <Container conPadding={conPadding}>
      {Children.map(children, (child, idx) => (
        <CardButton
          className={cn({ active: selectedIdx === idx })}
          idx={idx}
          gutter={gutter}
          onClick={() => onClickCard(idx)}
        >
          {child}
        </CardButton>
      ))}
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  space-between: 16px;
  padding: ${(props) => props.conPadding}px;
`;

const CardButton = styled(Card)`
  flex: 1;
  margin-left: ${(props) => props.gutter / 2}px;
  margin-right: ${(props) => props.gutter / 2}px;
  border: none;
  .ant-card-body {
    border:1px solid #E3E4E7;
    padding: 16px;
    border-radius: 8px;
  }
  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
  &.active {
    .ant-card-body {
      border: 2px solid var(--color-blue-500);
      padding: 15px;
    }
  }
`;

export default TabButtonList;
