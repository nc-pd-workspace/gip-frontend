import styled from 'styled-components';
import { Tabs } from 'antd';
import isArray from 'lodash-es/isArray';
import cn from 'classnames';

const { TabPane } = Tabs;

/**
 * @param fullHeight {boolean} - 부모 컴포넌트의 높이에 꽉 채우는 탭 컨텐츠를 구현할때 사용
 * @param defaultActiveKey {string} - 기본 활성화 키, 탭 순서에 따름
 */

function TabList({
  children, className, data, style, fullHeight, onChange, defaultActiveKey,
}) {
  if (!isArray(data)) {
    throw new Error('data is must be Array in TabList Component');
  }
  if (!data[0].name) {
    throw new Error('data rule requires a name property');
  }

  return (
    <Container style={style} className={cn('TabList', { fullHeight }, className)}>
      <Tabs defaultActiveKey={defaultActiveKey} onChange={onChange}>
        {
          data.map((item, index) => (
            <TabPane tab={item.name} key={index + 1}>
              {children(item)}
            </TabPane>
          ))
        }
      </Tabs>
    </Container>
  );
}

const Container = styled.div`
  // 상위 컨텐츠의 크기에 따라 탭컨텐츠를 꽉 채울 경우 fullHeight
  &.fullHeight {
    height: 100%;
    .ant-tabs {
      height: 100%;
      display: flex;
      flex-direction: column;
      .ant-tabs-content-holder {
        flex: 1;
        .ant-tabs-content {
          height: 100%;
        }
      }
    }
  }

  .ant-tabs-nav {
    margin: 0;
  }
  .ant-tabs-nav-list {
    flex: 1;
    .ant-tabs-tab {
      padding: 0;
      margin: 0;
      flex: 1;
      &.ant-tabs-tab-active {
        .ant-tabs-tab-btn {
          background:var(--color-steelGray-800);
          color:#fff;
        }
      }

      .ant-tabs-tab-btn {
        display: flex;
        align-items: center;
        justify-content: center;
        flex: 1;
        height: 28px;
        background:#F7F8FA;
        color:var(--color-gray-700);
        font-size: 13px;
        border-radius: 4px;
      }
    }
    .ant-tabs-ink-bar {
      display:none!important;
    }
  }

`;

export default TabList;
