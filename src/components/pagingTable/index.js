import { Table } from 'antd';
import { useEffect, useState, forwardRef, useImperativeHandle } from 'react';
import styled from 'styled-components';

import Pagination from './Pagination';
import LoadingComponent from '../loading';

import { cssTable } from '../../styles/Table';

import { alertMessage } from '../message';
import EmptyList from '../emptyList';

function PagingTable({
  columns, data, pagination, loading, rowKey, onChange, showRowIndex, onRowClick, maxSelectRowCount, rowSelect = false,
  onCheckedRow, rowSelected, className,
}, ref) {
  const [customColumns, setCustomColumns] = useState([]);
  const [selectedRowIndex, setSelectedRowIndex] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState();

  const [localPaging, setLocalPaging] = useState({
    current: 1,
    pageSize: 10,
    total: 0,
    showSizeChanger: false,
  });
  const [localFilters, setLocalFilters] = useState([]);
  const [localSorter, setLocalSorter] = useState({});

  const onChangeTable = (paging, filter, sorter) => {
    setLocalFilters(filter);
    setLocalSorter(sorter);
    onChange(localPaging, filter, sorter);
  };

  const onChangePaging = (paging) => {
    onChange(paging, localFilters, localSorter);
  };

  const onRow = (record, rowIndex) => ({
    onClick: (event) => {
      if (onRowClick) {
        if (event.target.localName === 'a') return;
        onRowClick(record, rowIndex);
      }
      if (!rowSelect) return;
      if (selectedRowIndex.includes(rowIndex)) {
        setSelectedRowIndex([]);
      } else {
        setSelectedRowIndex([rowIndex]);
      }
    },
  });

  const getRowClassName = (record, index) => {
    if (selectedRowIndex.includes(index)) {
      return 'active';
    }
    return '';
  };

  useImperativeHandle(ref, () => ({
    getSelectedRowData: () => data.filter((v, idx) => selectedRowIndex.includes(idx)),
    setReset: () => {
      setSelectedRowIndex([]);
      setSelectedRowKeys([]);
      setLocalFilters([]);
      setLocalSorter({});
    },
  }));

  useEffect(() => {
    setLocalPaging(pagination);
  }, [pagination]);

  useEffect(() => {
  }, [selectedRowIndex]);

  useEffect(() => {
    if (showRowIndex) {
      setCustomColumns([
        {
          title: '번호',
          key: 'index',
          render: (value, item, index) => (pagination.current === 0 ? 0 : pagination.current - 1) * pagination.pageSize + index + 1,
          width: '50px',
        },
        ...columns,
      ]);
    } else setCustomColumns(columns);
  }, [columns, pagination]);

  const rowSelection = {
    selectedRowKeys,
    onChange: (selectkeys) => {
      const nowData = data.map((v) => rowKey(v));
      // 페이지에 없는 애들 따로 저장
      const arr = [...selectedRowKeys].filter((v) => nowData.indexOf(v) === -1);
      let selectArr = [...selectkeys];
      if (maxSelectRowCount) {
        const currCnt = arr.length;
        const selectCnt = selectArr.length;
        if (currCnt + selectCnt > maxSelectRowCount) {
          const count = maxSelectRowCount - currCnt;
          if (count >= 0) {
            selectArr = selectArr.slice(0, count);
            alertMessage(`${maxSelectRowCount}개 이상 선택할 수 없습니다.`);
          }
        }
      }

      setSelectedRowKeys([...arr, ...selectArr]);
      onCheckedRow([...arr, ...selectArr]);
    },
  };
  // 상위 checkedRow state 변경시 Table Checked 반영
  useEffect(() => {
    setSelectedRowKeys(rowSelected);
  }, [rowSelected]);

  return (
    <Container className={className}>
      <CustomizeTable
        rowSelection={rowSelected ? rowSelection : false}
        columns={customColumns}
        dataSource={data}
        rowKey={rowKey}
        onRow={onRow}
        rowClassName={getRowClassName}
        loading={loading ? { indicator: <LoadingComponent isLoading /> } : false}
        pagination={false}
        locale={{ emptyText: (<EmptyList warningTitle={(
          <>
            조회 가능한 데이터가 없습니다.
            <br />
            조회 기준을 다시 설정해보세요.
          </>
        )}
        />) }}
        onChange={onChangeTable}
        scroll={{ x: 'max-content' }}
      />
      {!loading ? (
        <Pagination pagination={localPaging} onChange={onChangePaging} />
      ) : (
        <PaginationHusks>
          {/** loading으로 숨김 처리 할 때 공간이 사라지면서 스크롤 되는것을 방지 */}
        </PaginationHusks>
      )}
    </Container>
  );
}

const Container = styled.div`
  position: relative;
  ${cssTable}
`;
const CustomizeTable = styled(Table)`
  margin-top: 5px;

  table {
    table-layout: fixed !important;
    tr {
      height: 32px;
    }
  }

  .ant-table-content {
  }

  .ant-table-placeholder {
    height: 524px;
  }
  
  .ant-table-tbody {
    .ant-table-row {
      height: 49px;
    }

    tr.active td{
      background: var(--color-blue-50) !important;
    }
    tr {
      td {
        transition: initial !important;
        font-family: Pretendard;
        font-style: normal;
        font-size: 13px;
        line-height: 19px;
        vertical-align: middle;
        color: var(--color-gray-900);
      }
    }
  }
  .ant-table-scroll-horizontal .ant-table-content {
    margin-left:-20px;
    padding-left:20px;
  }
  .ant-table-cell {
    padding: 0 10px;
    vertical-align: middle;
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 19px;
  }

  th.ant-table-cell {
    padding: 0 0 0 10px;
    background: #fff;
    vertical-align: middle;
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
    color: var(--color-gray-700);
    text-align: left;

    &::before {
      width: 0 !important;
    }
  }

  .ant-spin-spinning {
    max-height: 524px !important;
  }

  .ant-table-cell-row-hover {
    transition: initial !important;
    background: linear-gradient(
      0deg, 
      rgba(255, 255, 255, 0.95), 
      rgba(255, 255, 255, 0.95)
    ), var(--color-blue-500) !important;
  }
`;

const PaginationHusks = styled.div`
  height: 20px;
  margin-top: 14px;
`;
export default forwardRef(PagingTable);
