import styled from 'styled-components';

import Images from '../../Images';

function Error() {
  return (
    <Container>
      <ErrorMessageWrap>
        <img src={Images.iconError} alt="Page Not Found" />
        <MessageTitle>
          요청하신 작업을 수행할 수 없습니다.
        </MessageTitle>
      </ErrorMessageWrap>
    </Container>
  );
}

const Container = styled.div`
  height: 270px;
  display:flex;
  align-items: center;
  justify-content: center;
`;
const ErrorMessageWrap = styled.div`
  text-align: center;
  
`;
const MessageTitle = styled.p`
  padding-top: 30px;
  font-size: 14px;
  font-weight: 700;
  color: var(--color-gray-700);
  text-align: center;
`;

export default Error;
