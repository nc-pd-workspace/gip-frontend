import styled from 'styled-components';
import isArray from 'lodash-es/isArray';

import moment from 'moment';

import Paper from '../paper';
import { ChevronRight } from '../../Images';
import TabChart from '../../views/main/components/chart/TabChart';
import { usePageTab } from '../../views/shared/pageTab/hooks/usePageTab';
import { PageTypes } from '../../constants/pageType';

function ChartCard({
  children, title, description, height = 404,
}) {
  const { openPage } = usePageTab();
  const handlerOpenPage = (titleValue) => {
    let pageQuery;
    const yesterday = moment(`${moment().startOf('days').subtract(1, 'days').format('YYYY-MM-DD')} 00:00:00`);
    const start = moment(yesterday.format('YYYY-MM-DD').substring(0, 7));
    const month = moment().startOf('days').subtract(1, 'days').startOf('month');
    switch (titleValue) {
    case '실적 지수':
      pageQuery = [PageTypes.BRAND_BRAND_LANDSCAPE, { searchStartDate: start.format('YYYYMMDD'), searchEndDate: yesterday.format('YYYYMMDD'), categoryData: [null, null, null] }];
      break;
    case '성별/연령별 전체 활동고객수':
      pageQuery = [PageTypes.CUSTOMER_ALL_ACTIVITY_CUSTOMER, { month: month.format('YYYY.MM'), catData: [null, null, null], cardType: 'total', activeKey: '1' }];
      break;
    case '전체 활동고객수':
      pageQuery = [PageTypes.CUSTOMER_ALL_ACTIVITY_CUSTOMER, { month: month.format('YYYY.MM'), catData: [null, null, null], cardType: 'total', activeKey: '1' }];
      break;
    case '유입경로별 전체 활동고객수':
      pageQuery = [PageTypes.CUSTOMER_STEP_CUSTOMER, { searchStartDate: start.format('YYYYMMDD'), searchEndDate: yesterday.format('YYYYMMDD'), cardType: 'total', activeKey: '4' }];
      break;
    case '카테고리(대) 전체 활동고객수':
      pageQuery = [PageTypes.CUSTOMER_STEP_CUSTOMER, { searchStartDate: start.format('YYYYMMDD'), searchEndDate: yesterday.format('YYYYMMDD'), cardType: 'total', activeKey: '1' }];
      break;
    case '단계별 전체 활동고객수':
      pageQuery = [PageTypes.CUSTOMER_STEP_CUSTOMER, { searchStartDate: start.format('YYYYMMDD'), searchEndDate: yesterday.format('YYYYMMDD'), cardType: 'total', activeKey: '1' }];
      break;
    default:
      break;
    }
    openPage(pageQuery[0], pageQuery[1]);
  };

  return (
    <Paper
      motion="hover"
      shadow={1}
    >
      <Container style={{ height: `${height}px` }}>
        <Header>
          <Title>
            <h3>
              {title}
            </h3>
            <span>
              {description}
            </span>
          </Title>
          <ButtonMore onClick={() => handlerOpenPage(`${title}`)}>
            <ChevronRight width="20" height="20" fill="var(--color-gray-900)" />
          </ButtonMore>
        </Header>
        <Body>
          {
            isArray(children)
              ? <TabChart data={children} />
              : children
          }
        </Body>
      </Container>
    </Paper>
  );
}

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
`;

const Title = styled.div`
  display: flex;
  align-items: flex-end;
  flex: 1;
  margin-bottom: 20px;
  h3 {
    font-size: 16px;
    color: var(--color-gray-900);
    font-weight: 700;
    line-height: 24px;
  }

  span {
    margin-left: 4px;
    font-size: 12px;
    color: var(--color-gray-500);
    font-weight: 400;
  }
`;

const ButtonMore = styled.button`
  margin-top: 2px;
  margin-right: -2px;
  padding: 0;
  height: 20px;
  cursor: pointer;
  display: block;
  border: none;
  background-color: transparent;
  border-radius: 2px;
  /* &:hover, &:active {
    background-color: var(--color-gray-50);
  } */
`;

const Body = styled.div`
  flex: 1;
  max-height: 100%;
`;

export default ChartCard;
