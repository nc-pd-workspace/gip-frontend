import styled, { css } from 'styled-components';

import { SvgArrowUp, SvgArrowDown, SvgNone } from '../../Images';
import { COLORS } from '../../styles/Colors';

function PercentView({ data, className }) {
  if (data > 0) {
    return (
      <ArrowUp className={className}>
        <SvgArrowUp width="16" height="16" fill={COLORS.BLUE[500]} />
        {Math.abs(data)}
        %
      </ArrowUp>
    );
  }
  if (data < 0) {
    return (
      <ArrowDown className={className}>
        <SvgArrowDown width="16" height="16" fill={COLORS.RED[500]} />
        {Math.abs(data)}
        %
      </ArrowDown>
    );
  }
  if (data === 0) {
    return (
      <ArrowEqual className={className}>
        <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />
        {Math.abs(data)}
        %
      </ArrowEqual>
    );
  }
  return (
    <ArrowEqual className={className}>
      <SvgNone width="16" height="16" fill={COLORS.GRAY[900]} />

    </ArrowEqual>
  );
}

const arrowStyle = css`
  display: inline-flex;
  height: 18px;
  line-height: 18px;
  font-weight: 400 !important;
  letter-spacing: -0.5px;
  align-items: center;
  svg {
    flex:0 0 16px;
  }
`;

const ArrowUp = styled.div`
  color: var(--color-blue-500);
  ${arrowStyle}
`;

const ArrowDown = styled.div`
  color: var(--color-red-500);
  ${arrowStyle}
`;

const ArrowEqual = styled.div`
  color: var(--color-gray-900);
  ${arrowStyle}
`;

export default PercentView;
