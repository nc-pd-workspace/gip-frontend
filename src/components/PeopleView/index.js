import styled, { css } from 'styled-components';

import { SvgNone } from '../../Images';
import { COLORS } from '../../styles/Colors';

function PeopleView({ data, className }) {
  if (data > 0) {
    return (
      <ArrowUp className={className}>
        <DataNumber number={data}>
          +
          {Math.abs(data)}
          명
        </DataNumber>
      </ArrowUp>
    );
  }
  if (data < 0) {
    return (
      <ArrowDown className={className}>
        <DataNumber number={data}>
          -
          {Math.abs(data)}
          명
        </DataNumber>
      </ArrowDown>
    );
  }
  if (data === 0) {
    return (
      <ArrowEqual className={className}>
        <SvgNone width="16" height="16" fill={COLORS.GRAY[700]} />
        {Math.abs(data)}
        명
      </ArrowEqual>
    );
  }
  return (
    <ArrowEqual className={className}>
      <SvgNone width="16" height="16" fill={COLORS.GRAY[700]} />

    </ArrowEqual>
  );
}

const arrowStyle = css`
  display: inline-flex;
  height: 18px;
  line-height: 18px;
  font-weight: 400 !important;
  letter-spacing: -0.5px;
  align-items: center;
  svg {
    flex:0 0 16px;
  }
`;

const ArrowUp = styled.div`
  color: var(--color-blue-500);
  ${arrowStyle}
`;

const ArrowDown = styled.div`
  color: var(--color-red-500);
  ${arrowStyle}
`;

const ArrowEqual = styled.div`
  color: var(--color-gray-900);
  ${arrowStyle}
`;

const DataNumber = styled.span`
  display: inline-block;
  color:${(props) => {
    if (props.number > 0) {
      return 'var(--color-blue-500)';
    }
    if (props.number < 0) {
      return 'var(--color-red-500)';
    }
    return 'var(--color-gray-700)';
  }}
`;

export default PeopleView;
