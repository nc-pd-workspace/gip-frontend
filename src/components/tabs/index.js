import { Children, useEffect, useState, cloneElement, useCallback } from 'react';

let uuid = 0;

export function TabPane({ children, id, tabindex, tab, activeKey, key, style }) {
  return (
    <div
      role="tabpanel"
      className={`ant-tabs-tabpane${activeKey === key ? ' ant-tabs-tabpane-active' : ''}`}
      tabIndex={tabindex}
      style={style}
      aria-hidden={activeKey === key ? 'false' : 'true'}
      id={id}
      aria-controls={tab}
    >
      { children }
    </div>
  );
}

export function Tabs({ children, activeKey, id, onChange }) {
  const [tabId, setTabId] = useState(id || null);
  const onClickTab = (key) => {
    onChange(key);
  };
  const onKeyPress = () => {

  };
  const getTabElement = useCallback((child, index) => {
    const { tab } = child.props;
    const { key } = child;

    return (
      <div className={`ant-tabs-tab${activeKey.toString() === key.toString() ? ' ant-tabs-tab-active' : ''}`}>
        <div
          role="tab"
          aria-selected="true"
          className="ant-tabs-tab-btn"
          tabIndex={index}
          id={`${tabId}-tab-${index}`}
          aria-controls={`${tabId}-panel-${index}`}
          onClick={() => onClickTab(key)}
          onKeyPress={(e) => onKeyPress(e)}
        >
          { tab || null}
        </div>
      </div>
    );
  }, [activeKey, tabId, onClickTab, onKeyPress]);

  const getTabPaneElement = useCallback((child, index) => (
    <>
      {
        (activeKey.toString() === child.key.toString()) && (
          <>
            {cloneElement(child, {
              activeKey,
              tabindex: index,
              id: `${tabId}-panel-${index}`,
              tab: `${tabId}-tab-${index}`,
            })}
          </>
        )
      }
    </>
  ), [activeKey]);

  useEffect(() => {
    if (!id) {
      setTabId(`rc-tabs-${uuid}`);
      uuid += 1;
    }
  }, []);
  return (
    <div className="ant-tabs ant-tabs-top">
      <div role="tablist" className="ant-tabs-nav">
        <div className="ant-tabs-nav-wrap">
          <div className="ant-tabs-nav-list">
            {Children.map(children, (child, idx) => getTabElement(child, idx))}
          </div>
        </div>
      </div>
      <div className="ant-tabs-content-holder">
        <div className="ant-tabs-content ant-tabs-content-top">
          {Children.map(children, (child, idx) => getTabPaneElement(child, idx))}
        </div>
      </div>
    </div>
  );
}
