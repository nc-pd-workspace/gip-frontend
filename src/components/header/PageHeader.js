import React from 'react';
import styled from 'styled-components';

import cn from 'classnames';

import Title from '../title';

function PageHeader({
  children, className, title, subTitle,
}) {
  return (
    <Container className={cn('PageHeader', className)}>
      <Title
        level={2}
        title={title}
        subTitle={subTitle}
      />
      <Tools>
        {children && children}
      </Tools>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 14px;
`;

const Tools = styled.div``;

export default PageHeader;
