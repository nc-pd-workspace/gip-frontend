import React from 'react';
import styled from 'styled-components';

import cn from 'classnames';

import Title from '../title';

function SectionHeader({
  children, className, title, subTitle,
}) {
  return (
    <Container className={cn('SectionHeader', className)}>
      <Title
        level={3}
        title={title}
        subTitle={subTitle}
      />
      <Tools>
        {children && children}
      </Tools>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 19px 19px 23px 19px;
`;

const Tools = styled.div``;

export default SectionHeader;
