import styled from 'styled-components';

import Images from '../../Images';
import Paper from '../paper';

function EmptyList({ warningTitle, warningSubTitle, height }) {
  return (
    <>
      <Paper>
        <EmptyContent style={{ height: `${height}px` }}>
          <img src={Images.empty_list} alt={warningTitle || '조회 가능한 데이터가 없습니다.'} />
          <WarningTitle>{warningTitle || '조회 가능한 데이터가 없습니다.'}</WarningTitle>
          {warningSubTitle
          && <WarningTitle>{warningSubTitle}</WarningTitle>}
        </EmptyContent>
      </Paper>
    </>
  );
}

const EmptyContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 500px;
  flex-direction: column;
`;
const WarningTitle = styled.span`
  font-style: normal;
  font-size: 14px;
  font-weight: 400;
  line-height: 24px;
  vertical-align: Center;
  color: var(--color-gray-700);
`;
export default EmptyList;
