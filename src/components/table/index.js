import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import { Table } from 'antd';

import Images from '../../Images';

import { columnAssign } from '../../constants/columnType';

import { cssTableHiddenHeader, cssTable } from '../../styles/Table';
import LoadingComponent from '../loading';

function TableComponent({
  columns,
  className,
  dataSource,
  onClickRow,
  onCheckedRow,
  rowSelected,
  scrollY,
  fixedHeader,
  rowKey,
  conditionalHighlight,
  isLoading = false,
}) {
  // column 타입에 따라 style 지정
  const [exportColumns, setExportColumns] = useState(columnAssign(columns));
  const [selectedRowKeys, setSelectedRowKeys] = useState();
  const [currentSelectedRow, setCurrentSelectedRow] = useState();

  // Row Cell 선택시 선택 Record 반환
  const changeSelectRow = (record) => {
    setCurrentSelectedRow(record.key);
    onClickRow(record);
  };

  // Table 체크박스 선택
  const onSelectChange = (selectedRow) => {
    onCheckedRow(selectedRow);
    setSelectedRowKeys(selectedRow);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const getRowClassName = (row) => {
    if (conditionalHighlight) {
      return conditionalHighlight(row);
    }
    if (onClickRow) {
      return (row.key === currentSelectedRow ? 'currentSelectedRow' : '');
    }
    return 'noClick';
  };

  const emptyText = {
    emptyText: (
      <>
        <img src={Images.empty_graph} alt="no data" />
      </>
    ),
  };

  // 상위 checkedRow state 변경시 Table Checked 반영
  useEffect(() => {
    setSelectedRowKeys(rowSelected);
  }, [rowSelected]);

  useEffect(() => {
    setExportColumns(columnAssign(columns));
  }, [columns]);

  if (isLoading) {
    return (
      <LoadingComponent isLoading={isLoading} />
    );
  }
  return (
    <Container>
      {
        fixedHeader ? (
          <>
            <HeaderTable
              columns={exportColumns}
              className="fixedTable"
              dataSource={dataSource.filter((column) => column.key === fixedHeader)}
              pagination={false}
              rowClassName="tableNotHover"
            />
            <FixedTable>
              <Table
                locale={emptyText}
                className={className}
                columns={exportColumns}
                dataSource={dataSource.filter((column) => column.key !== fixedHeader)}
                rowClassName={getRowClassName}
                rowKey={rowKey}
                onRow={
                  onClickRow && (
                    (record) => ({
                      onClick: () => changeSelectRow(record),
                    })
                  )
                }
                pagination={false}
                scroll={scrollY && { y: scrollY }}
              />
            </FixedTable>
          </>
        ) : (
          <Table
            locale={emptyText}
            className={className}
            columns={exportColumns}
            dataSource={dataSource}
            rowSelection={rowSelected ? rowSelection : false}
            rowClassName={getRowClassName}
            rowKey={rowKey}
            onRow={
              onClickRow && (
                (record) => ({
                  onClick: () => changeSelectRow(record),
                })
              )
            }
            scroll={scrollY && { y: scrollY }}
            pagination={false}
          />
        )
      }
    </Container>
  );
}

const FixedTable = styled.div`
  ${cssTableHiddenHeader}
  .ant-table-wrapper {
    position: relative;
    padding-top: 6px;
    &:before {
      position:absolute;
      top: 0;
      left: 0;
      right: 0;
      height: 6px;
      background-color: var(--color-gray-50);
      content:"";
      z-index:100;
      display: block;
    }
  }
`;

const HeaderTable = styled(Table)`
  .ant-table-body::-webkit-scrollbar {
    background-color: white;
  }
  .ant-table-body::-webkit-scrollbar-thumb {
    background-color: white;
  }
  .ant-table-body::-webkit-scrollbar-track {
    background-color: white;
  }

  .ant-table-tbody {
    padding-right: 6px;
  }
`;
const Container = styled.div`
  ${cssTable}
  .tableNotHover {
    .ant-table-cell-row-hover {
      background-color: #FFF !important;
      cursor: default !important;
    }
    .currentSelectedRow {
      .ant-table-cell-row-hover {
        background-color: #F2F9FF !important;
        cursor: default !important;
      }
    }
  }
  .ant-table-wrapper {
    max-width: unset !important;
  }
`;

export default TableComponent;
