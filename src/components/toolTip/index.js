import { useEffect, useRef } from 'react';
import styled from 'styled-components';

import Images, { SvgCloseX } from '../../Images';

function ToolTip({ children, text, position }) {
  const tooltipBox = useRef();

  const handleTooltipToggle = (event) => {
    event.stopPropagation();
    const toggle = tooltipBox.current.classList.contains('show');
    document.querySelectorAll('.tooltipBox').forEach((element) => {
      element.classList.remove('show');
    });
    if (toggle) {
      tooltipBox.current.classList.remove('show');
    } else {
      tooltipBox.current.classList.add('show');
      if (position === 'center') {
        const resetLeft = tooltipBox.current.offsetWidth / 3;
        tooltipBox.current.style.left = `-${resetLeft}px`;
      }
    }
  };

  useEffect(() => {
    function handleClickOutside(event) {
      // 현재 document에서 mousedown 이벤트가 동작하면 호출되는 함수입니다.
      if (tooltipBox.current && !tooltipBox.current.contains(event.target)) {
        tooltipBox.current.classList.remove('show');
      }
    }
    // 현재 document에 이벤트리스너를 추가합니다.
    document.addEventListener('mouseup', handleClickOutside);
    // useEffect 함수가 return하는 것은 마운트 해제하는 것과 동일합니다.
    // 즉, Class 컴포넌트의 componentWillUnmount 생명주기와 동일합니다.
    // 더 이상'mousedown'이벤트가 동작하더라도 handleClickOutside 함수가 실행되지 않습니다.
    return () => {
      document.removeEventListener('mouseup', handleClickOutside);
    };
  }, [tooltipBox]);

  if (children) {
    return (
      <>
        <TooltipLink onClick={handleTooltipToggle}>
          {children}
        </TooltipLink>
        <TooltipBox position={position} className="tooltipBox" ref={tooltipBox}>
          {text}
          <button onClick={handleTooltipToggle}>
            <SvgCloseX width="16" height="16" fill="#111111" />
          </button>
        </TooltipBox>
      </>
    );
  }
  return (
    <>
      <TooltipButton onClick={handleTooltipToggle}>
        <img className="circle" src={Images.exclamation_circle} alt="exclamation_circle" width={16} height={16} />
      </TooltipButton>
      <TooltipBox position={position} className="tooltipBox" ref={tooltipBox}>
        {text}
        <button onClick={handleTooltipToggle}>
          <SvgCloseX width="16" height="16" fill="#111111" />
        </button>
      </TooltipBox>
    </>
  );
}
const TooltipButton = styled.button`
  border: none;
  background-color: transparent;
  padding: 0;
  width: 16px;
  height: 16px;
  margin-left: 2px;
  cursor: help;
  vertical-align: text-top;
`;
const TooltipBox = styled.div`
  position: absolute;
  bottom: 22px;
  border-radius: 8px;
  background-color: #FFF;
  border: var(--border-default);
  padding: 12px 36px 12px 12px;
  z-index: 997;
  font-weight: 400;
  font-size: 12px;
  white-space: nowrap;
  display: none;
  filter: drop-shadow(0px 0px 1px rgba(0, 0, 0, 0.08)) drop-shadow(0px 2px 6px rgba(0, 0, 0, 0.05));
  cursor: auto;
  color: #666;
  left: ${(props) => ((props.position === 'right') ? 'auto !important' : '0')};
  right: ${(props) => ((props.position === 'right') ? '-18px' : 'auto')};
  text-align : left;

  &.show {
    display: block;
  }
  button {
    border: none;
    background-color: transparent;
    padding: 0;
    width: 16px;
    height: 16px;
    position: absolute;
    top: 12px;
    right: 8px;
    cursor: pointer;
  }
`;
const TooltipLink = styled.a`
  display: inline-flex;
  position: relative;
`;

export default ToolTip;
