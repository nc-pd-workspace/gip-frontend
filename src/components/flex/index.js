import styled from 'styled-components';

function Flex({
  children,
  className,
  display,
  justifyContent,
  flexDirection,
  flexGrow,
  flexBasis,
  flexShrink,
  flexWrap,
  flex,
  alignItems,
  margin,
  padding,
  width,
  height,
  maxWidth,
}) {
  return (
    <Container
      className={className}
      style={{
        display: display || 'flex',
        justifyContent: justifyContent || 'flex-start',
        flexDirection: flexDirection || 'row',
        flexGrow: flexGrow || 0,
        flexBasis: flexBasis || 'auto',
        flexShrink: flexShrink || 1,
        flexWrap: flexWrap || 'nowrap',
        flex: flex || '0 1 auto',
        alignItems: alignItems || 'stretch',
        margin: margin || '0',
        padding: padding || '0',
        width: width || 'auto',
        height: height || 'auto',
        maxWidth: maxWidth || 'none',
      }}
    >
      {children || ''}
    </Container>
  );
}

const Container = styled.div``;

export default Flex;
