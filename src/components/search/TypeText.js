import {
  useState, useImperativeHandle, forwardRef, useEffect,
} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { Select, Input } from 'antd';

import { SvgArrowDropdown } from '../../Images';

function TypeText({
  selectColumn, column, className, title, options, placeholder, isDetail, detailOnOff, width, defaultSelectValue = '', validation = null, validationTransform,
}, ref) {
  const initialState = {
    [selectColumn]: defaultSelectValue || '',
    [column]: '',
  };
  const [state, setState] = useState(initialState);
  const [error, setError] = useState(null);
  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const handleChangeSelect = (e) => {
    updateState({ [selectColumn]: e });
  };
  const handleChangeText = (e) => {
    setError(null);
    let result = e.target.value;

    if (validationTransform) {
      result = validationTransform(state[selectColumn], result);
    }
    updateState({ [column]: result });
  };

  const renderError = () => {
    if (error) {
      return (
        <ErrorWrap role="alert" className="ant-form-item-explain-error">
          {error}
        </ErrorWrap>
      );
    }
    return (<></>);
  };

  useImperativeHandle(ref, () => ({
    getResultData: () => {
      if (validation) {
        const validationResult = validation(state[column]);
        if (validationResult !== null) {
          setError(validationResult);
          return null;
        }
      }
      if (state[column] && state[selectColumn]) {
        return { [column]: state[column], [selectColumn]: state[selectColumn] };
      }
      return {};
    },
    setReset: () => {
      updateState(initialState);
    },
  }));

  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState);
    }
  }, [detailOnOff]);

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff} width={width}>
      <div className="title">{title}</div>
      <div className="content">
        <ContentWrap>
          <FormWrap>
            <SingleSelectComponent
              onChange={handleChangeSelect}
              options={options}
              value={state[selectColumn]}
              placeholder={placeholder}
              suffixIcon={<SvgArrowDropdown />}
            />
            <SingleInput placeholder="" value={state[column]} onChange={handleChangeText} />
          </FormWrap>
          {renderError()}
        </ContentWrap>
      </div>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding: 7px 0;
  width: ${(props) => props.width || '100%'};
  &.off {
    display: none;
  }
`;
const ContentWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const FormWrap = styled.div`
  display: flex;
`;

const SingleInput = styled(Input)`
  flex: 1;
  height: 34px;
  margin-left: 5px;
  box-sizing: border-box;

  &[disabled] {
    background: #fff;
  }
`;
const SingleSelectComponent = styled(Select)`
  flex-shrink: 0;
  width: 120px;
  height: 34px;

  .ant-select-selector, .ant-select-selection-search-input, 
  .ant-select-selection-item, .ant-select-selection-search {
    height: 34px !important;
    line-height: 34px !important;
    font-size: 13px;
  }
`;
const ErrorWrap = styled.div`
  margin-top:4px;
  width: 100%;
  height: auto;
  min-height: 18px;
  opacity: 1;
  color: #ff4d4f;
  font-size: 12px;
  line-height: 18px;

  span {
    padding-left: 1px;
    img {
      width: 14px;
    }
    svg {
      margin-right: 2px;
    }
  }
`;
export default forwardRef(TypeText);
