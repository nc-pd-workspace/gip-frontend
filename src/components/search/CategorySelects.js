import {
  useState, forwardRef, useImperativeHandle, useEffect, useCallback,
} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { Select } from 'antd';

import { SvgArrowDropdown } from '../../Images';
import CategoryLoading from '../loading/CategoryLoading';

// import { getSearchCategory } from '../../views/customer/allActivityCustomer/redux/slice';

function CategorySelects({
  column, className, depth, title, isDetail, detailOnOff, options, loading, placeholder, required = false, popupContainer = '.isActivePage',
}, ref) {
  const initialState = {
    [column]: [null, null, null],
    value: [null, null, null],
  };
  const [state, setState] = useState(initialState);
  const [localOptions, setLocalOptions] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => () => {
    resetState();
  }, []);
  const updateState = (value) => {
    setState({ ...setState, ...value });
  };

  const resetState = () => {
    setState(initialState);
    setLocalOptions([]);
  };

  const handleChange = useCallback((value, idx) => {
    if (typeof options[idx + 1] !== 'undefined') {
      const newArr = localOptions;
      let finArr = options[idx + 1].filter((v) => v.parent && value === v.parent);
      if (finArr.length > 0) {
        finArr.unshift({ label: '전체', value: '' });
      }
      finArr = finArr.map((v) => ({ label: v.label, value: v.value }));
      newArr.splice(idx + 1, 1);
      newArr.splice(idx + 1, 0, finArr);
      setLocalOptions(newArr);
    }
    const arr = state.value.map((v, index) => ((index >= idx) ? null : v));

    arr[idx] = value;
    if (!required) {
      if (typeof options[idx + 1] !== 'undefined') arr[idx + 1] = null;
    }
    setError(null);
    updateState({ [column]: arr, value: arr });
  }, [localOptions, state]);

  const getDisabled = (idx) => {
    if (idx === 0 && loading) return true;
    if (localOptions.length > idx && localOptions[idx].length === 0) return true;
    if (state.value[idx - 1] === null || state.value[idx - 1] === '') return true;
    return false;
  };

  const renderError = () => {
    if (error) {
      return (
        <ErrorWrap role="alert" className="ant-form-item-explain-error">
          {error}
        </ErrorWrap>
      );
    }
    return (<></>);
  };

  const getSuffixIcon = (idx) => {
    if (idx === 0 && loading) {
      return (<CategoryLoading />);
    } return (<SvgArrowDropdown />);
  };

  useImperativeHandle(ref, () => ({
    getColumnKey: () => column,
    getResultData: () => {
      if (isDetail && detailOnOff === 'off') return {}; // 상세보기 아이템인데 detailOnOff가 Off상태면 빈값 리턴
      if (state[column][0] === null && required) {
        setError('카테고리를 선택해주세요.');
      } else setError(null);
      return { [column]: state[column] };
    },
    setReset: () => {
      if (!required) {
        if (localOptions[0].length === 1) {
          updateState({ [column]: [null, null, null], value: [null, null, null] });
        } else {
          updateState({ [column]: ['', null, null], value: ['', null, null] });
        }
      } else {
        updateState(initialState);
      }
    },
    setValue: (value) => {
      updateState({ [column]: value, value });
    },
  }));
  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState);
    }

    if (options.length > 0 && options[0].length > 0) {
    // required 일때는 전체 항목이 카테고리 옵션에 있으면 안되서 없앰.
      if (required) {
        let arr = [...options];
        if (arr.length > 0) {
          arr[0] = arr[0].filter((v) => v.value !== '');
        }
        arr = arr.map((v) => v.map((c) => ({ label: c.label, value: c.value })));
        setLocalOptions([...arr]);
      } else { // 아닐때는 옵션이 변경될때 기본값이 전체여야 하므로 전체로 세팅.
        updateState({ [column]: ['', null, null], value: ['', null, null] });
        let arr = [...options];
        arr = arr.map((v) => v.map((c) => ({ label: c.label, value: c.value })));
        setLocalOptions([...arr]);
      }
    } else {
      resetState();
      setLocalOptions([...options]);
    }
  }, [detailOnOff, options]);

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
      <div className={`title${required === true ? ' required' : ''}`}>{title}</div>
      <CategoryWrap className="content">
        <div style={{ width: '100%' }}>
          {localOptions.map((option, idx) => (
            <CategorySelectsComponent
              key={idx}
              depth={depth}
              className={loading && idx === 0 ? 'loading' : ''}
              disabled={getDisabled(idx)}
              onChange={(value) => { handleChange(value, idx); }}
              options={option}
              placeholder={idx === 0 && loading ? '불러오는 중...' : placeholder}
              value={state.value[idx]}
              loading={loading}
              suffixIcon={getSuffixIcon(idx)}
              getPopupContainer={() => document.querySelector(`${popupContainer}`)}
            />
          ))}
        </div>
        {renderError()}
      </CategoryWrap>
    </Container>

  );
}

const Container = styled.div`
  display: flex;
  padding: 7px 0;
  
  &.off {
    display: none;
  }
  .title {
    position: relative;
  }
  .title.required:after {
    content: "";
    position:absolute;
    top: 11px;
    display: inline-block;
    margin-left: 3px;
    background: var(--color-danger);
    border-radius: 50%;
    width: 5px;
    height: 5px;
  }
`;
const CategorySelectsComponent = styled(Select)`
  width: ${(props) => {
    if (props.depth === 2) {
      return '48%';
    } if (props.depth === 3) {
      return '32%';
    }
    return '50%';
  }};
  margin-right: ${(props) => {
    if (props.key === 2) {
      return '0px';
    }
    return '8px';
  }};
  &.loading.ant-select-disabled {
    .ant-select-selector {
      background: #fff;
    }
  }
  `;

const CategoryWrap = styled.div`
  flex-direction: column;
  justify-content: center;
  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    height: 34px;
  }
`;

const ErrorWrap = styled.div`
  margin-top:4px;
  width: 100%;
  height: auto;
  min-height: 18px;
  opacity: 1;
  color: #ff4d4f;
  font-size: 12px;
  line-height: 18px;
  span {
    padding-left: 1px;
    img {
      width: 14px;
    }
    svg {
      margin-right: 2px;
    }
  }
`;
export default forwardRef(CategorySelects);
