import {
  Children, cloneElement, useRef, useImperativeHandle, forwardRef,
} from 'react';
import styled, { css } from 'styled-components';
import cn from 'classnames';
import isArray from 'lodash-es/isArray';

function MultipleRow({
  children, className, isDetail, detailOnOff,
}, ref) {
  const childrenRef = useRef([]);

  const getCloneElement = (child, idx) => {
    const elem = cloneElement(child, {
      key: idx,
      isDetail,
      detailOnOff,
      ref: (r) => {
        childrenRef.current[idx] = r;
      },
    });
    return elem;
  };

  useImperativeHandle(ref, () => ({
    getResultData: () => {
      let result = {};
      childrenRef.current.forEach((v) => {
        result = { ...result, ...(v.getResultData ? v.getResultData() : {}) };
      });
      return result;
    },
    getColumnKey: () => 'MultipleRow',
    setReset: () => {
      childrenRef.current.forEach((v) => {
        if (v.setReset) v.setReset();
      });
    },
    setValue: (search) => {
      childrenRef.current.forEach((v) => {
        const column = v.getColumnKey();
        if (isArray(column)) {
          const arr = [];
          column.forEach((item) => {
            if (Object.keys(search).indexOf(item) > -1) {
              arr.push(search[item]);
            } else {
              arr.push(null);
            }
          });
          if (arr.filter((item) => item !== null).length > 0) {
            v.setValue(arr);
          }
        } else if (Object.keys(search).indexOf(column) > -1) {
          v.setValue(search[column]);
        }
      });
    },

  }));

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
      {Children.map(children, (child, idx) => getCloneElement(child, idx))}
    </Container>
  );
}

const Container = styled.div`
  display: flex;

  ${(props) => props.detailOnOff && css`
    border-top: 1px solid #F5F3FB;
    margin-top: 9px;
    padding-top: 9px;
  `}
  &.off {
    display: none;
  }

  >div {
    flex: 1;
    padding-right: 20px;
  }
  >div:last-child {
    padding-right: 0;
  }
`;

export default forwardRef(MultipleRow);
