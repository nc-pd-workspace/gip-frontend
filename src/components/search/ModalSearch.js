import {
  Children, cloneElement, useRef,
} from 'react';
import styled from 'styled-components';
import { Button } from 'antd';

import Paper from '../paper';

/**
   * @param {setSearch} page에서 관리하는 Search Parameter Setter
   *
   * @참고사항
   *   하위 컴포넌트 개발 시 반드시 필요한 메소드 일람 searchTerm 컴포넌트 참조.
   *   useImperativeHandle 사용.
   *     getResultData(): 검색 버튼 누를 시 검색에 필요한 데이터를 하위컴포넌트에서 조립 후 리턴
   *                      ex: {startAt: '2022-01-01', endAt: '2022-01-02'}
   *                      @return 형식 : json
   *     setReset(): 초기화 버튼 누를 시 로컬에서 관리중인 데이터 초기화
   */
function ModalSearch({
  children, setSearch, className,
}) {
  const childrenRef = useRef([]);

  const getCloneElement = (child, idx) => {
    const elem = cloneElement(child, {
      key: idx,
      ref: (r) => {
        childrenRef.current[idx] = r;
      },
    });
    return elem;
  };
  const onClickSearchBtn = () => {
    let result = {};
    childrenRef.current.some((v) => {
      if (v.getResultData() === null) {
        result = null;
        return true;
      }
      result = { ...result, ...v.getResultData() };
      return false;
    });
    if (result !== null) {
      setSearch(result);
    }
  };

  return (
    <Container className={className}>
      <SearchPaper border>
        {Children.map(children, (child, idx) => getCloneElement(child, idx))}
      </SearchPaper>
      <FooterWrap>
        <SearchBtn type="primary" onClick={onClickSearchBtn}>검색</SearchBtn>

      </FooterWrap>
    </Container>
  );
}

const Container = styled.div`
`;
const SearchPaper = styled(Paper)`
  padding: 20px 20px 0 20px;
  background-color: #F7F8FA;
  border: 0;
  .title {
    flex-shrink: 0;
    width: 78px;
    font-size: 14px;
    line-height: 34px;
    height: 34px;
    margin-bottom: 14px;
  }
  .content {
    display: flex;
    align-items: center;
    flex: 1;
    padding: 0;
    margin-bottom: 14px;
  }
  `;
const FooterWrap = styled.div`
    display: flex;
    position: relative;
    justify-content: center;
    background-color: #F7F8FA;
  `;

const SearchBtn = styled(Button)`
    width: 150px;
    height: 40px;
    background-color: var(--color-steelGray-800) !important;
    border-color:var(--color-steelGray-800) !important;
    font-style: normal;
    font-size: 14px;
    line-height: 150%;
    border-radius: 4px;
    margin-bottom: 20px;

  `;

export default ModalSearch;
