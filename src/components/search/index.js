import {
  Children, cloneElement, useRef, forwardRef, useImperativeHandle,
} from 'react';
import styled from 'styled-components';
import isArray from 'lodash-es/isArray';

import ArrowToggle from '../arrowToggle';
import Paper from '../paper';
import Button from '../button';
import Images from '../../Images';

/**
 * @param {setSearch} page에서 관리하는 Search Parameter Setter
 *
 * @참고사항
 *   하위 컴포넌트 개발 시 반드시 필요한 메소드 일람 searchTerm 컴포넌트 참조.
 *   useImperativeHandle 사용.
 *     getResultData(): 검색 버튼 누를 시 검색에 필요한 데이터를 하위컴포넌트에서 조립 후 리턴
 *                      ex: {startAt: '2022-01-01', endAt: '2022-01-02'}
 *                      @return 형식 : json
 *     setReset(): 초기화 버튼 누를 시 로컬에서 관리중인 데이터 초기화
 */
function Search({
  children, setSearch, detailOnOff, setDetailOnOff, onReset,
}, ref) {
  const childrenRef = useRef([]);

  useImperativeHandle(ref, () => ({
    clickSearch: () => onClickSearchBtn(),
    clickReset: () => onClickResetBtn(),
    setValue: (search) => {
      childrenRef.current.forEach((v) => {
        const column = v.getColumnKey();
        if (isArray(column)) {
          const arr = [];
          column.forEach((item) => {
            if (Object.keys(search).indexOf(item) > -1) {
              arr.push(search[item]);
            } else {
              arr.push(null);
            }
          });
          if (arr.filter((item) => item !== null).length > 0) {
            v.setValue(arr);
          }
        } else if (Object.keys(search).indexOf(column) > -1) {
          v.setValue(search[column]);
        } else if (column === 'MultipleRow') {
          v.setValue(search);
        }
      });
    },
  }));

  const getCloneElement = (child, idx) => {
    const elem = cloneElement(child, {
      key: idx,
      ref: (r) => {
        childrenRef.current[idx] = r;
      },
    });
    return elem;
  };
  const onClickSearchBtn = () => {
    let result = {};
    childrenRef.current.forEach((v) => {
      result = { ...result, ...(v.getResultData ? v.getResultData() : {}) };
    });
    setSearch(result);
  };

  const onClickResetBtn = () => {
    childrenRef.current.forEach((v) => {
      v.setReset();
    });
    if (onReset) onReset();
  };

  const onClickDetail = () => {
    setDetailOnOff(detailOnOff === 'on' ? 'off' : 'on');
  };

  return (
    <Container>
      <SearchPaper border className="searchPaper">
        {Children.map(children, (child, idx) => getCloneElement(child, idx))}
      </SearchPaper>
      <FooterWrap>
        <ResetBtn onClick={onClickResetBtn} height="40" width="80">
          <img src={Images.iconRefresh} alt="" />
          초기화
        </ResetBtn>
        <SearchBtn type="fillBlue" onClick={onClickSearchBtn} height="40" width="150">조회</SearchBtn>
        { detailOnOff && (
          <DetailBtn onClick={onClickDetail}>
            상세조회
            <ArrowToggle {...{ open: detailOnOff === 'on' ? 'open' : '' }} />
          </DetailBtn>
        )}
      </FooterWrap>
    </Container>
  );
}

const Container = styled.div``;
const SearchPaper = styled(Paper)`
  padding: 13px 16px;
  .title {
    flex-shrink: 0;
    width: 78px;
    font-size: 14px;
    line-height: 34px;
    height: 34px;
  }
  .content {
    display: flex;
    align-items: center;
    flex: 1;
    padding: 0;
    height: 34px;
  }

  >div:last-child {
    border-bottom: 0;
  }
`;
const FooterWrap = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  padding-top: 14px;
  button + button {
    margin-left: 10px;
  }
`;
const ResetBtn = styled(Button)`
  width: 80px;
  height: 40px;
  font-style: normal;
  font-size: 14px;
  border: var(--border-default);
  line-height: 150%;
  border-radius: 4px;
  justify-content: center;
  padding: 0;
`;
const SearchBtn = styled(Button)`
  width: 150px;
  height: 40px;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 150%;
  border-radius: 4px;
  justify-content: center;
`;
const DetailBtn = styled.button`
  display: flex;
  align-items: flex-start;
  position: absolute;
  top: 0;
  right: 0;
  margin-top: 10px;
  border: 0;
  color: var(--color-gray-500);
  background: 0;
  font-size:13px;
  line-height: 16px;
  >span {
    vertical-align: middle;
  }
`;
export default forwardRef(Search);
