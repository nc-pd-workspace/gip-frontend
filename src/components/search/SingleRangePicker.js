/* eslint-disable no-nested-ternary */
import {
  useState, useImperativeHandle, forwardRef, useEffect, useLayoutEffect, memo,
} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { DatePicker, Radio } from 'antd';

import moment from 'moment';

import { useSelector } from 'react-redux';

import { IconCalendar } from '../../Images';

/**
 *
 * @param { column, className, title, isDetail, detailOnOff, disabledDate, showDateType=true, disabledToday = false, maxSelectDate, disabled2020, } param0
 * @param {*} ref
 *
 * column에서 키 값을 배열로 받아서 value와 mapping 함. [startAtKey, endAtKey]
 * showDateType을 false로 놓으면 일,월 타입 선택을 하지않음(자동 일로 설정)
 * @returns
 */

function SingleRangePicker({
  column, className, title, isDetail, detailOnOff, showDateType = true, setDefaultDate = true, maxSelectDate, disabled2020 = true, comparisonDefaultDate = false, dropdownClassName, maxSelectMonth,
}, ref) {
  const [errorMessage, setErrorMessage] = useState();
  const { defaultRangeDate, totalUpdatedDate, comparisonRangeDate } = useSelector((state) => state.common);

  const initialState = {
    initial: false,
    dayType: 'day',
    dayValue: setDefaultDate ? defaultRangeDate.day : (comparisonDefaultDate ? comparisonRangeDate.day : [null, null]),
  };
  const [state, setState] = useState(initialState);
  const [tick, setTick] = useState('');
  const [tempValue, setTempValue] = useState();
  const [dates, setDates] = useState();
  const [openCalendar, setOpenCalendar] = useState(true);
  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  function onPickerCell(e) {
    if (e.target.getAttribute('listener') === 'true' && maxSelectDate > 0) {
      document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-footer-extra').style.display = 'block';
      setErrorMessage((state.dayType === 'day') ? `시작일로부터 최대 ${maxSelectDate}일까지 조회 가능합니다.` : `시작월로부터 최대 ${maxSelectMonth}개월까지 조회 가능합니다.`);
    }
  }
  const onCalendarChange = (day, _, { range }) => {
    document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-footer-extra').style.display = 'none';
    setErrorMessage('');
    setTick(range);
    setDates(day);
  };

  const onChangeRangePicker = (day) => {
    // const calculatorDate = moment(day[1] - day[0]).date();  // 날짜계산용 변수
    // if (calculatorDate > maxSelectDate) {
    // }
    updateState({ ...state, dayValue: day });
  };

  const getResultData = (value) => {
    if (value[0] && value[1]) {
      if (state.dayType === 'day') {
        return {
          [column[0]]: moment(value[0]).format('YYYYMMDD'),
          [column[1]]: moment(value[1]).format('YYYYMMDD'),
        };
      }
      return {
        [column[0]]: moment(value[0]).format('YYYYMM'),
        [column[1]]: moment(value[1]).format('YYYYMM'),
      };
    }
    return {};
  };

  useImperativeHandle(ref, () => ({
    getColumnKey: () => column,
    getResultData: () => getResultData(state.dayValue),
    setReset: () => {
      updateState(initialState);
    },
    setValue: (value) => {
      const result = value.map((v) => moment(v));
      const filter = value.filter((v) => v !== null);
      let dayType = '';
      if (filter.length > 0 && filter[0].length > 6) {
        dayType = 'day';
      } else dayType = 'month';

      updateState({ ...state, dayValue: result, dayType });
    },
  }));

  useLayoutEffect(() => {
    if (setDefaultDate) {
      updateState({ dayValue: defaultRangeDate[state.dayType] });
    } else if (comparisonDefaultDate) {
      // 비교기간 날짜 셋팅
      updateState({ dayValue: comparisonRangeDate[state.dayType] });
    } else {
      updateState({ dayValue: [null, null] });
    }
  }, [state.dayType]);

  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState);
    }
  }, [detailOnOff]);

  const disabledDate = (current) => {
    if (openCalendar === false) {
      const pickerCell = document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-panel-container').querySelectorAll('.ant-picker-cell');
      pickerCell.forEach((elem) => {
        elem.querySelector('.ant-picker-cell-inner').setAttribute('listener', 'false');
      });
      if (tick === 'start' || tick === 'end') {
        const disabledPicker = document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-panel-container').querySelectorAll('.ant-picker-cell-disabled');
        disabledPicker.forEach((elem) => {
          elem.querySelector('.ant-picker-cell-inner').setAttribute('listener', 'true');
        });
      }
    }

    let standardDate = totalUpdatedDate;
    if (!standardDate) {
      // 집계 날짜가 없으면 어제 기준으로
      standardDate = moment().startOf('day').subtract(1, 'days').format('YYYY-MM-DD');
    }
    if (standardDate && current && current > moment(standardDate).endOf('day')) {
      return true;
    }
    if (disabled2020 && current && current < moment('2021-01').startOf('month')) {
      return true;
    }
    if (!dates || dates.length === 0) {
      return false;
    }
    if (maxSelectDate > 0 && state.dayType === 'day') {
      const tooLate = dates[0] && current.diff(dates[0], 'days') > maxSelectDate - 1;
      const tooEarly = dates[1] && dates[1].diff(current, 'days') > maxSelectDate;
      return tooEarly || tooLate;
    }
    if (maxSelectMonth > 0 && state.dayType === 'month') {
      const tooLateMonth = dates[0] && current.diff(dates[0], 'month') > maxSelectMonth - 2;
      const tooEarlyMonth = dates[1] && dates[1].diff(current, 'month') > maxSelectMonth;
      return tooEarlyMonth || tooLateMonth;
    }
    return false;
  };

  const onOpenChange = (open) => {
    setTick('');
    if (open) {
      setTempValue([]);
      setDates([]);
      setTimeout(() => {
        document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-footer-extra').style.display = 'none';
        const pickerCell = document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-panel-container').querySelectorAll('.ant-picker-cell');
        pickerCell.forEach((elem) => {
          setOpenCalendar(false);
          if (elem.getAttribute('listener') === null) {
            elem.querySelector('.ant-picker-cell-inner').addEventListener('click', onPickerCell, true);
          }
        });
      }, 0);
    } else {
      document.querySelector(`.${dropdownClassName}`).querySelector('.ant-picker-footer-extra').style.display = 'none';
      setTempValue(undefined);
      setErrorMessage('');
    }
  };

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
      <div className="title">{title}</div>
      <div className="content">
        {
          showDateType && (
            <Radio.Group
              value={state.dayType}
              onChange={(e) => {
                updateState({ dayType: e.target.value });
              }}
            >
              <Radio.Button value="day">일</Radio.Button>
              <Radio.Button value="month">월</Radio.Button>
            </Radio.Group>
          )
        }

        <DatePicker.RangePicker
          allowClear={false}
          onChange={onChangeRangePicker}
          onCalendarChange={onCalendarChange}
          value={tempValue || state.dayValue}
          format={state.dayType === 'month' ? 'YYYY. M.' : 'YYYY. M. D.'}
          picker={state.dayType === 'month' ? 'month' : undefined}
          separator={(<RangeArrow>~</RangeArrow>)}
          suffixIcon={<IconCalendar fill="#8F959D" />}
          getPopupContainer={() => document.querySelector('.isActivePage')}
          disabledDate={disabledDate}
          renderExtraFooter={() => errorMessage}
          onOpenChange={onOpenChange}
          dropdownClassName={dropdownClassName}
        />
      </div>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding: 7px 0;
  align-items: center;
  &.off {
    display: none;
  }
  .content {
    flex-grow: 1;
    height: 34px;
  }
  .ant-radio-group {
    margin-right: 5px;
  }
  .ant-radio-button-wrapper-checked{
    border-color: var(--color-steelGray-800) !important;
    background-color: var(--color-steelGray-800) !important;
    color: #fff !important;  
  }
  .ant-radio-button-wrapper {
    color: #333;
    font-size: 13px;
    font-weight: 400;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    min-width: 36px;
    padding: 4px 8px;
    border: 1px solid #e3e4e7;
    text-align: center;
    background-color: #fff;
  }
  .ant-picker-range .ant-picker-input {
    width: 82px !important;
  }
`;
const RangeArrow = styled.span`
  color: var(--color-gray-400);
`;
export default memo(forwardRef(SingleRangePicker));
