import {
  useState, useImperativeHandle, forwardRef, useEffect,
} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { Button, Input } from 'antd';

import PartnerInqueryModal from '../../views/settings/partners/components/Modal/PartnerInqueryModal';
import { alertMessage } from '../message';

function ProductSearch({
  column, className, title, isDetail, detailOnOff,
}, ref) {
  const initialState = {
    text: '',
    value: [],
  };
  const [state, setState] = useState(initialState);
  const [partnerInqueryModalVisible, setPartnerInqueryModalVisible] = useState(false);
  const updateState = (value) => {
    setState({ ...state, ...value });
  };

  const onClickModalOpen = () => {
    setPartnerInqueryModalVisible(true);
  };

  const handlePartnerOk = (param) => {
    if (param.length === 0) {
      alertMessage('목록을 선택해주세요.');
      return;
    }
    updateState({ text: param.map((v) => `${v.ptnNm}(${v.ptnId})`).join(','), value: param.map((v) => v.ptnIdx) });
    setPartnerInqueryModalVisible(false);
  };

  const handlePartnerCancel = () => {
    setPartnerInqueryModalVisible(false);
  };

  useImperativeHandle(ref, () => ({
    getColumnKey: () => column,
    getResultData: () => {
      if (state.value.length > 0) {
        return { [column]: state.value.join(',') };
      }
      return {};
    },
    setReset: () => {
      updateState(initialState);
    },
  }));
  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState);
    }
  }, [detailOnOff]);

  return (
    <>
      <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
        <div className="title">{title}</div>
        <div className="content">
          <ContentWrap>
            <FormWrap>
              <SingleInput className="disabled" disabled value={state.text} />
              <ModalButton onClick={onClickModalOpen}>파트너 조회</ModalButton>
            </FormWrap>
          </ContentWrap>
        </div>
      </Container>
      <PartnerInqueryModal
        forceRender
        visible={partnerInqueryModalVisible}
        onOk={handlePartnerOk}
        onClose={handlePartnerCancel}
        getContainer={false}

      />
    </>
  );
}

const Container = styled.div`
  display: flex;
  padding: 7px 0;
  &.off {
    display: none;
  }
`;
const ContentWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const FormWrap = styled.div`
  display: flex;
`;

const SingleInput = styled(Input)`
  flex-grow: 1;
  &.disabled {
    background: #fff !important;
    color: rgba(0,0,0,.85) !important;
  }
`;
const ModalButton = styled(Button)`
  flex-shrink: 0;
  width: 80px;
  height: 34px;
  padding: 10px 7px;
  margin-left: 6px;
  background: #fff;
  border: 1px solid var(--color-blue-500);
  border-radius: 4px;
  >span {
    font-size: 13px;
    line-height: 13px;
    text-align: center;
    vertical-align: top;
    color: var(--color-blue-500);
  }
`;

export default forwardRef(ProductSearch);
