import {
  useState, forwardRef, useImperativeHandle, useEffect, useCallback,
} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

import { Select, DatePicker, Input, Checkbox } from 'antd';
import { MultiSelect } from 'react-multi-select-component';
import { useSelector } from 'react-redux';

import moment from 'moment';

import Images, { SvgArrowDropdown, IconCalendar } from '../../Images';
import CategoryLoading from '../loading/CategoryLoading';

/**
 *
 * @param {type}  Input type 정의 (Text, Select, MultiSelect, DatePicker, CheckGroup, TypeText )
 * @param {*} ref
 * @returns
 *
 *
 *  Text: Input Text
 *  Select, MultiSelect: Input Select 컴포넌트. MultiSelect의 경우 [ ...value ] 배열로 값을 리턴.
 *  DatePicker: Date Picker 컴포넌트.
 *  CheckGroup: Checkbox 선택 컴포넌트. check된 리스트를 리턴 { [column]: [...checkvalue] }
 */
function SingleInputItem({
  type, column, className, width, title, isDetail, detailOnOff, defaultValue, disabledDate, onChange, options = [], loading, picker = 'day', placeholder = '',
}, ref) {
  const { defaultDate, totalUpdatedDate } = useSelector((state) => state.common);

  const getDefaultValue = (valueType) => {
    if (type === 'DatePicker') {
      if (valueType === 'column') return defaultDate[picker].format('YYYY.MM');
      return defaultDate[picker];
    }
    if (defaultValue !== undefined) return defaultValue;
    return null;
  };
  const initialState = {
    [column]: getDefaultValue('column'),
    value: getDefaultValue('value'),
  };

  const [state, setState] = useState(initialState);
  const updateState = (value) => {
    setState({ ...setState, ...value });
  };

  const getDisableDatePicker = useCallback((current) => {
    let standardDate = totalUpdatedDate;
    if (!standardDate) {
      // 집계 날짜가 없으면 어제 기준으로
      standardDate = moment().startOf('day').subtract(1, 'days').format('YYYY-MM-DD');
    }
    if (current && current < moment('2021-01').startOf('month')) {
      return true;
    }
    if (totalUpdatedDate && current && current > moment(standardDate).endOf('day')) {
      return true;
    }
    if (disabledDate && disabledDate(current)) return true;
    return false;
  }, [totalUpdatedDate]);

  useImperativeHandle(ref, () => ({
    getColumnKey: () => column,
    getResultData: () => {
      if (isDetail && detailOnOff === 'off') return {}; // 상세보기 아이템인데 detailOnOff가 Off상태면 빈값 리턴
      if (state[column]) {
        if (type === 'CheckGroup') {
          return { [column]: state[column].join(',') };
        }
        return { [column]: state[column] };
      }
      return {};
    },
    setReset: () => {
      updateState(initialState);
    },
    setValue: (value) => {
      updateState({ value: moment(value), [column]: value });
    },
  }));

  useEffect(() => {
    if (isDetail && detailOnOff === 'off') {
      updateState(initialState); // 상세보기가 꺼졌을 때 초기화
    }
  }, [detailOnOff]);
  const handleDatePickerChange = (date) => {
    updateState({ value: date, [column]: picker === 'month' ? date.format('YYYY.MM') : date.format('YYYY.MM.DD') });
  };
  const handleChange = (e) => {
    switch (type) {
    case 'Select':
    {
      updateState({ value: e, [column]: e });
      if (onChange) onChange(e);
      break;
    }
    case 'MultiSelect': {
      updateState({ value: e, [column]: e.map((v) => v.value) });
      if (onChange) onChange(e.map((v) => v.value));
      break;
    }
    case 'CheckGroup': {
      updateState({ value: e.length === 0 ? null : e, [column]: e.length === 0 ? null : e });
      break;
    }
    default: {
      updateState({ value: e.target.value, [column]: e.target.value });
      if (onChange) onChange(e.target.value);
    }
    }
  };
  // eslint-disable-next-line consistent-return
  const renderComponent = () => {
    switch (type) {
    case 'Text': {
      return (
        <SingleInputComponent
          width={width}
          onChange={handleChange}
          value={state.value}
          placeholder={placeholder}
          Arrow="adsa"
        />
      );
    }
    case 'Select': {
      return (
        <SingleSelectComponent
          width={width}
          onChange={handleChange}
          disabled={!!loading}
          options={options}
          value={state.value}
          className={loading ? 'loading' : ''}
          placeholder={loading ? '불러오는 중...' : placeholder}
          loading={loading}
          suffixIcon={loading ? (<CategoryLoading />) : (<SvgArrowDropdown />)}
          getPopupContainer={() => document.querySelector('.isActivePage')}
        />
      );
    }
    case 'MultiSelect': {
      return (
        <MultiSelectComponent
          width={width}
          disableSearch
          onChange={handleChange}
          options={options}
          value={state.value ? state.value : []}
          hasSelectAll
          overrideStrings={{
            selectSomeItems: placeholder || '전체',
            allItemsAreSelected: '전체 선택됨',
            selectAll: '전체',
          }}
        />
      );
    }
    case 'DatePicker': {
      return (
        <SingleDateComponent
          allowClear={false}
          width={width}
          onChange={handleDatePickerChange}
          value={state.value}
          disabledDate={getDisableDatePicker}
          format={picker === 'month' ? 'YYYY. M.' : 'YYYY. M. D.'}
          picker={picker}
          placeholder={placeholder}
          suffixIcon={<IconCalendar fill="#8F959D" />}
          getPopupContainer={() => document.querySelector('.isActivePage')}
        />
      );
    }
    case 'CheckGroup': {
      return (
        <Checkbox.Group options={options} onChange={handleChange} value={state.value} />
      );
    }
    default: {
      return null;
    }
    }
  };

  return (
    <Container className={cn(detailOnOff, className)} detailOnOff={detailOnOff}>
      <div className="title">{title}</div>
      <div className="content">
        {
          renderComponent()
        }
      </div>
    </Container>

  );
}

const Container = styled.div`
  display: flex;
  padding: 7px 0;

  &.off {
    display: none;
  }
  .content {
    display: flex;
    flex: 1;
    height: 34px;
    align-items: center;
  }
  .rmsc .clear-selected-button{
      margin-right:15px !important;
  }
`;
const SingleSelectComponent = styled(Select)`
  width: ${(props) => props.width || '100%'};
  height: 34px;

  .ant-select-selector, .ant-select-selection-search-input, 
  .ant-select-selection-item, .ant-select-selection-search {
    height: 34px !important;
    line-height: 34px !important;
    font-size: 13px;
  }
  &.loading.ant-select-disabled {
    .ant-select-selector {
      background: #fff;
    }
  }
`;
const SingleDateComponent = styled(DatePicker)`
  width: ${(props) => props.width || '100%'};
  height: 34px;
  .ant-picker-input {
    position: unset;
  }
  .ant-picker-suffix {
    position: absolute;
    top: 6px;
    right: 10px;
  }
`;
const SingleInputComponent = styled(Input)`
  width: ${(props) => props.width || '100%'};
  height: 34px;
`;

const MultiSelectComponent = styled(MultiSelect)`
  width: ${(props) => props.width || '100%'};

  .dropdown-container {
    border: 1px solid #d9d9d9;
    height: 34px;
    .dropdown-heading {
      color: var(--color-gray-700);
      height: 34px;
      font-size: 13px;
      &:after {
        content: "";
        position: absolute;
        top: 50%;
        right: 8px;
        display: block;
        width: 20px;
        height: 20px;
        margin-top: -12px;
        background-image:url(${Images.arrowDropdown});
      }
      & > svg {
        display: none;
      }
    }
    &[aria-expanded=true] .dropdown-heading:after {
      transform: rotate(180deg);
      margin-top: -11px;
    }
  }
  .dropdown-heading-value {
    height: 32px;
    line-height: 28px;
    span {
      color: var(--color-gray-700);
    }
  }
`;

export default forwardRef(SingleInputItem);
