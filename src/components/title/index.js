import styled from 'styled-components';
import cn from 'classnames';

function Title({
  className, level = 1, title, subTitle,
}) {
  return (
    <Container className={cn('Title', `titleLevel-${level}`, className)}>
      <StyledTitle as={`h${level}`}>{title}</StyledTitle>
      {
        subTitle
        && <p>{subTitle}</p>
      }
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  align-items: flex-end;

  h2 {
    font-size: 24px;
    font-weight: 700;
    line-height: 36px;
    color: var(--color-gray-900);
  }

  p {
    margin-left: 5px;
    font-size: 12px;
    font-weight: 400;
    line-height: 18px;
    color: var(--color-gray-700);
    transform: translateY(-4px);
  }
`;

const StyledTitle = styled.div`
  .titleLevel-1 > & {
    font-size: 30px;
    font-weight: 700;
    line-height: 36px;
    color: var(--color-gray-900);
  }

  .titleLevel-2 > & {
    font-size: 24px;
    font-weight: 700;
    line-height: 36px;
    color: var(--color-gray-900);
  }

  .titleLevel-3 > & {
    font-size: 20px;
    font-weight: 700;
    line-height: 30px;
    color: var(--color-gray-900);
  }

  .titleLevel-5 > & {
    font-size: 14px;
    font-weight: 400;
    line-height: 21px;
    color: #111;
  }
`;

export default Title;
