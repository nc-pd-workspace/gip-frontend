import styled from 'styled-components';

function TotalRow({ row }) {
  return (
    <>
      {
        row && (
          <Row>
            총&nbsp;
            {row}
            개
          </Row>
        )
      }
    </>
  );
}

const Row = styled.div`
  font-size: 13px;
  flex: 0 0 auto;
  padding: 6px 0 0 6px;
  font-weight: 400;
  color: var(--color-blue-500);
  align-self: center;
`;

export default TotalRow;
