import React from 'react';
import styled from 'styled-components';

function Descripition({ text, color, padding }) {
  return (
    <Container padding={padding}>
      {
        (typeof text === 'object') ? (
          <DescList>
            { text.map((list, index) => <li key={index}><Desc style={{ color }}>{list}</Desc></li>) }
          </DescList>
        ) : (
          // eslint-disable-next-line object-shorthand
          <Desc style={{ color }}>{text}</Desc>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  padding: ${(props) => ((props.padding) ? `${props.padding}` : '10px 0 0 4px')};
`;
const DescList = styled.ul``;
const Desc = styled.div`
  font-size: 12px;
  line-height: 18px;
  text-align: left;
  padding: 1px 0;
  color: var(--color-gray-500);
  &:before {
    content: "";
    margin-top: -2px;
    margin-right: 6px;
    display: inline-block;
    width: 2px;
    height: 2px;
    border-radius: 3px;
    vertical-align: middle;
    background-color: var(--color-gray-500);
  }
`;
export default Descripition;
