/* eslint-disable unused-imports/no-unused-vars */
import { useEffect, useState } from 'react';
import styled from 'styled-components';

import LoadingBar from './LoadingBar';

function LoadingComponent({ isLoading, background }) {
  const [loadingText, setLoadingText] = useState('데이터를 조회 중입니다.');
  const [handle, setHandle] = useState(null);

  useEffect(() => () => {
    if (handle) clearTimeout(handle);
  });
  useEffect(() => {
    setLoadingText('데이터를 조회 중입니다.');
    if (isLoading) {
      const h = setTimeout(() => {
        setLoadingText((
          <>
            데이터양이 많아 시간이 오래 걸릴 수 있습니다.
            <br />
            조금만 기다려주세요.
          </>));
      }, 4000);
      setHandle(h);
    } else if (handle) clearTimeout(handle);
  }, [isLoading]);
  return (
    <>
      {
        isLoading && (
          <Container background={background}>
            <LoadingBar />
            <LoadingText>{loadingText}</LoadingText>
          </Container>
        )
      }
    </>
  );
}

const Container = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: ${(props) => ((props.background) ? `${props.background}` : '#FFF')};
  border-radius: 8px;
  z-index: 999;
`;

const LoadingText = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: calc(50% + 5px);
  font-size: 14px;
  color: var(--color-gray-700);
  text-align: center;
`;

export default LoadingComponent;
