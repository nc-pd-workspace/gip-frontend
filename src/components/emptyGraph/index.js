import styled from 'styled-components';

import Images from '../../Images';

function EmptyGraph() {
  return (
    <Paper>
      <EmptyContent>
        <img src={Images.empty_graph} alt="조회 가능한 데이터가 없습니다." />
        <WarningTitle>조회 가능한 데이터가 없습니다.</WarningTitle>
      </EmptyContent>
    </Paper>
  );
}

const Paper = styled.div`
  height: 100%;
`;
const EmptyContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  flex-direction: column;
  padding-bottom: 30px;
  img {
    /* margin-top: -32px; */
  }
`;
const WarningTitle = styled.span`
  font-style: normal;
  font-size: 14px;
  font-weight: 400;
  line-height: 24px;
  vertical-align: Center;
  color: var(--color-gray-700);
`;
export default EmptyGraph;
