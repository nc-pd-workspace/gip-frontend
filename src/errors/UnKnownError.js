export class UnknownError extends Error {
  err;

  constructor(err) {
    super('일시적인 장애가 발생했어요. 잠시 후 다시 시도해주세요.');
    this.err = err;

    this.report();
  }

  // eslint-disable-next-line class-methods-use-this
  report() {
    // reportError(this.err);  에러 관리 툴로 레포트
  }
}
