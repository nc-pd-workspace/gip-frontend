export class SomeCodeError extends Error {
  err;

  constructor(err) {
    super('어떠한 에러 발생');
    this.err = err;
  }
}
