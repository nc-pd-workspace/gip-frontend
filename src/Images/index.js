import gsShopLogo from './gsShop_logo.png';
import excelFile from './excel_file.png';
import logo from './logo.svg';
import login_logo from './login_logo.svg';
import loginBanner from './loginBanner.png';
import account from './account.svg';
import activity from './activity.svg';
import angle_down from './angle_down.svg';
import angle_up from './angle_up.svg';
import arrow_up from './arrow_up.svg';
import arrow_right from './arrow_right.svg';
import arrow_down from './arrow_down.svg';
import arrow_left from './arrow_left.svg';
import arrow_main_up from './arrow_main_up.svg';
import arrow_main_down from './arrow_main_down.svg';
import arrow_user_down from './arrow_user_down.svg';
import chevron_right from './chevron_right.png';
import customer_grey from './customer_grey.svg';
import home from './home.svg';
import marketing from './marketing.svg';
import performance from './performance.svg';
import setting from './setting.svg';
import exclamation_circle from './exclamation_circle.svg';
import partner_empty from './partner_empty.svg';
import excel_log from './excel_log.svg';
import pagination_first from './pagination_first.svg';
import pagination_last from './pagination_last.svg';
import pagination_prev from './pagination_prev.svg';
import pagination_next from './pagination_next.svg';
import arrow_triple from './arrow_triple.svg';
import modal_empty from './modal_empty.svg';
import x_circle from './x_circle.svg';
import iconInputAlert from './iconInputAlert.svg';
import iconRefresh from './iconRefresh.svg';
import iconTreePlus from './iconTreePlus.svg';
import iconTreeMinus from './iconTreeMinus.svg';
import iconCloseLarge from './iconCloseLarge.svg';
import iconHelp from './iconHelp.svg';
import iconLink from './iconLink.svg';
import iconFile from './iconFile.svg';
import iconFileMaster from './iconFileMaster.svg';
import iconMail from './iconMail.svg';
import iconError from './iconError.svg';
import iconBeta from './iconBeta.png';
import iconSearch from './iconSearch.svg';
import iconAlert from './iconAlert.png';
import btnMoveToTop from './btnMoveToTop.svg';
import mainDashboard from './mainDashboard.png';
import empty_graph from './empty_graph.svg';
import arrowDropdown from './arrow_dropdown.svg';
import clock from './clock.svg';
import empty_list from './empty_list.svg';

const Images = {
  arrowDropdown,
  gsShopLogo,
  logo,
  login_logo,
  loginBanner,
  account,
  activity,
  angle_down,
  angle_up,
  arrow_up,
  arrow_right,
  arrow_down,
  arrow_left,
  arrow_main_up,
  arrow_main_down,
  arrow_user_down,
  chevron_right,
  customer_grey,
  home,
  marketing,
  performance,
  setting,
  excelFile,
  exclamation_circle,
  partner_empty,
  excel_log,
  pagination_first,
  pagination_last,
  pagination_prev,
  pagination_next,
  arrow_triple,
  modal_empty,
  x_circle,
  iconInputAlert,
  iconRefresh,
  iconTreePlus,
  iconTreeMinus,
  iconCloseLarge,
  iconHelp,
  iconFile,
  iconFileMaster,
  iconLink,
  iconMail,
  iconError,
  iconBeta,
  iconSearch,
  iconAlert,
  btnMoveToTop,
  mainDashboard,
  empty_graph,
  clock,
  empty_list,
};

export default Images;

export { ReactComponent as SvgCloseX } from './close_x.svg';
export { ReactComponent as IconCalendar } from './icon_calendar.svg';
export { ReactComponent as SvgIconRegister } from './iconRegister.svg';
export { ReactComponent as Logo } from './logo.svg';
export { ReactComponent as LoginLogo } from './login_logo.svg';
export { ReactComponent as LoginBanner } from './login_banner.svg';
export { ReactComponent as Account } from './account.svg';
export { ReactComponent as Activity } from './activity.svg';
export { ReactComponent as AngleDown } from './angle_down.svg';
export { ReactComponent as AngleUp } from './angle_up.svg';
export { ReactComponent as ArrowUp } from './arrow_up.svg';
export { ReactComponent as ArrowRight } from './arrow_right.svg';
export { ReactComponent as ArrowDown } from './arrow_down.svg';
export { ReactComponent as ArrowLeft } from './arrow_left.svg';
export { ReactComponent as ArrowMainUp } from './arrow_main_up.svg';
export { ReactComponent as ArrowMainDown } from './arrow_main_down.svg';
export { ReactComponent as ArrowUserDown } from './arrow_user_down.svg';
export { ReactComponent as ChevronRight } from './chevron_right.svg';
export { ReactComponent as CustomerGrey } from './customer_grey.svg';
export { ReactComponent as Home } from './home.svg';
export { ReactComponent as Marketing } from './marketing.svg';
export { ReactComponent as Performance } from './performance.svg';
export { ReactComponent as Setting } from './setting.svg';
export { ReactComponent as ExcelFile } from './excelfile.svg';
export { ReactComponent as ExclamationCircle } from './exclamation_circle.svg';
export { ReactComponent as PartnerEmpty } from './partner_empty.svg';
export { ReactComponent as ExcelLog } from './excel_log.svg';
export { ReactComponent as PaginationFirst } from './pagination_first.svg';
export { ReactComponent as PaginationLast } from './pagination_last.svg';
export { ReactComponent as PaginationPrev } from './pagination_prev.svg';
export { ReactComponent as PaginationNext } from './pagination_next.svg';
export { ReactComponent as ArrowTriple } from './arrow_triple.svg';
export { ReactComponent as ModalEmpty } from './modal_empty.svg';
export { ReactComponent as XCircle } from './x_circle.svg';
export { ReactComponent as SvgArrowUp } from './arrow_up.svg';
export { ReactComponent as SvgArrowDown } from './arrow_down.svg';
export { ReactComponent as SvgArrowDropdown } from './arrow_dropdown.svg';
export { ReactComponent as SvgNone } from './none.svg';
