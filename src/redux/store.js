import { configureStore, combineReducers } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import commonReducer from './commonReducer';
import mainReducer from '../views/main/redux/slice';
import brandLandscapeReducer from '../views/brand/brandLandscape/redux/slice';
import topBrandReducer from '../views/brand/topBrand/redux/slice';

import allActivityCustomerReducer from '../views/customer/allActivityCustomer/redux/slice';
import newCustomerReducer from '../views/customer/newCustomer/redux/slice';
import stepCustomerReducer from '../views/customer/stepCustomer/redux/slice';
import top5BrandCustomerReducer from '../views/customer/top5BrandCustomer/redux/slice';

import customerPerformanceReducer from '../views/period/brandPerformance/redux/slice';
import productCustomerReducer from '../views/period/productCustomer/redux/slice';
import stepConversionReducer from '../views/period/stepConversion/redux/slice';
import inflowKeywordReducer from '../views/period/inflowKeyword/redux/slice';

import partnersReducer from '../views/settings/partners/redux/slice';
import usersReducer from '../views/settings/users/redux/slice';
import myPageReducer from '../views/account/myPage/redux/slice';
import myPartnerPageReducer from '../views/account/myPartnerPage/redux/slice';
import loginReducer from '../views/account/login/redux/slice';

export const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    common: commonReducer,
    main: mainReducer,
    brand: combineReducers({
      brandLandscape: brandLandscapeReducer,
      topBrand: topBrandReducer,
    }),
    customer: combineReducers({
      allActivityCustomer: allActivityCustomerReducer,
      newCustomer: newCustomerReducer,
      productCustomer: productCustomerReducer,
    }),
    activity: combineReducers({
      top5BrandCustomer: top5BrandCustomerReducer,
      stepCustomer: stepCustomerReducer,
    }),
    marketing: combineReducers({
      customerPerformance: customerPerformanceReducer,
      stepConversion: stepConversionReducer,
      inflowKeyword: inflowKeywordReducer,
    }),
    settings: combineReducers({
      users: usersReducer,
      partners: partnersReducer,
    }),
    account: combineReducers({
      myPage: myPageReducer,
      myPartnerPage: myPartnerPageReducer,
      login: loginReducer,
    }),
  },
  middleware: [sagaMiddleware],
});
