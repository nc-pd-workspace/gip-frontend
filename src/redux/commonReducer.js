import { createSlice } from '@reduxjs/toolkit';

import moment from 'moment';

import { asyncApiState } from './constants';
import { pageTitle } from '../constants/pageType';

const initialCustomerSelect = {
  ageUnit: [],
  custGrade: [],
  gender: [],
  sido: [],
};

const initialState = {
  activePageId: 'main',
  resetSettings: false, // 페이지 전환시 settings의 탭 데이터 리셋여부
  openedPages: [{
    id: 'main',
    name: '대시보드 메인',
  }],
  userInfo: {},
  selectPtnIdx: null,
  partnerList: [],
  categoryOption: asyncApiState.initial([[], [], []]),
  brandOption: asyncApiState.initial([]),
  customerSelects: asyncApiState.initial(initialCustomerSelect),
  headerToggle: false,
  expiredTime: null,
  userRole: asyncApiState.initial([]),
  lastUpdateDate: asyncApiState.initial({}),
  messageList: [], // { id: number, type: 'alert or confirm', message: 'string', onOk: func() }   alert 및 confirm 공통 객체
  totalUpdatedDate: null,
  defaultRangeDate: {
    day: [null, null],
    month: [null, null],
  },
  comparisonRangeDate: {
    day: [null, null],
    month: [null, null],
  },
  defaultDate: {
    day: null,
    month: null,
  },
};

export const createNewPageData = (id, query) => ({
  id,
  name: pageTitle[id],
  active: true,
  ...(query ? { query } : {}),
});

export const { actions, reducer } = createSlice({
  name: 'common',
  initialState,
  reducers: {
    resetStore: (state) => ({
      ...initialState,
    }),
    updateStore: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    updatePages: (state, { payload }) => {
      state.openedPages = payload;
    },
    updateDefaultDate: (state) => {
      // 기준날짜는 기본적으로 오늘 날짜에서 어제로 설정.
      let standardDate = moment().startOf('days').subtract(1, 'days');

      const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');

      let lastUpdateDate = null;
      if (sessionUserInfo !== null) {
        const userData = JSON.parse(sessionUserInfo);
        if (userData.lastUpdateDate) {
          lastUpdateDate = userData.lastUpdateDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
          state.totalUpdatedDate = lastUpdateDate;
        }
      }

      if (lastUpdateDate) {
        standardDate = moment(lastUpdateDate).startOf('days');
      }

      const start = moment(standardDate).startOf('month');
      let lastStart = moment(standardDate).startOf('month').subtract(1, 'month');
      let lastStandardDate = standardDate.clone().subtract(1, 'month');
      state.defaultRangeDate.day = [start, standardDate];
      state.defaultDate.day = standardDate;
      // dayType month;
      const prevMonth = moment(standardDate).subtract(1, 'month');
      const currMonth = moment(standardDate);
      state.defaultRangeDate.month = [prevMonth, currMonth];
      state.defaultDate.month = currMonth;

      // 비교기간 날짜 오늘날짜가 지난달의 마지막날짜보다 크면 그달의 마지막날로 셋팅
      const lastMonthDay = start.clone().subtract('1', 'M').endOf('month').format('D');
      if (Number(currMonth.format('D')) > Number(lastMonthDay)) {
        lastStandardDate = standardDate.clone().subtract(1, 'month').endOf('month');
        lastStart = moment(standardDate).startOf('month').subtract(1, 'month');
      } else {
        lastStandardDate = standardDate.clone().subtract(1, 'month');
        lastStart = moment(standardDate).startOf('month').subtract(1, 'month');
      }
      state.comparisonRangeDate.day = [lastStart, lastStandardDate];
      state.comparisonRangeDate.month = [prevMonth, currMonth];
    },
    setActivePageId: (state, { payload }) => {
      const { id, isSideBar } = payload;
      state.activePageId = id;
      state.resetSettings = isSideBar;
    },
    addMessage: (state, { payload }) => {
      state.messageList = [...state.messageList, payload];
    },
    removeMessage: (state, { payload }) => {
      state.messageList = state.messageList.filter((v) => v.id !== payload);
    },
    setUserInfo: (state, { payload }) => {
      const refinedData = [];
      if (payload.partnerList && payload.partnerList.length > 0) {
        if (payload.userInfo.ptnIdx === 0) {
          const sortedPtnList = [...payload.partnerList];
          sortedPtnList.sort((a, b) => {
            if (a.sortSeq === b.sortSeq) return b.ptnIdx - a.ptnIdx;
            return a.sortSeq - b.sortSeq;
          });
          if (sortedPtnList && sortedPtnList.length > 0) {
            state.selectPtnIdx = sortedPtnList[0].ptnIdx;
          }
        } else {
          state.selectPtnIdx = payload.userInfo.ptnIdx;
        }
        const treeData = [...payload.partnerList];

        const roopChildFunc = (data) => {
          if (data && data.length !== 0) {
            const childNode = data.map((childData) => ({
              title: childData.accessYn === 'N' ? (
                <div className="disabled">
                  <span>{`${childData.ptnNm}(${childData.ptnId})`}</span>
                </div>
              ) : `${childData.ptnNm}(${childData.ptnId})`,
              // key: childData.ptnIdx.toString(),
              key: childData.accessYn === 'N' ? `${childData.ptnIdx.toString()}disabled` : childData.ptnIdx.toString(),
              ptnId: childData.ptnId,
              ptnNm: childData.ptnNm,
              sortSeq: childData.sortSeq,
              children: roopChildFunc(childData.subPtn),
            }));
            childNode.sort((a, b) => a.sortSeq - b.sortSeq);
            return childNode;
          }
          return '';
        };

        const roopFunc = (node) => {
          if (node && node.length !== 0) {
            refinedData.push({
              title: node.accessYn === 'N' ? (
                <div className="disabled">
                  <span>{`${node.ptnNm}(${node.ptnId})`}</span>
                </div>
              ) : `${node.ptnNm}(${node.ptnId})`,
              // key: node.ptnIdx.toString(),
              key: node.accessYn === 'N' ? `${node.ptnIdx.toString()}disabled` : node.ptnIdx.toString(),
              ptnId: node.ptnId,
              ptnNm: node.ptnNm,
              sortSeq: node.sortSeq,
              children: roopChildFunc(node.subPtn),
            });
            refinedData.sort((a, b) => a.sortSeq - b.sortSeq);
          }
        };

        treeData.forEach((element) => {
          roopFunc(element);
        });
      }
      state.userInfo = {
        ...payload,
        partnerList: refinedData,
      };
    },
    logout: (state, { payload }) => {
      if (payload) {
        state.messageList = [...state.messageList, { id: 'logout',
          type: 'alert',
          message: payload,
          onOk: () => {
            window.sessionStorage.removeItem('GIPADMIN_USER');
            window.sessionStorage.removeItem('openedPages');
            window.localStorage.setItem('GIPADMIN_LOGOUT', payload);
            window.localStorage.removeItem('GIPADMIN_LOGOUT');
            window.location.reload();
          } }];
      } else {
        window.sessionStorage.removeItem('GIPADMIN_USER');
        window.sessionStorage.removeItem('openedPages');
        window.localStorage.setItem('GIPADMIN_LOGOUT', 'true');
        window.localStorage.removeItem('GIPADMIN_LOGOUT');
        window.location.reload();
      }
    },
    updateToken: (state, { payload }) => {
      const { accessToken, refreshToken, expiredDate, refreshTokenExpiredDate } = payload;
      state.userInfo = { ...state.userInfo, accessToken, refreshToken, expiredDate, refreshTokenExpiredDate };

      window.localStorage.setItem('GIPADMIN_UPDATETOKEN', JSON.stringify(payload));
      window.localStorage.removeItem('GIPADMIN_UPDATETOKEN');
    },
    getHeaderPartnerList: (state) => state,
    getHeaderPartnerListSuccess: (state, { payload }) => {
      const refinedData = [];
      if (payload.data && payload.data.length > 0) {
        const treeData = [...payload.data];

        const roopChildFunc = (data) => {
          if (data && data.length !== 0) {
            const childNode = data.map((childData) => ({
              title: childData.accessYn === 'N' ? (
                <div className="disabled">
                  <span>{`${childData.ptnNm}(${childData.ptnId})`}</span>
                </div>
              ) : `${childData.ptnNm}(${childData.ptnId})`,
              key: childData.accessYn === 'N' ? `${childData.ptnIdx.toString()}disabled` : childData.ptnIdx.toString(),
              ptnId: childData.ptnId,
              ptnNm: childData.ptnNm,
              sortSeq: childData.sortSeq,
              children: roopChildFunc(childData.subPtn),
            }));
            childNode.sort((a, b) => a.sortSeq - b.sortSeq);
            return childNode;
          }
          return '';
        };

        const roopFunc = (node) => {
          if (node && node.length !== 0) {
            refinedData.push({
              title: node.accessYn === 'N' ? (
                <div className="disabled">
                  <span>{`${node.ptnNm}(${node.ptnId})`}</span>
                </div>
              ) : `${node.ptnNm}(${node.ptnId})`,
              key: node.accessYn === 'N' ? `${node.ptnIdx.toString()}disabled` : node.ptnIdx.toString(),
              ptnId: node.ptnId,
              ptnNm: node.ptnNm,
              sortSeq: node.sortSeq,
              children: roopChildFunc(node.subPtn),
            });
            refinedData.sort((a, b) => a.sortSeq - b.sortSeq);
          }
        };

        treeData.forEach((element) => {
          roopFunc(element);
        });
      }
      state.userInfo.partnerList = refinedData;
    },
    getHeaderPartnerListFailure: (state) => state,
    getHeaderUserInfo: (state) => state,
    getHeaderUserInfoSuccess: (state, { payload }) => {
      state.userInfo.userInfo = { ...state.userInfo.userInfo, usrNm: payload.data.usrNm };
    },
    getHeaderUserInfoFailure: (state, { payload }) => {
      state.userRole = asyncApiState.error(payload);
    },
    headerToggleChange: (state, { payload }) => {
      state.headerToggle = payload;
    },
    expiredTimeUpdate: (state, { payload }) => {
      state.expiredTime = payload;
    },
    getCustomerSelects: (state) => {
      state.customerSelects = asyncApiState.request(initialCustomerSelect);
    },
    getCustomerSelectsSuccess: (state, { payload }) => {
      // ageUnit, custGrade, gender, sido
      const result = payload.data;
      const objArr = {};
      if (result.ageUnit) {
        const changeArr = result.ageUnit.map((v) => ({
          label: v.value,
          value: v.key,
        }));
        const resultArr = changeArr.filter((v) => v.label !== '알 수 없음');
        objArr.ageUnit = resultArr;
      }
      if (result.custGrade) {
        const changeArr = result.custGrade.map((v) => ({
          label: v.value,
          value: v.key,
        }));
        const resultArr = changeArr.filter((v) => v.label !== '알 수 없음');
        objArr.custGrade = [{ label: '전체', value: '' }, ...resultArr];
      }
      if (result.gender) {
        const changeArr = result.gender.map((v) => ({
          label: v.value,
          value: v.key,
        }));
        const resultArr = changeArr.filter((v) => v.label !== '알 수 없음');
        objArr.gender = [{ label: '전체', value: '' }, ...resultArr];
      }
      if (result.sido) {
        const changeArr = result.sido.map((v) => ({
          label: v.value,
          value: v.key,
        }));
        const resultArr = changeArr.filter((v) => v.label !== '알 수 없음');
        objArr.sido = [{ label: '전국', value: '' }, ...resultArr];
      }
      state.customerSelects = asyncApiState.success({ data: objArr });
    },
    getCustomerSelectsFailure: (state, { payload }) => {
      state.customerSelects = asyncApiState.error(payload);
    },
    getUserRole: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userRole = asyncApiState.request(result);
    },
    getUserRoleSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      state.userRole = asyncApiState.success(result);
    },
    getUserRoleFailure: (state, { payload }) => {
      state.userRole = asyncApiState.error(payload);
    },
    getLastUpdateDate: (state) => state,
    getLastUpdateDateSuccess: (state, { payload }) => {
      const result = { ...payload || {} };
      if (result.data) {
        const sessionUserInfo = window.sessionStorage.getItem('GIPADMIN_USER');

        if (sessionUserInfo !== null) {
          const userData = JSON.parse(sessionUserInfo);
          userData.lastUpdateDate = result.data.lastUpdateDate;

          window.sessionStorage.setItem('GIPADMIN_USER', JSON.stringify(userData));
        }
      }
    },
    getLastUpdateDateFailure: (state) => state,

  },
});

export const {
  updateStore,
  logout,
  updateToken,
  updateDefaultDate,
  getHeaderPartnerList,
  getHeaderUserInfo,
  setUserInfo,
  getCustomerSelects,
  headerToggleChange,
  expiredTimeUpdate,
  getUserRole,
  getLastUpdateDate,
  updatePages,
  setActivePageId,
  updateUserTreeDataByPartners,
  updateUserTreeDataByMyPartner,
  addMessage,
  removeMessage,
} = actions;

export default reducer;
