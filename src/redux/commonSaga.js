import {
  all, fork, takeLatest,
} from 'redux-saga/effects';

import { createPromiseSaga } from './lib';

import API from '../api';
import { getCustomerSelects, getUserRole, getLastUpdateDate, getHeaderPartnerList, getHeaderUserInfo } from './commonReducer';

const getCustomerSelectsSaga = createPromiseSaga(getCustomerSelects, API.Common.getCustomerSelects);
const getUserRoleSaga = createPromiseSaga(getUserRole, API.Common.getUserRole);
const getLastUpdateDateSaga = createPromiseSaga(getLastUpdateDate, API.Common.getLastUpdateDate);
const getHeaderPartnerListSaga = createPromiseSaga(getHeaderPartnerList, API.Common.getHeaderPartnerList);
const getHeaderUserInfoSaga = createPromiseSaga(getHeaderUserInfo, API.Common.getHeaderUserInfo);

/* dispatch type 구독 설정, 종류에 따라 watch함수 분기해도 좋음 */
function* watchCommon() {
  yield takeLatest(getCustomerSelects, getCustomerSelectsSaga);
  yield takeLatest(getUserRole, getUserRoleSaga);
  yield takeLatest(getLastUpdateDate, getLastUpdateDateSaga);
  yield takeLatest(getHeaderPartnerList, getHeaderPartnerListSaga);
  yield takeLatest(getHeaderUserInfo, getHeaderUserInfoSaga);
}

/* watch 함수 병합 */
export default function* watch() {
  yield all([fork(watchCommon)]);
}
