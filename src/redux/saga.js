import { all, fork } from 'redux-saga/effects';

import commonSaga from './commonSaga';
import mainSaga from '../views/main/redux/saga';
import loginSaga from '../views/account/login/redux/saga';
import brandLandscapeSage from '../views/brand/brandLandscape/redux/saga';
import topBrandSaga from '../views/brand/topBrand/redux/saga';

import allActivityCustomerSaga from '../views/customer/allActivityCustomer/redux/saga';
import newCustomerSaga from '../views/customer/newCustomer/redux/saga';
import stepCustomerSaga from '../views/customer/stepCustomer/redux/saga';
import top5BrandCustomer from '../views/customer/top5BrandCustomer/redux/saga';

import brandPerformanceSaga from '../views/period/brandPerformance/redux/saga';
import stepConversionSaga from '../views/period/stepConversion/redux/saga';
import productCustomerSaga from '../views/period/productCustomer/redux/saga';
import inflowKeywordSaga from '../views/period/inflowKeyword/redux/saga';

import partnersSaga from '../views/settings/partners/redux/saga';
import usersSaga from '../views/settings/users/redux/saga';
import myPageSaga from '../views/account/myPage/redux/saga';
import myPartnerPageSaga from '../views/account/myPartnerPage/redux/saga';
import navSaga from '../views/shared/header/nav/redux/saga';

export default function* rootSaga() {
  yield all([
    fork(commonSaga),
    fork(mainSaga),
    fork(loginSaga),
    fork(brandLandscapeSage),
    fork(topBrandSaga),
    fork(top5BrandCustomer),
    fork(stepCustomerSaga),
    fork(allActivityCustomerSaga),
    fork(brandPerformanceSaga),
    fork(newCustomerSaga),
    fork(productCustomerSaga),
    fork(stepConversionSaga),
    fork(inflowKeywordSaga),
    fork(partnersSaga),
    fork(usersSaga),
    fork(myPageSaga),
    fork(myPartnerPageSaga),
    fork(navSaga),
  ]);
}
