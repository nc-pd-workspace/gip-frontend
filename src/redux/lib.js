import { put, call, all } from 'redux-saga/effects';

import { store } from './store';

let isLogout = false;

export const apiLogout = (message) => {
  store.dispatch({ type: 'common/logout', payload: message });
  isLogout = true;
};

export const createPromiseSaga = (type, api, disableLoading = false) => {
  const success = `${type}Success`;
  const failure = `${type}Failure`;

  return function* saga(action) {
    try {
      if (!disableLoading) {
        // yield put({ type: SET_LOADING, payload: { loading: true, type } });
      }
      const params = action.payload ? action.payload : {};
      const payload = yield call(api, params);
      if (payload && payload.data && payload.status === 200 && payload.data.error.errorCode === '0000') {
        if (success === 'account/login/postLoginCertSuccess') {
          isLogout = false;
        }
        yield put({ type: success, payload: payload.data });
      } else {
        // 공통 에러 처리 추가 필요
        yield put({ type: failure, error: true, payload });
      }
    } catch (err) {
      const errData = err.response && err.response.data ? err.response.data : {};
      if (errData.error && ['0001', '0002', '0003', '0004', '0008', '0009'].indexOf(errData.error.errorCode) > -1) {
        if (isLogout === false) {
          isLogout = true;
          yield put({ type: 'common/logout', payload: errData.error.errorDescription });
        }
      } else {
        yield put({ type: failure, error: true, payload: errData });
      }
    } finally {
      if (!disableLoading) {
        // yield put({ type: SET_LOADING, payload: { loading: false, type } });
      }
    }
  };
};

export const createPromiseMultiApiSaga = (type, api) => {
  const success = `${type}Success`;
  const failure = `${type}Failure`;

  function* safeCall(key, action) {
    try {
      const params = action.payload.params[key] ? action.payload.params[key] : {};
      if (Object.keys(params).length === 0) return;
      const payload = yield call(api[key], { params });

      if (payload && payload.data && payload.status === 200 && payload.data.error.errorCode === '0000') {
        yield put({ type: success, payload: { key, data: payload.data } });
      } else {
        yield put({ type: failure, error: true, payload: { key, data: payload } });
      }
    } catch (err) {
      const errData = err.response && err.response.data ? err.response.data : {};
      // if (errData.error && ['0001', '0002', '0003', '0004', '0008', '0009'].indexOf(errData.error.errorCode) > -1) {
      if (false) {
        if (isLogout === false) {
          isLogout = true;
          yield put({ type: 'common/logout', payload: errData.error.errorCode });
        }
      } else {
        yield put({ type: failure, key, error: true, payload: { key, data: errData } });
      }
    }
  }

  return function* saga(action) {
    const keys = Object.keys(api);
    yield all(keys.map((v) => call(safeCall, v, action)));
  };
};
